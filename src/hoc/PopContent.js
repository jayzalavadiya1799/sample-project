let PopComponent ={};


PopComponent['CommonPop'] = require('../Components/Modal/CustomPopup').default;

// User Module Popup Routes
PopComponent['BlockUser'] = require('../Components/Modal/User/BlockUser').default;
PopComponent['ViewTransitionHistory'] = require('../Components/Modal/User/ViewTransitionHistory').default;
PopComponent['ViewGamePlayedHistory'] = require('../Components/Modal/User/ViewGamePlayedHistory').default;
PopComponent['UpdateCashAndBonus'] = require('../Components/Modal/User/UpdateCashAndBonus').default;
PopComponent['AddCoinPopup'] = require('../Components/Modal/User/AddCoinPopup').default;
PopComponent['ViewKYCDetailsPopup'] = require('../Pages/Users/UserKYC/UserKYCRequest/ViewKYCDetailsPopup').default;
PopComponent['ApproveUserKYCRequest'] = require('../Pages/Users/UserKYC/UserKYCRequest/ApproveUserKYCRequest').default;
PopComponent['RejectUserKYCRequest'] = require('../Pages/Users/UserKYC/UserKYCRequest/RejectUserKYCRequest').default;

PopComponent['ForgotPassword'] = require('../Components/Modal/ForgotPassword').default;
PopComponent['AddNewGame'] = require('../Components/Modal/GamePopup/UpdateGame').default;
PopComponent['AddAdminUserList'] = require('../Components/Modal/AdminUser/AddAdminUserList').default;
PopComponent['CreateRoleCategory'] = require('../Components/Modal/AdminUser/CreateRoleCategory').default;
PopComponent['ActiveUserModal'] = require('../Components/Modal/AdminUser/ActiveUserModal').default;



//GamePopup
PopComponent['RejectedPopup'] = require('../Components/Modal/GamePopup/RejectedPopup').default;
PopComponent['ApprovedPendingPopup'] = require('../Components/Modal/GamePopup/ApprovedPendingPopup').default;
PopComponent['ActiveDeactivatedPopup'] = require('../Components/Modal/GamePopup/ActiveDeactivatedPopup').default;
PopComponent['ViewRejectedComment'] = require('../Components/Modal/GamePopup/ViewRejectedComment').default;
PopComponent['UpdateGame'] = require('../Components/Modal/GamePopup/UpdateGame').default;
PopComponent['ArchivedGamePopup'] = require('../Components/Modal/GamePopup/ArchivedGamePopup').default;
PopComponent['AddGameMode'] = require('../Pages/Games/GameDetails/GameTabDetails/GameModeTab/AddGameMode').default;

//GameTab Popup
PopComponent['CreateHeadToHeadPopup'] = require('../Pages/Games/GameDetails/GameTabDetails/HeadToHeadTab/CreateHeadToHeadPopup').default;
PopComponent['CreateRecurringTournament'] = require('../Pages/Games/GameDetails/GameTabDetails/TournamentTab/RecurringTournament/CreateRecurringTournament').default;
//Avatar

//AddHeaderSliderPop
PopComponent['AddHeaderSliderPop'] = require('../Components/Modal/WebsitePopup/AddHeaderSliderPop').default;

PopComponent['ViewGameDetails'] = require('../Pages/Website/TopGameList/ViewGameDetails').default;
PopComponent['AddGame'] = require('../Pages/Website/TopGameList/AddGame').default;
PopComponent['AddWinnerPopup'] = require('../Components/Modal/WebsitePopup/AddWinnerPopup').default;
PopComponent['ActivatedDeactivatedPop'] = require('../Components/Modal/WebsitePopup/ActivatedDeactivatedPop').default;
PopComponent['AddAboutPopup'] = require('../Components/Modal/WebsitePopup/AddAboutPopup').default;
PopComponent['AddSocialMediaLinkPopup'] = require('../Components/Modal/WebsitePopup/AddSocialMediaLinkPopup').default;
PopComponent['AddWinnerTitlePopup'] = require('../Components/Modal/WebsitePopup/AddWinnerTitlePopup').default;
PopComponent['AddFooterData'] = require('../Components/Modal/WebsitePopup/AddFooterData').default;

//CopyHeadToHeadPopup UpdateGameCurrency
PopComponent['DeleteHeadToHeadPopup'] = require('../Pages/Games/GameDetails/GameTabDetails/HeadToHeadTab/DeleteHeadToHeadPopup').default;


PopComponent['HeadToHeadDetailsPopup'] = require('../Pages/Games/GameDetails/GameTabDetails/HeadToHeadTab/HeadToHeadDetailsPopup').default;
PopComponent['CopyHeadToHeadPopup'] = require('../Pages/Games/GameDetails/GameTabDetails/HeadToHeadTab/CopyHeadToHeadPopup').default;
PopComponent['UpdateYoutubeLink'] = require('../Pages/Games/GameDetails/GameTabDetails/HowToPlayGameTab/UpdateYoutubeLink').default;
PopComponent['UpdateGameCurrency'] = require('../Pages/Games/GameDetails/GameTabDetails/GameCurrencyTab/UpdateGameCurrency').default;
//CreateGameRelease
PopComponent['CreateGameRelease'] = require('../Pages/Games/GameDetails/GameTabDetails/GameReleaseTab/CreateGameRelease').default;
PopComponent['CountryReleasePop'] = require('../Pages/Games/GameDetails/GameTabDetails/GameReleaseTab/CountryReleasePop').default;

//AddGenrePopup
PopComponent['AddGenrePopup'] = require('../Components/Modal/GamePopup/AddGenrePopup').default;

PopComponent['UpdateUserKYCPopup'] = require('../Pages/Users/UserKYC/UpdateUserKYCPopup').default;
PopComponent['ViewReportedUserList'] = require('../Components/Modal/User/ViewReportedUserList').default;


//AddLevelsPopup

PopComponent['AddDailyWheelBonusPop'] = require('../Pages/BonusSystem/DailyWheelBonus/AddDailyWheelBonusPop').default;
PopComponent['ViewReferAndEarnList'] = require('../Pages/BonusSystem/ReferAndEarn/ReferAndEarnTab/ViewReferAndEarnList').default;
PopComponent['AddReferAndEarnList'] = require('../Pages/BonusSystem/ReferAndEarn/ReferAndEarnTab/AddReferAndEarnList').default;
PopComponent['AddCouponCode'] = require('../Pages/BonusSystem/CouponCode/AddCouponCode').default;
PopComponent['AddOffer'] = require('../Pages/BonusSystem/Offer/AddOffer').default;

PopComponent['DocumentOpenPopup'] = require('../Pages/Users/UserKYC/DocumentOpenPopup').default;

PopComponent['ActiveDeactivateCategory'] = require('../Pages/Master/GenreCategory/ActiveDeactiveCategory').default;



//DeleteCommonModal
PopComponent['DeleteCommonModal'] = require('../Components/Modal/DeleteCommonModal').default;
PopComponent['CreateAds'] = require('../Pages/Master/InternalAdsList/CreateAds').default;
PopComponent['ActiveDeactivatePopup'] = require('../Pages/Master/InternalAdsList/ActiveDeactivatePopup').default;
PopComponent['ActivateDeactivatePopup'] = require('../Pages/Games/GameDetails/GameTabDetails/HeadToHeadTab/ActivateDeactivatePopup').default;
PopComponent['ActivateDeactivateEarnPopup'] = require('../Pages/BonusSystem/ReferAndEarn/ReferAndEarnTab/ActivateDeactivateEarnPopup').default;
//UpdateGamePlayRules
PopComponent['UpdateGamePlayRules'] = require('../Pages/Master/Document/GamePlayRules/UpdateGamePlayRules').default;
PopComponent['UpdateFairPlayPolicy'] = require('../Pages/Master/Document/FairPlayPolicy/UpdateFairPlayPolicy').default;

PopComponent['RejectedKYCPopup'] = require('../Pages/Users/UserKYC/RejectedKYCPopup').default;
PopComponent['ApprovedKYCPopup'] = require('../Pages/Users/UserKYC/ApprovedKYCPopup').default;
PopComponent['AddMGPRelease'] = require('../Pages/MGPRelease/AddMGPRelease').default;
//GameBuildsTab
PopComponent['AddGameBuilds'] = require('../Pages/Games/GameDetails/GameTabDetails/GameBuildsTab/AddGameBuilds').default;
PopComponent['ViewReleaseGuide'] = require('../Pages/Games/GameDetails/GameTabDetails/GameBuildsTab/ViewReleaseGuide').default;
PopComponent['UpdateGameBuild'] = require('../Pages/Games/GameDetails/GameTabDetails/GameBuildsTab/UpdateGameBuild').default;

//UserBlockListView

PopComponent['UserBlockListView'] = require('../Pages/Users/BlockUserList/UserBlockListView').default;
PopComponent['UnblockUserPopup'] = require('../Pages/Users/BlockUserList/UnblockUserPopup').default;

//Policy
PopComponent['UpdateLegalPolicyPop'] = require('../Pages/Master/Document/LegalPolicy/UpdateLegalPolicyPop').default;
PopComponent['UpdatePrivacyPolicy'] = require('../Pages/Master/Document/PrivacyPolicy/UpdatePrivacyPolicy').default;
PopComponent['UpdateTermsAndCondition'] = require('../Pages/Master/Document/TermsAndConditionsPolicy/UpdateTermsAndCondition').default;

PopComponent['AddTicketTypePop'] = require('../Pages/HelpAndSupport/Ticket/AddTicketTypePop').default;
//AddLobbyLabel
PopComponent['AddLobbyLabel'] = require('../Pages/Master/LobbyLabel/AddLobbyLabel').default;

//AddEmailPopup
PopComponent['AddEmailPopup'] = require('../Pages/HelpAndSupport/EmailModule/AddEmailPopup').default;
//AddHowToPlay
PopComponent['AddHowToPlay'] = require('../Pages/Games/GameDetails/GameTabDetails/HowToPlayGameTab/AddHowToPlay').default;
PopComponent['AddBankAndUpi'] = require('../Pages/BankAndUPI/AddBankAndUpi').default;
//DeleteHowToPlay
PopComponent['DeleteHowToPlay'] = require('../Pages/Games/GameDetails/GameTabDetails/HowToPlayGameTab/DeleteHowToPlay').default;
PopComponent['ApprovedWithdrawalRequest'] = require('../Pages/Users/UserWithdrawalRequests/ApprovedWithdrawalRequest').default;

PopComponent['HaltRollOutPopup'] = require('../Pages/MGPRelease/haltRollOutPopup').default;
PopComponent['UpdateRollOutPopup'] = require('../Pages/MGPRelease/UpdateRollOutPopup').default;
PopComponent['AddWithdrawalUPI'] = require('../Pages/BankAndUPI/AddWithdrawalUPI').default;
PopComponent['AddUpcomingGamesPopup'] = require('../Pages/PopularGames/UpcomingGames/AddUpcomingGamesPopup').default;
PopComponent['AddNumberOfDecksPopup'] = require('../Pages/Games/GameDetails/GameTabDetails/GameNumberOfDecksTab/AddNumberOfDecksPopup').default;
PopComponent['AddNumberOfPlayerPopup'] = require('../Pages/Games/GameDetails/GameTabDetails/GameNumberOfPlayer/AddNumberOfPlayerPopup').default;
PopComponent['AddCustomerCare'] = require('../Pages/HelpAndSupport/CustomerCareTab/AddCustomerCare').default;

PopComponent['AddLeaderboardBonus'] = require('../Pages/Master/Leaderboard/LeaderBoardBonusTab/AddLeaderboardBonus').default;
PopComponent['AddTicketsVideo'] = require('../Pages/HelpAndSupport/TicketsVideoTab/AddTicketsVideo').default;

PopComponent['AddMGPOnlinePlayers'] = require('../Pages/Master/MGPOnlinePlayer/AddMGPOnlinePlayers').default;

PopComponent['AddGameModeConfigList'] = require('../Pages/Games/GameDetails/GameTabDetails/GameModeTab/GameModeConfig/AddGameModeConfigList').default;
PopComponent['AddGameModeDesignConfig'] = require('../Pages/Master/GameModeDesignConfig/AddGameModeDesignConfig').default;
export default PopComponent;
