import React, { useState } from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import { visuallyHidden } from '@mui/utils';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import TableHead from '@mui/material/TableHead';
import TableSortLabel from '@mui/material/TableSortLabel';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';

const CustomTable = ({ headCells, rowData, totalDocs, pagination, setPagination, isSystemTotal, isCurrency, isAboutWebsite, isWinnerTitle, isPopularGame }) => {
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('calories');


    const handleChangePage = (event, newPage) => {
        setPagination({
            ...pagination,
            page: newPage
        })
    };
    const handleChangeRowsPerPage = (event) => {
        setPagination({
            ...pagination,
            page: 0,
            rowsPerPage: parseInt(event.target.value, 10)
        })
    };
    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    function EnhancedTableHead(props) {
        
        const { order, orderBy, onRequestSort } = props;
        const createSortHandler = (property) => (event) => {
            onRequestSort(event, property);
        };

        return (
            <TableHead>
                <TableRow>
                    {headCells.map((headCell) => {
                        return (
                            headCell.type === 'hide' ?
                                <th className={'hide_table_th'} />
                                :
                                <TableCell
                                    key={headCell.id}
                                    sortDirection={orderBy === headCell.id ? order : false}
                                    className={'table_cell_thead'}
                                >
                                    {
                                        headCell.isDisbanding &&
                                            (headCell.label === 'Action' || headCell.label === 'Avatar' || headCell.id === 'amount' || headCell.isDisbanding) ?
                                            <Box className={"common-table-filter-css"}>
                                                {headCell.label}
                                            </Box>
                                            :
                                            headCell?.twoLineText ?
                                                <Box className={""} >
                                                    <span className={'table_td_span'} dangerouslySetInnerHTML={{ __html: headCell.label }} />
                                                </Box>
                                                :
                                                headCell?.numeric ?
                                                    <TableSortLabel
                                                        active={orderBy === headCell.id}
                                                        direction={orderBy === headCell.id ? order : 'asc'}
                                                        onClick={createSortHandler(headCell.id)}
                                                        className={"common-table-filter-css "}
                                                    >
                                                        <span >{headCell.label}</span>
                                                        {orderBy === headCell.id ? (
                                                            <Box component="span" sx={visuallyHidden}>
                                                                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                                            </Box>
                                                        ) : null}
                                                    </TableSortLabel>
                                                    :
                                                    <TableSortLabel
                                                        active={orderBy === headCell.id}
                                                        direction={orderBy === headCell.id ? order : 'asc'}
                                                        onClick={createSortHandler(headCell.id)}
                                                        className={"common-table-filter-css"}
                                                    >
                                                        {headCell.label}
                                                        {orderBy === headCell.id ? (
                                                            <Box component="span" sx={visuallyHidden}>
                                                                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                                            </Box>
                                                        ) : null}
                                                    </TableSortLabel>
                                    }
                                </TableCell>


                        )
                    })}
                </TableRow>
            </TableHead>
        );
    }

    function getComparator(order, orderBy) {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }
    function descendingComparator(a, b, orderBy) {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }
    function stableSort(array, comparator) {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) {
                return order;
            }
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }


    return (
        <>
            <TableContainer className={'data-table'}>
                <Table
                    sx={{ minWidth: 750 }}
                    aria-labelledby="tableTitle"
                    size={'medium'}
                    className="publish_table"
                >
                    <EnhancedTableHead
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={handleRequestSort}
                        rowCount={rowData?.length}
                    />
                    <TableBody>
                        {
                            rowData?.length ?
                                stableSort(rowData, getComparator(order, orderBy))
                                    .map((_data, dataIndex) => {
                                        return (
                                            <>
                                                <TableRow className={'table_body'} hover role="checkbox" tabIndex={-1} key={_data.name}
                                                >
                                                    {
                                                        headCells?.map((col, i) => {
                                                            switch (col.type) {
                                                                case "custom": {
                                                                    return col.render(_data, dataIndex)
                                                                }
                                                                case "action": {
                                                                    return col.ActionContent(_data)
                                                                }
                                                                case 'hide': {
                                                                    return <td className={'hide_table_td'}></td>;
                                                                }
                                                                default: {
                                                                    return (
                                                                        <TableCell >{_data[col?.id]}</TableCell>
                                                                    )
                                                                }
                                                            }
                                                        })
                                                    }
                                                </TableRow>
                                            </>
                                        )
                                    })
                                :
                                <TableRow hover role="checkbox" tabIndex={-1} className={'table_row'} >
                                    <TableCell colSpan={headCells?.length} className={'data_notFound_box'}>
                                        No Data Found
                                    </TableCell>   </TableRow>
                        }
                    </TableBody>
                </Table>
            </TableContainer>
            {

                (!isSystemTotal && !isCurrency && !isAboutWebsite && !isWinnerTitle && !isPopularGame) &&
                <TablePagination
                    rowsPerPageOptions={[10, 25, 50, 100]}
                    component="div"
                    count={totalDocs}
                    rowsPerPage={pagination.rowsPerPage}
                    page={pagination.page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    className={'table_pagination'}
                />
            }
        </>
    )
}

export default CustomTable;