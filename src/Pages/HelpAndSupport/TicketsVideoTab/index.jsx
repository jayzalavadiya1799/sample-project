import React, {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import Loader from "../../../images/Loader";
import Paper from "@mui/material/Paper";
import {ActionFunction, hideActionFunc} from "../../../utils";
import CustomTable from "../../../hoc/CommonTable";
import {useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deleteHowToPlay, getHowToPlay} from "../../../Redux/games/action";
import TableCell from "@mui/material/TableCell";
import PopComponent from "../../../hoc/PopContent";
import CommonModal from "../../../hoc/CommonModal";

const TicketsVideoTab = () => {
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const dispatch = useDispatch();
    const [rowData, setRowData] = useState([]);
    const [loader, setLoader] = useState(false);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    useEffect(() => {
        getGamesList()
    }, []);

    const getGamesList = () => {
        // setLoader(true);
        // dispatch(getHowToPlay({ gameId: id })).then(res => {
        //     setLoader(false);
        //     if (res?.data?.success) {
        //
        //     } else {
        //         setRowData({
        //             ...rowData,
        //             list: [],
        //         })
        //     }
        // })
    }

    const columns = [
        {
            id: 'title',
            label: 'video Link'
        },
        ActionFunction('helpAndSupport', {
            id: 'Action',
            disablePadding: false,
            isDisbanding: true,
            label: 'Action',
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('AddHowToPlay', { redirectApiProps: getGamesList, isEdit: true, row })}>Edit</span>
                    <span className='edit_btn edit-btn-action  prTab'
                          onClick={() => handleOpenModal('DeleteHowToPlay',
                              { deleteListApiHandler: deleteHowToPlay({ howToPlayId: row?._id }), redirectApiProps: getGamesList, title: 'Do you want to delete this data?' })}>
                        Delete
                    </span>
                </TableCell>
            }
        })
    ];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'AddTicketsVideo': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DeleteCommonModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false });
            }
        }
    };
    return(
        <React.Fragment>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'admin_user_list'}>
                    <button className={'add_game_btn font-bold'} onClick={(e) => handleOpenModal('AddTicketsVideo')}> + Add Tickets Video</button>
                </div>
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                    isWinnerTitle={true}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={''} />
            </CommonModal>
        </React.Fragment>
    )
}
export default TicketsVideoTab