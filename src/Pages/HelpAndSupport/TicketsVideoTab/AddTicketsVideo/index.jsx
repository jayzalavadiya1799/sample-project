import React, {useCallback, useRef, useState} from "react";
import {Box} from "@mui/material";
import {Editor} from "react-draft-wysiwyg";
import UploaderImages from "../../../Website/TopGameList/UploaderImages";
import FilledButton from "../../../../Components/FileButton";
import CommonModal from "../../../../hoc/CommonModal";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../hoc/PopContent";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 650,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const AddTicketsVideo = ({modalValue, handleOpenModal }) => {
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [loader, setLoader] = useState(false);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false })
    let Modal = PopComponent[modalDetails?.modalName];
    const [formData, setFormData] = useState({
        title: '',
        youtubeLink: '',
    });
    const handleSubmit = () => {

    }
    const handleEditSubmit = () => {

    }

    const handleOpenModalError = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };
    return(
        <Box sx={style} className={'how_to_play_section_details'}>
            <div className={'game_details_view add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    {
                        modalValue?.isEdit ?
                            <h2>Edit Tickets Video</h2>
                            :
                            <h2> Add Tickets Video</h2>
                    }
                </div>
            </div>
            <div className={'add_game_details_sec add_admin_user_popup_content'}>
                <form onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)} method="post"
                      encType="multipart/form-data">
                    <div className={'game_display_form'}>
                        <div className="formData formData_field">
                            <label>Video Link </label>
                            <div className="emailWrap">
                                <input type="text" name='youtubeLink' placeholder={'Enter Video Link'} value={formData?.youtubeLink} onChange={(e) => setFormData({ ...formData, youtubeLink: e.target.value })} />
                            </div>
                            {simpleValidator.current.message("youtubeLink", formData?.youtubeLink, 'required')}
                        </div>
                        <div className={formData?.type === 'ImageSlider' ? 'formData_btn form_common_btn add_game_btn_details' : 'formData_btn form_common_btn add_game_btn_Top'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </div>

                </form>

            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModalError}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModalError} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default AddTicketsVideo