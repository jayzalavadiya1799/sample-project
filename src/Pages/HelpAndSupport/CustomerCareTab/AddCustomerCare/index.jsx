import { Box } from "@mui/material";
import FilledButton from "../../../../Components/FileButton";
import React, { useCallback, useEffect, useRef, useState } from "react";
import DatePicker from 'react-datepicker';
import SimpleReactValidator from "simple-react-validator";
import moment from "moment";
import { addCustomerCare, updateCustomerCare } from "../../../../Redux/HelpAndSupport/action";
import { useDispatch } from "react-redux";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const AddCustomerCare = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const [loader, setLoader] = useState(false);
    const forceUpdate = useCallback(() => updateState({}), []);
    const [formData, setFormData] = useState({ phoneNumber: '', isAllDay: false, startTime: null, endTime: null });
    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            setLoader(true)
            let payload = {
                ...formData,
                startTime: moment(formData?.startTime).format('hh:mm A'),
                endTime: moment(formData?.endTime).format('hh:mm A'),
            }
            dispatch(addCustomerCare(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    useEffect(() => {
        if (modalValue?.isEdit) {
            setFormData({
                ...formData,
                phoneNumber: modalValue?.row?.phoneNumber,
                isAllDay: modalValue?.row?.isAllDay,
                startTime: moment(modalValue?.row?.startTime, 'HH:mm A')?._d,
                endTime: moment(modalValue?.row?.endTime, 'HH:mm A')?._d
            })
        }
    }, [modalValue])

    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            setLoader(true)
            let payload = {
                ...formData,
                startTime: moment(formData?.startTime).format('hh:mm A'),
                endTime: moment(formData?.endTime).format('hh:mm A'),
                customerCareId: modalValue?.row?._id
            }
            dispatch(updateCustomerCare(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{modalValue?.isEdit ? ' Update Customer Care Detail' : ' Add Customer Care Detail'}</h2>
                </div>
                <div className={'add_admin_user_popup_content'}>
                    <form onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        <div className={'user_kyc_section'}>
                            <div className={'user_kyc_section_filed'}>
                                <label>Customer Care Number</label>
                                <div className={'user_kyc_section_input_filed'} >
                                    <input type={'number'} name={'phoneNumber'} value={formData?.phoneNumber} placeholder={'Enter Customer Care Number'} onWheel={event => event.currentTarget.blur()} onChange={(e) => setFormData({ ...formData, phoneNumber: e.target.value })} />
                                </div>
                                {simpleValidator.current.message("careNumber", formData.phoneNumber, 'required')}
                            </div>
                            <div className={'genre_popup_details'}>
                                <div className={'add_admin_user_popup_content'}>
                                    <div className={'formData checkbox_modal'}>
                                        <div className={'game_mode_btn'}>
                                            <div className={'game_mode_btn_option'}>
                                                <input type={'radio'} name={'isAllDay'} checked={formData?.isAllDay} onChange={(e) => setFormData({ ...formData, isAllDay: true })} />
                                                <label>All day Availability</label>
                                            </div>
                                            <div className={'game_mode_btn_option tab_radio'}>
                                                <input type={'radio'} name={'isAllDay'} checked={!formData?.isAllDay} onChange={(e) => setFormData({ ...formData, isAllDay: false })} />
                                                <label>Monday - Friday</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className={'date-picker-details-section'}>
                                <label className={'date-label'}>Availability Time Duration</label>
                                <div className={'care-datePicker'}>
                                    <div className={'user_kyc_section_filed start-time-date'}>
                                        <label>Start Time</label>
                                        <DatePicker
                                            selected={formData?.startTime}
                                            className="form_control"
                                            showTimeSelect
                                            showTimeSelectOnly
                                            onChange={(date) => setFormData({ ...formData, startTime: date })}
                                            timeCaption="Start At"
                                            dateFormat="h:mm aa"
                                            timeIntervals={1}
                                            placeholderText="hh:mm"
                                        />
                                        {simpleValidator.current.message("startTime", formData.startTime, 'required')}
                                    </div>
                                    <div className={'user_kyc_section_filed end-time-date'}>
                                        <label>End Time</label>
                                        <DatePicker
                                            selected={formData?.endTime}
                                            className="form_control"
                                            showTimeSelect
                                            showTimeSelectOnly
                                            timeCaption="End At"
                                            dateFormat="h:mm aa"
                                            onChange={(date) => setFormData({ ...formData, endTime: date })}
                                            timeIntervals={1}
                                            minTime={moment().isSame(moment(), 'day') ? new Date() : new Date(formData.startTime)}
                                            maxTime={moment().endOf('days').toDate()}
                                            placeholderText="hh:mm"
                                        />
                                        {simpleValidator.current.message("endTime", formData.endTime, 'required')}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
        </Box>
    )
}
export default AddCustomerCare