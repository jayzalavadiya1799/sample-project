import React, { useState } from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import TabPanel from "../../Components/TabPanel";
import AllTicketsList from "./AllTickets";
import Ticket from "./Ticket";
import EmailModule from "./EmailModule";
import CustomerCareTab from "./CustomerCareTab";
import TicketsVideoTab from "./TicketsVideoTab";

const HelpAndSupport = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const a11yProps = (index) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    };
    return (
        <Box sx={{ width: '100%' }} className={'setting_tab_section user_details_tab'}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                    <Tab className={'tab_listing tab_title'} label=" All Open Tickets" {...a11yProps(0)} />
                    <Tab className={'tab_listing tab_title'} label="Ticket Type" {...a11yProps(1)} />
                    <Tab className={'tab_listing tab_title'} label="Email" {...a11yProps(2)} />
                    <Tab className={'tab_listing tab_title'} label="Customer Care" {...a11yProps(3)} />
                    <Tab className={'tab_listing tab_title'} label="Tickets Video" {...a11yProps(4)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <AllTicketsList />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <Ticket />
            </TabPanel>
            <TabPanel value={value} index={2}>
                <EmailModule />
            </TabPanel>
            <TabPanel value={value} index={3}>
                <CustomerCareTab />
            </TabPanel>
            <TabPanel value={value} index={4}>
                <TicketsVideoTab/>
            </TabPanel>
        </Box>
    )
}
export default HelpAndSupport