import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { useDispatch } from "react-redux";
import ReorderIcon from '@mui/icons-material/Reorder';
import TableCell from "@mui/material/TableCell";
import DoneIcon from '@mui/icons-material/Done';
import Drawer from '@mui/material/Drawer';
import Paper from "@mui/material/Paper";
import Loader from "../../../images/Loader";
import CustomTable from "../../../hoc/CommonTable";
import ChatDrawer from "../ChatDrawer";
import { getHelpAndSupportList } from "../../../Redux/HelpAndSupport/action";
import moment from "moment";

const AllTicketsList = () => {
    const dispatch = useDispatch();
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [state, setState] = React.useState({ right: false, });
    const [loader, setLoader] = useState(false);
    const [helpAndSupportData, setHelpAndSupportData] = useState({ id: '', messages: [], userReceiverDetails: {} })
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const toggleDrawer = (anchor, open, id, messages, userDetails) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setHelpAndSupportData({
            ...helpAndSupportData,
            id: id,
            messages: messages,
            userReceiverDetails: userDetails
        })
        setState({ ...state, [anchor]: open });
    };

    const columns = [
        {
            id: 'id',
            label: 'Ticket Id',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{`#${row?.numericId}`}</TableCell>
            }
        },
        {
            id: '',
            label: 'Ticket Type',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >
                    {row?.ticketTypeId?.ticketType}
                </TableCell>
            }
        },
        {
            id: 'ticketTitle',
            label: 'Ticket Title'
        },
        {
            id: 'createdAt',
            label: 'Date Created',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.createdAt).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        {
            id: 'updatedAt',
            label: 'Last Modified',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.updatedAt).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        {
            id: 'action',
            label: 'Status',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    {
                        row?.status !== 'Open' ?
                            <span className='edit_btn edit-btn-action document_record_status_resolved' > <DoneIcon /> Resolved</span>
                            :
                            <span className='edit_btn edit-btn-action document_record_status' onClick={toggleDrawer('right', true, row?._id, row?.messages, row?.userId)}> <ReorderIcon /> Open</span>
                    }

                </TableCell>
            }
        },
    ];

    useEffect(() => {
        getHelpAndSupportListHandler();
    }, [pagination.rowsPerPage, pagination.page]);

    const getHelpAndSupportListHandler = () => {
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        };
        setLoader(true);
        dispatch(getHelpAndSupportList(payload)).then(res => {
            setLoader(false)
            setRowData({
                ...rowData,
                list: res?.data?.data?.docs,
                totalDocs: res?.data?.data?.totalDocs
            })
        });
    };

    return (
        <Box className={'documentation_main_section'}>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'documentation_section'}>
                    <div className={'documentation_how_to_start'}>
                        <div className={'how_to_start_title'}>
                            <h2>All Open Tickets</h2>
                        </div>
                        <div className={'help_support_table_ticket'}>
                            <CustomTable
                                headCells={columns}
                                rowData={rowData?.list}
                                totalDocs={rowData?.totalDocs}
                                pagination={pagination}
                                setPagination={setPagination}
                            />
                        </div>
                    </div>
                </div>
            </Paper>
            <Drawer
                anchor={'right'}
                open={state['right']}
                onClose={toggleDrawer('right', false)}
            >
                <box className={'help_support_drawer'}>
                    <ChatDrawer helpAndSupportData={helpAndSupportData} getHelpAndSupportListHandler={getHelpAndSupportListHandler} pagination={pagination} />
                </box>
            </Drawer>
        </Box>
    )
}
export default AllTicketsList