import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import React, { useState } from "react";
import AdminUserList from "./AdminUserList";
import CreateRoleCategory from "./CreateRoleCategory";
import TabPanel from "../../Components/TabPanel";

const AdminUser = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const a11yProps = (index) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    };
    return (
        <Box sx={{ width: '100%' }} className={'setting_tab_section user_details_tab'}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                    <Tab className={'tab_listing tab_title'} label="Admin User List" {...a11yProps(0)} />
                    <Tab className={'tab_listing tab_title'} label="Role Category List" {...a11yProps(1)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <AdminUserList setValue={setValue} />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <CreateRoleCategory />
            </TabPanel>
        </Box>
    )
}

export default AdminUser