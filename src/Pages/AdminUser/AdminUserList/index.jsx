import React, { useEffect, useState } from "react";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import TableCell from "@mui/material/TableCell";
import CommonModal from "../../../hoc/CommonModal";
import Loader from "../../../images/Loader";
import PopComponent from "../../../hoc/PopContent";
import { useDispatch } from "react-redux";
import { deleteAdminUserList, getAdminUserListing, getAllAdminUserRoleName } from "../../../Redux/AdminUser/action";


const AdminUserList = ({ setValue }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails?.modalName];
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });

    let columns = [
        {
            id: 'id',
            numeric: false,
            disablePadding: false,
            label: 'Admin Role ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{`ARID000${row?.numericId}`}</TableCell>
            }

        },
        {
            id: 'fullName',
            numeric: true,
            disablePadding: false,
            label: 'Full Name',

        },
        {
            id: 'email',
            numeric: true,
            disablePadding: false,
            label: 'Email',

        },
        {
            id: 'phoneNumber',
            numeric: true,
            disablePadding: false,
            label: 'Mobile Number',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.phoneNumber}</TableCell>
            }
        },
        {
            id: 'role',
            disablePadding: false,
            label: 'Role',
        },
        {
            id: 'status',
            isDisbanding: true,
            label: 'Status',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.isBlock ? 'Deactivate' : 'activate'}</TableCell>
            }
        },
        {
            id: "action",
            isDisbanding: true,
            label: "Action",
            type: "custom",
            render: (row) => {
                return (
                    <TableCell className={'role_field_id'}>
                        <span className="edit_btn edit-btn-action u_border" onClick={() => handleOpenModal('AddAdminUserList', { isEdit: true, data: row })}>Edit</span>
                        <span className='edit_btn edit-btn-action u_border prTab'
                            onClick={() => handleOpenModal('DeleteCommonModal',
                                { deleteListApiHandler: deleteAdminUserList({ adminUserId: row?._id, adminUserRole: row?.role }), title: 'Do you want to delete this data?' })}>
                            Delete
                        </span>
                        {!row?.isBlock ?
                            <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('ActiveUserModal', { adminUserId: row?._id, adminUserRole: row?.role, isBlock: true })}>Deactivate</span>
                            : <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('ActiveUserModal', { adminUserId: row?._id, adminUserRole: row?.role, isBlock: false })}>Activate</span>}
                    </TableCell>
                );
            },
        },
    ];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'AddAdminUserList': {
                dispatch(getAllAdminUserRoleName())
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ActiveUserModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DeleteCommonModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false });
            }
        }
    };

    useEffect(() => {
        getAdminUserListData();
    }, [pagination.rowsPerPage, pagination.page]);

    const getAdminUserListData = () => {
        setLoader(true);
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        };
        dispatch(getAdminUserListing(payload)).then(res => {
            setLoader(false)
            setRowData({
                ...rowData,
                list: res?.data?.data?.docs,
                totalDocs: res?.data?.data?.totalDocs
            });
        });
    };

    return (
        <>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'admin_user_list'}>
                    <button className={'add_game_btn'} onClick={() => handleOpenModal('AddAdminUserList', { isEdit: false, setValue: setValue })}> + Add Admin</button>
                </div>
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getAdminUserListData} />
            </CommonModal>
        </>
    )
}
export default AdminUserList