import React, { useEffect, useState } from "react";
import Paper from "@mui/material/Paper";
import PopComponent from "../../../hoc/PopContent";
import CommonModal from "../../../hoc/CommonModal";
import Loader from "../../../images/Loader";
import CustomTable from "../../../hoc/CommonTable";
import TableCell from "@mui/material/TableCell";
import { useDispatch } from "react-redux";
import { deleteRoleCategoryList, getRoleCategoryList } from "../../../Redux/AdminUser/action";
import { AdminRole } from "../../../utils";


const CreateRoleCategory = () => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 })
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false })
    let Modal = PopComponent[modalDetails.modalName];
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 })

    let columns = [
        {
            id: 'adminUserRoleName',
            numeric: true,
            disablePadding: false,
            label: 'Role Name',
        },
        {
            id: 'role',
            isDisbanding: true,
            label: 'Access Role Category',
            type: 'custom',
            render: (row) => {
                return <TableCell>
                    <ul>
                        {
                            AdminRole?.filter(item => Object?.keys(row?.permission)?.includes(item.value))?.map(item => {
                                return <li>{item.label} {row?.permission[item.value].viewer ? "Viewer" : ""}{row?.permission[item.value].editor ? ', ' + item.label + ' ' + "Editor" : ''}</li>
                            })
                        }
                    </ul>
                </TableCell>
            }
        },
        {
            id: "action",
            isDisbanding: true,
            label: "Action",
            type: "custom",
            render: (row) => {
                return (
                    <TableCell className={'role_field_id'}>
                        <span className="edit_btn edit-btn-action u_border" onClick={() => handleOpenModal('CreateRoleCategory', { isEdit: true, data: row })}>Edit</span>
                        <span className='edit_btn edit-btn-action u_border prTab'
                            onClick={() => handleOpenModal('DeleteCommonModal',
                                { deleteListApiHandler: deleteRoleCategoryList({ isDeleteCate: true, adminUserRoleId: row?._id }), title: 'Do you want to delete the Role?' })}>
                            Delete
                        </span>
                        <span className="edit_btn edit-btn-action  prTab" onClick={() => handleOpenModal('ActiveUserModal', { isEnableCate: true, isActive: !row?.isActive, adminUserRoleId: row?._id })}>{row?.isActive ? 'Enable' : 'Disable'}</span>
                    </TableCell>
                );
            },
        },
    ];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CreateRoleCategory': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DeleteCommonModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ActiveUserModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                localStorage.setItem('closeModal', true)
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        getAdminCategoryListHandler()
    }, [pagination.rowsPerPage, pagination.page]);

    const getAdminCategoryListHandler = () => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        }
        dispatch(getRoleCategoryList(payload)).then(res => {
            setLoader(false)
            setRowData({
                ...rowData,
                list: res?.data?.data?.docs,
                totalDocs: res.data.data.totalDocs
            })
        })
    };

    return (
        <>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'admin_user_list'}>
                    <button className={'add_game_btn'} onClick={() => handleOpenModal('CreateRoleCategory')}> + Add Role Category</button>
                </div>
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={` ${modalDetails?.modalName} Approved-reject-section`} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails?.modalIsOpen} YesNoText={{ yes: "Yes", no: "Cancel" }} redirectApiHandler={getAdminCategoryListHandler} />
            </CommonModal>
        </>
    )
}
export default CreateRoleCategory