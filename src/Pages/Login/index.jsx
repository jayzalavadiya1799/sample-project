import React, { useCallback, useRef, useState } from 'react';
import { Box } from '@mui/material';
import Modal from '@mui/material/Modal';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { loginUser } from "../../Redux/auth/action";
import PopComponent from "../../hoc/PopContent";
import CommonModal from "../../hoc/CommonModal";
import FilledButton from "../../Components/FileButton";
import SimpleReactValidator from "simple-react-validator";
import Logo from "../../assets/images/logo75.png";
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import VisibilityIcon from '@mui/icons-material/Visibility';

const Login = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails?.modalName];
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [showPassword, setShowPassword] = useState(false)
    const [formData, setFormData] = useState({ email: '', password: '' });

    const handleChange = (e) => {
        const { value, name } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const userLoginHandler = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            setLoader(true);
            let payload = {
                email: formData?.email?.trim(),
                password: formData?.password?.trim()
            }
            dispatch(loginUser(payload)).then(res => {
                if (res.data.statusCode === 200 && res.data.success) {
                    navigate('/dashboard')
                    setLoader(false)
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }

    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ForgotPassword': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <div className={'login_section'}>
            <Box className='bg_black' p={2} sx={{ textAlign: 'center' }}>
                {/*<SideLogo />*/}
                <img src={Logo} alt={"Logo"} className={"Logo"} height={68} />
            </Box>
            <Box className='login_container'>
                <Box className='login_form' boxShadow={1} sx={{ borderRadius: 2 }}>
                    <form className="form_login" onSubmit={(e) => userLoginHandler(e)}>
                        <h1 className={"Login_Title"}>Super Admin Login</h1>
                        <div className="formData">
                            <label>Email address</label>
                            <div className="emailWrap"><input type="text" name='email' value={formData.email} onChange={(e) => handleChange(e)} />
                                {simpleValidator.current.message("Email", formData?.email, "required")}
                            </div>
                        </div>
                        <div className="formData">
                            <label>Password</label>
                            <div className="passWrap">
                                <div className="passInput">
                                    <input type={showPassword ? "text" : "password"} name="password" value={formData.password} onChange={(e) => handleChange(e)} />
                                    <button type="button" className="sd_passoword_toggle cursor_pointer" onClick={() => setShowPassword(!showPassword)}>
                                        {
                                            !showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />
                                        }
                                    </button>
                                </div>
                            </div>
                            {simpleValidator.current.message("Password", formData?.password, "required")}
                        </div>
                        <div className="forgetContainer">
                            <div className="rememberWrap">
                                <div className="checkWrap">
                                    {/*<input type="checkbox" /> <label>Remember me</label>*/}
                                </div>
                                <div className={'forgot-password'}>
                                    <button type="button" onClick={() => handleOpenModal('ForgotPassword')}>Forgot Password</button>
                                </div>
                            </div>
                        </div>
                        <FilledButton type={'submit'} value={'Log In'} className={'formloginBtn loader_css'} loading={loader} />
                    </form>
                </Box>
            </Box>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </div>
    )
}

export default Login