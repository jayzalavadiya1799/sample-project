import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import { jsonToFormData, profileImages } from "../../../utils";
import user from "../../../assets/images/avatar.png";
import FilledButton from "../../../Components/FileButton";
import CommonModal from "../../../hoc/CommonModal";
import { useDispatch, useSelector } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../hoc/PopContent";
import draftToHtml from "draftjs-to-html";
import { Editor } from "react-draft-wysiwyg";
import { convertToRaw, EditorState } from "draft-js";
import DropdownMode from "../../Games/GameDetails/GameTabDetails/HeadToHeadTab/CreateHeadToHeadPopup/DropdownMode";
import { MultiSelect } from "react-multi-select-component";
import { getAllCountriesRestrictGeo } from "../../../Redux/settings/action";
import { createMGPRelease } from "../../../Redux/MGPRelease/action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 0,
    borderRadius: "5px",
};
const AddMGPRelease = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const geoData = useSelector(state => state?.settingReducer?.restrictedGeo);
    const [loader, setLoader] = useState(false);
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const forceUpdate = useCallback(() => updateState({}), []);
    const [formData, setFormData] = useState({
        releaseBuild: '',
        rolloutPercentage: '',
        releaseNumber: '',
        releaseNote: '',
        isAvailableAllCountry: true,
        countryAvailability: [],
        rolloutStage: ''
    })
    useEffect(() => {
        dispatch(getAllCountriesRestrictGeo({}));
    }, []);
    const getCountryData = () => {
        return geoData?.country?.map((item) => { return { value: item, label: item } }) || [];
    };
    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData
            }
            if (payload?.isAvailableAllCountry) {
                delete payload?.countryAvailability
            }
            setLoader(true)
            dispatch(createMGPRelease(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };
    const onEditorStateChange = (editorState) => {
        setEditorState(editorState);
        setFormData({
            ...formData,
            releaseNote: draftToHtml(convertToRaw(editorState.getCurrentContent())),
        })
    };


    return (
        <Box sx={style} className={'mgp_release_section'}>
            <div className={'add_admin_user_popup mgp_release_inner_section'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>Add MGP Release</h2>
                </div>
                <div className={'add_admin_user_popup_content'}>
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <div className={'user_kyc_section'}>
                            <div className={'documents_fields'}>
                                <div className={'user_kyc_section_filed'}>
                                    <label>Release Version</label>
                                    <input type={'text'} name={'releaseNumber'} placeholder={'Release Number'} onChange={(e) => handleChange(e)} />
                                    {simpleValidator.current.message("releaseNumber", formData?.releaseNumber, 'required')}
                                </div>
                                <div className={'user_kyc_section_filed'}>
                                    <label>Release Note</label>
                                    <Editor
                                        editorState={editorState}
                                        onEditorStateChange={(editorState) => onEditorStateChange(editorState)}
                                        toolbar={
                                            {
                                                options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link'],
                                                fontFamily: {
                                                    options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Inter'],
                                                }
                                            }
                                        }
                                    />
                                </div>
                                <div className={'user_kyc_section_filed'}>
                                    <label>Country availability</label>
                                    <div className={'d-flex-wrap'}>
                                        <div className={'form_data_field'}>
                                            <input type="radio" name='isAvailableAllCountry' checked={formData?.isAvailableAllCountry} className={'checkbox_field_tournament'} onChange={(value) => setFormData({ ...formData, isAvailableAllCountry: true })} />
                                            <label>Available in all targeted countries</label>
                                        </div>
                                        <div className={'form_data_field tab_no'}>
                                            <input type="radio" name={'isAvailableAllCountry'} checked={!formData?.isAvailableAllCountry} className={'checkbox_field_tournament'} onChange={(value) => setFormData({ ...formData, isAvailableAllCountry: false })} />
                                            <label>Select specific countries</label>
                                        </div>
                                    </div>
                                    {
                                        !formData?.isAvailableAllCountry &&
                                        <div>
                                            <MultiSelect
                                                value={formData?.countryAvailability}
                                                options={getCountryData()}
                                                selected={formData?.countryAvailability}
                                                onChange={(value) => setFormData({ ...formData, countryAvailability: value })}
                                                labelledBy={"Select"}
                                                className={"select_game_release"}
                                            />
                                            {simpleValidator.current.message("countryAvailability", formData?.countryAvailability, 'required')}
                                        </div>
                                    }


                                </div>
                                <div className={'user_kyc_section_filed_document'}>
                                    <label>Build upload</label>
                                    <div className={'document_details_kyc'}>
                                        <div className="u_flex u_align-center">
                                            <div className="u_file-attachment add ">
                                                {
                                                    formData?.userKYCAadharCard ?
                                                        profileImages(formData?.userKYCAadharCard, user)
                                                        :
                                                        <svg width="18" height="18" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" className="">
                                                            <path className="u_fill" d="M23.512 11.503l-9.708 9.523a1.682 1.682 0 00-.524 1.196 1.655 1.655 0 00.503 1.205 1.72 1.72 0 001.228.493 1.743 1.743 0 001.22-.514l9.71-9.52a5.002 5.002 0 001.508-3.572c0-1.34-.542-2.624-1.508-3.571a5.202 5.202 0 00-3.641-1.48 5.202 5.202 0 00-3.642 1.48l-9.71 9.523a8.413 8.413 0 00-1.906 2.731 8.277 8.277 0 00-.041 6.491 8.407 8.407 0 001.87 2.755 8.598 8.598 0 002.81 1.835 8.732 8.732 0 006.619-.042 8.592 8.592 0 002.785-1.87l9.71-9.52 2.427 2.38-9.71 9.523a12.033 12.033 0 01-3.898 2.554 12.221 12.221 0 01-9.197 0 12.035 12.035 0 01-3.898-2.554 11.772 11.772 0 01-2.604-3.823 11.587 11.587 0 010-9.02 11.771 11.771 0 012.604-3.822l9.712-9.521A8.673 8.673 0 0122.268 2c2.25.02 4.403.905 5.994 2.465a8.336 8.336 0 012.513 5.879 8.331 8.331 0 01-2.409 5.92l-9.708 9.526a5.157 5.157 0 01-1.67 1.095 5.238 5.238 0 01-3.943 0 5.159 5.159 0 01-1.67-1.096 5.044 5.044 0 01-1.117-1.639 4.966 4.966 0 010-3.866 5.046 5.046 0 011.117-1.638l9.71-9.523 2.427 2.38z" fill="#4E525F" />
                                                        </svg>
                                                }
                                                <div className="u_file-attachment-label">
                                                    <input type="file" title="" name={'releaseBuild'} accept=".zip" id="upload" autoComplete="off" onChange={(e) => setFormData({ ...formData, releaseBuild: e.target.files[0] })} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>{formData?.releaseBuild ? formData?.releaseBuild?.name : ''}</p>
                                    {simpleValidator.current.message("releaseBuild", formData?.releaseBuild, 'required')}
                                </div>

                                <div className={'select_game_option_mode'}>
                                    <label>Rollout Stage</label>
                                    <div className={'select_game_option'}>
                                        <DropdownMode options={['Regular', 'Force Fully']} formData={formData} name={'rolloutStage'} setFormData={setFormData} />
                                    </div>
                                    {simpleValidator.current.message("rolloutStage", formData?.rolloutStage, 'required')}
                                </div>
                                <div className={'user_kyc_section_filed'}>
                                    <label>Rollout Percentage</label>
                                    <input type={'number'} onWheel={(e) => e.currentTarget.blur()} name={'rolloutPercentage'} placeholder={'Rollout Percentage'} onChange={(e) => handleChange(e)} />
                                    {simpleValidator.current.message("rolloutPercentage", formData?.rolloutPercentage, 'required')}
                                </div>
                            </div>
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    );
};
export default AddMGPRelease