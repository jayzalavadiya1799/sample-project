import React, {useCallback, useRef, useState} from "react";
import {Box} from "@mui/material";
import {profileImages} from "../../../../utils";
import user from "../../../../assets/images/avatar.png";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFns";
import {DatePicker} from "@mui/x-date-pickers/DatePicker";
import TextField from "@mui/material/TextField";
import FilledButton from "../../../../Components/FileButton";
import CommonModal from "../../../../hoc/CommonModal";
import {useDispatch} from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../hoc/PopContent";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const AddGameModeDesignConfig = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const [formData, setFormData] = useState({
        gameModeDesignIcon: '',
        designName:'',
    });

    const changeHandler = (e) => {
        const { value, name } = e.target;
        setFormData({
            ...formData,
            [name]: value
        })
    };

    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const handleChange = () => {

    }

    const handleSubmit = () => {

    }

    return(
        <Box sx={style}>
            <div className={'add_admin_user_popup game-mode-config-design'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{'Add Game Mode Design Config'}</h2>
                </div>
                <div className={'header_slider_details header_slider_details_Ads'}>
                    <form className='form_group ' onSubmit={(e) => handleSubmit(e)}>
                        <div className={'user_kyc_section'}>
                            <div className={'user_kyc_section_filed'}>
                                <label>Design Name</label>
                                <div className={'user_kyc_section_input_filed'}>
                                    <input type={'text'} placeholder={'Enter Design Name'} value={formData?.designName} name={'designName'} onChange={(e) => changeHandler(e)} />
                                </div>
                                {simpleValidator.current.message("designName", formData?.designName, 'required')}
                            </div>
                        </div>
                        {/*--------------------------------------- Game Logo [Start] ----------------------------------------- */}
                        <div className='user_profile'>
                            <label htmlFor='' className='profile_label'>Design Image</label>
                            <div className={'header_section_slider'}>
                                <div className='user_profile_pic'>
                                    {profileImages(formData?.gameModeDesignIcon, user)}
                                    <span className='add_new'>
                                        <input type='file' name='internalAdsBanner' id='' onChange={(e) => handleChange(e)} /> </span>
                                </div>
                            </div>
                            {simpleValidator.current.message("gameModeDesignIcon", formData?.gameModeDesignIcon, "required")}
                        </div>
                        {/*--------------------------------------- Game Logo [End] ----------------------------------------- */}
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default AddGameModeDesignConfig