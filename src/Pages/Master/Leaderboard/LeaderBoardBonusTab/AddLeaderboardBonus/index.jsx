import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import TextField from "@mui/material/TextField";
import FilledButton from "../../../../../Components/FileButton";
import CommonModal from "../../../../../hoc/CommonModal";
import { useDispatch } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../../hoc/PopContent";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import moment from "moment";
import {addLeaderboardBonusList} from "../../../../../Redux/Master/action";
import {keyboardImplementation} from "@testing-library/user-event/dist/keyboard/keyboardImplementation";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 0,
    borderRadius: "5px",
};
const AddLeaderboardBonus = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const [, updateState] = useState({});
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [formData, setFormData] = useState({ numberOfTopPlayer: '',  startDate: '',  bonusPositions: [] });
    const forceUpdate = useCallback(() => updateState({}), []);
    const [openCale, setOpenCale] = useState({ expireDate: false, startDate: false });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
                let payload = {
                    numberOfTopPlayer:formData?.numberOfTopPlayer,
                    bonusPositions:formData?.bonusPositions,
                    date: moment(formData?.startDate).format('DD')
                }
            dispatch(addLeaderboardBonusList(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                numberOfTopPlayer:formData?.numberOfTopPlayer,
                bonusPositions:formData?.bonusPositions,
                date: moment(formData?.startDate).format('DD')
            }
            dispatch(addLeaderboardBonusList(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };
    const handleDatePicker = (newValue, type) => {
        setFormData({ ...formData, [type]: newValue });
        setOpenCale({ ...openCale, [type]: false });
    };

    useEffect(() => {
        if (formData?.numberOfTopPlayer) {
            setFormData({
                ...formData,
                bonusPositions: Array(+formData?.numberOfTopPlayer).fill('')
            })
        } else {
            setFormData({
                ...formData,
                bonusPositions: []
            })
        }
    }, [formData?.numberOfTopPlayer])

    const positionChangeHandler = (e, index) => {
        let temp = [...formData.bonusPositions]
        temp[index] = e.target.value;
        setFormData({
            ...formData,
            bonusPositions: temp
        })
    }
console.log(modalValue)
    useEffect(()=>{
        if(modalValue?.isEdit){
            setFormData({
                ...formData,
                numberOfTopPlayer: modalValue?.row?.numberOfTopPlayer,
                bonusPositions:modalValue?.row?.bonusPositions
            })
        }
    },[])

    console.log(formData)

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup leaderboard-bonus-section'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{modalValue?.isEdit ? 'Update Leaderboard Bonus' : 'Add Leaderboard Bonus'}</h2>
                </div>
                <div className={'header_slider_details header_slider_details_Ads'}>
                    <form className='form_group ' onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        {/*--------------------------------------- Game Logo [Start] ----------------------------------------- */}
                        <div className={'date-picker_coupon'}>
                            <div className={'start-date-picker'}>
                                <label>Date</label>
                                <div className={'date_picker_value'}>
                                    <LocalizationProvider dateAdapter={AdapterDateFns}  >
                                        <DatePicker
                                            name='start-date'
                                            value={formData?.startDate}
                                            open={openCale.startDate}
                                            onChange={(newValue) => handleDatePicker(newValue, 'startDate')}
                                            minDate={new Date()}
                                            renderInput={(params) => {
                                                return <TextField {...params} onClick={() => setOpenCale({ ...openCale, startDate: !openCale?.startDate, expireDate: false })} />
                                            }}
                                            shouldDisableDate={(date) => {
                                                return date.getDate() === 29 || date.getDate() === 30 || date.getDate() === 31
                                            }}
                                            inputFormat="MMM dd, yyyy"
                                            className={'datePicker_details'}
                                            inputProps={{ readOnly: true }}
                                        />
                                    </LocalizationProvider>
                                </div>
                                {simpleValidator.current.message("startDate", formData?.startDate, "required")}
                            </div>
                        </div>
                        <div className={'user_kyc_section_filed'}>
                            <label>Number Of Top Player</label>
                            <div className={'user_kyc_section_input_filed'}>
                                <input type={'number'} onWheel={(e) => e.currentTarget.blur()} placeholder={'Enter Number Of Top Player'} value={formData?.numberOfTopPlayer} name={'lobbyType'} onChange={(e) => setFormData({ ...formData, numberOfTopPlayer: e.target.value })} />
                            </div>
                            {simpleValidator.current.message("noOfPlayer", formData?.numberOfTopPlayer, 'required')}
                        </div>
                        {
                            formData?.bonusPositions?.map((item, i) => {
                                return (
                                    <div className={'user_kyc_section_filed'}>
                                        <label>{`${i + 1} Positions`}</label>
                                        <div className={'user_kyc_section_input_filed'}>
                                            <input type={'number'} onWheel={(e) => e.currentTarget.blur()} placeholder={'Enter Positions Bonus'} value={item} name={'lobbyType'} onChange={(e) => positionChangeHandler(e, i)} />
                                        </div>
                                        {simpleValidator.current.message("Positions", item, 'required')}
                                    </div>
                                )
                            })
                        }
                        {/*--------------------------------------- Game Logo [End] ----------------------------------------- */}
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    );
};
export default AddLeaderboardBonus;