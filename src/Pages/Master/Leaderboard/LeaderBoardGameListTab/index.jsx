import { useDispatch } from "react-redux";
import React, { useEffect, useState } from "react";
import TableCell from "@mui/material/TableCell";
import user from "../../../../assets/images/avatar.png";
import { getLeaderboardGameList, getLeaderboardList } from "../../../../Redux/Master/action";
import Box from "@mui/material/Box";
import Loader from "../../../../images/Loader";
import LeaderBoardGameListDropdown from "./LeaderBoardGameListDropdown";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../../hoc/CommonTable";

const LeaderBoardGameListTab = () => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [gameFilterData, setGameFilterData] = useState([])
    const [gameId, setGameId] = useState('');

    const columns = [
        {
            id: '',
            label: ' User Profile',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell className={'game_icon_img'}>
                    <img src={row?.userId?.profileImage || user} alt={''} />
                </TableCell>
            }
        },
        {
            id: 'fullName',
            label: 'User Name',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.userId?.fullName}</TableCell>
            }
        },
        {
            id: 'rank',
            label: 'Rank',
        },
        {
            id: 'phoneNumber',
            label: 'Mobile Number',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.userId?.phoneNumber}</TableCell>
            }
        },
        {
            id: 'score',
            label: 'Score',
        }
    ];

    useEffect(() => {
        dispatch(getLeaderboardGameList()).then(res => {
            setGameFilterData(res.data.data?.docs)
        })
    }, []);

    useEffect(() => {
        getLeaderboardDetailsList();
    }, [gameId]);

    const getLeaderboardDetailsList = () => {
        if (gameId) {
            let payload = {
                gameId: gameId,
                limit: pagination.rowsPerPage,
                start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            }
            setLoader(true)
            dispatch(getLeaderboardList(payload)).then(res => {
                setLoader(false)
                if (res.data.success) {
                    setRowData({
                        ...rowData,
                        list: res?.data?.data?.docs,
                        totalDocs: res?.data?.data?.totalDocs
                    })
                } else {
                    setRowData({
                        ...rowData,
                        list: [],
                        totalDocs: 0
                    })
                }
            })
        }
    };

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <div className={'leader_board_game_list_filter'}>
                <LeaderBoardGameListDropdown options={gameFilterData} name={'gameId'}
                    setFormData={setGameId} formData={gameId} />
            </div>
            <Paper sx={{ mb: 2 }} className="outerbox">
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
        </Box>
    )
}
export default LeaderBoardGameListTab