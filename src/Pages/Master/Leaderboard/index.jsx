import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import TabPanel from "../../../Components/TabPanel";
import LeaderBoardGameListTab from "./LeaderBoardGameListTab";
import LeaderBoardBonusTab from "./LeaderBoardBonusTab";

const Leaderboard = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const a11yProps = (index) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    };
    return (
        <Box sx={{ width: '100%' }} className={'setting_tab_section user_details_tab'}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                    <Tab className={'tab_listing tab_title'} label="Leaderboard Game List" {...a11yProps(0)} />
                    <Tab className={'tab_listing tab_title'} label="Leaderboard Bonus" {...a11yProps(1)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <LeaderBoardGameListTab />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <LeaderBoardBonusTab />
            </TabPanel>
        </Box>
    )
};
export default Leaderboard;