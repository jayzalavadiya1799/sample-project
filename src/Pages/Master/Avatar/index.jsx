import React, { useEffect, useState } from "react";
import Loader from "../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import CommonModal from "../../../hoc/CommonModal";
import { useDispatch, useSelector } from "react-redux";
import { Box, Avatar } from '@mui/material';
import PopComponent from "../../../hoc/PopContent";
import TableCell from "@material-ui/core/TableCell";
import AddAvatar from "./AddAvatar";
import { deleteAvatarData, getAllAvatars } from "../../../Redux/Avatar/action";
import { ActionFunction, hideActionFunc } from "../../../utils";
import MainCommonFilter from "../../../Components/MainCommonFilter";

const AvatarDetails = () => {
    const dispatch = useDispatch();
    const [isEdit, setIsEdit] = useState(false);
    const [avatarEditRow, setAvatarEditRow] = useState({});
    const [loader, setLoader] = useState(false);
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [filterData, setFilterData] = useState({ search: "", filterClose: false, });
    const getAllAvatar = useSelector((state) => state.avatarReducer.getAllAvatar);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    useEffect(() => {
        getAllAvatarData();
    }, [pagination.rowsPerPage, pagination.page]);

    const getAllAvatarData = (startDate, endDate, search) => {
        let payload = {
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            limit: pagination.rowsPerPage,
            searchText: search,
        };
        setLoader(true);
        dispatch(getAllAvatars(payload))
            .then((res) => {
                if (res.data.statusCode === 200 && res.data.success) {
                    setLoader(false);
                } else {
                    setLoader(false);
                }
            })
            .catch((e) => {
                setLoader(false);
            });
    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'DeleteCommonModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false });
            }
        }
    };

    const columns = [
        {
            label: "Avatar",
            sortable: false,
            isDisbanding: true,
            type: "custom",
            render: (row) => {
                return (
                    <TableCell>
                        <Box>
                            <Avatar
                                src={
                                    row?.avatarImage
                                        ? row?.avatarImage
                                        : `images/avatar-default.png`
                                }
                                sx={{ width: 44, height: 44 }}
                            />
                        </Box>
                    </TableCell>
                );
            },
        },
        {
            id: "avatarName",
            numeric: true,
            disablePadding: false,
            label: "Name",
        },
        ActionFunction('master', {
            id: "action",
            disablePadding: false,
            isDisbanding: true,
            label: "Action",
            type: "custom",
            render: (row) => {
                return (
                    <TableCell className={'role_field_id'}>
                        <Box>
                            <Box
                                component="span"
                                sx={{ fontWeight: 500 }}
                                className="pointer link_color u_border"
                                onClick={() => editAvatarHandler(row)}
                            >
                                {" "}
                                Edit{" "}
                            </Box>
                            <Box
                                component="span"
                                ml={1}
                                sx={{ fontWeight: 500 }}
                                className="pointer link_color prTab"
                                onClick={() => handleOpenModal('DeleteCommonModal',
                                    { deleteListApiHandler: deleteAvatarData({ avatarId: row?._id }), title: 'Do you want to delete this data?' })}
                            >
                                {" "}
                                Delete{" "}
                            </Box>


                        </Box>
                    </TableCell>
                );
            },
        })

    ];

    const editAvatarHandler = (row) => {
        setAvatarEditRow(row);
        setIsEdit(true);
    };

    return (
        <Box>
            {loader ? <Loader /> : ""}
            {
                hideActionFunc('master') &&
                <AddAvatar isEdit={isEdit} getAllAvatarData={getAllAvatarData} handleOpenModal={handleOpenModal} avatarEditRow={avatarEditRow}
                    setIsEdit={setIsEdit} filterData={filterData} setFilterData={setFilterData} setPagination={setPagination} pagination={pagination} />
            }

            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'small-device-show'}>
                    <MainCommonFilter
                        filterData={filterData}
                        setFilterData={setFilterData}
                        searchApiHandler={getAllAvatarData}
                        pagination={pagination}
                        setPagination={setPagination}
                        addPropsFilter={{ isAvatar: true }}
                    />
                </div>

                <CustomTable
                    headCells={columns}
                    rowData={getAllAvatar?.list}
                    totalDocs={getAllAvatar?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getAllAvatarData} />
            </CommonModal>
        </Box>
    );
};
export default AvatarDetails;

