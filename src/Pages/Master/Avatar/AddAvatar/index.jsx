import { Avatar, Box, Button } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { createAvatarData, editAvatarData } from "../../../../Redux/Avatar/action";
import { jsonToFormData, profileImages } from "../../../../utils";
import { useDispatch } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import FilledButton from "../../../../Components/FileButton";
import icon_plus from "../../../../assets/images/plus.svg";
import user from '../../../../assets/images/avatar.png';
import MainCommonFilter from "../../../../Components/MainCommonFilter";

const AddAvatar = ({ isEdit, getAllAvatarData, handleOpenModal, avatarEditRow, setIsEdit, filterData, setFilterData, pagination, setPagination }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [formData, setFormData] = useState({ avatarName: '', avatarImage: '', avatarId: '', isImageUpdated: false, avatarURL: '' });

    useEffect(() => {
        if (isEdit && Object.keys(avatarEditRow)?.length > 0) {
            setFormData({
                ...formData,
                avatarName: avatarEditRow?.avatarName,
                avatarURL: avatarEditRow?.avatarImage
            })
        }
    }, [isEdit]);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                avatarName: formData?.avatarName,
                avatarImage: formData?.avatarImage,
            }
            setLoader(true)
            dispatch(createAvatarData(jsonToFormData(payload))).then((res) => {
                if (res.data.statusCode === 200 && res.data.success) {
                    getAllAvatarData();
                    setFormData({
                        ...formData,
                        avatarName: '',
                        avatarImage: '',
                        avatarURL: ''
                    })
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            });
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const editAvatarDataHandler = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                avatarId: avatarEditRow?._id
            }
            if (!payload.isImageUpdated) { delete payload.avatarImage };
            setLoader(true)
            dispatch(editAvatarData(jsonToFormData(payload))).then((res) => {
                if (res.data.statusCode === 200 && res.data.success) {
                    getAllAvatarData();
                    setIsEdit(false);
                    setLoader(false)
                    setFormData({ avatarName: '', avatarImage: '', avatarId: '', isImageUpdated: false, avatarURL: '' });
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message });
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg });
                }
            }).catch(e => {
                setLoader(false)
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const closeAvatarHandler = () => {
        setIsEdit(false);
        setFormData({ avatarName: '', avatarImage: '', avatarId: '', isImageUpdated: false, avatarURL: '' });
    };

    const FileUpload = (e) => {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(e.target.files[0]);
        fileReader.onload = () => {
            if (isEdit) {
                setFormData({ ...formData, avatarImage: e.target.files[0], avatarURL: fileReader?.result, isImageUpdated: true });
            } else {
                setFormData({ ...formData, avatarImage: e.target.files[0], avatarURL: fileReader?.result });
            }
        };
    };

    const handleChange = (e) => {
        setFormData({
            ...formData,
            avatarName: e.target.value
        });
    }

    return (
        <div className={'avatar_box'}>
            <Box className="outerbox bg_white box_shadow radius_8 small-outer-box">
                <form
                    onSubmit={isEdit ? editAvatarDataHandler : handleSubmit}
                    className={"avatar-section-data"}
                >
                    <Box className={"avatar-container"}>
                        <Box className="upload_avatar">
                            {/*<Box className="upload_avatar_image">*/}
                            {/*    <input*/}
                            {/*        type="file"*/}
                            {/*        name="avatar"*/}
                            {/*        accept="image/png, image/gif, image/jpeg, image/jpg"*/}
                            {/*        onChange={(e) => FileUpload(e)}*/}
                            {/*    />*/}
                            {/*    <Avatar*/}
                            {/*        src={`${formData?.avatarURL*/}
                            {/*            ? formData?.avatarURL*/}
                            {/*            : "images/avatar-default.png"*/}
                            {/*        }`}*/}
                            {/*        className={'avatar_uploader '}*/}
                            {/*        sx={{ width: 44, height: 44 }}*/}
                            {/*    />*/}
                            {/*    {simpleValidator.current.message("avatar", formData?.avatarURL, "required")}*/}
                            {/*</Box>*/}
                            <div className='user_profile'>
                                <div className='form_group profile'>
                                    <div className='user_profile'>
                                        <div className='user_profile_pic'>
                                            {profileImages(formData?.avatarURL, user)}
                                            <span className='addnew'>
                                                <img src={icon_plus} alt='' />
                                                <input type='file' name='member_photo' id='' onChange={(e) => FileUpload(e)} accept="image/png, image/jpeg, image/jpg" />
                                            </span>
                                        </div>
                                    </div>
                                    {simpleValidator.current.message("avatar", formData?.avatarURL, "required")}
                                </div>
                            </div>
                        </Box>
                        <div className={"avatar-modal"}>
                            <Box className={"avatar-name-data"}>
                                <label> Name </label>
                                <input
                                    size="small"
                                    id="outlined-basic"
                                    name="avatarName"
                                    value={formData.avatarName}
                                    onChange={(e) => handleChange(e)}
                                />
                                {simpleValidator.current.message("avatarName", formData?.avatarName, "required")}
                            </Box>
                        </div>

                        <Box className={isEdit ? 'avtar_btn_details' : ''}>
                            <Box>
                                {" "}
                                <label>Action</label>{" "}
                            </Box>
                            {isEdit ? (
                                <div className={'edit-avatar-btn'}>
                                    <FilledButton type={'submit'} value={<EditIcon />} className={'loader_css submit_btn'} loading={loader} />
                                    <button type={"button"} onClick={() => closeAvatarHandler()}><CloseIcon /></button>
                                </div>
                            ) : (
                                <>
                                    <FilledButton type={'submit'} value={<AddIcon />} className={'loader_css'} loading={loader} />
                                </>
                            )}
                        </Box>
                    </Box>
                </form>
            </Box>
            <Box className="outerbox bg_white box_shadow radius_8 avatar_right_box search_box_avatar large-device-show">
                <MainCommonFilter
                    filterData={filterData}
                    setFilterData={setFilterData}
                    searchApiHandler={getAllAvatarData}
                    pagination={pagination}
                    setPagination={setPagination}
                    addPropsFilter={{ isAvatar: true }}
                />
            </Box>
        </div>
    )
};

export default AddAvatar;