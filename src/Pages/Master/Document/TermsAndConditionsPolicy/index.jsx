import React, { useEffect, useState } from "react";
import { ContentState, convertToRaw, EditorState } from "draft-js";
import PopComponent from "../../../../hoc/PopContent";
import { useDispatch } from "react-redux";
import Loader from "../../../../images/Loader";
import Paper from "@mui/material/Paper";
import { Editor } from "react-draft-wysiwyg";
import CommonModal from "../../../../hoc/CommonModal";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import { createTermsAndCondition, getTermsAndConditionList } from "../../../../Redux/Documentation/action";
import { hideActionFunc } from "../../../../utils";

const TermsAndConditionsPolicy = () => {
    const [formData, setFormData] = useState('');
    const [rowData, setRowData] = useState();
    const [loader, setLoader] = React.useState(false);
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const dispatch = useDispatch();

    useEffect(() => {
        getTermsAndConditionsPolicy();
    }, []);
    const getTermsAndConditionsPolicy = () => {
        setLoader(true);
        dispatch(getTermsAndConditionList({})).then(res => {
            setRowData(res?.data.data)
            setLoader(false);
        });
    };

    useEffect(() => {
        if (rowData) {
            const contentBlock = htmlToDraft(rowData?.description);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                setEditorState(editorState);
            }
            setFormData(rowData?.title)
        }
    }, [rowData]);

    const onEditorStateChange = (editorState) => {
        setEditorState(editorState);
    };

    const handleSubmit = () => {
        let payload = {
            description: draftToHtml(convertToRaw(editorState.getCurrentContent())),
            title: formData
        }
        dispatch(createTermsAndCondition(payload)).then(res => {
            if (res.data.statusCode === 200) {
                handleOpenModal('CommonPop', { header: "Success", body: res.data.message });
                getTermsAndConditionsPolicy();
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: (res.data.message || res.data.msg) });
            }
        })
    };

    const checkRchEmpty = (html = rowData?.description) => {
        let div = document.createElement('div');
        div.innerHTML = html;
        let isImage = div?.getElementsByTagName?.('img')?.length > 0;
        if (isImage) {
            return false;
        }
        return (div.innerText.replaceAll('\n', '').trim() === '' || div.innerText.replaceAll('\n', '').trim() === 'undefined');
    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'UpdateTermsAndCondition': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox game-rules-section">
                <div className={'formData policy_form'}>
                    <label className={'fontFamily'}>Title</label>
                    <div className={'policy_title'}>
                        <input className={'fontFamily'} placeholder={'Enter Title'} type={'text'} name={'title'} value={formData} onChange={(e) => setFormData(e.target.value)} />
                    </div>
                </div>
                <div className={'policy-editor'}>
                    <label className={'fontFamily'}>Description</label>
                    <Editor
                        editorState={editorState}
                        onEditorStateChange={(editorState) => onEditorStateChange(editorState)}
                        toolbar={
                            {
                                options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link'],
                                fontFamily: {
                                    options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Inter'],
                                }
                            }
                        }
                    />
                </div>
                {
                    hideActionFunc('master') &&
                    <div className={'game-play-rules'}>
                        {
                            checkRchEmpty() ?
                                <button className={'fontFamily'} onClick={(e) => handleSubmit(e)}>Save Terms & Conditions</button>
                                :
                                <button className={'fontFamily'} onClick={(e) => handleOpenModal('UpdateTermsAndCondition', { description: draftToHtml(convertToRaw(editorState.getCurrentContent())), title: formData })}>Update Terms & Conditions</button>
                        }
                    </div>
                }

            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getTermsAndConditionsPolicy} />
            </CommonModal>
        </>
    )
}
export default TermsAndConditionsPolicy