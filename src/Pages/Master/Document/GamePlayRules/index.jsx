import React, { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { Editor } from 'react-draft-wysiwyg';
import { useDispatch, useSelector } from 'react-redux';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import PopComponent from "../../../../hoc/PopContent";
import { createGamePlayRules, getGameRules } from "../../../../Redux/settings/action";
import Loader from "../../../../images/Loader";
import CommonModal from "../../../../hoc/CommonModal";
import { hideActionFunc } from "../../../../utils";

const GamePlayRules = () => {
    const [loader, setLoader] = React.useState(false);
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const gameListRules = useSelector(state => state?.settingReducer?.gameRules);
    const dispatch = useDispatch();

    useEffect(() => {
        gameRulesData();
    }, []);

    useEffect(() => {
        if (gameListRules?.gameRules) {
            const contentBlock = htmlToDraft(gameListRules?.gameRules);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                setEditorState(editorState);
            }
        }
    }, [gameListRules]);

    const onEditorStateChange = (editorState) => {
        setEditorState(editorState);
    };

    const gameRulesData = () => {
        setLoader(true);
        dispatch(getGameRules({})).finally(_ => {
            setLoader(false)
        });
    };

    const gameRulesSubmitHandler = () => {
        dispatch(createGamePlayRules({ gameRules: draftToHtml(convertToRaw(editorState.getCurrentContent())) })).then(res => {
            if (res.data.statusCode === 200) {
                handleOpenModal('CommonPop', { header: "Success", body: res.data.message });
                gameRulesData();
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: (res.data.message || res.data.msg) });
            }
        })
    };

    const checkRchEmpty = (html = gameListRules?.gameRules) => {
        let div = document.createElement('div');
        div.innerHTML = html;
        let isImage = div?.getElementsByTagName?.('img')?.length > 0;
        if (isImage) {
            return false;
        }
        return (div.innerText.replaceAll('\n', '').trim() === '' || div.innerText.replaceAll('\n', '').trim() === 'undefined');
    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'UpdateGamePlayRules': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox game-rules-section">
                <div className={'policy-editor'}>
                    <Editor
                        editorState={editorState}
                        onEditorStateChange={(editorState) => onEditorStateChange(editorState)}
                        toolbar={
                            {
                                options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link'],
                                fontFamily: {
                                    options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Inter'],
                                }
                            }
                        }
                    />
                </div>
                {
                    hideActionFunc('master') &&
                    <div className={'game-play-rules'}>
                        {
                            checkRchEmpty() ?
                                <button className={'fontFamily'} onClick={(e) => gameRulesSubmitHandler(e)}>Save Game Play Rules</button>
                                :
                                <button className={'fontFamily'} onClick={(e) => handleOpenModal('UpdateGamePlayRules', { editorState: editorState })}>Update Game Play Rules</button>
                        }
                    </div>
                }

            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={gameRulesData} />
            </CommonModal>
        </>
    )
};

export default GamePlayRules