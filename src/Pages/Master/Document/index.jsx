import React from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import { a11yProps } from "../../../utils";
import Tabs from "@mui/material/Tabs";
import TabPanel from "../../../Components/TabPanel";
import LegalPolicy from "./LegalPolicy";
import PrivacyPolicyPolicy from "./PrivacyPolicy";
import TermsAndConditionsPolicy from "./TermsAndConditionsPolicy";
import FairPlayPolicy from "./FairPlayPolicy";
import GamePlayRules from "./GamePlayRules";

const Document = () => {
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
            <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >
                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                    <Tab className={'tab_title'} label="Privacy Policy" {...a11yProps(0)} />
                    <Tab className={'tab_title'} label="Terms & Conditions" {...a11yProps(1)} />
                    <Tab className={'tab_title'} label="Legal Policy" {...a11yProps(2)} />
                    <Tab className={'tab_title'} label="Fair Play Policy" {...a11yProps(3)} />
                    <Tab className={'tab_title'} label="Game Play Rules" {...a11yProps(4)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0} >
                <PrivacyPolicyPolicy />
            </TabPanel>
            <TabPanel value={value} index={1} >
                <TermsAndConditionsPolicy />
            </TabPanel>

            <TabPanel value={value} index={2} >
                <LegalPolicy />
            </TabPanel>

            <TabPanel value={value} index={3} >
                <FairPlayPolicy />
            </TabPanel>
            <TabPanel value={value} index={4} >
                <GamePlayRules />
            </TabPanel>
        </Box>
    )
}
export default Document