import React, { useEffect, useState } from "react";
import { ContentState, convertToRaw, EditorState } from "draft-js";
import { useDispatch, useSelector } from "react-redux";
import htmlToDraft from "html-to-draftjs";
import draftToHtml from "draftjs-to-html";
import Paper from "@mui/material/Paper";
import { Editor } from "react-draft-wysiwyg";
import PopComponent from "../../../../hoc/PopContent";
import { createFairPlayPolicy, getFairPlayPolicy } from "../../../../Redux/settings/action";
import CommonModal from "../../../../hoc/CommonModal";
import Loader from "../../../../images/Loader";
import { hideActionFunc } from "../../../../utils";

const FairPlayPolicy = () => {
    const dispatch = useDispatch();
    const [loader, setLoader] = React.useState(false);
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const gameListRules = useSelector(state => state?.settingReducer?.fairRules);

    useEffect(() => {
        gameFairPlayPolicy();
    }, []);

    useEffect(() => {
        if (gameListRules?.fairPlayPolicy) {
            const contentBlock = htmlToDraft(gameListRules?.fairPlayPolicy);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                setEditorState(editorState);
            }
        }
    }, [gameListRules]);

    const onEditorStateChange = (editorState) => {
        setEditorState(editorState);
    };

    const gameFairPlayPolicy = () => {
        setLoader(true);
        dispatch(getFairPlayPolicy({})).finally(_ => {
            setLoader(false)
        });
    };

    const fairPlayPolicySubmitHandler = () => {
        dispatch(createFairPlayPolicy({ fairPlayPolicy: draftToHtml(convertToRaw(editorState.getCurrentContent())) })).then(res => {
            if (res.data.statusCode === 200) {
                handleOpenModal('CommonPop', { header: "Success", body: res.data.message });
                gameFairPlayPolicy();
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: (res.data.message || res.data.msg) });
            }
        })
    };

    const checkRchEmpty = (html = gameListRules?.fairPlayPolicy) => {
        let div = document.createElement('div');
        div.innerHTML = html;
        let isImage = div?.getElementsByTagName?.('img')?.length > 0;
        if (isImage) {
            return false;
        }
        return (div.innerText.replaceAll('\n', '').trim() === '' || div.innerText.replaceAll('\n', '').trim() === 'undefined');
    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'UpdateFairPlayPolicy': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox game-rules-section">
                <div className={'policy-editor'}>
                    <Editor
                        editorState={editorState}
                        onEditorStateChange={(editorState) => onEditorStateChange(editorState)}
                        toolbar={
                            {
                                options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link'],
                                fontFamily: {
                                    options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Inter'],
                                }
                            }
                        }
                    />
                </div>
                {
                    hideActionFunc('master') &&
                    <div className={'game-play-rules'}>
                        {
                            checkRchEmpty() ?
                                <button className={'fontFamily'} onClick={(e) => fairPlayPolicySubmitHandler(e)}>Submit Fair Play Policy</button>
                                :
                                <button className={'fontFamily'} onClick={(e) => handleOpenModal('UpdateFairPlayPolicy', { editorState: editorState })}>Update Fair Play Policy</button>
                        }
                    </div>
                }

            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={gameFairPlayPolicy} />
            </CommonModal>
        </>
    )
}
export default FairPlayPolicy