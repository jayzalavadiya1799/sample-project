import React, { useEffect, useState } from "react";
import { ContentState, convertToRaw, EditorState } from "draft-js";
import PopComponent from "../../../../hoc/PopContent";
import { useDispatch } from "react-redux";
import htmlToDraft from "html-to-draftjs";
import draftToHtml from "draftjs-to-html";
import Loader from "../../../../images/Loader";
import Paper from "@mui/material/Paper";
import { Editor } from "react-draft-wysiwyg";
import CommonModal from "../../../../hoc/CommonModal";
import { createLegalPolicy, getLegalPolicy } from "../../../../Redux/Documentation/action";
import { hideActionFunc } from "../../../../utils";

const LegalPolicy = () => {
    const [loader, setLoader] = React.useState(false);
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [formData, setFormData] = useState('')
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const [rowData, setRowData] = useState();
    const dispatch = useDispatch();

    useEffect(() => {
        getLegalPolicyHandler();
    }, []);
    const getLegalPolicyHandler = () => {
        setLoader(true);
        dispatch(getLegalPolicy({})).then(res => {
            setRowData(res?.data.data)
            setLoader(false);
        });
    };

    useEffect(() => {
        if (rowData) {
            const contentBlock = htmlToDraft(rowData?.description);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                setEditorState(editorState);
            }
            setFormData(rowData?.title)
        }
    }, [rowData]);

    const onEditorStateChange = (editorState) => {
        setEditorState(editorState);
    };



    const handleSubmit = () => {
        let payload = {
            description: draftToHtml(convertToRaw(editorState.getCurrentContent())),
            title: formData
        }
        dispatch(createLegalPolicy(payload)).then(res => {
            if (res.data.statusCode === 200) {
                handleOpenModal('CommonPop', { header: "Success", body: res.data.message });
                getLegalPolicyHandler();
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: (res.data.message || res.data.msg) });
            }
        })
    };

    const checkRchEmpty = (html = rowData) => {
        let div = document.createElement('div');
        div.innerHTML = html;
        let isImage = div?.getElementsByTagName?.('img')?.length > 0;
        if (isImage) {
            return false;
        }
        return (div.innerText.replaceAll('\n', '').trim() === '' || div.innerText.replaceAll('\n', '').trim() === 'undefined');
    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'UpdateLegalPolicyPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox game-rules-section">
                <div className={'formData policy_form'}>
                    <label className={'fontFamily'}>Title</label>
                    <div className={'policy_title'}>
                        <input type={'text'} className={'fontFamily'} placeholder={'Enter Title'} name={'title'} value={formData} onChange={(e) => setFormData(e.target.value)} />
                    </div>
                </div>
                <div className={'policy-editor'}>
                    <label className={'fontFamily'}>Description</label>
                    <Editor
                        editorState={editorState}
                        onEditorStateChange={(editorState) => onEditorStateChange(editorState)}
                        toolbar={
                            {
                                options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link'],
                                fontFamily: {
                                    options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Inter'],
                                }
                            }
                        }
                        className={'editor-details'}
                    />
                </div>
                {
                    hideActionFunc('master') &&
                    <div className={'game-play-rules'}>
                        {
                            checkRchEmpty() ?
                                <button onClick={(e) => handleSubmit(e)} className={'fontFamily'}>Save Legal Policy</button>
                                :
                                <button className={'fontFamily'} onClick={(e) => handleOpenModal('UpdateLegalPolicyPop', { description: draftToHtml(convertToRaw(editorState.getCurrentContent())), title: formData })}>Update Legal Policy</button>
                        }
                    </div>
                }

            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getLegalPolicyHandler} />
            </CommonModal>
        </>
    )
}
export default LegalPolicy