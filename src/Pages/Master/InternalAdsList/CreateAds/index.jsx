import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import { jsonToFormData, profileImages } from "../../../../utils";
import user from "../../../../assets/images/avatar.png";
import FilledButton from "../../../../Components/FileButton";
import CommonModal from "../../../../hoc/CommonModal";
import { useDispatch } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../hoc/PopContent";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import TextField from "@mui/material/TextField";
import moment from "moment";
import { createInternalAdsList, updateInternalAdsList } from "../../../../Redux/Master/action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const CreateAds = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const [, updateState] = useState({});
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [formData, setFormData] = useState({ internalAdsBanner: '', expireDate: '', startDate: '', isInternalAdsUpdated: false });
    const forceUpdate = useCallback(() => updateState({}), []);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [openCale, setOpenCale] = useState({
        expireDate: false,
        startDate: false
    })
    let Modal = PopComponent[modalDetails.modalName];

    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                startDate: moment(formData?.startDate).format("YYYY-MM-DD"),
                expireDate: moment(formData?.expireDate).format("YYYY-MM-DD"),
            }
            delete payload?.isInternalAdsUpdated
            setLoader(true)
            dispatch(createInternalAdsList(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleChange = (e) => {
        let img = new Image();
        img.src = window.URL.createObjectURL(e.target.files[0]);
        img.onload = () => {
            if (img.width === 320 && img.height === 50) {
                setFormData({ ...formData, internalAdsBanner: e.target.files[0], isInternalAdsUpdated: true });
            } else {
                handleOpenErrorModal('CommonPop', { header: "Info", body: 'The width and height of the image should be  320 * 50 size' });
            }
        }
    };

    const handleDatePicker = (newValue, type) => {
        setFormData({ ...formData, [type]: newValue });
        setOpenCale({ ...openCale, [type]: false });
    };


    useEffect(() => {
        if (modalValue?.isEdit) {
            setFormData({
                ...formData,
                internalAdsBanner: modalValue?.row?.inetrnalAdsBanner,
                expireDate: moment(modalValue?.row?.expireDate).format("YYYY-MM-DD"),
                startDate: moment(modalValue?.row?.startDate).format("YYYY-MM-DD")
            })
        }
    }, [modalValue]);

    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                startDate: moment(formData?.startDate).format("YYYY-MM-DD"),
                expireDate: moment(formData?.expireDate).format("YYYY-MM-DD"),
                internalAdsId: modalValue?.row?._id
            }
            if (!payload?.isInternalAdsUpdated) {
                delete payload?.internalAdsBanner;
            }
            setLoader(true)
            dispatch(updateInternalAdsList(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler();
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{modalValue?.isEdit ? 'Update Internal Ads' : 'Add Internal Ads'}</h2>
                </div>
                <div className={'header_slider_details header_slider_details_Ads'}>
                    <form className='form_group ' onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        {/*--------------------------------------- Game Logo [Start] ----------------------------------------- */}
                        <div className='user_profile'>
                            <label htmlFor='' className='profile_label'>Ads Banner (320 * 50 size) </label>
                            <div className={'header_section_slider'}>
                                <div className='user_profile_pic'>
                                    {profileImages(formData?.internalAdsBanner, user)}
                                    <span className='add_new'>
                                        <input type='file' name='internalAdsBanner' id='' onChange={(e) => handleChange(e)} /> </span>
                                </div>
                            </div>
                            {simpleValidator.current.message("internalAdsBanner", formData?.internalAdsBanner, "required")}
                        </div>
                        <div className={'date-picker_coupon'}>
                            <div className={'start-date-picker'}>
                                <label>Start Date</label>
                                <div className={'date_picker_value'}>
                                    <LocalizationProvider dateAdapter={AdapterDateFns}  >
                                        <DatePicker
                                            name='start-date'
                                            value={formData?.startDate}
                                            open={openCale.startDate}
                                            onChange={(newValue) => handleDatePicker(newValue, 'startDate')}
                                            minDate={new Date()}
                                            renderInput={(params) => {
                                                return <TextField {...params} onClick={() => setOpenCale({ ...openCale, startDate: !openCale?.startDate, expireDate: false })} />
                                            }}
                                            inputFormat="MMM dd, yyyy"
                                            className={'datePicker_details'}
                                            inputProps={{ readOnly: true }}
                                        />
                                    </LocalizationProvider>
                                </div>
                                {simpleValidator.current.message("startDate", formData?.startDate, "required")}
                            </div>
                            <div className={'end-date-picker'}>
                                <label>Expired Date</label>
                                <div className={'date_picker_value'}>
                                    <LocalizationProvider dateAdapter={AdapterDateFns} >
                                        <DatePicker
                                            name='expireDate'
                                            value={formData?.expireDate}
                                            onChange={(newValue) => handleDatePicker(newValue, 'expireDate')}
                                            inputFormat="MMM dd, yyyy"
                                            minDate={formData?.startDate}
                                            open={openCale.expireDate}
                                            renderInput={(params) => <TextField {...params} onClick={() => setOpenCale({ ...openCale, expireDate: !openCale?.expireDate, startDate: false })} />}
                                            inputProps={{ readOnly: true }}
                                        />
                                    </LocalizationProvider>
                                </div>
                                {simpleValidator.current.message("expireDate", formData?.expireDate, "required")}
                            </div>
                        </div>
                        {/*--------------------------------------- Game Logo [End] ----------------------------------------- */}
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default CreateAds;