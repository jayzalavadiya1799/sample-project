import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import SportsEsportsIcon from "@mui/icons-material/SportsEsports";
import AndroidIcon from "@mui/icons-material/Android";
import AppleIcon from "@mui/icons-material/Apple";
import SelectDropdown from "../../../Components/SelectDropdown";
import LocalActivityIcon from "@mui/icons-material/LocalActivity";
import FilledButton from "../../../Components/FileButton";
import Paper from "@mui/material/Paper";
import SimpleReactValidator from "simple-react-validator";
import { useDispatch, useSelector } from "react-redux";
import icon_plus from '../../../assets/images/plus.svg';
import ScreenLockLandscapeIcon from '@mui/icons-material/ScreenLockLandscape';
import ScreenLockPortraitIcon from '@mui/icons-material/ScreenLockPortrait';
import PopComponent from "../../../hoc/PopContent";
import CommonModal from "../../../hoc/CommonModal";
import { addGame } from "../../../Redux/games/action";
import { jsonToFormData } from "../../../utils";
import user from '../../../assets/images/avatar.png';
import Tooltip from '@mui/material/Tooltip';
import { getGenreNames } from "../../../Redux/games/GenreGame/action";
import PhonelinkIcon from '@mui/icons-material/Phonelink';


const AddNewGame = () => {
    const dispatch = useDispatch();
    const genreNamesList = useSelector(state => state?.gameReducer?.genreNamesList);
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [filterStage, setFilterStage] = useState({ android: false, ios: false, crossPlatform: false });
    const [filterOrientation, setFilterOrientation] = useState({ portrait: false, landscape: false, orientationField: "" });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const [formData, setFormData] = useState({
        gameName: '', genre: '', description: '', platform: '', isOrientationPortrait: '', format: '',
        engine: '', gameIcon: '', gameDesignDocLink: '', youtubeVideoLink: '', isGameModeOption: false, isNoOfPlayer: false, isMultipleDeck: false
    });

    useEffect(() => {
        genreList();
    }, []);

    //get genre list
    const genreList = () => {
        dispatch(getGenreNames({ genreStatus: 'Active' }));
    };

    const stageCheckboxFilter = (e) => {
        const { checked, value, name } = e.target;
        setFilterStage({ ...filterStage, [e.target.name]: checked });
        setFormData({ ...formData, platform: name });
        if (name === 'Android' && checked) {
            setFilterStage({ ...filterStage, android: true, ios: false, crossPlatform: false })
        }
        if (name === 'Ios' && checked) {
            setFilterStage({ ...filterStage, android: false, ios: true, crossPlatform: false })
        }
        if (name === 'Cross-Platform' && checked) {
            setFilterStage({ ...filterStage, android: false, ios: false, crossPlatform: true })
        }
    };

    const orientationCheckboxFilter = (e) => {
        const { checked, value, name } = e.target;
        setFilterOrientation({ ...filterOrientation, [e.target.name]: checked });
        if (name === 'portrait' && checked) {
            setFilterOrientation({ ...filterOrientation, portrait: true, landscape: false, orientationField: e.target.name })
        }
        if (name === 'landscape' && checked) {
            setFilterOrientation({ ...filterOrientation, portrait: false, landscape: true, orientationField: e.target.name })
        }
    };

    // add Game fun
    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            setLoader(true);
            let payload = {
                ...formData,
                isOrientationPortrait: filterOrientation?.orientationField === "portrait",
                format: formData?.format === 'Live Multiplayer (Sync)' ? 'Sync' : 'Async'
            }
            setLoader(true)
            dispatch(addGame(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message, auth: true, redirect: '/games/all-games' })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const handleChange = (e) => {
        const { value, name } = e.target;
        setFormData({ ...formData, [name]: value })
    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'AddGenrePopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };
    // reset filed fun
    const resetFieldHandler = () => {
        setFilterOrientation({ portrait: false, landscape: false, orientationField: "" });
        setFilterStage({ android: false, ios: false, crossPlatform: false });
        setFormData({
            gameName: '', genre: '', description: '', platform: '', isOrientationPortrait: '', format: '', mode: '',
            engine: '', gameIcon: '', gameDesignDocLink: '', youtubeVideoLink: ''
        });
    };

    const gameLogoHandler = (e) => {
        let img = new Image();
        img.src = window.URL.createObjectURL(e.target.files[0]);
        img.onload = () => {
            if (img.width === 512 && img.height === 512) {
                setFormData({ ...formData, gameIcon: e.target.files[0] });
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: 'The width and height of the image should be  512 * 512 size' });
            }
        }

    }

    return (
        <Box >
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'add_game_popup'}>
                    <div className={'add_game_popup_title'}>
                        <h2>Add Game information and get started!</h2>
                    </div>
                    <div className={'add_game_popup_content'}>
                        <div className={'add_game_popup_content_sub_title'}>
                            <div className={'icon_game'}>
                                <SportsEsportsIcon />
                            </div>
                            <div className={'add_game_popup_content_sub_title_content'}>
                                <h3>Basic Information</h3>
                                <p>Get started by adding information of your game</p>
                            </div>
                        </div>
                        <div className={'add_game_popup_content_form'}>
                            <form className={'popup_form'} method={'POST'} onSubmit={(e) => handleSubmit(e)}>
                                <div>
                                    <div className={'popup_modal_details_data'}>
                                        <div className={'add_game_section_content_form'}>
                                            <div className="formData">
                                                <label>Enter Game Name <span className={'validation-star'}>*</span></label>
                                                <div className="emailWrap input_length_counter">
                                                    <input type="text" value={formData?.gameName} className={'wrap_input_modal'} maxLength={20} name='gameName' placeholder={'Enter Game Name'} onChange={(e) => handleChange(e)} />
                                                    <span>{formData?.gameName?.length}/20</span>
                                                </div>
                                                {simpleValidator.current.message("game name", formData?.gameName, 'required')}
                                            </div>
                                            {/*--------------------------------------- Game Logo [Start] ----------------------------------------- */}
                                            <div className='form_group profile new_game_section'>
                                                <div className='user_profile'>
                                                    <div className='user_profile_pic'>
                                                        <img src={formData?.gameIcon ? URL.createObjectURL(formData?.gameIcon) : user} alt='' />
                                                        <span className='addnew'>
                                                            <img src={icon_plus} alt='' />
                                                            <input type='file' name='member_photo' id='' onChange={(e) => gameLogoHandler(e)} />
                                                        </span>
                                                    </div>
                                                </div>
                                                <label htmlFor='' className='profile_label'>Game Logo <span className={'size-validation'}>(512*512 size)</span> <span className={'validation-star'}>*</span></label>
                                                {simpleValidator.current.message("gameIcon", formData?.gameIcon, 'required')}
                                            </div>
                                            {/*--------------------------------------- Game Logo [End] ----------------------------------------- */}
                                        </div>
                                        <div className={'popup_form_checkbox'}>
                                            <div className="formData">
                                                <label>Platform <span className={'validation-star'}>*</span></label>
                                                <div className={'platform_field'}>
                                                    <Tooltip title="Android Platform">
                                                        <div className={filterStage.android ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <AndroidIcon />
                                                            <input type="checkbox" name='Android' checked={filterStage.android} onChange={(e) => stageCheckboxFilter(e)} />
                                                        </div>
                                                    </Tooltip>
                                                    <Tooltip title="iOS Platform">
                                                        <div className={filterStage.ios ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <AppleIcon />
                                                            <input type="checkbox" name='Ios' checked={filterStage.ios} onChange={(e) => stageCheckboxFilter(e)} />
                                                        </div>
                                                    </Tooltip>
                                                    <Tooltip title="Cross-Platform">
                                                        <div className={filterStage.crossPlatform ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <PhonelinkIcon />
                                                            <input type="checkbox" name='Cross-Platform' checked={filterStage.crossPlatform} onChange={(e) => stageCheckboxFilter(e)} />
                                                        </div>
                                                    </Tooltip>
                                                </div>
                                                {simpleValidator.current.message("Platform", formData.platform, 'required')}
                                            </div>
                                            <div className="formData orientation_filed">
                                                <label>Orientation <span className={'validation-star'}>*</span> </label>
                                                <div className={'platform_field'}>
                                                    <Tooltip title="Portrait Orientation">
                                                        <div className={filterOrientation.portrait ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <ScreenLockPortraitIcon />
                                                            <input type="checkbox" name='portrait' checked={filterOrientation.portrait} onChange={(e) => orientationCheckboxFilter(e)} />
                                                        </div>
                                                    </Tooltip>
                                                    <Tooltip title="Landscape Orientation">
                                                        <div className={filterOrientation.landscape ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <ScreenLockLandscapeIcon />
                                                            <input type="checkbox" name='landscape' checked={filterOrientation.landscape} onChange={(e) => orientationCheckboxFilter(e)} />
                                                        </div>
                                                    </Tooltip>
                                                </div>
                                                {simpleValidator.current.message("Orientation", filterOrientation.orientationField, 'required')}
                                            </div>
                                        </div>
                                        {/*--------------------------------------- Game Dropdown Genre,Format,Mode,Engine [Start] ----------------------------------------- */}
                                        <div className={'select_game_platform_value'}>
                                            <div className={'select_label tab01'}>
                                                <label>Genre <span className={'validation-star'}>*</span></label>
                                                <SelectDropdown name={'genre'} options={genreNamesList} formData={formData} setFormData={setFormData} handleOpenModal={handleOpenModal} />
                                                {simpleValidator.current.message("selectGenre", formData.genre, 'required')}
                                            </div>
                                            <div className={'select_label tab02'}>
                                                <label>Format <span className={'validation-star'}>*</span></label>
                                                <SelectDropdown name={'format'} isFormat={true} formData={formData} setFormData={setFormData} handleOpenModal={handleOpenModal} />
                                                {simpleValidator.current.message("selectFormat", formData.format, 'required')}
                                            </div>
                                        </div>
                                        <div className={'select_game_platform_value game_mode game_mode_main_section'}>

                                            <div className={'select_label tab-left-side'}>
                                                <label>Engine <span className={'validation-star'}>*</span></label>
                                                <SelectDropdown name={'engine'} options={['Unity']} formData={formData} setFormData={setFormData} handleOpenModal={handleOpenModal} />
                                                {simpleValidator.current.message("selectEngine", formData.engine, 'required')}
                                            </div>
                                            <div className={'select_label game-number-of-player-merge'}>
                                                <div className={'select_label tab01 game_mode_left_details'}>
                                                    <label>Is Game Mode ? <span className={'validation-star'}>*</span></label>
                                                    <div className={'game_mode_btn'}>
                                                        <div className={'game_mode_btn_option'}>
                                                            <input type={'radio'} name={'isGameMode'} checked={formData?.isGameModeOption} onChange={(e) => setFormData({ ...formData, isGameModeOption: true })} />
                                                            <label>Yes</label>
                                                        </div>
                                                        <div className={'game_mode_btn_option tab_radio'}>
                                                            <input type={'radio'} name={'isGameMode'} checked={!formData?.isGameModeOption} onChange={(e) => setFormData({ ...formData, isGameModeOption: false })} />
                                                            <label>No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/*-------------------------------------------- Number of Player ----------------------------------------------------------------*/}
                                                <div className={'select_game_platform_value game_mode game_mode_main_section'}>
                                                    <div className={'select_label tab01 game_mode_left_details'}>
                                                        <label>Is Number Of Player ? <span className={'validation-star'}>*</span></label>
                                                        <div className={'game_mode_btn'}>
                                                            <div className={'game_mode_btn_option'}>
                                                                <input type={'radio'} name={'isNoOfPlayer'} checked={formData?.isNoOfPlayer} onChange={(e) => setFormData({ ...formData, isNoOfPlayer: true })} />
                                                                <label>Yes</label>
                                                            </div>
                                                            <div className={'game_mode_btn_option tab_radio'}>
                                                                <input type={'radio'} name={'isNoOfPlayer'} checked={!formData?.isNoOfPlayer} onChange={(e) => setFormData({ ...formData, isNoOfPlayer: false })} />
                                                                <label>No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/*--------------------------------------------End Number of Player ----------------------------------------------------------------*/}
                                                {
                                                    formData?.genre === 'Card' &&
                                                    <div className={'select_game_platform_value game_mode game_mode_main_section'}>
                                                        <div className={'select_label tab01 game_mode_left_details'}>
                                                            <label>Is Multiple Number Of Deck?<span className={'validation-star'}>*</span></label>
                                                            <div className={'game_mode_btn'}>
                                                                <div className={'game_mode_btn_option'}>
                                                                    <input type={'radio'} name={'isMultipleDeck'} checked={formData?.isMultipleDeck} onChange={(e) => setFormData({ ...formData, isMultipleDeck: true })} />
                                                                    <label>Yes</label>
                                                                </div>
                                                                <div className={'game_mode_btn_option tab_radio'}>
                                                                    <input type={'radio'} name={'isMultipleDeck'} checked={!formData?.isMultipleDeck} onChange={(e) => setFormData({ ...formData, isMultipleDeck: false })} />
                                                                    <label>No</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                        {/*-----------------------------------------------------Game Dropdown [End]---------------------------------------------------------*/}
                                        <div className="formData">
                                            <label>Enter Game Description (120 Chars) <span className={'validation-star'}>*</span></label>
                                            <div className="text_Wrap">
                                                <textarea name='description' placeholder={'Enter Game Description'} rows={4} value={formData?.description} maxLength={120} onChange={(e) => handleChange(e)} />
                                            </div>
                                            {simpleValidator.current.message("description", formData.description, 'required')}
                                        </div>
                                    </div>

                                    <div className={'design_document'}>
                                        <div className={'design_document_title'}>
                                            <LocalActivityIcon />
                                            <div className={'sub_title_content'}>
                                                <h4>How to Play Youtube Video Link</h4>
                                                <input type={'text'} name={'youtubeVideoLink'} value={formData?.youtubeVideoLink} placeholder={'Share with us the Youtube Video Link.'} onChange={(e) => handleChange(e)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={'formData_btn'}>
                                    <button className={'cancel_btn'} type={'reset'} onClick={() => resetFieldHandler()}>Clear</button>
                                    <FilledButton type={'submit'} value={'Save'} className={'submit_btn loader_css'} loading={loader} />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Paper>
            {/*---------------------------------------------------Common Modal ------------------------------------------------------------*/}
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={genreList} />
            </CommonModal>
        </Box>
    )
}
export default AddNewGame;