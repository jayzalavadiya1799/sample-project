import React, { useEffect, useState } from "react";
import Loader from "../../../images/Loader";
import Box from "@material-ui/core/Box";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import PopComponent from "../../../hoc/PopContent";
import TableCell from "@mui/material/TableCell";
import CommonModal from "../../../hoc/CommonModal";
import moment from "moment";
import { getGameList, getOptimizeStatus } from "../../../Redux/games/action";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import user from "../../../assets/images/avatar.png";
import { ActionFunction } from "../../../utils";

const ApprovedGameList = () => {
    const navigate = useNavigate();
    const [loader, setLoader] = useState(false);
    const gameList = useSelector(state => state.gameReducer?.gameAll);
    const dispatch = useDispatch();
    const [stepDetailsVerify, setStepDetailsVerify] = useState({})
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
        exportFileName: 'Export File',
        statusField: 'All Status',
        platformName: 'All Platform'
    })
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    let columns = [
        {
            id: 'icon',
            label: 'Game Icon',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell className={'game_icon_img'}>
                    <img src={row?.gameIcon || user} alt={''} />
                </TableCell>
            }
        },
        {
            id: 'numericId',
            label: 'Game Id',
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className='edit_btn'
                    onClick={() => navigate(`/game-tab/${row.id}`)}>{`GID000${row?.numericId}`}
                </span></TableCell>
            }
        },
        {
            id: 'gameName',
            label: 'Game Name',
        },

        {
            id: 'platform',
            label: 'Game Platform',
        },
        {
            id: 'gameStatus',
            label: 'Status',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.isActive ? 'Activated' : 'Deactivated'}</TableCell>
            }
        },
        {
            id: 'updatedAt',
            label: 'Update Date',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.updatedAt).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        ActionFunction('game', {
            id: 'action',
            label: 'Action',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('ArchivedGamePopup', { gameId: row?._id, isArchive: true })}>Archive</span>
                    {
                        row?.isActive ?
                            <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('ApprovedPendingPopup', { isActivePop: true, gameId: row?._id, isActive: !row?.isActive })}>Deactivate</span> :
                            <span className={'edit_btn edit-btn-action prTab'} onClick={() => handleOpenModal('ActiveDeactivatedPopup', { isActivePop: true, gameId: row?._id, isActive: !row?.isActive, data: row })}>Active</span>
                    }
                </TableCell>
            }

        })

    ];
    const handleActiveModalHandler = (row) => {
        dispatch(getOptimizeStatus({ gameId: row?._id, publisherId: row?.publisherId?._id })).then(res => {
            setStepDetailsVerify(res.data.data);
            if (res.data.success) {
                const { isGameReleaseCreated } = res.data.data;
                if (isGameReleaseCreated) {
                    handleOpenModal('ActiveDeactivatedPopup', { isActivePop: true, gameId: row?._id, isActive: !row?.isActive, data: row });
                } else {
                    handleOpenModal('CommonPop', { header: "info", body: 'Please Complete Game Release Section  Before Game Activate.' });
                }
            } else {
                handleOpenModal('ActiveDeactivatedPopup', { isActivePop: true, gameId: row?._id, isActive: !row?.isActive, data: row });
            }
        })

    }
    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'ApprovedPendingPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'RejectedPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ArchivedGamePopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ActiveDeactivatedPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            allApprovedGameListFetch(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.statusField, filterData.platformName, filterData.startDate, filterData.endDate])

    const allApprovedGameListFetch = (startDate, endDate, search) => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate: startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload,
            gameStatus: 'Approve',
            platform: filterData?.platformName !== 'All Platform' ? filterData?.platformName : '',
            activeStatus: filterData?.statusField !== 'All Status' ? filterData?.statusField : ''
        }

        Object?.keys(payload).forEach(ele => {
            if (payload[ele] === '' || payload[ele] === null) { delete payload[ele] }
        });
        dispatch(getGameList(payload)).then((res) => {
            setLoader(false)
            if (res?.data?.data?.filePath) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            }
        });
    }
    useEffect(() => {
        if (filterData.startDate && filterData.endDate && filterData?.statusValue === 'Custom') {
            setPagination({
                ...pagination,
                page: 0
            });
            allApprovedGameListFetch(filterData.startDate, filterData.endDate)
        }
    }, [filterData.startDate, filterData.endDate]);
    const redirectApiDetails = () => {
        allApprovedGameListFetch(filterData.startDate, filterData.endDate)
    }

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter
                    filterData={filterData}
                    setFilterData={setFilterData}
                    searchApiHandler={allApprovedGameListFetch}
                    pagination={pagination}
                    setPagination={setPagination}
                    statusOption={['All Status', 'Active', 'Deactive']}
                    plateFormOption={['All Platform', 'Ios', 'Android', 'Cross-Platform']}
                    addPropsFilter={{ isGameList: true }}
                />
                <CustomTable
                    headCells={columns}
                    rowData={gameList?.list}
                    totalDocs={gameList?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={redirectApiDetails} filterData={filterData} />
            </CommonModal>
        </Box>
    );
};

export default ApprovedGameList;