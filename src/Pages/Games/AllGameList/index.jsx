import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Loader from "../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import TableCell from "@mui/material/TableCell";
import moment from "moment";
import PopComponent from "../../../hoc/PopContent";
import CommonModal from "../../../hoc/CommonModal";
import { getGameList, getSingleGameDetails } from "../../../Redux/games/action";
import { useDispatch, useSelector } from "react-redux";
import user from '../../../assets/images/avatar.png';
import { useNavigate } from "react-router-dom";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import { getGenreNames } from "../../../Redux/games/GenreGame/action";
import { ActionFunction } from "../../../utils";

const AllGameList = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [loader, setLoader] = useState(false);
    const gameList = useSelector(state => state.gameReducer?.gameAll);
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
        exportFileName: 'Export File',
        statusField: 'All Status',
        platformName: 'All Platform',
        gameTag: 'All Game Tag'
    })
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });

    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    let columns = [
        {
            id: 'icon',
            label: 'Game Icon',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell className={'game_icon_img'}>
                    <img src={row?.gameIcon || user} alt={''} />
                </TableCell>
            }
        },
        {
            id: 'numericId',
            label: 'Game Id',
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className='edit_btn'
                    onClick={() => navigate(`/game-tab/${row.id}`,)}>{`GID000${row?.numericId}`}
                </span></TableCell>
            }
        },
        {
            id: 'gameName',
            label: 'Game Name',
        },

        {
            id: 'platform',
            label: 'Game Platform',
        },
        {
            id: 'gameStatus',
            label: 'Status',
            type: 'custom',
            render: (row) => {
                return <TableCell><span className={row?.gameStatus === 'Reject' ? 'req_rejected' : ''}>{row?.gameStatus}</span></TableCell>
            }
        },
        {
            id: 'gameTag',
            label: 'Game Tag',
            type: 'custom',
            render: (row) => {
                return <TableCell>{row?.gameTag}</TableCell>
            }
        },
        {
            id: 'updatedAt',
            label: 'Update Date',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.updatedAt).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        ActionFunction('game', {
            id: 'action',
            label: 'Action',
            isDisbanding: true,
            type: 'action',
            ActionContent: (row) => {
                return <TableCell className={'role_field_id'}>
                    <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('UpdateGame', row)}>Edit</span>
                    {
                        row?.gameStatus === 'Pending' ?
                            <>
                                <span className='edit_btn edit-btn-action u_border prTab' onClick={() => handleOpenModal('ApprovedPendingPopup', { gameId: row?._id, isApprove: true })}>Approve</span>
                                <span className='edit_btn edit-btn-action u_border prTab' onClick={() => handleOpenModal('RejectedPopup', { gameId: row?._id, isApprove: false })}>Reject</span>
                            </>
                            :
                            row?.gameStatus === 'Reject' ?
                                <>
                                    <span className='edit_btn edit-btn-action u_border prTab' onClick={() => handleOpenModal('ApprovedPendingPopup', { gameId: row?._id, isApprove: true })}>Approve</span>
                                </>
                                : ''
                    }
                    {
                        row?.gameStatus === 'Archive' ?
                            <span className={'edit_btn edit-btn-action  prTab'} onClick={() => handleOpenModal('ArchivedGamePopup', { gameId: row?._id, isArchive: false })}>unArchived</span>
                            :
                            <span className={'edit_btn edit-btn-action  prTab'} onClick={() => handleOpenModal('ArchivedGamePopup', { gameId: row?._id, isArchive: true })}>Archive</span>
                    }


                </TableCell>
            }
        })
    ];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'ApprovedPendingPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'RejectedPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'UpdateGame': {
                dispatch(getGenreNames({ genreStatus: 'Active' }));
                dispatch(getSingleGameDetails({ gameId: data?._id }))
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ArchivedGamePopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };


    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            allGameListFetch(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.statusField,
    filterData.platformName, filterData?.gameTag, filterData.startDate, filterData.endDate])

    const allGameListFetch = (startDate, endDate, search) => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate: startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload,
            platform: filterData?.platformName !== 'All Platform' ? filterData?.platformName : '',
            gameStatus: filterData?.statusField !== 'All Status' ? filterData?.statusField : '',
            gameTag: filterData?.gameTag !== 'All Game Tag' ? filterData?.gameTag : ''
        }

        Object?.keys(payload).forEach(ele => {
            if (payload[ele] === '' || payload[ele] === null) { delete payload[ele] }
        });
        dispatch(getGameList(payload)).then((res) => {
            setLoader(false)
            if (res?.data?.data?.filePath) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            }
        });
    }

    useEffect(() => {
        if (filterData.startDate && filterData.endDate && filterData?.statusValue === 'Custom') {
            setPagination({
                ...pagination,
                page: 0
            })
            allGameListFetch(filterData.startDate, filterData.endDate)
        }
    }, [filterData.startDate, filterData.endDate]);

    const redirectApiDetails = () => {
        allGameListFetch(filterData.startDate, filterData.endDate)
    }
    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter
                    filterData={filterData}
                    setFilterData={setFilterData}
                    searchApiHandler={allGameListFetch}
                    pagination={pagination}
                    setPagination={setPagination}
                    statusOption={['All Status', 'Approve', 'Pending', 'Reject']}
                    plateFormOption={['All Platform', 'Ios', 'Android', 'Cross-Platform']}
                    gameTagOption={['All Game Tag', 'New Game', 'Popular Game',]}
                    addPropsFilter={{ isGameList: true, gameTag: true }}
                />
                <CustomTable
                    headCells={columns}
                    rowData={gameList?.list}
                    totalDocs={gameList?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={modalDetails?.modalName === 'UpdateGame' ? 'update_game_modal_details' : 'Approved-reject-section'}
                modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={redirectApiDetails} />
            </CommonModal>
        </Box>
    );
};

export default AllGameList;