import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../../../images/Loader";
import BasicSteps from "./BasicSteps";
import CustomizeGameplay from "./CustomizeGameplay";
import GameInfo from "./BasicSteps/GameInfo";
import { useDispatch, useSelector } from "react-redux";
import { getOptimizeStatus, getSingleGameDetails } from "../../../../../Redux/games/action";
import { useParams } from "react-router-dom";

const OptimizeTab = ({ steps, setSteps, setValue, handleOpenModal }) => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const [rowData, setRowData] = useState()
    const [loader, setLoader] = useState(false);
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);

    useEffect(() => {
        dispatch(getSingleGameDetails({ gameId: id }))
        getOptimizeStatusHandler();
    }, []);

    const getOptimizeStatusHandler = () => {
        setLoader(true)
        dispatch(getOptimizeStatus({ gameId: id, publisherId: gameDetails?.publisherId?._id })).then(res => {
            setRowData(res.data.data);
            setLoader(false);
        });
    };


    return (
        <React.Fragment>
            <Box>
                {
                    loader &&
                    <Loader />
                }
                {
                    !steps.setupModal ?
                        <div className={'optimize_tab'}>
                            <BasicSteps setSteps={setSteps} steps={steps} handleOpenModal={handleOpenModal} rowData={rowData} setValue={setValue} gameDetails={gameDetails} />
                            <CustomizeGameplay setValue={setValue} rowData={rowData} />
                        </div>
                        : steps?.setupModal && steps.basicStep.gameInfo ?
                            <GameInfo setSteps={setSteps} steps={steps} handleOpenModal={handleOpenModal} getOptimizeStatusHandler={getOptimizeStatusHandler} />
                            :
                            ''
                }
            </Box>
        </React.Fragment>
    )
}
export default OptimizeTab