import React, { useState } from "react";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Loader from "../../../../../../images/Loader";
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import successIcon from "../../../../../../assets/images/right-sucss-icon.svg";

const BasicSteps = ({ setSteps, steps, rowData, setValue, gameDetails }) => {
    const [loader, setLoader] = useState(false);
    return (
        <Box>
            {
                loader &&
                <Loader />
            }
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'game_tab_overView head_to_head_gameTab'}>
                    <div className={'game_tab_overView_title'}>
                        <h2 className={'fontFamily'}>Basic Steps</h2>
                    </div>
                    <div className={'basic_step_info'}>
                        <div className={'optimize_set_content'}>
                            <div className={ gameDetails?.isGameModeOption ? 'optimize_set_box margin_Right' : 'optimize_set_box'} onClick={() => setSteps({
                                ...steps,
                                setupModal: true,
                                basicStep: { ...steps.basicStep, gameInfo: true }
                            })}>
                                <div className={'box_information'}>
                                    <div className={'box_title'}>
                                        <h3 className={'fontFamily'}>Game Info </h3>
                                        {
                                            rowData?.gameInfoStatus ?
                                                <p className={'success_class fontFamily'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>
                                                :
                                                <p className={'unSuccess_class fontFamily'}><HighlightOffIcon /> <span>Incomplete</span></p>
                                        }
                                    </div>
                                    <p className={'note_description fontFamily'}>Create the basic configurations of your game here</p>
                                </div>
                                <div className={'box_arrow'} >
                                    <ArrowForwardIosIcon />
                                </div>
                            </div>
                            {
                                gameDetails?.isGameModeOption &&
                                <div className={'optimize_set_box margin_Left'} onClick={() => setValue(3)}>
                                    <div className={'box_information'}>
                                        <div className={'box_title'}>
                                            <h3 className={'fontFamily'}>Game Of Mode </h3>
                                            {
                                                rowData?.isTwoGameModeCreated ?
                                                    <p className={'success_class fontFamily'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>
                                                    :
                                                    <p className={'unSuccess_class fontFamily'}><HighlightOffIcon /> <span>Incomplete</span></p>
                                            }
                                        </div>
                                        <p className={'note_description fontFamily'}>Create the basic configurations of your game here</p>
                                    </div>
                                    <div className={'box_arrow'} >
                                        <ArrowForwardIosIcon />
                                    </div>
                                </div>
                            }



                            {/*<div className={'optimize_set_box'} onClick={() => setSteps({*/}
                            {/*    ...steps,*/}
                            {/*    setupModal: true,*/}
                            {/*    basicStep: { ...steps.basicStep, sdkSetup: true }*/}
                            {/*})}>*/}
                            {/*    <div className={'box_information'}>*/}
                            {/*        <div className={'box_title'}>*/}
                            {/*            <h3>Verify SDK Setup </h3>*/}
                            {/*            {*/}
                            {/*                rowData?.gameVerifySdkStatus ?*/}
                            {/*                    <p className={'success_class'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>*/}
                            {/*                    :*/}
                            {/*                    <p><HighlightOffIcon /> <span>Incomplete</span></p>*/}
                            {/*            }*/}
                            {/*        </div>*/}
                            {/*        <p className={'note_description'}>Download SDK, test integration and confirm Fairness</p>*/}
                            {/*    </div>*/}
                            {/*    <div className={'box_arrow'} >*/}
                            {/*        <ArrowForwardIosIcon />*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </div>



                        <div className={'optimize_set_content'}>
                            {
                                gameDetails?.isMultipleDeck &&
                                <div className={ (gameDetails?.isNoOfPlayer &&  gameDetails?.isMultipleDeck) ? 'optimize_set_box margin_Right' :'optimize_set_box'} onClick={() => setValue(4)}>
                                    <div className={'box_information'}>
                                        <div className={'box_title'}>
                                            <h3 className={'fontFamily'}>Game Decks </h3>
                                            {
                                                rowData?.isNumberOfDeckCreated ?
                                                    <p className={'success_class fontFamily'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>
                                                    :
                                                    <p className={'unSuccess_class fontFamily'}><HighlightOffIcon /> <span>Incomplete</span></p>
                                            }
                                        </div>
                                        <p className={'note_description fontFamily'}>Create the basic configurations of your game here</p>
                                    </div>
                                    <div className={'box_arrow'} >
                                        <ArrowForwardIosIcon />
                                    </div>
                                </div>
                            }
                            {
                                gameDetails?.isNoOfPlayer &&
                                <div className={ (gameDetails?.isNoOfPlayer &&  gameDetails?.isMultipleDeck) ? 'optimize_set_box margin_Left' : 'optimize_set_box'} onClick={() => setValue(5)}>
                                    <div className={'box_information'}>
                                        <div className={'box_title'}>
                                            <h3 className={'fontFamily'}>Game Number Of Players </h3>
                                            {
                                                rowData?.isNumberOfPlayerCreated ?
                                                    <p className={'success_class fontFamily'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>
                                                    :
                                                    <p className={'unSuccess_class fontFamily'}><HighlightOffIcon /> <span>Incomplete</span></p>
                                            }
                                        </div>
                                        <p className={'note_description fontFamily'}>Create the basic configurations of your game here</p>
                                    </div>
                                    <div className={'box_arrow'} >
                                        <ArrowForwardIosIcon />
                                    </div>
                                </div>
                            }

                            {/*<div className={'optimize_set_box'} onClick={() => setSteps({*/}
                            {/*    ...steps,*/}
                            {/*    setupModal: true,*/}
                            {/*    basicStep: { ...steps.basicStep, sdkSetup: true }*/}
                            {/*})}>*/}
                            {/*    <div className={'box_information'}>*/}
                            {/*        <div className={'box_title'}>*/}
                            {/*            <h3>Verify SDK Setup </h3>*/}
                            {/*            {*/}
                            {/*                rowData?.gameVerifySdkStatus ?*/}
                            {/*                    <p className={'success_class'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>*/}
                            {/*                    :*/}
                            {/*                    <p><HighlightOffIcon /> <span>Incomplete</span></p>*/}
                            {/*            }*/}
                            {/*        </div>*/}
                            {/*        <p className={'note_description'}>Download SDK, test integration and confirm Fairness</p>*/}
                            {/*    </div>*/}
                            {/*    <div className={'box_arrow'} >*/}
                            {/*        <ArrowForwardIosIcon />*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
            </Paper>
        </Box>
    )
}
export default BasicSteps