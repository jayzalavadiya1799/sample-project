import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../../../../../images/Loader";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import EditGameInformation from "./EditGameInformation";
import { useSelector } from "react-redux";
import GameInfoNew from "./GameInfoNew";


const GameInfo = ({ setSteps, steps, handleOpenModal, getOptimizeStatusHandler }) => {
    const [loader, setLoader] = useState(false);
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);
    const [editUser, setEditUser] = useState(false);

    const handleBack = () => {
        setSteps({
            ...steps,
            setupModal: false,
            basicStep: { ...steps.basicStep, gameInfo: false }

        })
        getOptimizeStatusHandler()
    }
    const editUserDetailsHandler = () => {
        setEditUser(true);
    };


    return (
        <React.Fragment>
            <Box className={'game_information_box'}>
                {
                    loader &&
                    <Loader />
                }
                <div className={'back_arrow'} onClick={() => handleBack()}>
                    <ArrowBackIosIcon />
                    <span className={'fontFamily'}>Back</span>
                </div>
                <Box className="outerbox bg_white box_shadow radius_8">
                    {
                        !editUser ?
                            <GameInfoNew gameProfile={gameDetails} editUserDetailsHandler={editUserDetailsHandler} handleOpenModal={handleOpenModal} />
                            :
                            <div className={'edit_game_info_section'}>
                                <EditGameInformation setEditGameInfo={setEditUser} handleOpenModal={handleOpenModal} />
                            </div>
                    }
                </Box>
            </Box>
        </React.Fragment>
    )
}
export default GameInfo