import React from "react";
import user from "../../../../../../../../assets/images/avatar.png";
import { dotGenerator, hideActionFunc } from "../../../../../../../../utils";

const GameInfoNew = ({ gameProfile, editUserDetailsHandler, handleOpenModal }) => {

    return (
        <div className={'user_details_inner_section game_details_info_section'}>
            <div className={'user_details_inner_profile'}>
                <div className={'profile'}>
                    <img src={gameProfile?.gameIcon ? gameProfile?.gameIcon : user} alt={'profile'} />
                    <h2 className={'fontFamily'}>{gameProfile?.gameName}</h2>
                </div>
                {
                    hideActionFunc('game') &&
                    <div className={'last_section'}>
                        <button onClick={() => editUserDetailsHandler()}>Edit</button>
                    </div>
                }

            </div>
            <div className={'user_details_section'}>
                <div className={'user_inner_div'}>
                    <div className={'let_section'}>
                        <div className={'personal_information_content'}>
                            <div className={'form_data_row'}>
                                <div className={'form_data'}>
                                    <h6 className={'fontFamily'}>Game Title</h6> :
                                    <p> {gameProfile?.gameName}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Description</h6> :
                                    <p>{dotGenerator(gameProfile?.description, handleOpenModal, 'Game Description')}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Game Platform</h6> :
                                    <p>{gameProfile?.platform}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={'let_section middle_section'}>
                        <div className={'personal_information_content'}>
                            <div className={'form_data_row'}>
                                <div className={'form_data'}>
                                    <h6>Game Orientation</h6> :
                                    <p> {gameProfile?.isOrientationPortrait ? 'Portrait' : 'landscape'}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Win Metric</h6> :
                                    <p> {gameProfile?.isHighestScoreWin ? 'Highest Score' : 'Low Score'}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Entry Fee Types</h6> :
                                    <p> {gameProfile?.entryFeesType}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={'right_section'}>
                        <div className={'personal_information_content'}>
                            <div className={'form_data_row'}>
                                <div className={'form_data'}>
                                    <h6>Game iOS App Id</h6> :
                                    <p>{gameProfile?.iosAppId}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>iOS Bundle ID</h6> :
                                    <p>{gameProfile?.iosBundleId}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>iOS Store URL</h6> :
                                    <p>{gameProfile?.iosStoreUrl}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Android Package Name</h6> :
                                    <p>{gameProfile?.androidPackageName}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Unity Id</h6> :
                                    <p>{gameProfile?.unityId}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Unity Package Name</h6> :
                                    <p>{gameProfile?.unityPackageName}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Game Server Link</h6> :
                                    <p>{gameProfile?.gameServerLink}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {/*<div className={'last_section'}>*/}
                {/*    <button onClick={() => editUserDetailsHandler()}>Edit</button>*/}
                {/*</div>*/}
            </div>
        </div>
    )
}
export default GameInfoNew