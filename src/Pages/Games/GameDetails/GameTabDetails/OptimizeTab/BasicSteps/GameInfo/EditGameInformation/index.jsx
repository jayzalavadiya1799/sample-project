import React, { useCallback, useEffect, useRef, useState } from "react";
import FilledButton from "../../../../../../../../Components/FileButton";
import user from "../../../../../../../../assets/images/avatar.png";
import CommonDropdown from "../CommonDropdown";
import { useDispatch, useSelector } from "react-redux";
import { getSingleGameDetails, updateGameInfo } from "../../../../../../../../Redux/games/action";
import { jsonToFormData, profileImages } from "../../../../../../../../utils";
import SimpleReactValidator from "simple-react-validator";
import icon_plus from "../../../../../../../../assets/images/plus.svg";

const EditGameInformation = ({ setEditGameInfo, handleOpenModal }) => {
    const dispatch = useDispatch();
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);
    const [loader, setLoader] = useState(false);
    const [formData, setFormData] = useState({
        gameName: '',
        description: '',
        platform: '',
        iosAppId: '',
        iosBundleId: '',
        iosStoreUrl: '',
        entryFeesType: '',
        orientation: '',
        isOrientationPortrait: false,
        isIconUpdated: false,
        gameIcon: '',
        androidPackageName: '',
        unityId: '',
        unityPackageName: '',
        isHighestScoreWin: '',
        gameServerLink: ''
    })
    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                isOrientationPortrait: formData?.orientation === 'Portrait',
                gameId: gameDetails?._id,
                publisherId: gameDetails?.publisherId?._id,
                isHighestScoreWin: formData?.isHighestScoreWin === 'Highest Score'
            }
            if (typeof formData?.gameIcon === "string") {
                delete payload.gameIcon;
            }
            delete payload?.orientation;
            setLoader(true);
            dispatch(updateGameInfo(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false);
                    setEditGameInfo(false)
                    dispatch(getSingleGameDetails({ gameId: gameDetails?._id }))
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message });

                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }
    useEffect(() => {
        if (gameDetails) {
            setFormData({
                ...formData,
                gameName: gameDetails?.gameName,
                description: gameDetails?.description,
                platform: gameDetails?.platform,
                orientation: gameDetails?.isOrientationPortrait ? 'Portrait' : 'Landscape',
                gameIcon: gameDetails?.gameIcon,
                iosAppId: gameDetails?.iosAppId,
                iosBundleId: gameDetails?.iosBundleId,
                iosStoreUrl: gameDetails?.iosStoreUrl,
                entryFeesType: gameDetails?.entryFeesType,
                androidPackageName: gameDetails?.androidPackageName,
                unityId: gameDetails?.unityId,
                unityPackageName: gameDetails?.unityPackageName,
                gameServerLink: gameDetails?.gameServerLink,
                isHighestScoreWin: gameDetails?.isHighestScoreWin ? 'Highest Score' : 'Low Score'
            })
        }
    }, [gameDetails]);

    const handleChangeProfile = (e) => {
        setFormData({
            ...formData,
            gameIcon: e.target.files[0],
            isIconUpdated: true
        })
    }
    return (
        <div>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_content'}>
                    <form method={'POST'} className={'form_game_content_details'} onSubmit={(e) => handleSubmit(e)}>
                        <div className={'game_info_edit_form'}>
                            <div>
                                <div className="formData">
                                    <label className={'fontFamily'}>Game Title</label>
                                    <div className="emailWrap">
                                        <input type="text" name='gameName' value={formData?.gameName} onChange={(e) => handleChange(e)} />
                                    </div>
                                    {simpleValidator.current.message("gameName", formData?.gameName, 'required')}
                                </div>
                                <div className="formData">
                                    <label className={'fontFamily'}>App Description</label>
                                    <div className="emailWrap">
                                        <textarea rows={2} name={'description'} value={formData?.description} onChange={(e) => handleChange(e)} />
                                    </div>
                                </div>

                                <div className="formData">
                                    <label className={'fontFamily'}>Game Platform</label>
                                    <CommonDropdown option={['Ios', 'Android', 'Cross-Platform']} name={'platform'} setFormData={setFormData} formData={formData} />
                                    {simpleValidator.current.message("platform", formData?.platform, 'required')}
                                </div>
                                <div className={'d_flex_filed'}>
                                    <div className="formData field_left">
                                        <label className={'fontFamily'}>Game iOS App ID</label>
                                        <div className="emailWrap">
                                            <input type="text" name='iosAppId' value={formData?.iosAppId} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                    <div className="formData field_right">
                                        <label className={'fontFamily'}>IOS Bundle ID</label>
                                        <div className="emailWrap">
                                            <input type="text" name='iosBundleId' value={formData.iosBundleId} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                </div>
                                <div className={'d_flex_filed'}>
                                    <div className="formData field_left">
                                        <label className={'fontFamily'}>IOS Store URL</label>
                                        <div className="emailWrap">
                                            <input type="text" name='iosStoreUrl' value={formData?.iosStoreUrl} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                    <div className="formData field_right">
                                        <label className={'fontFamily'}>Android Package Name</label>
                                        <div className="emailWrap">
                                            <input type="text" name='androidPackageName' value={formData?.androidPackageName} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                </div>
                                <div className={'d_flex_filed'}>
                                    <div className="formData field_left">
                                        <label className={'fontFamily'}>Entry Fee Types</label>
                                        <CommonDropdown option={['Cash']} name={'entryFeesType'} setFormData={setFormData} formData={formData} />
                                        {simpleValidator.current.message("entryFeesType", formData?.entryFeesType, 'required')}
                                    </div>
                                    <div className="formData field_right">
                                        <label className={'fontFamily'}>Game Orientation</label>
                                        <div className="emailWrap">
                                            <CommonDropdown option={['Portrait', 'Landscape']} name={'orientation'} setFormData={setFormData} formData={formData} />
                                        </div>
                                    </div>
                                </div>
                                <div className={'d_flex_filed'}>
                                    <div className="formData field_left">
                                        <label className={'fontFamily'}>Unity Package Name</label>
                                        <div className="emailWrap">
                                            <input type="text" name='unityPackageName' value={formData?.unityPackageName} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                    <div className="formData field_right">
                                        <label className={'fontFamily'}>Unity Id</label>
                                        <div className="emailWrap">
                                            <input type="text" name='unityId' value={formData?.unityId} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                </div>
                                <div className={'d_flex_filed'}>
                                    <div className="formData field_left">
                                        <label className={'fontFamily'}>Game Server Link </label>
                                        <div className="emailWrap">
                                            <input type="text" name='gameServerLink' value={formData?.gameServerLink} onChange={(e) => handleChange(e)} />
                                        </div>
                                        {simpleValidator.current.message("gameServerLink", formData?.gameServerLink, 'required')}
                                    </div>
                                </div>
                                <div className="formData">
                                    <label className={'fontFamily'}>Win Metric</label>
                                    <div className="emailWrap">
                                        <CommonDropdown option={['Highest Score', 'Low Score']} name={'isHighestScoreWin'} value={formData?.isHighestScoreWin} setFormData={setFormData} formData={formData} />
                                    </div>
                                </div>
                            </div>
                            <div className='form_group profile new_game_section game_edit_info'>
                                <div className='user_profile'>
                                    <div className='user_profile_pic'>
                                        {profileImages(formData?.gameIcon, user)}
                                        <span className='addnew'>
                                            <img src={icon_plus} alt='' />
                                            <input type='file' name='member_photo' id='' onChange={(e) => handleChangeProfile(e)} />
                                        </span>
                                    </div>
                                    <label htmlFor='' className='profile_label fontFamily' >Game Logo</label>
                                </div>
                                {simpleValidator.current.message("gameIcon", formData?.gameIcon, 'required')}
                            </div>
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => setEditGameInfo(false)}>Cancel</button>
                            <FilledButton type={'submit'} value={'Update'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default EditGameInformation