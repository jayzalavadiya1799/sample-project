import React, { useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../../../../images/Loader";
import Paper from "@mui/material/Paper";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import successIcon from "../../../../../../assets/images/right-sucss-icon.svg";

const CustomizeGameplay = ({ setValue, rowData }) => {
    const [loader, setLoader] = useState(false)
    return (
        <Box>
            {
                loader &&
                <Loader />
            }
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'configuration-customization'}>
                    <div className={'game_tab_overView head_to_head_gameTab game-play'}>
                        <div className={'game_tab_overView_title'}>
                            <h2 className={'fontFamily'}>Customize Gameplay</h2>
                        </div>
                        <div className={'basic_step_info'}>
                            <div className={'basic_step_info'} >
                                <div className={'optimize_set_content'}>
                                    <div className={'optimize_set_box'} onClick={() => setValue(1)}>
                                        <div className={'box_information'}>
                                            <div className={'box_title'}>
                                                <h3 className={'fontFamily'}>Lobbies</h3>
                                                {
                                                    rowData?.isHeadToHeadCreated ?
                                                        <p className={'success_class fontFamily'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>
                                                        :
                                                        <p className={'fontFamily unSuccess_class'}><HighlightOffIcon /> <span>Incomplete</span></p>
                                                }
                                            </div>
                                            <p className={'note_description fontFamily'}>Create the Customize Gameplay configurations of your game here</p>
                                        </div>
                                        <div className={'box_arrow'}>
                                            <ArrowForwardIosIcon />
                                        </div>
                                    </div>
                                    {/*<div className={'optimize_set_box optimize_disable_box'} >*/}
                                    {/*    <div className={'box_information'}>*/}
                                    {/*        <div className={'box_title'}>*/}
                                    {/*            <h3>Tournament </h3>*/}
                                    {/*            <p><HighlightOffIcon /> <span>Incomplete</span></p>*/}
                                    {/*        </div>*/}
                                    {/*        <p className={'note_description'}>Create the basic configurations of your game here</p>*/}
                                    {/*    </div>*/}
                                    {/*    <div className={'box_arrow'}>*/}
                                    {/*        <ArrowForwardIosIcon />*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={'game_tab_overView head_to_head_gameTab config'}>
                        <div className={'game_tab_overView_title'}>
                            <h2 className={'fontFamily'}>Customize Config</h2>
                        </div>
                        <div className={'basic_step_info'}>
                            <div className={'basic_step_info'} >
                                <div className={'optimize_set_content'}>
                                    <div className={'optimize_set_box'} onClick={() => setValue(9)}>
                                        <div className={'box_information'}>
                                            <div className={'box_title'}>
                                                <h3 className={'fontFamily'}>Config</h3>
                                                {
                                                    rowData?.isGameConfigCreated ?
                                                        <p className={'success_class fontFamily'}> <img src={successIcon} alt={'successIcon'} /> <span>Complete</span></p>
                                                        :
                                                        <p className={'fontFamily unSuccess_class'}><HighlightOffIcon /> <span>Incomplete</span></p>
                                                }
                                            </div>
                                            <p className={'note_description fontFamily'}>Create the Customize configurations of your game here</p>
                                        </div>
                                        <div className={'box_arrow'}>
                                            <ArrowForwardIosIcon />
                                        </div>
                                    </div>
                                    {/*<div className={'optimize_set_box optimize_disable_box'} >*/}
                                    {/*    <div className={'box_information'}>*/}
                                    {/*        <div className={'box_title'}>*/}
                                    {/*            <h3>Tournament </h3>*/}
                                    {/*            <p><HighlightOffIcon /> <span>Incomplete</span></p>*/}
                                    {/*        </div>*/}
                                    {/*        <p className={'note_description'}>Create the basic configurations of your game here</p>*/}
                                    {/*    </div>*/}
                                    {/*    <div className={'box_arrow'}>*/}
                                    {/*        <ArrowForwardIosIcon />*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Paper>
        </Box>
    )
}
export default CustomizeGameplay