import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../../../images/Loader";
import Paper from "@mui/material/Paper";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { gameOverViewDetails } from "../../../../../Redux/games/action";

const GameOverViewTab = () => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);
    const [rowData, setRowData] = useState({})
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        if (gameDetails?.publisherId?._id) {
            setLoader(true)
            dispatch(gameOverViewDetails({ gameId: id, publisherId: gameDetails?.publisherId?._id })).then(res => {
                setRowData(res.data.data)
                setLoader(false)
            })
        }
    }, [gameDetails]);

    return (
        <React.Fragment>
            <Box>
                {loader ? <Loader /> : ""}
                <Paper sx={{ mb: 2 }} className="outerbox">
                    <div className={'game_tab_overView'}>
                        <div className={'game_tab_overView_title'}>
                            <h2 className={'fontFamily'}>Analytics Overview</h2>
                            <p className={'fontFamily'}>To activate your analytics for this game, please submit your game build to the app stores</p>
                        </div>
                        <div className={'game_tab_overView_content'}>
                            <div className={'game_tab_details'}>
                                <h3>Daily Active Users</h3>
                                <p>{rowData?.DailyActiveUsers || 0}</p>
                            </div>
                            <div className={'game_tab_details'}>
                                <h3>Installs</h3>
                                <p>{rowData?.installCount || 0}</p>
                            </div>
                            <div className={'game_tab_details'}>
                                <h3>D1 Retention</h3>
                                <p>{rowData?.D1Retention || 0}</p>
                            </div>
                            <div className={'game_tab_details'}>
                                <h3>Daily Monthly User</h3>
                                <p>{rowData?.MonthlyActiveUsers || 0}</p>
                            </div>
                            <div className={'game_tab_details'}>
                                <h3>Revenue (Total Earnings)</h3>
                                <p>0</p>
                            </div>
                        </div>
                    </div>
                </Paper>
            </Box>
        </React.Fragment>
    )
}
export default GameOverViewTab