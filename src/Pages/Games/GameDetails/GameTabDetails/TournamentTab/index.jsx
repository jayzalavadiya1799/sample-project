import React, { useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../../../images/Loader";
import RecurringTournament from "./RecurringTournament";
import SingleTournament from "./SingleTournament";

const TournamentTab = ({ handleOpenModal }) => {
    const [loader, setLoader] = useState(false)

    return (
        <React.Fragment>
            <Box>
                {loader ? <Loader /> : ""}
                <RecurringTournament handleOpenModal={handleOpenModal} />
                <SingleTournament handleOpenModal={handleOpenModal} />
            </Box>
        </React.Fragment>
    )
}
export default TournamentTab