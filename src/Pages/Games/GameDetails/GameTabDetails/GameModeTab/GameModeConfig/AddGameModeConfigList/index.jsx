import React, {useCallback, useRef, useState} from "react";
import {Box} from "@mui/material";
import FilledButton from "../../../../../../../Components/FileButton";
import CommonModal from "../../../../../../../hoc/CommonModal";
import {useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../../../../hoc/PopContent";
import GameConfigDropdown from "./GameConfigDropdown";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const AddGameModeConfigList = ({ modalValue, handleOpenModal }) => {
    const { id } = useParams()
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [, updateState] = useState({});
    const simpleValidator = useRef(new SimpleReactValidator());
    const forceUpdate = useCallback(() => updateState({}), []);
    const [formData, setFormData] = useState({ numberOfPlayer: '' });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                gameId: id
            }
            // setLoader(true)
            // dispatch(createGameNumberOfPlayers(payload)).then(res => {
            //     if (res.data.success) {
            //         setLoader(false)
            //         modalValue.redirectApiProps();
            //         handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message });
            //     } else {
            //         setLoader(false)
            //         handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg });
            //     }
            // })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                gameId: id,
                gameModeId: modalValue?.row?._id
            }
            // setLoader(true)
            // dispatch(updateGameModeList(jsonToFormData(payload))).then(res=>{
            //     if(res.data.success){
            //         setLoader(false)
            //         modalValue.redirectApiProps();
            //         handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message });
            //     }else{
            //         setLoader(false)
            //         handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg });
            //     }
            // })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    return(
        <Box sx={style}>
            <div className={'add_admin_user_popup number-Of-decks'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{modalValue?.isEdit ? 'Edit Game Mode config' : 'Add Game Mode config'}</h2>
                </div>
                <div className={'add_game_details_sec add_admin_user_popup_content'}>
                    <form onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        <div className={'select_game_platform_value game_mode game_mode_main_section-config'}>
                            <GameConfigDropdown/>
                            {/*<div className={'select_label tab01 game_mode_left_details'}>*/}
                            {/*    <div className={'game_mode_btn'}>*/}
                            {/*        <div className={'game_mode_btn_option'}>*/}
                            {/*            <input type={'radio'} name={'isNoOfPlayer'} checked={formData?.isNoOfPlayer} onChange={(e) => setFormData({ ...formData, isNoOfPlayer: true })} />*/}
                            {/*            <label>Round Button</label>*/}
                            {/*        </div>*/}
                            {/*        <div className={'game_mode_btn_option tab_radio'}>*/}
                            {/*            <input type={'radio'} name={'isNoOfPlayer'} checked={!formData?.isNoOfPlayer} onChange={(e) => setFormData({ ...formData, isNoOfPlayer: false })} />*/}
                            {/*            <label>Mode Tab</label>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </div>
                        <div className={'formData_btn form_common_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>

            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default AddGameModeConfigList