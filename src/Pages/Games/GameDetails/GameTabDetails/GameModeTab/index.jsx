import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../../../hoc/CommonTable";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import TableCell from "@mui/material/TableCell";
import { deleteGameModeList, getGameModeList } from "../../../../../Redux/games/action";
import { ActionFunction, hideActionFunc } from "../../../../../utils";
import GameModeConfig from "./GameModeConfig";

const GameModeTab = ({ handleOpenModal }) => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const [loader, setLoader] = useState(false);
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });

    useEffect(() => {
        getGameModeListHandler()
    }, [pagination.rowsPerPage, pagination.page]);

    const getGameModeListHandler = () => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            gameId: id
        }
        dispatch(getGameModeList(payload)).then(res => {
            setLoader(false)
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                })
            }
        })
    };

    const columns = [
        {
            id: 'id',
            label: 'ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>
                    <span >{`GMID000${row?.numericId}`}</span></TableCell>
            }
        },
        {
            id: 'gameModeIcon',
            isDisbanding: true,
            label: 'Icon',
            type: 'custom',
            render: (row, i) => {
                return <TableCell className={'game_icon_img'}>
                    <img src={row?.gameModeIcon} alt={''} />
                </TableCell>
            }
        },
        {
            id: 'gameModeName',
            label: 'Title',

        },
        ActionFunction('game', {
            id: 'Action',
            disablePadding: false,
            isDisbanding: true,
            label: 'Action',
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('AddGameMode', { redirectApiProps: getGameModeListHandler, isEdit: true, row })}>Edit</span>
                    <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('DeleteCommonModal',
                        { deleteListApiHandler: deleteGameModeList({ gameModeId: row?._id, gameId: id }), title: 'Do you want to delete the game mode?' })}>Delete</span>
                </TableCell>
            }
        })
    ];

    return (
        <React.Fragment>
            <Box>
                {
                    loader &&
                    <Loader />
                }
                <GameModeConfig/>
                <Paper sx={{ mb: 2 }} className="outerbox">
                    <div className={'game_tab_overView head_to_head_gameTab'}>
                        <div className={'game_tab_overView_title game_tab_overView_title_lobby'}>
                            <h2>Game Mode</h2>
                            {
                                hideActionFunc('game') &&
                                <button className={'font-bold'} onClick={() => handleOpenModal('AddGameMode', { redirectApiProps: getGameModeListHandler, isEdit: false })}>+ Add Game Mode</button>
                            }

                        </div>
                        <div className={'head_to_head_gameTab_table'}>

                            <CustomTable
                                headCells={columns}
                                rowData={rowData?.list}
                                totalDocs={rowData?.totalDocs}
                                pagination={pagination}
                                setPagination={setPagination}
                            />
                        </div>
                    </div>
                </Paper>
            </Box>

        </React.Fragment>
    );
};
export default GameModeTab;