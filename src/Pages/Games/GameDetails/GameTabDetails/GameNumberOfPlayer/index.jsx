import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { hideActionFunc } from "../../../../../utils";
import { gameNumberOfPlayer } from "../../../../../Redux/games/action";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../../../hoc/CommonTable";
import Loader from "../../../../../images/Loader";

const GameNumberOfPlayer = ({ handleOpenModal }) => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const [loader, setLoader] = useState(false);
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });

    useEffect(() => {
        getGameNumberOfPlayerListHandler()
    }, [pagination.rowsPerPage, pagination.page]);

    const getGameNumberOfPlayerListHandler = () => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            gameId: id
        }
        dispatch(gameNumberOfPlayer(payload)).then(res => {
            setLoader(false)
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res.data.data?.docs,
                    totalDocs: res.data.data.totalDocs
                })
            }
        })
    };

    const columns = [
        {
            id: 'numberOfPlayer',
            label: 'Number Of Player',

        },
        // ActionFunction( 'game',{
        //     id: 'Action',
        //     disablePadding: false,
        //     isDisbanding:true,
        //     label: 'Action',
        //     type: 'custom',
        //     render: (row) => {
        //         return <TableCell className={'role_field_id'}>
        //             <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('AddNumberOfPlayerPopup', { redirectApiProps: getGameNumberOfPlayerListHandler, isEdit: true,row })}>Edit</span>
        //             <span className='edit_btn edit-btn-action prTab' onClick={()=> handleOpenModal('DeleteCommonModal',
        //                 {deleteListApiHandler : deleteGameModeList({gameModeId:row?._id,gameId:id}), title: 'Do you want to delete the game mode?'})}>Delete</span>
        //         </TableCell>
        //     }
        // })
    ];

    return (
        <React.Fragment>
            <Box>
                {
                    loader &&
                    <Loader />
                }
                <Paper sx={{ mb: 2 }} className="outerbox">
                    <div className={'game_tab_overView head_to_head_gameTab'}>
                        <div className={'game_tab_overView_title game_tab_overView_title_lobby'}>
                            <h2>Game Number Of Player</h2>
                            {
                                hideActionFunc('game') &&
                                <button className={'font-bold'} onClick={() => handleOpenModal('AddNumberOfPlayerPopup', { redirectApiProps: getGameNumberOfPlayerListHandler, isEdit: false })}>+ Add Number Of Players</button>
                            }

                        </div>
                        <div className={'head_to_head_gameTab_table'}>

                            <CustomTable
                                headCells={columns}
                                rowData={rowData?.list}
                                totalDocs={rowData?.totalDocs}
                                pagination={pagination}
                                setPagination={setPagination}
                            />
                        </div>
                    </div>
                </Paper>
            </Box>

        </React.Fragment>
    );
};
export default GameNumberOfPlayer