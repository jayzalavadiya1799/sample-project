import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { a11yProps } from "../../../../utils";
import TabPanel from "../../../../Components/TabPanel";
import React, { useEffect, useState } from "react";
import GameOverViewTab from "./GameOverViewTab";
import HeadToHeadTab from "./HeadToHeadTab";
import OptimizeTab from "./OptimizeTab";
import GameBuildsTab from "./GameBuildsTab";
import HowToPlayGameTab from "./HowToPlayGameTab";
import GameReleaseTab from "./GameReleaseTab";
import { useLocation } from "react-router-dom";
import GameModeTab from "./GameModeTab";
import GameNumberOfDecksTab from "./GameNumberOfDecksTab";
import GameNumberOfPlayer from "./GameNumberOfPlayer";
import GameConfigTab from "./GameConfigTab";
import { useSelector } from "react-redux";

const GameTabDetails = ({ handleOpenModal, redirectApiProps }) => {
    const { state } = useLocation();
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);
    const [steps, setSteps] = useState({
        basicStep: { gameInfo: false, sdkSetup: false },
        customizePlay: { tournament: false },
        setupModal: false
    });
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    useEffect(() => {
        if (state?.isRedirectTab) {
            setValue(1);
        }
    }, [state])

    useEffect(() => {
        if (gameDetails?.isGameModeOption && gameDetails?.isMultipleDeck && gameDetails?.isNoOfPlayer) {

        } else if (gameDetails?.isGameModeOption && gameDetails?.isMultipleDeck && !gameDetails?.isNoOfPlayer) {

        } else if (gameDetails?.isGameModeOption && !gameDetails?.isMultipleDeck && gameDetails?.isNoOfPlayer) {

        } else if (!gameDetails?.isGameModeOption && gameDetails?.isMultipleDeck && gameDetails?.isNoOfPlayer) {

        }

    }, [])

    console.log(gameDetails?.isMultipleDeck && gameDetails?.isNoOfPlayer)

    return (
        <>
            {
                // gameDetails?.isGameModeOption && gameDetails?.isMultipleDeck && gameDetails?.isNoOfPlayer ?

                gameDetails?.isGameModeOption && gameDetails?.isMultipleDeck && gameDetails?.isNoOfPlayer ?
                    <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                        {
                            (!steps.basicStep.sdkSetup) &&
                            <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                    <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                    <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                    <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                    <Tab className={'tab_title'} label="Game Mode" {...a11yProps(3)} />
                                    <Tab className={'tab_title'} label="Game Decks" {...a11yProps(4)} />
                                    <Tab className={'tab_title'} label="Game Number of Players" {...a11yProps(5)} />
                                    <Tab className={'tab_title'} label="Game builds" {...a11yProps(6)} />
                                    <Tab className={'tab_title'} label="How to Play" {...a11yProps(7)} />
                                    <Tab className={'tab_title'} label="Game Release" {...a11yProps(8)} />
                                    <Tab className={'tab_title'} label="Game Config" {...a11yProps(9)} />
                                </Tabs>
                            </Box>
                        }
                        <TabPanel value={value} index={0} >
                            <GameOverViewTab />
                        </TabPanel>
                        <TabPanel value={value} index={1} >
                            <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                        </TabPanel>
                        <TabPanel value={value} index={2} >
                            <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                        </TabPanel>
                        <TabPanel value={value} index={3} >
                            <GameModeTab handleOpenModal={handleOpenModal} />
                        </TabPanel>
                        <TabPanel value={value} index={4} >
                            <GameNumberOfDecksTab handleOpenModal={handleOpenModal} />
                        </TabPanel>
                        <TabPanel value={value} index={5} >
                            <GameNumberOfPlayer handleOpenModal={handleOpenModal} />
                        </TabPanel>
                        <TabPanel value={value} index={6} >
                            <GameBuildsTab handleOpenModal={handleOpenModal} />
                        </TabPanel>
                        <TabPanel value={value} index={7} >
                            <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                        </TabPanel>
                        <TabPanel value={value} index={8} >
                            <GameReleaseTab handleOpenModal={handleOpenModal} />
                        </TabPanel>
                        <TabPanel value={value} index={9} >
                            <GameConfigTab handleOpenModal={handleOpenModal} />
                        </TabPanel>
                    </Box>
                    : !gameDetails?.isGameModeOption && !gameDetails?.isMultipleDeck && !gameDetails?.isNoOfPlayer ?
                        <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                            {
                                (!steps.basicStep.sdkSetup) &&
                                <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                    <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                        <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                        <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                        <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                        <Tab className={'tab_title'} label="Game builds" {...a11yProps(3)} />
                                        <Tab className={'tab_title'} label="How to Play" {...a11yProps(4)} />
                                        <Tab className={'tab_title'} label="Game Release" {...a11yProps(5)} />
                                        <Tab className={'tab_title'} label="Game Config" {...a11yProps(6)} />
                                    </Tabs>
                                </Box>
                            }
                            <TabPanel value={value} index={0} >
                                <GameOverViewTab />
                            </TabPanel>
                            <TabPanel value={value} index={1} >
                                <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                            </TabPanel>
                            <TabPanel value={value} index={2} >
                                <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                            </TabPanel>
                            <TabPanel value={value} index={3} >
                                <GameBuildsTab handleOpenModal={handleOpenModal} />
                            </TabPanel>
                            <TabPanel value={value} index={4} >
                                <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                            </TabPanel>
                            <TabPanel value={value} index={5} >
                                <GameReleaseTab handleOpenModal={handleOpenModal} />
                            </TabPanel>
                            <TabPanel value={value} index={6} >
                                <GameConfigTab handleOpenModal={handleOpenModal} />
                            </TabPanel>
                        </Box>
                        : gameDetails?.isGameModeOption && gameDetails?.isMultipleDeck ?
                            <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                                {
                                    (!steps.basicStep.sdkSetup) &&
                                    <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                        <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                            <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                            <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                            <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                            <Tab className={'tab_title'} label="Game Mode" {...a11yProps(3)} />
                                            <Tab className={'tab_title'} label="Game Decks" {...a11yProps(4)} />
                                            <Tab className={'tab_title'} label="Game builds" {...a11yProps(5)} />
                                            <Tab className={'tab_title'} label="How to Play" {...a11yProps(6)} />
                                            <Tab className={'tab_title'} label="Game Release" {...a11yProps(7)} />
                                            <Tab className={'tab_title'} label="Game Config" {...a11yProps(8)} />
                                        </Tabs>
                                    </Box>
                                }
                                <TabPanel value={value} index={0} >
                                    <GameOverViewTab />
                                </TabPanel>
                                <TabPanel value={value} index={1} >
                                    <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                                </TabPanel>
                                <TabPanel value={value} index={2} >
                                    <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                                </TabPanel>
                                <TabPanel value={value} index={3} >
                                    <GameModeTab handleOpenModal={handleOpenModal} />
                                </TabPanel>
                                <TabPanel value={value} index={4} >
                                    <GameNumberOfPlayer handleOpenModal={handleOpenModal} />
                                </TabPanel>
                                <TabPanel value={value} index={5} >
                                    <GameBuildsTab handleOpenModal={handleOpenModal} />
                                </TabPanel>
                                <TabPanel value={value} index={6} >
                                    <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                                </TabPanel>
                                <TabPanel value={value} index={7} >
                                    <GameReleaseTab handleOpenModal={handleOpenModal} />
                                </TabPanel>
                                <TabPanel value={value} index={8} >
                                    <GameConfigTab handleOpenModal={handleOpenModal} />
                                </TabPanel>
                            </Box>
                            : gameDetails?.isMultipleDeck && gameDetails?.isNoOfPlayer ?
                                <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                                    {
                                        (!steps.basicStep.sdkSetup) &&
                                        <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                            <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                                <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                                <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                                <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                                <Tab className={'tab_title'} label="Game Decks" {...a11yProps(3)} />
                                                <Tab className={'tab_title'} label="Game Number of Players" {...a11yProps(4)} />
                                                <Tab className={'tab_title'} label="Game builds" {...a11yProps(5)} />
                                                <Tab className={'tab_title'} label="How to Play" {...a11yProps(6)} />
                                                <Tab className={'tab_title'} label="Game Release" {...a11yProps(7)} />
                                                <Tab className={'tab_title'} label="Game Config" {...a11yProps(8)} />
                                            </Tabs>
                                        </Box>
                                    }
                                    <TabPanel value={value} index={0} >
                                        <GameOverViewTab />
                                    </TabPanel>
                                    <TabPanel value={value} index={1} >
                                        <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                                    </TabPanel>
                                    <TabPanel value={value} index={2} >
                                        <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                                    </TabPanel>
                                    <TabPanel value={value} index={3} >
                                        <GameNumberOfDecksTab handleOpenModal={handleOpenModal} />
                                    </TabPanel>
                                    <TabPanel value={value} index={4} >
                                        <GameNumberOfPlayer handleOpenModal={handleOpenModal} />
                                    </TabPanel>
                                    <TabPanel value={value} index={5} >
                                        <GameBuildsTab handleOpenModal={handleOpenModal} />
                                    </TabPanel>
                                    <TabPanel value={value} index={6} >
                                        <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                                    </TabPanel>
                                    <TabPanel value={value} index={7} >
                                        <GameReleaseTab handleOpenModal={handleOpenModal} />
                                    </TabPanel>
                                    <TabPanel value={value} index={8} >
                                        <GameConfigTab handleOpenModal={handleOpenModal} />
                                    </TabPanel>
                                </Box>
                                : gameDetails?.isGameModeOption && gameDetails?.isNoOfPlayer ?
                                    <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                                        {
                                            (!steps.basicStep.sdkSetup) &&
                                            <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                                    <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                                    <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                                    <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                                    <Tab className={'tab_title'} label="Game Mode" {...a11yProps(3)} />
                                                    <Tab className={'tab_title'} label="Game Number of Players" {...a11yProps(4)} />
                                                    <Tab className={'tab_title'} label="Game builds" {...a11yProps(5)} />
                                                    <Tab className={'tab_title'} label="How to Play" {...a11yProps(6)} />
                                                    <Tab className={'tab_title'} label="Game Release" {...a11yProps(7)} />
                                                    <Tab className={'tab_title'} label="Game Config" {...a11yProps(8)} />
                                                </Tabs>
                                            </Box>
                                        }
                                        <TabPanel value={value} index={0} >
                                            <GameOverViewTab />
                                        </TabPanel>
                                        <TabPanel value={value} index={1} >
                                            <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                                        </TabPanel>
                                        <TabPanel value={value} index={2} >
                                            <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                                        </TabPanel>
                                        <TabPanel value={value} index={3} >
                                            <GameModeTab handleOpenModal={handleOpenModal} />
                                        </TabPanel>
                                        <TabPanel value={value} index={4} >
                                            <GameNumberOfPlayer handleOpenModal={handleOpenModal} />
                                        </TabPanel>
                                        <TabPanel value={value} index={5} >
                                            <GameBuildsTab handleOpenModal={handleOpenModal} />
                                        </TabPanel>
                                        <TabPanel value={value} index={6} >
                                            <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                                        </TabPanel>
                                        <TabPanel value={value} index={7} >
                                            <GameReleaseTab handleOpenModal={handleOpenModal} />
                                        </TabPanel>
                                        <TabPanel value={value} index={8} >
                                            <GameConfigTab handleOpenModal={handleOpenModal} />
                                        </TabPanel>
                                    </Box>
                                    : gameDetails?.isGameModeOption ?
                                        <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                                            {
                                                (!steps.basicStep.sdkSetup) &&
                                                <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                                    <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                                        <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                                        <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                                        <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                                        <Tab className={'tab_title'} label="Game Mode" {...a11yProps(3)} />
                                                        <Tab className={'tab_title'} label="Game builds" {...a11yProps(4)} />
                                                        <Tab className={'tab_title'} label="How to Play" {...a11yProps(5)} />
                                                        <Tab className={'tab_title'} label="Game Release" {...a11yProps(6)} />
                                                        <Tab className={'tab_title'} label="Game Config" {...a11yProps(7)} />
                                                    </Tabs>
                                                </Box>
                                            }
                                            <TabPanel value={value} index={0} >
                                                <GameOverViewTab />
                                            </TabPanel>
                                            <TabPanel value={value} index={1} >
                                                <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                                            </TabPanel>
                                            <TabPanel value={value} index={2} >
                                                <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                                            </TabPanel>
                                            <TabPanel value={value} index={3} >
                                                <GameModeTab handleOpenModal={handleOpenModal} />
                                            </TabPanel>
                                            <TabPanel value={value} index={4} >
                                                <GameBuildsTab handleOpenModal={handleOpenModal} />
                                            </TabPanel>
                                            <TabPanel value={value} index={5} >
                                                <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                                            </TabPanel>
                                            <TabPanel value={value} index={6} >
                                                <GameReleaseTab handleOpenModal={handleOpenModal} />
                                            </TabPanel>
                                            <TabPanel value={value} index={7} >
                                                <GameConfigTab handleOpenModal={handleOpenModal} />
                                            </TabPanel>
                                        </Box>
                                        : gameDetails?.isMultipleDeck ?
                                            <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                                                {
                                                    (!steps.basicStep.sdkSetup) &&
                                                    <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                                        <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                                            <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                                            <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                                            <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                                            <Tab className={'tab_title'} label="Game Decks" {...a11yProps(3)} />
                                                            <Tab className={'tab_title'} label="Game builds" {...a11yProps(4)} />
                                                            <Tab className={'tab_title'} label="How to Play" {...a11yProps(5)} />
                                                            <Tab className={'tab_title'} label="Game Release" {...a11yProps(6)} />
                                                            <Tab className={'tab_title'} label="Game Config" {...a11yProps(7)} />
                                                        </Tabs>
                                                    </Box>
                                                }
                                                <TabPanel value={value} index={0} >
                                                    <GameOverViewTab />
                                                </TabPanel>
                                                <TabPanel value={value} index={1} >
                                                    <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                                                </TabPanel>
                                                <TabPanel value={value} index={2} >
                                                    <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                                                </TabPanel>
                                                <TabPanel value={value} index={3} >
                                                    <GameNumberOfDecksTab handleOpenModal={handleOpenModal} />
                                                </TabPanel>
                                                <TabPanel value={value} index={4} >
                                                    <GameBuildsTab handleOpenModal={handleOpenModal} />
                                                </TabPanel>
                                                <TabPanel value={value} index={5} >
                                                    <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                                                </TabPanel>
                                                <TabPanel value={value} index={6} >
                                                    <GameReleaseTab handleOpenModal={handleOpenModal} />
                                                </TabPanel>
                                                <TabPanel value={value} index={7} >
                                                    <GameConfigTab handleOpenModal={handleOpenModal} />
                                                </TabPanel>
                                            </Box>
                                            : gameDetails?.isNoOfPlayer ?
                                                <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                                                    {
                                                        (!steps.basicStep.sdkSetup) &&
                                                        <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >
                                                            <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                                                <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                                                <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                                                <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                                                <Tab className={'tab_title'} label="Game Number of Players" {...a11yProps(3)} />
                                                                <Tab className={'tab_title'} label="Game builds" {...a11yProps(4)} />
                                                                <Tab className={'tab_title'} label="How to Play" {...a11yProps(5)} />
                                                                <Tab className={'tab_title'} label="Game Release" {...a11yProps(6)} />
                                                                <Tab className={'tab_title'} label="Game Config" {...a11yProps(7)} />
                                                            </Tabs>
                                                        </Box>
                                                    }
                                                    <TabPanel value={value} index={0} >
                                                        <GameOverViewTab />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={1} >
                                                        <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={2} >
                                                        <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={3} >
                                                        <GameNumberOfPlayer handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={4} >
                                                        <GameBuildsTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={5} >
                                                        <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={6} >
                                                        <GameReleaseTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={7} >
                                                        <GameConfigTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                </Box>
                                                : <Box sx={{ width: '100%' }} className={'user_details_tab game_details_tab'}>
                                                    {
                                                        (!steps.basicStep.sdkSetup) &&
                                                        <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >

                                                            <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                                                                <Tab className={'tab_title'} label="Game Overview" {...a11yProps(0)} />
                                                                <Tab className={'tab_title'} label="Lobby List" {...a11yProps(1)} />
                                                                <Tab className={'tab_title'} label="Game Setup" {...a11yProps(2)} />
                                                                <Tab className={'tab_title'} label="Game Mode" {...a11yProps(3)} />
                                                                <Tab className={'tab_title'} label="Game Decks" {...a11yProps(4)} />
                                                                <Tab className={'tab_title'} label="Game Number of Players" {...a11yProps(5)} />
                                                                <Tab className={'tab_title'} label="Game builds" {...a11yProps(6)} />
                                                                <Tab className={'tab_title'} label="How to Play" {...a11yProps(7)} />
                                                                <Tab className={'tab_title'} label="Game Release" {...a11yProps(8)} />
                                                                <Tab className={'tab_title'} label="Game Config" {...a11yProps(9)} />
                                                            </Tabs>
                                                        </Box>
                                                    }
                                                    <TabPanel value={value} index={0} >
                                                        <GameOverViewTab />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={1} >
                                                        <HeadToHeadTab handleOpenModal={handleOpenModal} redirectApiProps={redirectApiProps} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={2} >
                                                        <OptimizeTab setSteps={setSteps} steps={steps} setValue={setValue} handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={3} >
                                                        <GameModeTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={4} >
                                                        <GameNumberOfDecksTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={5} >
                                                        <GameNumberOfPlayer handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={6} >
                                                        <GameBuildsTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={7} >
                                                        <HowToPlayGameTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={8} >
                                                        <GameReleaseTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={9} >
                                                        <GameConfigTab handleOpenModal={handleOpenModal} />
                                                    </TabPanel>
                                                </Box>

            }

        </>
    )
}
export default GameTabDetails