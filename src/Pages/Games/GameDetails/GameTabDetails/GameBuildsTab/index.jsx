import React, { useEffect, useState } from "react";
import Loader from "../../../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../../../hoc/CommonTable";
import CommonModal from "../../../../../hoc/CommonModal";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import PopComponent from "../../../../../hoc/PopContent";
import TableCell from "@mui/material/TableCell";
import { ActionFunction, dotGenerator, hideActionFunc } from "../../../../../utils";
import { deleteGameBuildsList, getGameBuildsList } from "../../../../../Redux/games/action";

const GameBuildsTab = () => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    const handleCopyURL = (url) => {
        navigator.clipboard.writeText(url).then(res => {
            handleOpenModal('CommonPop', { header: "Success", body: 'Code URL is Copy Successfully ' })
        });
    };

    const columns = [
        {
            id: 'releaseVersion',
            label: 'MPG </br>  Version ',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.mgpReleaseId?.releaseNumber} </TableCell>
            }
        },
        {
            id: 'releaseVersion',
            label: 'Release </br>  Version ',
            twoLineText: true,
        },
        {
            id: 'buildScript',
            label: 'Script(code)',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell ><a href={row?.buildScript} className={'edit_btn edit-btn-action'} target={'_blank'} >Download</a> </TableCell>
            }
        },
        {
            id: 'buildPlugging',
            label: 'Plugging',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell ><a href={row?.buildPlugging} className={'edit_btn edit-btn-action'} target={'_blank'} >Download</a> </TableCell>
            }
        },
        {
            id: 'buildScene',
            label: 'Scene </br>  .unity ',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell ><a href={row?.buildScene} className={'edit_btn edit-btn-action'} target={'_blank'} >Download</a> </TableCell>
            }
        },
        {
            id: 'buildAndroidAssets',
            label: 'Asset Build </br>  Android ',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell > {(gameDetails?.platform === "Android" || gameDetails?.platform === 'Cross-Platform') ? <a href={row?.buildAndroidAssets} className={'edit_btn edit-btn-action'} target={'_blank'} >Download</a> : '-'}  </TableCell>
            }
        },
        {
            id: 'buildIosAssets',
            label: 'Asset Build </br>  IOS ',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >  {(gameDetails?.platform === "Ios" || gameDetails?.platform === 'Cross-Platform') ? <a href={row?.buildIosAssets} className={'edit_btn edit-btn-action'} target={'_blank'} >Download</a> : '-'} </TableCell>
            }
        },
        {
            id: 'codeUrl',
            label: 'Code URL',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.codeUrl ? <span className='edit_btn edit-btn-action' onClick={() => handleCopyURL(row?.codeUrl)}>Copy</span> : '-'}   </TableCell>
            }
        },
        {
            id: 'releaseGuideLink',
            label: 'Release Guide </br>  Link ',
            type: 'custom',
            twoLineText: true,
            render: (row) => {
                return <TableCell > {row?.releaseGuideLink ? <span className='edit_btn edit-btn-action' onClick={() => handleOpenModal('ViewReleaseGuide', row)}>View</span> : '-'}  </TableCell>
            }
        },
        {
            id: 'releaseNotes',
            label: 'Release Note',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                let text = new DOMParser().parseFromString(row?.releaseNotes, "text/html").documentElement.textContent
                return <TableCell >{dotGenerator(text, handleOpenModal, 'Release Notes')}</TableCell>
            }
        },
        ActionFunction('game', {
            id: 'action',
            label: 'Action',
            type: 'custom',
            twoLineText: true,
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('UpdateGameBuild', row)}>Edit</span>
                    <span className='edit_btn edit-btn-action prTab'
                        onClick={() => handleOpenModal('DeleteCommonModal',
                            { deleteListApiHandler: deleteGameBuildsList({ gameId: id, gameBuildId: row?._id }), title: 'Do you want to delete this data?' })}
                    >Delete</span>
                </TableCell>
            }
        })
    ];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'ViewRejectedComment': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'AddGameBuilds': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewReleaseGuide': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'UpdateGameBuild': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DeleteCommonModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        gameBuildListDetails();
    }, []);

    const gameBuildListDetails = () => {
        let payload = {
            gameId: id,
            publisherId: gameDetails?.publisherId?._id,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            limit: pagination.rowsPerPage,
        }
        dispatch(getGameBuildsList(payload)).then(res => {
            setLoader(false)
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                });
            }
        })
    };

    return (
        <React.Fragment>
            {loader ? <Loader /> : ""}
            <div>
                <Paper sx={{ mb: 2 }} className="outerbox">
                    <div className={'admin_user_list'}>
                        {hideActionFunc('game') &&
                            <button className={'add_game_btn font-bold'} onClick={() => handleOpenModal('AddGameBuilds')}>  + Add Game Builds </button>
                        }

                    </div>
                    <CustomTable
                        headCells={columns}
                        rowData={rowData?.list}
                        totalDocs={rowData?.totalDocs}
                        pagination={pagination}
                        setPagination={setPagination}
                    />
                </Paper>
                <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                    <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={gameBuildListDetails} />
                </CommonModal>
            </div>
        </React.Fragment>
    )
}
export default GameBuildsTab