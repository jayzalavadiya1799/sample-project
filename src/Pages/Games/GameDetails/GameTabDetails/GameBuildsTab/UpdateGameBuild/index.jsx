import { Editor } from "react-draft-wysiwyg";
import FilledButton from "../../../../../../Components/FileButton";
import Box from "@mui/material/Box";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import {
    getOptimizeStatus,
    getSingleGameDetails,
    getUniqueMgpReleases,
    updateGameBuild
} from "../../../../../../Redux/games/action";
import { jsonToFormData } from "../../../../../../utils";
import { useParams } from "react-router-dom";
import htmlToDraft from 'html-to-draftjs';
import { ContentState, convertToRaw, EditorState } from "draft-js";
import draftToHtml from "draftjs-to-html";
import DropdownMode from "../../HeadToHeadTab/CreateHeadToHeadPopup/DropdownMode";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const UpdateGameBuild = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);
    const gameBuildMPGRelease = useSelector(state => state?.gameReducer?.gameBuildMPGRelease);

    const [rowData, setRowData] = useState({});
    const [formData, setFormData] = useState({
        releaseGuideLink: '',
        buildScript: '',
        buildPlugging: '',
        buildScene: '',
        buildAndroidAssets: '',
        buildIosAssets: '',
        releaseVersion: '',
        releaseNotes: '',
        codeUrl: '',
        mgpReleaseId: '',
        isBuildScriptUpdated: false,
        isBuildPluggingUpdated: false,
        isBuildSceneUpdated: false,
        isBuildAndroidAssetsUpdated: false,
        isBuildIosAssetsUpdated: false,
        mgpReleaseArray: []

    })

    useEffect(() => {
        dispatch(getSingleGameDetails({ gameId: id }))
        getOptimizeStatusHandler()
    }, [])
    const getOptimizeStatusHandler = () => {
        dispatch(getOptimizeStatus({ gameId: id, publisherId: gameDetails?.publisherId?._id })).then(res => {
            setRowData(res.data.data);
        })
    };

    useEffect(() => {
        dispatch(getUniqueMgpReleases({ gameId: id }))
    }, [])
    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                publisherId: gameDetails?.publisherId?._id,
                gameId: gameDetails?._id,
                gameBuildId: modalValue?._id
            };
            if (!payload?.isBuildScriptUpdated) { delete payload?.buildScript }
            if (!payload?.isBuildPluggingUpdated) { delete payload?.buildPlugging }
            if (!payload?.isBuildSceneUpdated) { delete payload?.buildScene }
            if (!payload?.isBuildAndroidAssetsUpdated) { delete payload?.buildAndroidAssets }
            if (!payload?.isBuildIosAssetsUpdated) { delete payload?.buildIosAssets }
            setLoader(true)

            dispatch(updateGameBuild(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    setFormData({
                        ...formData,
                        releaseGuideLink: '',
                        buildScript: '',
                        buildPlugging: '',
                        buildScene: '',
                        buildAndroidAssets: '',
                        buildIosAssets: '',
                        releaseVersion: '',
                        releaseNotes: '',
                        codeUrl: '',
                        mgpReleaseId: '',
                        isBuildScriptUpdated: false,
                        isBuildPluggingUpdated: false,
                        isBuildSceneUpdated: false,
                        isBuildAndroidAssetsUpdated: false,
                        isBuildIosAssetsUpdated: false,
                    })
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                    setLoader(false);
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const onEditorStateChange = (editorState) => {
        setEditorState(editorState);
        setFormData({
            ...formData,
            releaseNotes: draftToHtml(convertToRaw(editorState.getCurrentContent())),
        })
    };

    const handleFileChanges = (e) => {
        switch (e.target.name) {
            case 'buildScript': {
                setFormData({
                    ...formData,
                    [e.target.name]: e.target.files[0],
                    isBuildScriptUpdated: true
                })
                break;
            }
            case 'buildPlugging': {
                setFormData({
                    ...formData,
                    [e.target.name]: e.target.files[0],
                    isBuildPluggingUpdated: true
                })
                break;
            }
            case 'buildScene': {
                setFormData({
                    ...formData,
                    [e.target.name]: e.target.files[0],
                    isBuildSceneUpdated: true
                })
                break;
            }
            case 'buildAndroidAssets': {
                setFormData({
                    ...formData,
                    [e.target.name]: e.target.files[0],
                    isBuildAndroidAssetsUpdated: true
                })
                break;
            }
            default: {
                setFormData({
                    ...formData,
                    [e.target.name]: e.target.files[0],
                    isBuildIosAssetsUpdated: true
                })
            }
        }

    };

    useEffect(() => {
        if (modalValue) {
            setFormData({
                ...formData,
                releaseGuideLink: modalValue?.releaseGuideLink,
                buildScript: { name: modalValue?.buildScript?.split('/')[[modalValue?.buildScript?.split('/')?.length]?.[0] - 1] },
                buildPlugging: { name: modalValue?.buildPlugging?.split('/')[[modalValue?.buildPlugging?.split('/')?.length]?.[0] - 1] },
                buildScene: { name: modalValue?.buildScene?.split('/')[[modalValue?.buildScene?.split('/')?.length]?.[0] - 1] },
                buildAndroidAssets: { name: modalValue?.buildAndroidAssets?.split('/')[[modalValue?.buildAndroidAssets?.split('/')?.length]?.[0] - 1] },
                buildIosAssets: { name: modalValue?.buildIosAssets?.split('/')[[modalValue?.buildIosAssets?.split('/')?.length]?.[0] - 1] },
                releaseVersion: modalValue?.releaseVersion,
                releaseNotes: modalValue?.releaseNotes,
                codeUrl: modalValue?.codeUrl,
                mgpReleaseId: modalValue?.mgpReleaseId?._id,
                mgpReleaseArray: [modalValue?.mgpReleaseId]
            });
            const contentBlock = htmlToDraft(modalValue?.releaseNotes);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                setEditorState(editorState)
            }
        }
    }, [modalValue]);


    return (
        <Box sx={style} className={'user_popup_section'}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>Update Game Builds</h2>
                </div>
            </div>
            {/*<div className={'game_tab_overView head_to_head_gameTab'}>*/}
            {/*    <div className={'game_tab_overView_title'}>*/}
            {/*        <h2>Manage Game Builds</h2>*/}
            {/*    </div>*/}
            {/*</div>*/}
            <div className={'add_admin_user_popup_content'}>
                <form className={'manage_game_builds'} onSubmit={(e) => handleSubmit(e)}>
                    <div className={''}>
                        <div className={'manage_game_builds_details_field field_tab_build01'}>
                            <div className={'manage_game_builds_details_flex'}>
                                <div className={'release_Guide_section'}>
                                    <div className="formData">
                                        <label className={'main_label'}>Release Version</label>
                                        <div className="emailWrap">
                                            <input type="text" name='releaseVersion' value={formData?.releaseVersion} onChange={(e) => setFormData({ ...formData, releaseVersion: e.target.value })} />
                                        </div>
                                        {simpleValidator.current.message("releaseVersion", formData?.releaseVersion, "required")}
                                    </div>
                                </div>
                                <div className={'select_game_option_mode mb_build'}>
                                    <label className={'main_label'}>MGP Release Version</label>
                                    <div className={'select_game_option'}>
                                        <DropdownMode options={gameBuildMPGRelease?.length > 0 ? gameBuildMPGRelease : formData?.mgpReleaseArray} formData={formData} name={'mgpReleaseId'} setFormData={setFormData} isUpdateDisabled={true} />
                                    </div>
                                </div>
                            </div>

                            <div>
                                <label className={'main_label'}>Release Build</label>
                                <div className={'game_build_uploader'}>
                                    <div className={'user_kyc_section_filed_document'}>
                                        <label>Script (code)</label>
                                        <div className={'document_details_kyc'}>
                                            <div className="u_flex u_align-center">
                                                <div className="u_file-attachment add ">
                                                    <svg width="18" height="18" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" className="">
                                                        <path className="u_fill" d="M23.512 11.503l-9.708 9.523a1.682 1.682 0 00-.524 1.196 1.655 1.655 0 00.503 1.205 1.72 1.72 0 001.228.493 1.743 1.743 0 001.22-.514l9.71-9.52a5.002 5.002 0 001.508-3.572c0-1.34-.542-2.624-1.508-3.571a5.202 5.202 0 00-3.641-1.48 5.202 5.202 0 00-3.642 1.48l-9.71 9.523a8.413 8.413 0 00-1.906 2.731 8.277 8.277 0 00-.041 6.491 8.407 8.407 0 001.87 2.755 8.598 8.598 0 002.81 1.835 8.732 8.732 0 006.619-.042 8.592 8.592 0 002.785-1.87l9.71-9.52 2.427 2.38-9.71 9.523a12.033 12.033 0 01-3.898 2.554 12.221 12.221 0 01-9.197 0 12.035 12.035 0 01-3.898-2.554 11.772 11.772 0 01-2.604-3.823 11.587 11.587 0 010-9.02 11.771 11.771 0 012.604-3.822l9.712-9.521A8.673 8.673 0 0122.268 2c2.25.02 4.403.905 5.994 2.465a8.336 8.336 0 012.513 5.879 8.331 8.331 0 01-2.409 5.92l-9.708 9.526a5.157 5.157 0 01-1.67 1.095 5.238 5.238 0 01-3.943 0 5.159 5.159 0 01-1.67-1.096 5.044 5.044 0 01-1.117-1.639 4.966 4.966 0 010-3.866 5.046 5.046 0 011.117-1.638l9.71-9.523 2.427 2.38z" fill="#4E525F" />
                                                    </svg>
                                                    <div className="u_file-attachment-label">
                                                        <input type="file" title="" name={'buildScript'} accept=".zip" id="upload" autoComplete="off" onChange={(e) => handleFileChanges(e)} />
                                                    </div>
                                                </div>
                                            </div>
                                            <p className={'small_font'}>{formData?.buildScript ? formData?.buildScript?.name : ''}</p>
                                            {simpleValidator.current.message("buildScript", formData?.buildScript, "required")}
                                        </div>

                                    </div>
                                    <div className={'user_kyc_section_filed_document'}>
                                        <label>Plugging</label>
                                        <div className={'document_details_kyc'}>
                                            <div className="u_flex u_align-center">
                                                <div className="u_file-attachment add ">
                                                    <svg width="18" height="18" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" className="">
                                                        <path className="u_fill" d="M23.512 11.503l-9.708 9.523a1.682 1.682 0 00-.524 1.196 1.655 1.655 0 00.503 1.205 1.72 1.72 0 001.228.493 1.743 1.743 0 001.22-.514l9.71-9.52a5.002 5.002 0 001.508-3.572c0-1.34-.542-2.624-1.508-3.571a5.202 5.202 0 00-3.641-1.48 5.202 5.202 0 00-3.642 1.48l-9.71 9.523a8.413 8.413 0 00-1.906 2.731 8.277 8.277 0 00-.041 6.491 8.407 8.407 0 001.87 2.755 8.598 8.598 0 002.81 1.835 8.732 8.732 0 006.619-.042 8.592 8.592 0 002.785-1.87l9.71-9.52 2.427 2.38-9.71 9.523a12.033 12.033 0 01-3.898 2.554 12.221 12.221 0 01-9.197 0 12.035 12.035 0 01-3.898-2.554 11.772 11.772 0 01-2.604-3.823 11.587 11.587 0 010-9.02 11.771 11.771 0 012.604-3.822l9.712-9.521A8.673 8.673 0 0122.268 2c2.25.02 4.403.905 5.994 2.465a8.336 8.336 0 012.513 5.879 8.331 8.331 0 01-2.409 5.92l-9.708 9.526a5.157 5.157 0 01-1.67 1.095 5.238 5.238 0 01-3.943 0 5.159 5.159 0 01-1.67-1.096 5.044 5.044 0 01-1.117-1.639 4.966 4.966 0 010-3.866 5.046 5.046 0 011.117-1.638l9.71-9.523 2.427 2.38z" fill="#4E525F" />
                                                    </svg>
                                                    <div className="u_file-attachment-label">
                                                        <input type="file" title="" name={'buildPlugging'} accept=".zip" id="upload" autoComplete="off" onChange={(e) => handleFileChanges(e)} />
                                                    </div>
                                                </div>
                                            </div>
                                            {simpleValidator.current.message("buildPlugging", formData?.buildPlugging, "required")}
                                            <p className={'small_font'}>{formData?.buildPlugging ? formData?.buildPlugging?.name : ''}</p>
                                        </div>

                                    </div>
                                    <div className={'user_kyc_section_filed_document'}>
                                        <label>Scene</label>
                                        <div className={'document_details_kyc'}>
                                            <div className="u_flex u_align-center">
                                                <div className="u_file-attachment add ">
                                                    <svg width="18" height="18" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" className="">
                                                        <path className="u_fill" d="M23.512 11.503l-9.708 9.523a1.682 1.682 0 00-.524 1.196 1.655 1.655 0 00.503 1.205 1.72 1.72 0 001.228.493 1.743 1.743 0 001.22-.514l9.71-9.52a5.002 5.002 0 001.508-3.572c0-1.34-.542-2.624-1.508-3.571a5.202 5.202 0 00-3.641-1.48 5.202 5.202 0 00-3.642 1.48l-9.71 9.523a8.413 8.413 0 00-1.906 2.731 8.277 8.277 0 00-.041 6.491 8.407 8.407 0 001.87 2.755 8.598 8.598 0 002.81 1.835 8.732 8.732 0 006.619-.042 8.592 8.592 0 002.785-1.87l9.71-9.52 2.427 2.38-9.71 9.523a12.033 12.033 0 01-3.898 2.554 12.221 12.221 0 01-9.197 0 12.035 12.035 0 01-3.898-2.554 11.772 11.772 0 01-2.604-3.823 11.587 11.587 0 010-9.02 11.771 11.771 0 012.604-3.822l9.712-9.521A8.673 8.673 0 0122.268 2c2.25.02 4.403.905 5.994 2.465a8.336 8.336 0 012.513 5.879 8.331 8.331 0 01-2.409 5.92l-9.708 9.526a5.157 5.157 0 01-1.67 1.095 5.238 5.238 0 01-3.943 0 5.159 5.159 0 01-1.67-1.096 5.044 5.044 0 01-1.117-1.639 4.966 4.966 0 010-3.866 5.046 5.046 0 011.117-1.638l9.71-9.523 2.427 2.38z" fill="#4E525F" />
                                                    </svg>
                                                    <div className="u_file-attachment-label">
                                                        <input type="file" title="" name={'buildScene'} accept=".unity" id="upload" autoComplete="off" onChange={(e) => handleFileChanges(e)} />
                                                    </div>
                                                </div>
                                            </div>
                                            {simpleValidator.current.message("buildScene", formData?.buildScene, "required")}
                                            <p className={'small_font'}>{formData?.buildScene ? formData?.buildScene?.name : ''}</p>
                                        </div>

                                    </div>
                                    {
                                        (gameDetails?.platform === "Android" || gameDetails?.platform === 'Cross-Platform') &&
                                        <div className={'user_kyc_section_filed_document'}>
                                            <label>Asset Build Android</label>
                                            <div className={'document_details_kyc'}>
                                                <div className="u_flex u_align-center">
                                                    <div className="u_file-attachment add ">
                                                        <svg width="18" height="18" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" className="">
                                                            <path className="u_fill" d="M23.512 11.503l-9.708 9.523a1.682 1.682 0 00-.524 1.196 1.655 1.655 0 00.503 1.205 1.72 1.72 0 001.228.493 1.743 1.743 0 001.22-.514l9.71-9.52a5.002 5.002 0 001.508-3.572c0-1.34-.542-2.624-1.508-3.571a5.202 5.202 0 00-3.641-1.48 5.202 5.202 0 00-3.642 1.48l-9.71 9.523a8.413 8.413 0 00-1.906 2.731 8.277 8.277 0 00-.041 6.491 8.407 8.407 0 001.87 2.755 8.598 8.598 0 002.81 1.835 8.732 8.732 0 006.619-.042 8.592 8.592 0 002.785-1.87l9.71-9.52 2.427 2.38-9.71 9.523a12.033 12.033 0 01-3.898 2.554 12.221 12.221 0 01-9.197 0 12.035 12.035 0 01-3.898-2.554 11.772 11.772 0 01-2.604-3.823 11.587 11.587 0 010-9.02 11.771 11.771 0 012.604-3.822l9.712-9.521A8.673 8.673 0 0122.268 2c2.25.02 4.403.905 5.994 2.465a8.336 8.336 0 012.513 5.879 8.331 8.331 0 01-2.409 5.92l-9.708 9.526a5.157 5.157 0 01-1.67 1.095 5.238 5.238 0 01-3.943 0 5.159 5.159 0 01-1.67-1.096 5.044 5.044 0 01-1.117-1.639 4.966 4.966 0 010-3.866 5.046 5.046 0 011.117-1.638l9.71-9.523 2.427 2.38z" fill="#4E525F" />
                                                        </svg>
                                                        <div className="u_file-attachment-label">
                                                            <input type="file" title="" name={'buildAndroidAssets'} accept=".zip" id="upload" autoComplete="off" onChange={(e) => handleFileChanges(e)} />
                                                        </div>
                                                    </div>
                                                </div>
                                                {(gameDetails?.platform === "Android" || gameDetails?.platform === 'Cross-Platform') ? simpleValidator.current.message("buildAndroidAssets", formData?.buildAndroidAssets, "required") : ''}
                                                <p className={'small_font'}>{formData?.buildAndroidAssets ? formData?.buildAndroidAssets?.name : ''}</p>
                                            </div>
                                        </div>
                                    }
                                    {
                                        (gameDetails?.platform === "Ios" || gameDetails?.platform === 'Cross-Platform') &&
                                        <div className={'user_kyc_section_filed_document'}>
                                            <label>Asset Build IOS</label>
                                            <div className={'document_details_kyc'}>
                                                <div className="u_flex u_align-center">
                                                    <div className="u_file-attachment add ">
                                                        <svg width="18" height="18" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" className="">
                                                            <path className="u_fill" d="M23.512 11.503l-9.708 9.523a1.682 1.682 0 00-.524 1.196 1.655 1.655 0 00.503 1.205 1.72 1.72 0 001.228.493 1.743 1.743 0 001.22-.514l9.71-9.52a5.002 5.002 0 001.508-3.572c0-1.34-.542-2.624-1.508-3.571a5.202 5.202 0 00-3.641-1.48 5.202 5.202 0 00-3.642 1.48l-9.71 9.523a8.413 8.413 0 00-1.906 2.731 8.277 8.277 0 00-.041 6.491 8.407 8.407 0 001.87 2.755 8.598 8.598 0 002.81 1.835 8.732 8.732 0 006.619-.042 8.592 8.592 0 002.785-1.87l9.71-9.52 2.427 2.38-9.71 9.523a12.033 12.033 0 01-3.898 2.554 12.221 12.221 0 01-9.197 0 12.035 12.035 0 01-3.898-2.554 11.772 11.772 0 01-2.604-3.823 11.587 11.587 0 010-9.02 11.771 11.771 0 012.604-3.822l9.712-9.521A8.673 8.673 0 0122.268 2c2.25.02 4.403.905 5.994 2.465a8.336 8.336 0 012.513 5.879 8.331 8.331 0 01-2.409 5.92l-9.708 9.526a5.157 5.157 0 01-1.67 1.095 5.238 5.238 0 01-3.943 0 5.159 5.159 0 01-1.67-1.096 5.044 5.044 0 01-1.117-1.639 4.966 4.966 0 010-3.866 5.046 5.046 0 011.117-1.638l9.71-9.523 2.427 2.38z" fill="#4E525F" />
                                                        </svg>
                                                        <div className="u_file-attachment-label">
                                                            <input type="file" title="" name={'buildIosAssets'} accept=".zip" id="upload" autoComplete="off" onChange={(e) => handleFileChanges(e)} />
                                                        </div>
                                                    </div>
                                                </div>
                                                {(gameDetails?.platform === "Ios" || gameDetails?.platform === 'Cross-Platform') ? simpleValidator.current.message("buildIosAssets", formData?.buildIosAssets, "required") : ''}
                                                <p className={'small_font'}>{formData?.buildIosAssets ? formData?.buildIosAssets?.name : ''}</p>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>

                        </div>
                        <div className={'manage_game_builds_details_field field_tab_build02'}>
                            <div className={"Add-sdk-input-sce"}>
                                <label className={'main_label'}>Release Notes</label>
                                <Editor
                                    editorState={editorState}
                                    onEditorStateChange={(editorState) => onEditorStateChange(editorState)}
                                    toolbar={
                                        {
                                            options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link'],
                                            fontFamily: {
                                                options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Inter'],
                                            }
                                        }
                                    }
                                />
                                {simpleValidator.current.message("releaseNotes", formData?.releaseNotes, "required")}
                            </div>

                            <div className={'manage_builds_link_flex'}>
                                <div className={'release_Guide_section release_guide_link'}>
                                    <div className="formData">
                                        <label className={'main_label'}>Release Guide Link</label>
                                        <div className="emailWrap">
                                            <input type="text" name='releaseGuideLink' value={formData?.releaseGuideLink} value={formData.releaseGuideLink} onChange={(e) => setFormData({ ...formData, releaseGuideLink: e.target.value })} />
                                        </div>
                                    </div>
                                </div>
                                <div className={'release_Guide_section release_code_url'}>
                                    <div className="formData">
                                        <label className={'main_label'}> Code URL</label>
                                        <div className="emailWrap">
                                            <input type="text" name='codeUrl' value={formData?.codeUrl} onChange={(e) => setFormData({ ...formData, codeUrl: e.target.value })} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={'formData_btn'}>
                        <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                        <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                    </div>
                    {/*{*/}
                    {/*    rowData?.gameInfoStatus ?*/}
                    {/*      */}
                    {/*        :*/}
                    {/*        <div className={'formData_btn'}>*/}
                    {/*            <button className={'submit_btn disabledBtn'} type={'button'} disabled={true}>Submit</button>*/}
                    {/*        </div>*/}
                    {/*}*/}

                </form>
            </div>
        </Box>
    )
}
export default UpdateGameBuild