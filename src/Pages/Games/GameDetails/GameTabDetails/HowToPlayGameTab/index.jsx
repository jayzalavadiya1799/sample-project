import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../../../hoc/CommonTable";
import TableCell from "@mui/material/TableCell";
import { useDispatch } from "react-redux";
import { deleteHowToPlay, getHowToPlay } from "../../../../../Redux/games/action";
import { useParams } from "react-router-dom";
import { ActionFunction, hideActionFunc } from "../../../../../utils";


const HowToPlayGameTab = ({ handleOpenModal }) => {

    const { id } = useParams();
    const dispatch = useDispatch();
    const [rowData, setRowData] = useState([]);
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        getGamesList()
    }, []);

    const getGamesList = () => {
        setLoader(true);
        dispatch(getHowToPlay({ gameId: id })).then(res => {
            setLoader(false);
            if (res?.data?.success) {
                setRowData({
                    ...rowData,
                    list: Object?.keys(res.data.data)?.length > 0 ? [res.data.data] : [],
                })
            } else {
                setRowData({
                    ...rowData,
                    list: [],
                })
            }
        })
    }

    const columns = [
        {
            id: 'title',
            label: 'Title'
        },
        {
            id: '',
            label: 'Description',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return row?.type === "TextEditor" ? <TableCell dangerouslySetInnerHTML={{ __html: row?.howToPlay?.value }} /> :
                    row?.type === "TextEditor" ? <TableCell>{row?.howToPlay?.value}</TableCell> : <TableCell>View</TableCell>
            }
        },
        ActionFunction('game', {
            id: 'Action',
            disablePadding: false,
            isDisbanding: true,
            label: 'Action',
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('AddHowToPlay', { redirectApiProps: getGamesList, isEdit: true, row })}>Edit</span>
                    <span className='edit_btn edit-btn-action  prTab'
                        onClick={() => handleOpenModal('DeleteHowToPlay',
                            { deleteListApiHandler: deleteHowToPlay({ howToPlayId: row?._id }), redirectApiProps: getGamesList, title: 'Do you want to delete this data?' })}>
                        Delete
                    </span>
                </TableCell>
            }
        })
    ];

    return (
        <React.Fragment>
            <Box>
                {loader && <Loader />}
                <Paper sx={{ mb: 2 }} className="outerbox">
                    <div className={'game_tab_overView head_to_head_gameTab'}>
                        <div className={'game_tab_overView_title game_tab_overView_title_lobby'}>
                            <h2>How To Play</h2>
                            {
                                (hideActionFunc('game') && !rowData?.list?.length > 0) &&
                                <button className={'font-bold'} onClick={() => handleOpenModal('AddHowToPlay', { redirectApiProps: getGamesList, isEdit: false })}>+ Add How To Play</button>
                            }
                        </div>
                    </div>
                    <div className={'head_to_head_gameTab_table'}>
                        <CustomTable
                            headCells={columns}
                            rowData={rowData?.list}
                            totalDocs={0}
                            isCurrency={true}
                        />
                    </div>
                </Paper>
            </Box>
        </React.Fragment>
    )
}
export default HowToPlayGameTab;