import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import FilledButton from "../../../../../../Components/FileButton";
import { useDispatch } from "react-redux";
import { Editor } from "react-draft-wysiwyg";
import { ContentState, convertToRaw, EditorState } from "draft-js";
import UploaderImages from "../../../../../Website/TopGameList/UploaderImages";
import SimpleReactValidator from "simple-react-validator";
import draftToHtml from "draftjs-to-html";
import { useParams } from "react-router-dom";
import { jsonToFormData } from "../../../../../../utils";
import { addHowToPlay, uploadSliderImagePlay } from "../../../../../../Redux/games/action";
import PopComponent from "../../../../../../hoc/PopContent";
import CommonModal from "../../../../../../hoc/CommonModal";
import htmlToDraft from "html-to-draftjs";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 650,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const AddHowToPlay = ({ modalValue, handleOpenModal }) => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const [deleteScreenShort, setDeleteScreenShort] = useState([])
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [loader, setLoader] = useState(false);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false })
    let Modal = PopComponent[modalDetails?.modalName];
    const [formData, setFormData] = useState({
        type: 'TextEditor',
        title: '',
        youtubeLink: '',
        description: '',
        screenShots: []
    });

    const onEditorStateChange = (editorState) => {
        setFormData({
            ...formData,
            description: draftToHtml(convertToRaw(editorState.getCurrentContent()))
        })
        setEditorState(editorState);
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            switch (formData?.type) {
                case 'TextEditor': {
                    let payload = {
                        gameId: id,
                        type: formData?.type,
                        title: formData?.title,
                        value: draftToHtml(convertToRaw(editorState.getCurrentContent()))
                    }
                    dispatch(addHowToPlay(payload)).then(res => {
                        if (res.data.success) {
                            setLoader(false);
                            modalValue.redirectApiProps();
                            handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                        } else {
                            setLoader(false);
                            handleOpenModalError('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                        }
                    })
                    break;
                }
                case 'VideoLink': {
                    let payloadVideo = {
                        gameId: id,
                        type: formData?.type,
                        title: formData?.title,
                        value: formData?.youtubeLink
                    }
                    dispatch(addHowToPlay(payloadVideo)).then(res => {
                        if (res.data.success) {
                            setLoader(false);
                            modalValue.redirectApiProps();
                            handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                        } else {
                            setLoader(false);
                            handleOpenModalError('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                        }
                    })
                    break;
                }
                default: {
                    let payloadSlider = {
                        gameId: id,
                        type: formData?.type,
                        title: formData?.title,
                    }
                    setLoader(true)
                    dispatch(addHowToPlay(payloadSlider)).then(res => {
                        if (res.data.success) {
                            debugger
                            setLoader(false);
                            formData?.screenShots?.forEach(ele => {
                                dispatch(uploadSliderImagePlay(jsonToFormData({ howToPlaySliderImage: ele, howToPlayId: res.data.data?._id })))
                            });
                            modalValue.redirectApiProps();
                            handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                        } else {
                            setLoader(false);
                            handleOpenModalError('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                        }
                    })

                    break;
                }
            }
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleOpenModalError = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };
    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            switch (formData?.type) {
                case 'TextEditor': {
                    let payload = {
                        gameId: id,
                        type: formData?.type,
                        title: formData?.title,
                        value: draftToHtml(convertToRaw(editorState.getCurrentContent()))
                    }
                    dispatch(addHowToPlay(payload)).then(res => {
                        if (res.data.success) {
                            setLoader(false);
                            modalValue.redirectApiProps();
                            handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                        } else {
                            setLoader(false);
                            handleOpenModalError('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                        }
                    })
                    break;
                }
                case 'VideoLink': {
                    let payloadVideo = {
                        gameId: id,
                        type: formData?.type,
                        title: formData?.title,
                        value: formData?.youtubeLink
                    }
                    dispatch(addHowToPlay(payloadVideo)).then(res => {
                        if (res.data.success) {
                            setLoader(false);
                            modalValue.redirectApiProps();
                            handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                        } else {
                            setLoader(false);
                            handleOpenModalError('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                        }
                    })
                    break;
                }
                default: {
                    let payloadSlider = {
                        gameId: id,
                        type: formData?.type,
                        title: formData?.title,
                    }
                    setLoader(true)
                    dispatch(addHowToPlay(payloadSlider)).then(res => {
                        if (res.data.success) {
                            debugger
                            setLoader(false);
                            formData?.screenShots?.forEach(ele => {
                                dispatch(uploadSliderImagePlay(jsonToFormData({ howToPlaySliderImage: ele, howToPlayId: res.data.data?._id })))
                            });
                            modalValue.redirectApiProps();
                            handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                        } else {
                            setLoader(false);
                            handleOpenModalError('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                        }
                    })

                    break;
                }
            }
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    useEffect(() => {
        if (modalValue?.isEdit) {
            setFormData({
                type: modalValue?.row?.type,
                title: modalValue?.row?.title
            });
            if (modalValue?.row?.type === 'TextEditor') {
                const contentBlock = htmlToDraft(modalValue?.row?.howToPlay?.value);
                if (contentBlock) {
                    const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                    const editorState = EditorState.createWithContent(contentState);
                    setEditorState(editorState)
                }
            }
            if (modalValue?.row?.type === 'VideoLink') {
                setFormData({
                    ...formData,
                    type: modalValue?.row?.type,
                    title: modalValue?.row?.title,
                    youtubeLink: modalValue?.row?.howToPlay?.value
                })
            }
            if (modalValue?.row?.type === 'ImageSlider') {
                setFormData({
                    ...formData,
                    screenShots: modalValue?.row?.howToPlay?.value,
                    type: modalValue?.row?.type,
                    title: modalValue?.row?.title
                })
            }
        }
    }, [])

    return (
        <Box sx={style} className={'how_to_play_section_details'}>
            <div className={'game_details_view add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    {
                        modalValue?.isEdit ?
                            <h2>Edit How To Play</h2>
                            :
                            <h2>Add How To Play</h2>
                    }
                </div>
            </div>
            <div className={'add_game_details_sec add_admin_user_popup_content'}>
                <form onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)} method="post"
                    encType="multipart/form-data">
                    <div className={'game_display_form'}>
                        <div className="formData formData_field">
                            <label>Title </label>
                            <div className="emailWrap">
                                <input type="text" value={formData?.title} name='title' placeholder={'Enter Title'} onChange={(e) => setFormData({ ...formData, title: e.target.value })} />
                            </div>
                            {simpleValidator.current.message("title", formData?.title, 'required')}
                        </div>
                        <div className={'select_label tab01 game_mode_left_details'}>
                            <div className={'game_mode_btn'}>
                                <div className={'game_mode_btn_option '}>
                                    <input type={'radio'} name={'isGameMode'} checked={formData?.type === 'TextEditor'} onChange={() => setFormData({ ...formData, type: 'TextEditor' })} />
                                    <label>Text Editor</label>
                                </div>
                                <div className={'game_mode_btn_option tab_radio'}>
                                    <input type={'radio'} name={'isGameMode'} checked={formData?.type === 'VideoLink'} onChange={() => setFormData({ ...formData, type: 'VideoLink' })} />
                                    <label>Video Link</label>
                                </div>
                                <div className={'game_mode_btn_option tab_radio'}>
                                    <input type={'radio'} name={'isGameMode'} checked={formData?.type === 'ImageSlider'} onChange={() => setFormData({ ...formData, type: 'ImageSlider' })} />
                                    <label>Image Slider</label>
                                </div>
                            </div>
                        </div>
                        {
                            formData?.type === 'VideoLink' &&
                            <div className="formData formData_field">
                                <label>Video Link </label>
                                <div className="emailWrap">
                                    <input type="text" name='youtubeLink' placeholder={'Enter Video Link'} value={formData?.youtubeLink} onChange={(e) => setFormData({ ...formData, youtubeLink: e.target.value })} />
                                </div>
                                {/*{simpleValidator.current.message("youtubeLink", formData?.youtubeLink, 'required')}*/}
                            </div>
                        }
                        {
                            formData?.type === 'TextEditor' ?
                                <div className={'Add-sdk-form-sec formData_field'}>
                                    <div className={'Add-sdk-input-sce'}>
                                        <label>Text Editor</label>
                                        <Editor
                                            editorState={editorState}
                                            onEditorStateChange={(editorState) => onEditorStateChange(editorState)}
                                            toolbar={
                                                {
                                                    options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link'],
                                                    fontFamily: {
                                                        options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Inter'],
                                                    }
                                                }
                                            }
                                        />
                                    </div>
                                    {/*{simpleValidator.current.message("description", formData?.description, 'required') }*/}
                                </div> : ''
                        }
                        {
                            formData?.type === 'ImageSlider' &&
                            <div className={'formData_field upload_img_section'}>
                                <label>Image Slider</label>
                                <UploaderImages
                                    setFormData={setFormData}
                                    formData={formData}
                                    modalValue={modalValue?.row}
                                    isHowToPlay={true}
                                    setDeleteScreenShort={setDeleteScreenShort}
                                    deleteScreenShort={deleteScreenShort} />
                            </div>
                        }

                        <div className={formData?.type === 'ImageSlider' ? 'formData_btn form_common_btn add_game_btn_details' : 'formData_btn form_common_btn add_game_btn_Top'}>
                            <button className={'cancel_btn'} type={'reset'}
                                onClick={() => handleOpenModal()}>Cancel
                            </button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'}
                                className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </div>

                </form>

            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModalError}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModalError} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default AddHowToPlay