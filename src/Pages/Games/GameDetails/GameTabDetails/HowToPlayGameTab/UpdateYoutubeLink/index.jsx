import { Box } from "@mui/material";
import FilledButton from "../../../../../../Components/FileButton";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getSingleGameDetails, updateYoutubeVideoLink } from "../../../../../../Redux/games/action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const UpdateYoutubeLink = ({ modalValue, handleOpenModal }) => {
    const dispatch = useDispatch()
    const [loader, setLoader] = useState(false);
    const [formData, setFormData] = useState('')
    const handleSubmit = (e) => {
        e.preventDefault();
        let payload = {
            youtubeVideoLink: formData,
            gameId: modalValue?._id,
            publisherId: modalValue?.publisherId?._id
        }
        dispatch(updateYoutubeVideoLink(payload)).then(res => {
            if (res.data.success) {
                setLoader(false)
                dispatch(getSingleGameDetails({ gameId: modalValue?._id }))
                handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
            } else {
                handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message || res?.data?.msg })
            }
        })
    }
    useEffect(() => {
        if (modalValue) {
            setFormData(modalValue?.youtubeVideoLink)
        }
    }, []);

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{'Update Game Youtube Link'}</h2>
                </div>
                <div className={'add_admin_user_popup_content'}>
                    <form method={'POST'} onSubmit={(e) => handleSubmit(e)}>
                        <div className="formData">
                            <label>Youtube Link </label>
                            <div className="emailWrap">
                                <input type="text" name='fullName' value={formData} onChange={(e) => setFormData(e.target.value)} />
                            </div>
                        </div>

                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={'Submit'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
        </Box>
    )
}
export default UpdateYoutubeLink