import React, { useState } from "react";
import { Box, Button } from "@mui/material";
import Typography from "@mui/material/Typography";
import FilledButton from "../../../../../../Components/FileButton";
import { useDispatch, useSelector } from "react-redux";
import { createHeadToHeadGame } from "../../../../../../Redux/games/action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const CopyHeadToHeadPopup = ({ modalValue, handleOpenModal }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails);

    const handleSubmit = (e) => {
        e.preventDefault();
        const { data } = modalValue;
        let payload = {
            tournamentName: data?.tournamentName,
            gameId: gameDetails?._id,
            publisherId: gameDetails?.publisherId?._id,
            description: data?.description,
            isAvailableAllCountry: data?.isAvailableAllCountry,
            countryAvailability: data?.countryAvailability,
            entryfee: data?.entryfee,
            winningPrice: data?.entryfee,
            tournamentType: data?.tournamentType,
            isRealMoney: data?.isRealMoney
        }
        setLoader(true)
        dispatch(createHeadToHeadGame(payload)).then(res => {
            if (res.data.success) {
                setLoader(false)
                modalValue.redirectApiProps()
                handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
            } else {
                setLoader(false)
                handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message || res?.data?.msg })
            }
        })
    };

    return (
        <Box sx={style} className={'user_popup_section'}>
            <div className='sd_flex sd_justcenter '>
                <Typography id="modal-modal-title" variant="h6" component="h2" className={"block-user-title"}>
                    Do you want to Copy this data?
                </Typography>
            </div>
            <div className='publisher_popup'>
                <form onSubmit={(e) => handleSubmit(e)}>
                    <div className='form_main'>
                        <Box mb={3} className={"publisher_popup_box"}>
                            <Button className={'cancel_btn'} variant="outlined" type='reset' onClick={() => handleOpenModal()}> No </Button>
                            <FilledButton type={'submit'} value={'Yes'} className={'submit_btn loader_css'} loading={loader} />
                        </Box>
                    </div>
                </form>
            </div>
        </Box>
    )
}
export default CopyHeadToHeadPopup;