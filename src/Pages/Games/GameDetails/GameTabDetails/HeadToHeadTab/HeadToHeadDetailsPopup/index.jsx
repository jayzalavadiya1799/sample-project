import { Box } from "@mui/material";
import React from "react";
import {currencyFormat, decimalGenerate} from "../../../../../../utils";
import {useSelector} from "react-redux";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const HeadToHeadDetailsPopup = ({ modalValue, handleOpenModal }) => {
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails)
    console.log(modalValue,"modalValue")
    //gameDetails?.lobbyType?.filter(item => item?._id === row?.lobbyType)?.[0]?.lobbyType
    return (
        <Box sx={style} className={'user_popup_section'}>
            <div className={'create_headToHead_modal create_headToHead_modal_view add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>Head-to-Head Details</h2>
                </div>
            </div>
            <div className={'headToHead_popup_details'}>
                <div className={'formData'}>
                    <label>Entry Fee :</label>
                    <div className={'information_value'}>
                        {currencyFormat(modalValue?.data?.entryfee)}
                    </div>
                </div>
                <div className={'formData'}>
                    <label>Lobby Type :</label>
                    <div className={'information_value'}>
                        {/*{gameDetails?.lobbyType?.filter(item => item?._id === row?.lobbyType)?.[0]?.lobbyType}*/}
                    </div>
                </div>
                <div className={'formData'}>
                    <label>Mode Of Game :</label>
                    <div className={'information_value'}>
                        {modalValue?.data?.gameModeOptions}
                    </div>
                </div>
                <div className={'formData'}>
                    <label>Use Bot :</label>
                    <div className={'information_value'}>
                        {modalValue?.data?.isUseBot ? 'YES' : 'NO'}
                    </div>
                </div>
                <div className={'formData'}>
                    <label>Game type:</label>
                    <div className={'information_value'}>
                        {modalValue?.data?.tournamentType}
                    </div>
                </div>
                <div className={'formData'}>
                    <label>Leaderboard Multiplication Score :</label>
                    <div className={'information_value'}>
                        {modalValue?.data?.leaderboardScore}
                    </div>
                </div>

            </div>
            <div className={'headToHead_popup_winning_price'}>
                <div className={'headToHead_popup_winning_price_left'}>
                    <label>Total Prize Pool</label>
                    <h2>{currencyFormat(modalValue?.data?.winningPrice)}</h2>
                </div>
                <div className={'headToHead_popup_winning_price_right'}>
                    <label>Players</label>
                    <h2>2</h2>
                </div>
            </div>
            {/*<div className={'formData_btn_headToHead'}>*/}
            {/*    <button onClick={() => handleOpenModal('CreateHeadToHeadPopup', modalValue)}>Edit</button>*/}
            {/*</div>*/}
        </Box>
    )
}
export default HeadToHeadDetailsPopup