import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import TableCell from "@mui/material/TableCell";
import { useParams } from "react-router-dom";
import Paper from "@mui/material/Paper";
import Loader from "../../../../../images/Loader";
import CustomTable from "../../../../../hoc/CommonTable";
import { getHeadToHeadsGameList } from "../../../../../Redux/games/action";
import MainCommonFilter from "../../../../../Components/MainCommonFilter";
import { ActionFunction, currencyFormat, dotGenerator, hideActionFunc } from "../../../../../utils";
import Switch from '@mui/material/Switch';


const HeadToHeadTab = ({ handleOpenModal }) => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails)
    const [loader, setLoader] = useState(false);
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
        exportFileName: 'Export File'
    });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const label = { inputProps: { 'aria-label': 'Switch demo' } };

    const columns = [
        {
            id: 'numericId',
            label: 'Lobby ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>
                    <span className='edit_btn'
                        onClick={() => handleOpenModal('HeadToHeadDetailsPopup', { redirectApiProps: getHeadToHeadList, data: row, isEdit: true })}>{`TH00000${row?.numericId}`}</span></TableCell>
            }
        },
        {
            id: 'tournamentName',
            isDisbanding: true,
            label: 'Name',
        },
        {
            id: 'gameModeOptions',
            isDisbanding: true,
            label: 'Type of Games',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{gameDetails?.gameModes?.filter(item => item?._id === row?.gameModeId)?.[0]?.gameModeName || '-'}</TableCell>
            }
        },
        {
            id: 'lobbyType',
            isDisbanding: true,
            label: 'Lobby Type',
            type: 'custom',
            render: (row) => {
                return <TableCell >{gameDetails?.lobbyType?.filter(item => item?._id === row?.lobbyType)?.[0]?.lobbyType}</TableCell>
            }
        },
        {
            id: 'isUseBot',
            label: 'Use Bot',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.isUseBot ? 'YES' : 'NO'}</TableCell>
            }
        },
        {
            id: 'leaderboardScore',
            label: 'Score </br> (for Leaderboard) ',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell className={'text_center'}>
                    {row?.leaderboardScore}</TableCell>
            }
        },
        {
            id: 'entryfee',
            label: 'Entry Fee',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell className={'text_center'}>{currencyFormat(row?.entryfee)}</TableCell>
            }
        },
        {
            id: 'winningPrice',
            label: 'Winning </br> Amount',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell className={'text_center'}>{currencyFormat(row?.winningPrice)}</TableCell>
            }
        },
        hideActionFunc('game') ?
            {
                id: '',
                label: 'Enabled  / Disabled',
                isDisbanding: true,
                type: 'custom',
                render: (row) => {
                    return <TableCell  >
                        {
                            row?.isActive ?
                                <Switch {...label} checked={row?.isActive === true} onClick={() => handleOpenModal('ActivateDeactivatePopup', { redirectApiProps: headToHeadRedirectApi, headToHeadId: row?._id, isActive: false })} />
                                :
                                <Switch {...label} checked={row?.isActive === true} onClick={() => handleOpenModal('ActivateDeactivatePopup', { redirectApiProps: headToHeadRedirectApi, headToHeadId: row?._id, isActive: true })} />
                        }

                    </TableCell>
                }
            }
            : {
                id: 'action',
                type: 'hide'
            },
        {
            id: 'description',
            isDisbanding: true,
            label: 'Description',
            twoLineText: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.description ? dotGenerator(row?.description, handleOpenModal, 'Head To Head Description') : ''}</TableCell>
            }
        },
        ActionFunction('game', {
            id: 'Action',
            disablePadding: false,
            isDisbanding: true,
            label: 'Action',
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('CreateHeadToHeadPopup', { redirectApiProps: headToHeadRedirectApi, data: row, isEdit: true })}>Edit</span>
                    <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('DeleteHeadToHeadPopup', { redirectApiProps: headToHeadRedirectApi, id: row?._id })}>Delete</span>
                    {/*<span className='edit_btn edit-btn-action u_border  prTab' onClick={() => handleOpenModal('CopyHeadToHeadPopup', { redirectApiProps: headToHeadRedirectApi, data: row })}>Copy</span>*/}
                    {/*{*/}
                    {/*    row?.isActive ?*/}
                    {/*        <span className='edit_btn edit-btn-action  prTab' onClick={()=>handleOpenModal('ActivateDeactivatePopup',{redirectApiProps: headToHeadRedirectApi, headToHeadId:row?._id,isActive:false})}>Deactivate</span>*/}
                    {/*        :*/}
                    {/*        <span className='edit_btn edit-btn-action prTab' onClick={()=>handleOpenModal('ActivateDeactivatePopup',{redirectApiProps: headToHeadRedirectApi, headToHeadId:row?._id,isActive:true})}>Activate</span>*/}
                    {/*}*/}
                </TableCell>
            }
        })
    ];

    const headToHeadRedirectApi = () => {
        getHeadToHeadList(filterData.startDate, filterData.endDate);
    };

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getHeadToHeadList(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData?.statusValue, filterData.startDate, filterData.endDate]);

    const getHeadToHeadList = (startDate, endDate, search) => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate:
                startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload,
            gameId: id
        }
        dispatch(getHeadToHeadsGameList(payload)).then(res => {
            setLoader(false)
            if (res?.data?.data?.filePath) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            } else {
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                })
            }
        })
    };

    useEffect(() => {
        if (filterData.startDate && filterData.endDate && filterData?.statusValue === 'Custom') {
            setPagination({
                ...pagination,
                page: 0
            })
            getHeadToHeadList(filterData.startDate, filterData.endDate)
        }
    }, [filterData.startDate, filterData.endDate]);

    return (
        <React.Fragment>
            <Box>
                {
                    loader &&
                    <Loader />
                }
                <Paper sx={{ mb: 2 }} className="outerbox">
                    <div className={'game_tab_overView head_to_head_gameTab'}>
                        {hideActionFunc('game') &&
                            <div className={'game_tab_overView_title d_flex_head'}>
                                <button className={'font-bold'} onClick={() => handleOpenModal('CreateHeadToHeadPopup', { redirectApiProps: getHeadToHeadList, isEdit: false })}>+ Create Lobby</button>
                            </div>
                        }

                        <div className={'head_to_head_gameTab_table'}>
                            <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={getHeadToHeadList} pagination={pagination} setPagination={setPagination} />
                            <CustomTable
                                headCells={columns}
                                rowData={rowData?.list}
                                totalDocs={rowData?.totalDocs}
                                pagination={pagination}
                                setPagination={setPagination}
                            />
                        </div>
                    </div>
                </Paper>
            </Box>
        </React.Fragment>
    )
}
export default HeadToHeadTab;