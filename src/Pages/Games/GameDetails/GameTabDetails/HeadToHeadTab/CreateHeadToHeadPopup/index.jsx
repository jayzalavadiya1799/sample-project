import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import DropdownMode from "./DropdownMode";
import Tooltip from '@mui/material/Tooltip';
import { useDispatch, useSelector } from "react-redux";
import PopComponent from "../../../../../../hoc/PopContent";
import CommonModal from "../../../../../../hoc/CommonModal";
import SimpleReactValidator from "simple-react-validator";
import FilledButton from "../../../../../../Components/FileButton";
import {
    createHeadToHeadGame, gameConfigList,
    getAllLabels,
    getSingleGameDetails,
    updateHeadToHeadGame
} from "../../../../../../Redux/games/action";
import DropdownCustom from "./DropdownCustom";
import { useParams } from "react-router-dom";
import { Select } from "react-select";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 612,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 0,
    borderRadius: "5px",
};

const CreateHeadToHeadPopup = ({ modalValue, handleOpenModal }) => {
    const { id } = useParams();
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails)
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const dispatch = useDispatch();
    const [formData, setFormData] = useState({
        tournamentName: '',
        description: '',
        entryfee: '',
        winningPrice: 0,
        isGameModeOption: true,
        gameModeId: '',
        leaderboardScore: '',
        isUseBot: false,
        lobbyType: '',
        maxPlayer: '',
        minPlayer: '',
        noOfPlayer: '',
        noOfDecks: '',
        rake:''
    });
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const geoData = useSelector(state => state?.settingReducer?.restrictedGeo);
    const [loader, setLoader] = useState(false);
    const [config, setConfigList] = useState();

    useEffect(() => {
        dispatch(getSingleGameDetails({ gameId: id }));
        dispatch(gameConfigList({ gameId: id })).then(res => {
            if (res.data.statusCode === 200) {
                setConfigList(res?.data?.data?.gameConfig);
                setFormData({
                    ...formData,
                    rake: res?.data?.data?.gameConfig?.PLATFORM_COMMISSION
                })
            }
        });
    }, []);

    useEffect(() => {
        setFormData({
            ...formData,
            isGameModeOption: gameDetails?.isGameModeOption,
        })
    }, [modalValue]);

    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                gameId: gameDetails?._id,
                publisherId: gameDetails?.publisherId?._id,
                headToHeadId: modalValue?.data?._id,
                maxPlayer: Number(formData?.maxPlayer),
                minPlayer: Number(formData?.minPlayer),
                entryfee: Number(formData?.entryfee),
                lobbyType: formData?.lobbyType
            }
            if (!payload?.isGameModeOption) { delete payload?.gameModeOptions }
            if (payload?.countryAvailability?.length === geoData?.country?.length) { payload = { ...payload, isAvailableAllCountry: true, countryAvailability: [] } }
            dispatch(updateHeadToHeadGame(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    modalValue.redirectApiProps();
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message });
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg });
                }
            });
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                gameId: gameDetails?._id,
                publisherId: gameDetails?.publisherId?._id,
                entryfee: Number(formData?.entryfee),
                lobbyType: formData?.lobbyType
            }
            Object?.keys(payload).forEach(ele => {
                if ((payload[ele] === undefined || payload[ele] === '')) { delete payload[ele] }
            })
            if (!payload?.isGameModeOption) { delete payload?.gameModeOptions }
            setLoader(true)
            dispatch(createHeadToHeadGame(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    modalValue.redirectApiProps();
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message });
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg });
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const handleChangeCheckbox = (e, type) => {
        setFormData({
            ...formData,
            [e.target.name]: type,
        });
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === 'entryfee') {
            let fees = ((value * 2) - ((value * 2) * 0.1));
            setFormData({
                ...formData,
                entryfee: value,
                winningPrice: fees
            });
        } else {
            setFormData({
                ...formData,
                [name]: value
            })
        }
    };

    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        if (modalValue?.isEdit) {
            const { data } = modalValue;
            setFormData({
                ...formData,
                tournamentName: data?.tournamentName,
                description: data?.description,
                isAvailableAllCountry: data?.isAvailableAllCountry,
                entryfee: data?.entryfee,
                winningPrice: data?.winningPrice,
                isGameModeOption: data?.isGameModeOption,
                gameModeId: data?.gameModeId,
                leaderboardScore: data?.leaderboardScore,
                maxPlayer: data?.maxPlayer,
                minPlayer: data?.minPlayer,
                noOfPlayer: data?.noOfPlayer,
                noOfDecks: data?.noOfDecks,
                lobbyType: data?.lobbyType
            });
        }
    }, [modalValue]);

    return (
        <Box sx={style}>
            <div className={'create_headToHead_modal  add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{modalValue?.isEdit ? `Update Lobby` : `Create Lobby`}</h2>
                </div>
                <div className={'add_admin_user_popup_content'}>
                    <form method={'POST'} onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        <div className="formData">
                            <label> Name </label>
                            <div className="emailWrap">
                                <input type="text" value={formData?.tournamentName} className={'wrap_input_modal'} maxLength={20} name='tournamentName' placeholder={'Enter name'} onChange={(e) => handleChange(e)} />
                                <span>{formData?.tournamentName?.length}/20</span>
                            </div>
                            {simpleValidator.current.message("Name", formData?.tournamentName, 'required')}
                        </div>
                        <div className="formData">
                            <label>Description (optional) </label>
                            <div className="text_Wrap emailWrap">
                                <input type={'text'} className={'wrap_input_modal'} maxLength={50} value={formData.description} name='description' placeholder={'Enter description'} onChange={(e) => handleChange(e)} />
                                <span>{formData?.description?.length}/50</span>
                            </div>
                        </div>
                        <div className={'formData checkbox_modal real_money_field'}>
                            <div>
                                <label>Lobby Type</label>
                            </div>
                            <div className={'select_game_option_mode'}>
                                <div className={'select_game_option'}>
                                    <DropdownCustom options={gameDetails?.lobbyType} formData={formData} setFormData={setFormData} />
                                    {simpleValidator.current.message("lobbyType", formData?.lobbyType, 'required')}
                                </div>
                            </div>
                        </div>
                        {
                            formData?.lobbyType === 'CONTEST (One vs Many)' &&
                            <div className={'formData tournament_setting_content'}>
                                <div className={'tournament_setting_amount'}>
                                    <div className={"formData form_amount_tab01"}>
                                        <label>Minimum Number of Player</label>
                                        <div className={"emailWrap"}>
                                            <input onWheel={event => event.currentTarget.blur()} value={formData?.minPlayer >= 2 ? formData?.minPlayer : ''} type={'number'} name={'minPlayer'} onChange={(e) => handleChange(e)} />
                                        </div>
                                        {
                                            (formData?.minPlayer <= 1 && formData?.minPlayer) && <span className={'srv-validation-message'}> Please enter a minimum equal to or greater than 2 Players.</span>
                                        }
                                        {simpleValidator.current.message("MinNumberOfPlayer", formData?.minPlayer, 'required')}
                                    </div>
                                    <div className="formData form_amount_tab03 ">
                                        <label>Maximum Number of Player</label>
                                        <div className="emailWrap">
                                            <input type={'number'} value={formData?.maxPlayer >= 2 ? formData?.maxPlayer : ''} name='maxPlayer' onChange={(e) => handleChange(e)} />
                                        </div>
                                        {
                                            (formData?.maxPlayer <= 1 && formData?.maxPlayer) && <span className={'srv-validation-message'}> Please enter a maximum equal to or greater than 2 Players.</span>
                                        }
                                        {simpleValidator.current.message("MaxNumberOfPlayer", formData?.maxPlayer, 'required')}
                                    </div>

                                </div>
                            </div>
                        }

                        {
                            gameDetails?.isGameModeOption &&
                            <div className={'formData checkbox_modal real_money_field'}>
                                <div>
                                    <label>Mode Of Game  </label>
                                </div>
                                <div className={'select_game_option_mode'}>
                                    <div className={'select_game_option'}>
                                        <DropdownMode options={gameDetails?.gameModes} isGameMode={true} name={'gameModeId'} formData={formData} setFormData={setFormData} />
                                        {simpleValidator.current.message("gameMode", formData?.gameModeId, 'required')}
                                    </div>
                                </div>
                            </div>
                        }

                        {
                            gameDetails?.isNoOfPlayer &&
                            <div className={'formData checkbox_modal real_money_field'}>
                                <div>
                                    <label>Number Of Players  </label>
                                </div>
                                {
                                    gameDetails?.isNoOfPlayer &&
                                    <div className={'select_game_option_mode'}>
                                        <div className={'select_game_option'}>
                                            <DropdownMode options={gameDetails?.numberOfPlayer} isNumberOfPlayer={true} name={'noOfPlayer'} formData={formData} setFormData={setFormData} />
                                            {simpleValidator.current.message("numberOfPlayer", formData?.noOfPlayer, 'required')}
                                        </div>
                                    </div>
                                }

                            </div>
                        }
                        {gameDetails?.isMultipleDeck &&
                            <div className={'formData checkbox_modal real_money_field'}>
                                <div><label>Number Of Decks  </label></div>
                                {gameDetails?.isMultipleDeck &&
                                    <div className={'select_game_option_mode'}>
                                        <div className={'select_game_option'}>
                                            <DropdownMode options={gameDetails?.numberOfDeck} isNumberOfDecks={true} name={'noOfDecks'} formData={formData} setFormData={setFormData} />
                                            {simpleValidator.current.message("numberOfDeck", formData?.noOfDecks, 'required')}
                                        </div>
                                    </div>}
                            </div>}
                        <div className="formData leaderboard_field">
                            <label> Leaderboard Multiplication Score (ex. 1, 2, 3, 4, 5)</label>
                            <div className="emailWrap">
                                <input type="number" onWheel={event => event.currentTarget.blur()} className={'wrap_input_modal'} value={formData?.leaderboardScore} name='leaderboardScore' placeholder={'Enter leader board score'} onChange={(e) => setFormData({ ...formData, leaderboardScore: e.target.value })} />
                            </div>
                            {simpleValidator.current.message("leaderboardScore", formData?.leaderboardScore, 'required')}
                        </div>
                        <div className="formData leaderboard_field">
                            <label> Rake (% Platform Commission)</label>
                            <div className="emailWrap">
                                <input type="number" onWheel={event => event.currentTarget.blur()} className={'wrap_input_modal'} value={formData?.rake} name='rake' placeholder={'Enter Platform Commission(%)'} onChange={(e) => setFormData({ ...formData, rake: e.target.value })} />
                            </div>
                            {simpleValidator.current.message("rake", formData?.rake, 'required')}
                        </div>
                        <div className={'formData checkbox_modal real_money_field'}>
                            <label>Use Bot  </label>
                            <div className={'d-flex-wrap'}>
                                <div className={'form_data_field'}>
                                    <input type="radio" name='isUseBot' checked={formData?.isUseBot} className={'checkbox_field_tournament'} onChange={(e) => handleChangeCheckbox(e, true)} />
                                    <label>Yes</label>
                                </div>
                                <div className={'form_data_field tab_no'}>
                                    <input type="radio" name={'isUseBot'} checked={!formData?.isUseBot} className={'checkbox_field_tournament'} onChange={(e) => handleChangeCheckbox(e, false)} />
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div className={'formData tournament_setting_content'}>
                            <h3> Settings
                                <Tooltip title={`[Entry Fee x 2] - [${100 - config?.PLATFORM_COMMISSION}% commission]`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="#1976d2" height="100%" width="100%" preserveAspectRatio="xMidYMid meet" focusable="false">
                                        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd" /></svg>
                                </Tooltip>
                            </h3>
                            <div className={'tournament_setting_amount'}>
                                <div className={"formData form_amount_tab01"}>
                                    <label>Entry Fee</label>
                                    <div className={"emailWrap"}>
                                        <input onWheel={event => event.currentTarget.blur()} value={formData?.entryfee} placeholder={'Enter Entry Fee'} type={'number'} name={'entryfee'} onChange={(e) => handleChange(e)} />
                                    </div>
                                    {simpleValidator.current.message("entryfee", formData.entryfee, 'required')}
                                </div>
                                <div className="formData form_amount_tab02">
                                    <label>Winning Prize Amount</label>
                                    <div className="emailWrap">
                                        <input type="text" name='winningPrice' value={formData?.winningPrice} readOnly={true} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? "Update" : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default CreateHeadToHeadPopup;