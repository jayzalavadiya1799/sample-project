import React, { useState } from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import TabPanel from "../../Components/TabPanel";
import TopGameList from "./TopGameList";
import HeaderSlider from "./HeaderSlider";
import WinnerList from "./WinnerList";
import About from "./About";
import SocialLink from "./SocialLink";
import Footer from "./Footer";
import DownloadList from "./DownloadList";

const Website = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const a11yProps = (index) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    };
    return (
        <Box sx={{ width: '100%' }} className={'setting_tab_section user_details_tab'}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                    <Tab className={'tab_listing tab_title'} label=" Games" {...a11yProps(0)} />
                    <Tab className={'tab_listing tab_title'} label="Header Slider" {...a11yProps(1)} />
                    <Tab className={'tab_listing tab_title'} label="Winner" {...a11yProps(2)} />
                    <Tab className={'tab_listing tab_title'} label="About" {...a11yProps(3)} />
                    <Tab className={'tab_listing tab_title'} label="Social Media" {...a11yProps(4)} />
                    <Tab className={'tab_listing tab_title'} label="Footer" {...a11yProps(4)} />
                    <Tab className={'tab_listing tab_title'} label="Download List" {...a11yProps(5)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <TopGameList />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <HeaderSlider />
            </TabPanel>
            <TabPanel value={value} index={2}>
                <WinnerList />
            </TabPanel>
            <TabPanel value={value} index={3}>
                <About />
            </TabPanel>
            <TabPanel value={value} index={4}>
                <SocialLink />
            </TabPanel>
            <TabPanel value={value} index={5}>
                <Footer />
            </TabPanel>
            <TabPanel value={value} index={6}>
                <DownloadList />
            </TabPanel>
        </Box>
    )
}
export default Website