import React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Logo from "../../assets/images/logo86.png";

function Dashboard() {
    return (
        <Box sx={{ m: "32px 40px", borderRadius: 2 }} >
            <Paper variant="outlined">
                <Box sx={{ m: "32px 40px" }} sx={{ p: "80px 32px", textAlign: 'center' }} className={"dashboard-section"} >
                    <img src={Logo} alt={"Logo"} className={"Logo"} />
                    <Box component="h6" mt={4} >Welcome to Multi Gaming Platform</Box>
                </Box>
            </Paper>
        </Box>
    )
}
export default Dashboard;