import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../../images/Loader";
import Paper from "@mui/material/Paper";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import CustomTable from "../../../hoc/CommonTable";
import CommonModal from "../../../hoc/CommonModal";
import { useDispatch } from "react-redux";
import PopComponent from "../../../hoc/PopContent";
import moment from "moment";
import { revenueOverAll } from "../../../Redux/revenue/action";
import TableCell from "@mui/material/TableCell";
import { currencyFormat } from "../../../utils";

const OverallRevenue = () => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    let Modal = PopComponent[modalDetails.modalName];
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
    })

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    }

    const columns = [
        {
            id: 'totalRevenue',
            label: 'Total Earning Revenue',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalRevenue)}</TableCell>
            }
        },
        {
            id: 'totalDeposits',
            label: 'Total Deposits',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalDeposits)}</TableCell>
            }
        },
        {
            id: 'totalWithdrawals',
            label: 'Total Withdrawals',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalWithdrawals)}</TableCell>
            }
        },
        {
            id: 'totalBonus',
            label: 'Total Bonus',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalBonus)}</TableCell>
            }
        }
    ];

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getOverAllRevenueDetails(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.startDate, filterData.endDate]);

    const getOverAllRevenueDetails = (startDate, endDate) => {
        let payload = {
            startDate:
                startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
        };
        Object?.keys(payload).forEach(ele => {
            if (payload[ele] === '' || payload[ele] === null) { delete payload[ele] }
        });
        setLoader(true);
        dispatch(revenueOverAll(payload)).then(res => {
            setLoader(false);
            if (res?.data?.data === null) {
                setRowData({ ...rowData, list: [] });
            } else {
                setRowData({ ...rowData, list: res.data.data });
            }

        });
    };
    useEffect(() => {
        if (filterData.startDate && filterData.endDate && filterData?.statusValue === 'Custom') {
            setPagination({
                ...pagination,
                page: 0
            })
            getOverAllRevenueDetails(filterData.startDate, filterData.endDate)
        }

    }, [filterData.startDate, filterData.endDate]);

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={''} pagination={pagination} setPagination={setPagination} addPropsFilter={{ revenueGame: true }} />
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={0}
                    pagination={pagination}
                    setPagination={setPagination}
                    isWinnerTitle={true}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    );
};
export default OverallRevenue;