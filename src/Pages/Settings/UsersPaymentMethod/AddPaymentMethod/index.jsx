import React, { useCallback, useEffect, useRef, useState } from "react";
import Paper from "@mui/material/Paper";
import { MultiSelect } from "react-multi-select-component";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import {
    createUserPaymentMethod,
    getAllCountriesRestrictGeo, updateUserPaymentMethod
} from "../../../../Redux/settings/action";
import { useDispatch, useSelector } from "react-redux";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import SimpleReactValidator from "simple-react-validator";
import Tooltip from "@mui/material/Tooltip";
import FilledButton from "../../../../Components/FileButton";
import EditIcon from "@mui/icons-material/Edit";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";

const useStyles = makeStyles((theme) => ({
    margin: {
        minWidth: "100%",
        margin: '0'
    },
}));

const BootstrapInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 14,
        height: '48px',
        padding: '0px 26px 0px 12px',
        display: 'flex',
        alignItems: 'center',
        marginTop: '5px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#1976d2',
            outlineWidth: '1px',
            boxShadow: 'none',
            outline: '1px solid #1976d2'
        },
    },
    svg: {
        right: '11px'
    }
}))(InputBase);


const AddPaymentMethod = ({ handleOpenModal, redirectApiHandler, isEdit, setIsEdit }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const countryList = useSelector(state => state?.settingReducer?.restrictedGeo);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [formData, setFormData] = useState({
        countries: [],
        paymentMethod: ''
    });

    let listingDetails = [
        {
            label: 'Select Payment Method',
            value: 'Select Payment Method'
        },
        {
            label: "Bank Transfer ",
            value: "bank_account",
        },
        {
            label: "UPI",
            value: "vpa",
        },
    ];

    useEffect(() => {
        dispatch(getAllCountriesRestrictGeo({}));
    }, []);

    const getCountryData = () => {
        return countryList?.country?.map((item) => { return { value: item, label: item } }) || [];
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                countries: formData?.countries?.map(item => item?.value)
            }
            setLoader(true)
            dispatch(createUserPaymentMethod(payload)).then((res) => {
                if (res.data.success) {
                    setFormData({
                        ...formData,
                        countries: [],
                        paymentMethod: 'Select Payment Method'
                    })
                    redirectApiHandler()
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            });
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    useEffect(() => {
        if (isEdit?.isEditStatus) {
            setFormData({
                ...formData,
                countries: isEdit?.List?.countries?.map((item) => { return { value: item, label: item } }) || [],
                paymentMethod: isEdit?.List?.paymentMethod
            });
        }
    }, [isEdit]);

    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                countries: formData?.countries?.map(item => item?.value),
                userPaymentMethodId: isEdit?.List?._id
            }
            dispatch(updateUserPaymentMethod(payload)).then((res) => {
                if (res.data.success) {
                    setFormData({
                        ...formData,
                        countries: [],
                        paymentMethod: 'Select Payment Method'
                    })
                    redirectApiHandler();
                    setIsEdit({ ...isEdit, isEditStatus: false, List: [] });
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            });
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };
    const handleClose = () => {
        setIsEdit({ ...isEdit, isEditStatus: false, List: [] });
        setFormData({
            ...formData,
            countries: [],
            paymentMethod: 'Select Payment Method'
        })
    };


    return (
        <div>
            <Paper sx={{ mb: 2 }} className="outerbox">
                <form className={'restrictGeo_details user_payment_method_settings payment-method-details'} onSubmit={isEdit?.isEditStatus ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                    <div className={'formData'}>
                        <label>Payment Method</label>
                        <div className={'filter_days_details_dropDown payment_method_filter'}>
                            <FormControl className={classes.margin}>
                                <Select
                                    name={'gameType'}
                                    value={formData?.paymentMethod === '' ? 'Select Payment Method' : formData?.paymentMethod}
                                    onChange={(e) => setFormData({ ...formData, paymentMethod: e.target.value === 'Select Payment Method' ? '' : e.target.value })}
                                    input={<BootstrapInput />}
                                    className={'fontFamily'}
                                >
                                    {
                                        listingDetails?.map(item => {
                                            return <MenuItem value={item?.value} className={'fontFamily'}>{item?.label}</MenuItem>
                                        })
                                    }
                                </Select>
                            </FormControl>
                        </div>
                        {simpleValidator.current.message("paymentMethod", formData?.paymentMethod, "required")}
                    </div>
                    <div className={'formData'}>
                        <label>Countries List </label>
                        <MultiSelect
                            options={getCountryData()}
                            value={formData?.countries}
                            onChange={(value) => setFormData({ ...formData, countries: value })}
                            labelledBy="Select State"
                            name='adminUserPermission'
                            arrowRenderer={() => <ArrowDropUpIcon />}
                        />
                        {simpleValidator.current.message("countries", formData?.countries, "required")}
                    </div>
                    <div className={isEdit?.isEditStatus ? ' restrict_geo_btn edit_restrict_section' : 'restrict_geo_btn'}>
                        <label>Action </label>
                        {
                            isEdit?.isEditStatus ?
                                <div className={'edit_restrict_btn'}>
                                    <Tooltip title="Edit">
                                        <FilledButton type={'submit'} value={<EditIcon />} className={'loader_css submit_btn edit_btn_margin'} loading={loader} />
                                    </Tooltip>
                                    <button type={"button"} onClick={() => handleClose()}><CloseIcon /></button>
                                </div>
                                :
                                <div>
                                    <FilledButton type={'submit'} value={<AddIcon />} className={'loader_css'} loading={loader} />
                                </div>
                        }
                    </div>
                </form>
            </Paper>
        </div>
    )
};
export default AddPaymentMethod