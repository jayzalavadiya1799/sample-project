import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import PopComponent from "../../../hoc/PopContent";
import CommonModal from "../../../hoc/CommonModal";
import AddPaymentMethod from "./AddPaymentMethod";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import { useDispatch } from "react-redux";
import { TableCell } from "@mui/material";
import {
    deleteUserPaymentMethodList,
    getUserPaymentMethodList
} from "../../../Redux/settings/action";
import Loader from "../../../images/Loader";
import { ActionFunction, hideActionFunc } from "../../../utils";


const UsersPaymentMethod = () => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [filterData, setFilterData] = useState({ search: "", filterClose: false, });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [isEdit, setIsEdit] = useState({ isEditStatus: false, List: {} });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'BlockUser': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DeleteCommonModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const columns = [
        {
            id: "paymentMethod",
            label: "Method",
            sortable: false,
            type: "custom",
            render: (row) => {
                return <TableCell>
                    {row?.paymentMethod === "vpa" ? "UPI" : "Bank Transfer"}
                </TableCell>
            }
        },
        {
            id: "countries",
            numeric: true,
            disablePadding: false,
            label: "Country",
            type: "custom",
            render: (row) => {
                return <TableCell>
                    {/*className={'payment-method-country'}*/}
                    <ul >
                        {
                            row?.countries?.map(item => {
                                return <li>{item}</li>
                            })
                        }
                    </ul>
                </TableCell>
            }
        },
        ActionFunction('setting', {
            id: "action",
            disablePadding: false,
            isDisbanding: true,
            label: "Action",
            type: "custom",
            render: (row) => {
                return (
                    <TableCell className={'role_field_id'}>
                        <span className='edit_btn edit-btn-action u_border' onClick={() => setIsEdit({ ...isEdit, isEditStatus: true, List: row })}>Edit</span>
                        <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('DeleteCommonModal',
                            { deleteListApiHandler: deleteUserPaymentMethodList({ userPaymentMethodId: row?._id }), title: 'Do you want to delete this data?' })}>Delete</span>
                    </TableCell>
                );
            },
        })
    ];

    useEffect(() => {
        getUserPaymentMethod()
    }, [])

    const getUserPaymentMethod = (startDate, endDate, search) => {
        let payload = {
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            limit: pagination.rowsPerPage,
            searchText: search === "UPI" ? "vpa" : search === 'Bank Transfer' ? "bank_account" : ''
        };
        setLoader(true);
        dispatch(getUserPaymentMethodList(payload)).then((res) => {
            if (res.data.success) {
                setLoader(false);
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                });
            } else {
                setLoader(false);
            }
        })
            .catch((e) => {
                setLoader(false);
            });
    };

    return (
        <Box>
            {loader ? <Loader /> : ""}
            {
                hideActionFunc('setting') &&
                <AddPaymentMethod handleOpenModal={handleOpenModal} redirectApiHandler={getUserPaymentMethod} isEdit={isEdit} setIsEdit={setIsEdit} />
            }

            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter
                    filterData={filterData}
                    setFilterData={setFilterData}
                    searchApiHandler={getUserPaymentMethod}
                    pagination={pagination}
                    setPagination={setPagination}
                    addPropsFilter={{ isAvatar: true }}
                />
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                    isCommonProps={{ isAvatar: true }}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getUserPaymentMethod} />
            </CommonModal>
        </Box>
    )
};
export default UsersPaymentMethod