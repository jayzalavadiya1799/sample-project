import React, { useCallback, useEffect, useRef, useState } from "react";
import Paper from "@mui/material/Paper";
import { createRestrictGeo, getAllStateRestrictGeo, getCountriesRestrictGeo, updateRestrictGeo } from "../../../../Redux/settings/action";
import { useDispatch } from "react-redux";
import FilledButton from "../../../../Components/FileButton";
import AddIcon from "@mui/icons-material/Add";
import SimpleReactValidator from "simple-react-validator";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import EditIcon from "@mui/icons-material/Edit";
import CloseIcon from "@mui/icons-material/Close";
import { MultiSelect } from "react-multi-select-component";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import Tooltip from "@mui/material/Tooltip";
import CountryDropdown from "./CountryDropdown";


const useStyles = makeStyles((theme) => ({
    margin: {
        minWidth: "100%",
        margin: '0'
    },
}));

const BootstrapInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 14,
        height: '48px',
        padding: '0px 26px 0px 12px',
        display: 'flex',
        alignItems: 'center',
        marginTop: '5px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#1976d2',
            outlineWidth: '1px',
            boxShadow: 'none',
            outline: '1px solid #1976d2'
        },
    },
    svg: {
        right: '11px'
    }
}))(InputBase);


const AddRestrictGeo = ({ handleOpenModal, redirectApiHandler, isEdit, setIsEdit }) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [loader, setLoader] = useState(false);
    const [countryList, setCountryList] = useState({ country: [], state: [] });
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [formData, setFormData] = useState({ country: "", gameType: 'All Games', states: [], countryIsoCode: '' });
    let listingDetails = [
        {
            label: 'All Games',
            value: 'All Games'
        },
        {
            label: "All Games except Card Games",
            value: "All Games except Card Games",
        },
        {
            label: "No Games",
            value: "No Games",
        },
    ];

    useEffect(() => {
        dispatch(getCountriesRestrictGeo({})).then(res => {
            setCountryList({ ...countryList, country: res.data.data?.map((item) => { return { value: item?.name, label: item?.name, isoCode: item?.isoCode } }) || [] })
        })
    }, []);

    useEffect(() => {
        if (formData?.countryIsoCode) {
            let data = { isoCode: formData?.countryIsoCode };
            dispatch(getAllStateRestrictGeo(data)).then(res => {
                setCountryList({ ...countryList, state: res.data.data?.map((item) => { return { value: item?.name, label: item?.name } }) || [] })
            })
        }
    }, [formData?.countryIsoCode]);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                states: formData?.states?.map(item => item?.value)
            }
            delete payload?.countryIsoCode;
            dispatch(createRestrictGeo(payload)).then((res) => {
                if (res.data.success) {
                    setFormData({
                        ...formData, country: "", gameType: 'All Games', states: [], countryIsoCode: ''
                    })
                    redirectApiHandler()
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            });
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    useEffect(() => {
        if (isEdit?.isEditStatus) {
            setFormData({
                ...formData,
                country: isEdit?.List?.country,
                states: isEdit?.List?.states?.map((item) => { return { value: item, label: item } }) || [],
                gameType: isEdit?.List?.gameType,
                countryIsoCode: countryList?.country?.filter(item => item?.label === isEdit?.List?.country)?.[0]?.isoCode
            });
        }
    }, [isEdit]);

    const handleClose = () => {
        setIsEdit({ ...isEdit, isEditStatus: false, List: [] });
        setFormData({
            ...formData,
            country: "",
            gameType: 'All Games',
            states: [],
            countryIsoCode: ''
        })
    };

    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                states: formData?.states?.map(item => item?.value),
                restrictGeoId: isEdit?.List?._id
            }

            delete payload?.countryIsoCode;
            dispatch(updateRestrictGeo(payload)).then((res) => {
                if (res.data.success) {
                    setFormData({
                        ...formData,
                        country: "",
                        gameType: 'All Games',
                        states: [],
                        countryIsoCode: ''
                    })
                    redirectApiHandler();
                    setIsEdit({ ...isEdit, isEditStatus: false, List: [] });
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            });
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };


    return (
        <>
            <Paper sx={{ mb: 2 }} className="outerbox">
                <form className={'restrictGeo_details restrictGeo_details_form'} onSubmit={isEdit?.isEditStatus ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                    <div className="formData restrict_geo_section">
                        <label>Country</label>
                        <div className="emailWrap">
                            <CountryDropdown options={countryList?.country} name={'country'} formData={formData} setFormData={setFormData} />
                            {/*<SelectCountryDropdown formData={formData} setFormData={setFormData} data={countryList?.country} />*/}
                        </div>
                        {simpleValidator.current.message("country", formData?.country, "required")}
                    </div>
                    <div className={'formData'}>
                        <label>State </label>
                        <MultiSelect
                            options={countryList?.state}
                            value={formData?.states}
                            onChange={(value) => setFormData({ ...formData, states: value })}
                            labelledBy="Select State"
                            name='adminUserPermission'
                            arrowRenderer={() => <ArrowDropUpIcon />}
                        />
                        {simpleValidator.current.message("state", formData?.states, "required")}
                    </div>
                    <div className={'formData'}>
                        <label>Game Type </label>
                        <div className={'filter_days_details_dropDown payment_method_filter'}>
                            <FormControl className={classes.margin}>
                                <Select
                                    name={'gameType'}
                                    value={formData?.gameType}
                                    onChange={(e) => setFormData({ ...formData, gameType: e.target.value })}
                                    input={<BootstrapInput />}
                                    className={'fontFamily'}
                                >
                                    {
                                        listingDetails?.map(item => {
                                            return <MenuItem value={item?.value} className={'fontFamily'}>{item?.label}</MenuItem>
                                        })
                                    }
                                </Select>
                            </FormControl>
                        </div>
                        {simpleValidator.current.message("GameType", formData?.gameType, "required")}
                    </div>
                    <div className={isEdit?.isEditStatus ? ' restrict_geo_btn edit_restrict_section' : 'restrict_geo_btn'}>
                        <label>Action </label>
                        {
                            isEdit?.isEditStatus ?
                                <div className={'edit_restrict_btn'}>
                                    <Tooltip title="Edit">
                                        <FilledButton type={'submit'} value={<EditIcon />} className={'loader_css submit_btn edit_btn_margin'} loading={loader} />
                                    </Tooltip>
                                    <button type={"button"} onClick={() => handleClose()}><CloseIcon /></button>
                                </div>
                                :
                                <div>
                                    <FilledButton type={'submit'} value={<AddIcon />} className={'loader_css'} loading={loader} />
                                </div>
                        }
                    </div>
                </form>
            </Paper>
        </>
    );
};
export default AddRestrictGeo;