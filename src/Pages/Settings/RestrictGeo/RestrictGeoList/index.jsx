import React, { useState } from "react";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../../hoc/CommonTable";
import Box from "@mui/material/Box";
import MainCommonFilter from "../../../../Components/MainCommonFilter";
import { TableCell } from "@mui/material";
import { deleteRestrictGeoList } from "../../../../Redux/settings/action";
import { ActionFunction } from "../../../../utils";

const RestrictGeoList = ({ rowData, pagination, setPagination, handleOpenModal, setIsEdit, isEdit, getRestrictGeo }) => {
    const [filterData, setFilterData] = useState({ search: "", filterClose: false, });

    const columns = [
        {
            id: 'country',
            label: 'Country',
        },
        {
            id: 'states',
            label: 'State',
            type: "custom",
            render: (row) => {
                if (row?.states?.length > 0) {
                    return <TableCell>
                        <ul>
                            {
                                row?.states?.map(item => {
                                    return <li>{item}</li>
                                })
                            }
                        </ul>
                    </TableCell>
                } else {
                    return <TableCell />
                }

            }
        },
        {
            id: 'gameType',
            label: 'Game type',
        },
        ActionFunction('setting', {
            id: 'action',
            label: 'Action',
            isDisbanding: true,
            type: "custom",
            render: (row) => {
                return (
                    <TableCell className={'role_field_id'}>
                        <span className='edit_btn edit-btn-action u_border' onClick={() => setIsEdit({ ...isEdit, isEditStatus: true, List: row })}>Edit</span>
                        <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('DeleteCommonModal',
                            { deleteListApiHandler: deleteRestrictGeoList({ restrictGeoId: row?._id }), title: 'Do you want to delete this data?' })}>Delete</span>
                    </TableCell>
                )
            }
        })
    ];

    return (
        <Box>
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter
                    filterData={filterData}
                    setFilterData={setFilterData}
                    searchApiHandler={getRestrictGeo}
                    pagination={pagination}
                    setPagination={setPagination}
                    addPropsFilter={{ isAvatar: true }}
                />
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                    isCommonProps={{ isAvatar: true }}
                />
            </Paper>
        </Box>
    )
}
export default RestrictGeoList