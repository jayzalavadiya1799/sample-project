import React, { useEffect, useState } from "react";
import AddRestrictGeo from "./AddRestrictGeo";
import RestrictGeoList from "./RestrictGeoList";
import PopComponent from "../../../hoc/PopContent";
import CommonModal from "../../../hoc/CommonModal";
import { useDispatch } from "react-redux";
import { getRestrictGeoList } from "../../../Redux/settings/action";
import Loader from "../../../images/Loader";
import { hideActionFunc } from "../../../utils";

const RestrictGeo = () => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [isEdit, setIsEdit] = useState({ isEditStatus: false, List: {} });
    let Modal = PopComponent[modalDetails.modalName];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DeleteCommonModal': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false });
            }
        }
    };

    useEffect(() => {
        getRestrictGeo()
    }, [])

    const getRestrictGeo = (startDate, endDate, search) => {
        let payload = {
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            limit: pagination.rowsPerPage,
            searchText: search
        };
        setLoader(true);
        dispatch(getRestrictGeoList(payload)).then((res) => {
            if (res.data.success) {
                setLoader(false);
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                });
            } else {
                setLoader(false);
            }
        })
            .catch((e) => {
                setLoader(false);
            });
    };

    return (
        <div>
            {loader ? <Loader /> : ""}
            {
                hideActionFunc('setting') &&
                <AddRestrictGeo handleOpenModal={handleOpenModal} redirectApiHandler={getRestrictGeo} isEdit={isEdit} setIsEdit={setIsEdit} />
            }

            <RestrictGeoList rowData={rowData} pagination={pagination} setPagination={setPagination} handleOpenModal={handleOpenModal} setIsEdit={setIsEdit} isEdit={isEdit} getRestrictGeo={getRestrictGeo} />
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getRestrictGeo} />
            </CommonModal>
        </div>
    )
};
export default RestrictGeo