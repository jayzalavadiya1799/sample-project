import React, { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import { useDispatch } from 'react-redux';
import { configList, createConfigList } from '../../../Redux/settings/action';
import CommonModal from '../../../hoc/CommonModal';
import PopComponent from '../../../hoc/PopContent';
import { hideActionFunc } from "../../../utils";

const Config = () => {
    const dispatch = useDispatch();
    const [modalValue, setModalValue] = useState('');
    const [modalName, setModalName] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(false);
    let Modal = PopComponent[modalName];
    const [config, setConfigList] = useState({
        configs: '{}'
    });

    useEffect(() => {
        configListData();
    }, []);

    const configListData = () => {
        dispatch(configList()).then(res => {
            if (res.data.statusCode === 200) {
                setConfigList({
                    ...config,
                    configs: JSON.stringify(res.data.data ? res.data.data : {}, null, 2)
                })
            }
        });
    };

    const handleChange = (e) => {
        setConfigList({
            ...config,
            configs: e.target.value
        })
    };

    const configHandler = () => {
        try {
            const configList = JSON.parse(config.configs);
            dispatch(createConfigList({ config: configList?.CONFIG })).then(res => {
                if (res.data.statusCode === 200) {
                    configListData();
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message });
                } else {
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res.data.msg });
                }
            })
        } catch {
            handleOpenModal('CommonPop', { header: "Info", body: 'Wrong JSON format' });
        }

    };

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalName(type)
                setModalIsOpen(true)
                setModalValue(data)
            }
                break
            default: {
                setModalIsOpen(false)
            }
        }
    };

    return (
        <Paper sx={{ mb: 2 }} className="outerbox">
            <div className={'config-data'}>
                <TextareaAutosize
                    name={"config"}
                    value={config.configs}
                    minRows={30}
                    maxRows={30}
                    onChange={(e) => { handleChange(e) }}
                />
            </div>
            {
                hideActionFunc('setting') &&
                <div className={'game-play-rules'}>
                    <button onClick={() => configHandler()}>Save</button>
                </div>
            }

            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalIsOpen} YesNoText={{ yes: "Yes", no: "Cancel" }} />
            </CommonModal>
        </Paper>
    )
};

export default Config