import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { a11yProps } from "../../utils";
import TabPanel from "../../Components/TabPanel";
import PopularDetailsGames from "./PopularDetailsGames";
import UpcomingGames from "./UpcomingGames";


const PopularGames = () => {
    const [value, setValue] = React.useState(0);
    // tab change fun
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <Box sx={{ width: '100%' }} className={'user_details_tab'}>
            <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >
                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                    <Tab className={'tab_title'} label="Popular Games" {...a11yProps(0)} />
                    <Tab className={'tab_title'} label="Upcoming Games" {...a11yProps(1)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0} >
                <PopularDetailsGames />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <UpcomingGames />
            </TabPanel>
        </Box>
    )
}
export default PopularGames


