import React, { useEffect, useState } from "react";
import Select2 from 'react-select2-wrapper';
import 'react-select2-wrapper/css/select2.css';

const GamePopularDropdown = ({ options, name, formData, setFormData }) => {
    const [gameMenu, setGameMenu] = useState([])
    const handleChange = (e) => {
        if (e.target.value !== '') {
            setFormData({
                ...formData,
                [name]: e.target.value
            })
        }
    }
    console.log(formData,"---------------------------------")
    useEffect(() => {
        let temp = []
        options?.forEach(((item => {
            temp.push({ text: item?.gameName, id: item?._id })
        })))
        setGameMenu(temp)
    }, [options])
    return (
        <Select2
            defaultValue={formData[name]}
            data={gameMenu}
            name={name}
            options={{
                placeholder: 'Select Game',
            }}
            onChange={(e) => handleChange(e)}
            className={'select-7894'}
        />
    );
}
export default GamePopularDropdown