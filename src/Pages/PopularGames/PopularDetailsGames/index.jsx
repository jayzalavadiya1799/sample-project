import { useDispatch } from "react-redux";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { jsonToFormData, profileImages } from "../../../utils";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import FilledButton from "../../../Components/FileButton";
import user from "../../../assets/images/avatar.png";
import { getLeaderboardGameList } from "../../../Redux/Master/action";
import icon_plus from "../../../assets/images/plus.svg";
import SimpleReactValidator from "simple-react-validator";
import GamePopularDropdown from "./GamePopularDropdown";
import FourthSection from "./FourthSection";
import SecondSection from "./SecondSection";
import ThirdSection from "./ThirdSection";
import { createPopularGames, getPopularGamesDetails } from "../../../Redux/popularGames/action";
import CommonModal from "../../../hoc/CommonModal";
import PopComponent from "../../../hoc/PopContent";

const PopularDetailsGames = () => {
    const dispatch = useDispatch();
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [loader, setLoader] = useState(false);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [gameFilterData, setGameFilterData] = useState([]);
    const [rowData, setRowData] = useState([]);
    let Modal = PopComponent[modalDetails.modalName];
    const [formData, setFormData] = useState({
        firstGameId: '',
        firstGameNumber: '1',
        firstGamePoster: '',
        firstIsGamePosterUpdated: false,
    });

    useEffect(() => {
        dispatch(getLeaderboardGameList()).then(res => {
            setGameFilterData(res.data.data?.docs)
        })
        getPopularGameDetailsList()
    }, []);

    const getPopularGameDetailsList = () => {
        dispatch(getPopularGamesDetails()).then(res => {
            if (res.data.success) {
                let temp = res?.data?.data?.filter(item => item?.gameNumber === '1')?.reduce((acc, cur) => { return { ...acc, ...cur } }, {});
                setRowData(res?.data?.data)
                setFormData({
                    ...formData,
                    firstGameId: temp?.gameId?._id,
                    firstGameNumber: '1',
                    firstGamePoster: temp?.gamePoster,
                    firstIsGamePosterUpdated: false,
                })
            }
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                gameId: formData?.firstGameId,
                gameNumber: formData?.firstGameNumber,
                gamePoster: formData?.firstGamePoster,
                isGamePosterUpdated: formData?.firstIsGamePosterUpdated
            }
            setLoader(true)
            dispatch(createPopularGames(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false);
                    getPopularGameDetailsList();
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false });
            }
        }
    };
    const handleChange = (e) => {
        let img = new Image();
        img.src = window.URL.createObjectURL(e.target.files[0]);
        img.onload = () => {
            if (img.width === 778 && img.height === 1246) {
                setFormData({ ...formData, firstGamePoster: e.target.files[0], firstIsGamePosterUpdated: true })
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: 'The width and height of the image should be  778 * 1246 size' });
            }
        }
    }
    return (
        <Box>
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'popular-game-main-section'}>
                    <div className={'header_slider_details header_slider_details_Ads popular-game-main-images'}>
                        <div className={'left-side-popular-game'}>
                            <form className='form_group ' onSubmit={(e) => handleSubmit(e)}>
                                <div className={'popular-game-first-section'}>
                                    <div>
                                        <div className='form_group profile'>
                                            <div className='user_profile'>
                                                <label htmlFor='' className='profile_label'> Banner (778 * 1246 size) </label>
                                                <div className='user_profile_pic'>
                                                    {profileImages(formData?.firstGamePoster, user)}
                                                    <span className='addnew'>
                                                        <img src={icon_plus} alt='' />
                                                        <input type='file' name='member_photo' id='' onChange={(e) => handleChange(e)} />
                                                    </span>
                                                </div>
                                                {simpleValidator.current.message("gameBanner", formData?.firstGamePoster, 'required')}
                                            </div>
                                        </div>
                                    </div>

                                    <div className={'dropdown-popular-game'}>
                                        <label>Select Game</label>
                                        <div className={'leader_board_game_list_filter'}>
                                            <GamePopularDropdown
                                                options={gameFilterData} name={'firstGameId'}
                                                setFormData={setFormData} formData={formData} />
                                        </div>
                                        {simpleValidator.current.message("gameName", formData?.firstGameId, 'required')}
                                        <div className={'formData_btn'}>
                                            <FilledButton type={'submit'} value={'Save'} className={'submit_btn loader_css'} loading={loader} />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className={'right-side-popular-game'}>
                            <SecondSection gameFilterData={gameFilterData} handleOpenModal={handleOpenModal} getPopularGameDetailsList={getPopularGameDetailsList} rowData={rowData?.length > 0 && rowData?.filter(item => item?.gameNumber === '2')?.reduce((acc, cur) => { return { ...acc, ...cur } }, {})} />
                            <ThirdSection gameFilterData={gameFilterData} handleOpenModal={handleOpenModal} getPopularGameDetailsList={getPopularGameDetailsList} rowData={rowData?.length > 0 && rowData?.filter(item => item?.gameNumber === '3')?.reduce((acc, cur) => { return { ...acc, ...cur } }, {})} />
                        </div>
                    </div>
                </div>
                <FourthSection gameFilterData={gameFilterData} handleOpenModal={handleOpenModal} getPopularGameDetailsList={getPopularGameDetailsList} rowData={rowData?.length > 0 && rowData?.filter(item => item?.gameNumber === '4')?.reduce((acc, cur) => { return { ...acc, ...cur } }, {})} />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default PopularDetailsGames