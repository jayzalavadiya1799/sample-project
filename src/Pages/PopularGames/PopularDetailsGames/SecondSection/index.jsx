import { jsonToFormData, profileImages } from "../../../../utils";
import user from "../../../../assets/images/avatar.png";
import icon_plus from "../../../../assets/images/plus.svg";
import GamePopularDropdown from "../GamePopularDropdown";
import FilledButton from "../../../../Components/FileButton";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import { createPopularGames } from "../../../../Redux/popularGames/action";

const SecondSection = ({ gameFilterData, handleOpenModal, rowData, getPopularGameDetailsList }) => {
    const dispatch = useDispatch();
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [loader, setLoader] = useState(false);
    const [formData, setFormData] = useState({
        secondGameId: '',
        secondGameNumber: '2',
        secondGamePoster: '',
        secondIsGamePosterUpdated: false,
    });

    useEffect(() => {
        if (Object.keys(rowData || {})?.length > 0) {
            setFormData({
                ...formData,
                secondGameId: rowData?.gameId?._id,
                secondGameNumber: '2',
                secondGamePoster: rowData?.gamePoster,
                secondIsGamePosterUpdated: false,
            })
        }
    }, [rowData])

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                gameId: formData?.secondGameId,
                gameNumber: formData?.secondGameNumber,
                gamePoster: formData?.secondGamePoster,
                isGamePosterUpdated: formData?.secondIsGamePosterUpdated
            }
            setLoader(true)
            dispatch(createPopularGames(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false);
                    getPopularGameDetailsList()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    const handleChange = (e) => {
        let img = new Image();
        img.src = window.URL.createObjectURL(e.target.files[0]);
        img.onload = () => {
            if (img.width === 1024 && img.height === 778) {
                setFormData({ ...formData, secondGamePoster: e.target.files[0], secondIsGamePosterUpdated: true })
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: 'The width and height of the image should be  1024 * 778 size' });
            }
        }
    }

    return (
        <form className='form_group first-banner-form-right' onSubmit={(e) => handleSubmit(e)}>
            <div className={'popular-game-first-section'}>
                <div>
                    <div className='form_group profile'>
                        <div className='user_profile'>
                            <label htmlFor='' className='profile_label'> Banner (1024 * 778 size) </label>
                            <div className='user_profile_pic'>
                                {profileImages(formData?.secondGamePoster, user)}
                                <span className='addnew'>
                                    <img src={icon_plus} alt='' />
                                    <input type='file' name='secondGamePoster' id='' onChange={(e) => handleChange(e)} />
                                </span>
                            </div>
                            {simpleValidator.current.message("gameBanner", formData?.secondGamePoster, 'required')}
                        </div>
                    </div>
                </div>
                <div className={'dropdown-popular-game'}>
                    <label>Select Game</label>
                    <div className={'leader_board_game_list_filter'}>
                        <GamePopularDropdown
                            options={gameFilterData} name={'secondGameId'}
                            setFormData={setFormData} formData={formData} />
                    </div>
                    {simpleValidator.current.message("gameName", formData?.secondGameId, 'required')}
                    <div className={'formData_btn'}>
                        <FilledButton type={'submit'} value={'Save'} className={'submit_btn loader_css'} loading={loader} />
                    </div>
                </div>
            </div>
        </form>
    )
}
export default SecondSection