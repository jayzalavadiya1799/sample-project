import React, { useCallback, useEffect, useRef, useState } from "react";
import { jsonToFormData, profileImages } from "../../../../utils";
import user from "../../../../assets/images/avatar.png";
import icon_plus from "../../../../assets/images/plus.svg";
import GamePopularDropdown from "../GamePopularDropdown";
import FilledButton from "../../../../Components/FileButton";
import SimpleReactValidator from "simple-react-validator";
import { useDispatch } from "react-redux";
import { createPopularGames } from "../../../../Redux/popularGames/action";

const FourthSection = ({ gameFilterData, handleOpenModal, getPopularGameDetailsList, rowData }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [formData, setFormData] = useState({
        fourthGameId: '',
        fourthGameNumber: '4',
        fourthGamePoster: '',
        fourthIsGamePosterUpdated: false,
    });

    useEffect(() => {
        if (Object.keys(rowData || {})?.length > 0) {
            setFormData({
                ...formData,
                fourthGameId: rowData?.gameId?._id,
                fourthGameNumber: '4',
                fourthGamePoster: rowData?.gamePoster,
                fourthIsGamePosterUpdated: false,
            })
        }
    }, [rowData])

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                gameId: formData?.fourthGameId,
                gameNumber: formData?.fourthGameNumber,
                gamePoster: formData?.fourthGamePoster,
                isGamePosterUpdated: formData?.fourthIsGamePosterUpdated
            }
            setLoader(true)
            dispatch(createPopularGames(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false);
                    getPopularGameDetailsList();
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }
    const handleChange = (e) => {
        let img = new Image();
        img.src = window.URL.createObjectURL(e.target.files[0]);
        img.onload = () => {
            if (img.width === 1736 && img.height === 778) {
                setFormData({ ...formData, fourthGamePoster: e.target.files[0], fourthIsGamePosterUpdated: true })
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: 'The width and height of the image should be  1736 * 778 size' });
            }
        }
    }
    return (
        <div className={'popular-game-main-section popular-main-last-section'}>
            <div className={'header_slider_details header_slider_details_Ads popular-game-main-images popular-last-section'}>
                <form className='form_group ' onSubmit={(e) => handleSubmit(e)}>
                    <div className={'popular-game-first-section'}>
                        <div className={'user_profileDetails-section'}>
                            <div className='form_group profile'>
                                <div className='user_profile'>
                                    <label htmlFor='' className='profile_label'> Banner (1736 * 778 size) </label>
                                    <div className='user_profile_pic'>
                                        {profileImages(formData?.fourthGamePoster, user)}
                                        <span className='addnew'>
                                            <img src={icon_plus} alt='' />
                                            <input type='file' name='fourthGamePoster' id='' onChange={(e) => handleChange(e)} />
                                        </span>
                                    </div>
                                    {simpleValidator.current.message("gameBanner", formData?.fourthGamePoster, 'required')}
                                </div>
                            </div>
                        </div>
                        <div className={'dropdown-popular-game'}>
                            <label>Select Game</label>
                            <div className={'leader_board_game_list_filter'}>
                                <GamePopularDropdown
                                    options={gameFilterData} name={'fourthGameId'}
                                    setFormData={setFormData} formData={formData} />
                            </div>
                            {simpleValidator.current.message("gameName", formData?.fourthGameId, 'required')}
                            <div className={'formData_btn'}>
                                <FilledButton type={'submit'} value={'Save'} className={'submit_btn loader_css'} loading={loader} />
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    )
}
export default FourthSection