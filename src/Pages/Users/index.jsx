import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Loader from '../../images/Loader';
import Paper from '@mui/material/Paper';
import CustomTable from '../../hoc/CommonTable';
import CommonModal from '../../hoc/CommonModal';
import { useDispatch, useSelector } from 'react-redux';
import { userDetailsData } from '../../Redux/user/action';
import TableCell from '@mui/material/TableCell';
import PopComponent from '../../hoc/PopContent';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';
import MainCommonFilter from "../../Components/MainCommonFilter";
import { ActionFunction, currencyFormat } from "../../utils";

const Users = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const userList = useSelector(state => state.userReducer.userList)
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 })
    let Modal = PopComponent[modalDetails.modalName];
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
        exportFileName: 'Export File'
    });

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getUserListData(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.startDate, filterData.endDate]);

    // get User Api and All Filter Api
    const getUserListData = (startDate, endDate, search) => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate:
                startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload
        }
        Object?.keys(payload).forEach(ele => {
            if (payload[ele] === '' || payload[ele] === null) { delete payload[ele] }
        });
        dispatch(userDetailsData(payload)).then(res => {
            setLoader(false)
            if (res?.data?.data?.filePath) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            }
        })
    };

    useEffect(() => {
        if (filterData.startDate && filterData.endDate && filterData?.statusValue === 'Custom') {
            setPagination({
                ...pagination,
                page: 0
            })
            getUserListData(filterData.startDate, filterData.endDate)
        }

    }, [filterData.startDate, filterData.endDate]);

    // table columns
    let columns = [

        {
            id: 'id',
            numeric: false,
            disablePadding: false,
            label: 'Users ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className='edit_btn'
                    onClick={() => navigate(`/users-tab/${row.id}`)}>{`UID000${row?.numericId}`}</span></TableCell>
            }
        },
        {
            id: 'fullName',
            numeric: true,
            disablePadding: false,
            label: 'User Name',
        },
        {
            id: 'phoneNumber',
            numeric: true,
            disablePadding: false,
            label: 'Mobile',
        },
        {
            id: 'country',
            disablePadding: false,
            label: 'Country',
        },
        {
            id: 'deviceType',
            disablePadding: false,
            label: 'Device Type',
        },
        {
            id: 'cash',
            numeric: true,
            disablePadding: false,
            label: 'Deposit Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.cash)}</TableCell>
            }
        },
        {
            id: 'winCash',
            numeric: true,
            disablePadding: false,
            label: 'Winning',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.winCash)}</TableCell>
            }
        },

        {
            id: 'bonus',
            numeric: true,
            disablePadding: false,
            label: 'Bonus',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.bonus)}</TableCell>
            }
        },

        {
            id: 'totalCash',
            numeric: true,
            disablePadding: false,
            label: 'Total Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalCash)} </TableCell>
            }
        },
        {
            id: 'totalDeposits',
            numeric: true,
            disablePadding: false,
            label: 'Total Deposits',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalDeposits)}</TableCell>
            }
        },
        {
            id: 'totalWithdrawals',
            numeric: true,
            disablePadding: false,
            label: 'Total Withdrawals ',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalWithdrawals)}</TableCell>
            }
        },
        ActionFunction('popularGame', {
            id: 'Action',
            isDisbanding: true,
            label: 'Action',
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.isBlock ?
                    <span className='edit_btn edit-btn-action'
                        onClick={() => handleOpenModal('BlockUser', { userId: row.id, isBlock: !row?.isBlock })}>Unblock</span> : <span className='edit_btn edit-btn-action'
                            onClick={() => handleOpenModal('BlockUser', { userId: row.id, isBlock: !row?.isBlock })}>Block</span>}</TableCell>
            }
        })
    ];

    // custom PopUp function
    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'BlockUser': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const userRedirectApi = () => {
        getUserListData(filterData.startDate, filterData.endDate)
    };

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={getUserListData} pagination={pagination} setPagination={setPagination} />
                <CustomTable
                    headCells={columns}
                    rowData={userList?.list}
                    totalDocs={userList?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={userRedirectApi} />
            </CommonModal>
        </Box>
    )
}
export default Users