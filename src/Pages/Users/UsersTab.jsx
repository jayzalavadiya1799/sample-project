import React, { useEffect, useState } from 'react'
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box'
import UserDetails from './UserDetails';
import UserTransition from './UserTransition';
import UserGameStatistics from './UserGameStatistics';
import UserGamePlayedTab from './UserGamePlayedTab';
import UserNote from './UserNote';
import PopComponent from '../../hoc/PopContent';
import CommonModal from '../../hoc/CommonModal';
import PaymentHistory from "./PaymentHistory";
import { a11yProps } from "../../utils";
import { getUserProfile } from "../../Redux/user/action";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import TabPanel from "../../Components/TabPanel";
import UserKYCTab from "./UserKYCTab";

function UsersTab() {
    const { id } = useParams();
    const dispatch = useDispatch();
    const userProfile = useSelector(state => state.userReducer.userProfile);
    const [value, setValue] = React.useState(0);
    // custom modal state
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    // user profile details Api fun
    useEffect(() => {
        getUserProfileDetails()
    }, []);

    const getUserProfileDetails = () => {
        dispatch(getUserProfile({ userId: id }))
    };

    // tab change fun
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    // custom PopUp function
    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewTransitionHistory': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewGamePlayedHistory': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'UpdateCashAndBonus': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'AddCoinPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <>
            {value === 0 ? "" :
                <div className='user_detail_info'>
                    <div className={'info_filed'}>
                        <span className={'info_title fontFamily'}>User ID : </span>
                        <span className={'fontFamily'}>UID000{userProfile?.numericId}</span>
                    </div>
                    <div className={'info_filed'}>
                        <span className={'info_title fontFamily'}>Name : </span>
                        <span className={'fontFamily'}>{userProfile?.fullName}</span>
                    </div>
                    <div className={'info_filed'}>
                        <span className={'info_title fontFamily'}>Email : </span>
                        <span className={'fontFamily'}>{userProfile?.email}</span>
                    </div>
                    <div className={'info_filed'}>
                        <span className={'info_title fontFamily'}>Phone no : </span>
                        <span className={'fontFamily'}>{userProfile?.phoneNumber}</span>
                    </div>
                </div>
            }
            <Box sx={{ width: '100%' }} className={'user_details_tab'}>
                <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >
                    <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                        <Tab className={'tab_title'} label="User Profile" {...a11yProps(0)} />
                        <Tab className={'tab_title'} label="Transactions History" {...a11yProps(1)} />
                        <Tab className={'tab_title'} label="Payment History" {...a11yProps(2)} />
                        <Tab className={'tab_title'} label="Game Statistics" {...a11yProps(3)} />
                        <Tab className={'tab_title'} label="Games Played" {...a11yProps(4)} />
                        <Tab className={'tab_title'} label="User KYC" {...a11yProps(5)} />
                        <Tab className={'tab_title'} label="Notes" {...a11yProps(6)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0} >
                    <UserDetails handleOpenModal={handleOpenModal} />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <UserTransition handleOpenModal={handleOpenModal} />
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <PaymentHistory />
                </TabPanel>
                <TabPanel value={value} index={3}>
                    <UserGameStatistics />
                </TabPanel>
                <TabPanel value={value} index={4}>
                    <UserGamePlayedTab handleOpenModal={handleOpenModal} />
                </TabPanel>
                <TabPanel value={value} index={5}>
                    <UserKYCTab />
                </TabPanel>
                <TabPanel value={value} index={6}>
                    <UserNote />
                </TabPanel>
            </Box>
            {/*--------------------------------------------------------CommonModal-------------------------------------------------*/}
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </>
    );
}

export default UsersTab