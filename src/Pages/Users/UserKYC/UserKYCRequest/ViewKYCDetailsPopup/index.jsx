import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Loader from "../../../../../images/Loader";
import CustomTable from "../../../../../hoc/CommonTable";
import { useDispatch } from "react-redux";
import TableCell from "@mui/material/TableCell";
import CommonModal from "../../../../../hoc/CommonModal";
import PopComponent from "../../../../../hoc/PopContent";
import { userKYCViewRequestList } from "../../../../../Redux/user/action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 950,
    bgcolor: 'white',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const ViewKYCDetailsPopup = ({ modalValue }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [rowData, setRowData] = useState({ NewList: [], OldList: [] });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });
    //userKYCViewRequestList
    useEffect(() => {
        dispatch(userKYCViewRequestList({ kycUpdateRequestId: modalValue?._id })).then(res => {
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    NewList: Object?.keys(res?.data?.data?.newKYCDetails || {})?.length ? [res?.data?.data?.newKYCDetails] : [],
                    OldList: Object?.keys(res?.data?.data?.oldKYCDetails || {})?.length ? [res?.data?.data?.oldKYCDetails] : [],
                })
            }
        })
    }, [])

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DocumentOpenPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const columnsNewAadhar = [
        {
            id: '',
            numeric: true,
            isDisbanding: true,
            label: 'Aadhar Card Name',
            type: 'custom',
            render: (row) => {
                return <TableCell> {row?.updateKYCDetails?.aadharName}</TableCell>
            }
        },
        {
            id: 'aadharCardNumber',
            label: 'Aadhar Card Number',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{(row?.updateKYCDetails?.aadharCardNumber === null || row?.updateKYCDetails?.aadharCardNumber === '') ? '-' : row?.updateKYCDetails?.aadharCardNumber}</TableCell>
            }
        },
        {
            id: 'AadharCardPhoto',
            label: 'Aadhar Card Photo',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell>
                    {
                        (row?.updateKYCDetails?.aadharCardNumber === null || row?.updateKYCDetails?.aadharCardNumber === '') ? '-' :
                            <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('DocumentOpenPopup', { front: row?.updateKYCDetails?.aadharCardFrontImage, back: row?.updateKYCDetails?.aadharCardBackImage })}>View</span>
                    }
                </TableCell>
            }
        },
        {
            id: 'dateOfBirth',
            label: 'Date Of Birth',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.updateKYCDetails?.dateOfBirth}</TableCell>
            }
        },
        {
            id: 'isAutomaticVerify',
            label: 'Automatic verify',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.isAutomaticVerify ? 'Yes' : 'No'}</TableCell>
            }
        },
    ];

    const columnsOldAadhar = [
        {
            id: '',
            numeric: true,
            isDisbanding: true,
            label: 'Aadhar Card Name',
            type: 'custom',
            render: (row) => {
                return <TableCell> {row?.aadharName}</TableCell>
            }
        },
        {
            id: 'aadharCardNumber',
            label: 'Aadhar Card Number',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{(row?.aadharCardNumber === null || row?.aadharCardNumber === '') ? '-' : row?.aadharCardNumber}</TableCell>
            }
        },
        {
            id: 'AadharCardPhoto',
            label: 'Aadhar Card Photo',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell>
                    {
                        (row?.aadharCardNumber === null || row?.aadharCardNumber === '') ? '-' :
                            <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('DocumentOpenPopup', { front: row?.aadharCardFrontImage, back: row?.aadharCardBackImage })}>View</span>
                    }
                </TableCell>
            }
        },
        {
            id: 'dateOfBirth',
            label: 'Date Of Birth',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.dateOfBirth}</TableCell>
            }
        },
        {
            id: 'isAutomaticVerify',
            label: 'Automatic verify',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.isAutomaticVerify ? 'Yes' : 'No'}</TableCell>
            }
        },
    ];

    const columnsPanCard = [
        {
            id: 'panCardNumber',
            label: 'Pan Card Number',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{(row?.updateKYCDetails?.panCardImage === null || row?.updateKYCDetails?.panCardImage === '') ? '-' : row?.updateKYCDetails?.panCardNumber}</TableCell>
            }

        },
        {
            id: '',
            label: 'Pan Card Photo',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell>
                    {
                        (row?.updateKYCDetails?.panCardImage === null || row?.updateKYCDetails?.panCardImage === '') ? '-' :
                            <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('DocumentOpenPopup', { front: row?.updateKYCDetails?.panCardImage })}>View</span>
                    }

                </TableCell>
            }
        },
    ];

    return (
        <Box sx={style} className={'kyc-details-popup'}>
            {loader ? <Loader /> : ""}
            <h2 >New Aadhar Card Details</h2>
            <CustomTable
                headCells={modalValue?.isUpdateAadharCard ? columnsNewAadhar : columnsPanCard}
                rowData={rowData?.NewList}
                totalDocs={rowData?.totalDocs}
                pagination={pagination}
                setPagination={setPagination}
                isWinnerTitle={true}
            />
            <div className={'old-list-Aadhar-card-details'}>
                <h2>Old Aadhar Card Details</h2>
                <CustomTable
                    headCells={modalValue?.isUpdateAadharCard ? columnsOldAadhar : columnsPanCard}
                    rowData={rowData?.OldList}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                    isWinnerTitle={true}
                />
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={''} />
            </CommonModal>
        </Box>
    )
}
export default ViewKYCDetailsPopup