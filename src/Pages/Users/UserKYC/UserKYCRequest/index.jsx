import Box from "@mui/material/Box";
import Loader from "../../../../images/Loader";
import Paper from "@mui/material/Paper";
import MainCommonFilter from "../../../../Components/MainCommonFilter";
import CustomTable from "../../../../hoc/CommonTable";
import CommonModal from "../../../../hoc/CommonModal";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import PopComponent from "../../../../hoc/PopContent";
import TableCell from "@mui/material/TableCell";
import { ActionFunction, dotGenerator } from "../../../../utils";
import moment from "moment";
import { getUserKYCListRequest } from "../../../../Redux/user/action";

const UserKYCRequest = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 })
    let Modal = PopComponent[modalDetails.modalName];
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusField: 'Pending',
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
    });
    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewKYCDetailsPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ApproveUserKYCRequest': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'RejectUserKYCRequest': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewRejectedComment': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const columns = [
        {
            id: '',
            numeric: false,
            isDisbanding: true,
            label: 'Users ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className='edit_btn' onClick={() => navigate(`/users-tab/${row?.userId._id}`)}>{`UID000${row?.userId?.numericId || 1}`}</span></TableCell>
            }
        },
        {
            id: 'userName',
            numeric: true,
            isDisbanding: true,
            label: 'User Name',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.userId?.fullName}</TableCell>
            }
        },
        {
            id: 'createdAt',
            numeric: true,
            disablePadding: false,
            label: 'Date',
            type: "custom",
            render: (row) => {
                return <TableCell >{moment(row?.createdAt).format("MMM DD YYYY, HH:MM A")}</TableCell>
            }
        },
        {
            id: 'isUpdateAadharCard',
            label: 'KYC Type',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.isUpdateAadharCard ? 'Aadhar Card' : 'Pan Card'}</TableCell>
            }
        },
        {
            id: 'status',
            isDisbanding: true,
            label: 'KYC Details',
            type: 'custom',
            render: (row, i) => {
                return <TableCell className='edit_btn edit-btn-action u_border_details' onClick={(e) => (row?.status === 'Reject' || row?.status === 'Approve') ? '' : handleOpenModal('ViewKYCDetailsPopup', row)}>{(row?.status === 'Reject' || row?.status === 'Approve') ? '-' : 'View'}</TableCell>
            }
        },
        {
            id: 'rejectReason',
            label: 'KYC Reject Reason',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.rejectReason ? dotGenerator(row?.rejectReason, handleOpenModal, 'Reject KYC Request Reason') : '-'}</TableCell>
            }
        },
        {
            id: 'status',
            label: 'Status',
        },

        ActionFunction('user', {
            id: 'Action',
            label: 'Action',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {

                return <TableCell className={'role_field_id'}>
                    {
                        (row?.status === 'Pending') ?
                            <>
                                <span className='edit_btn edit-btn-action u_border prTab' onClick={() => handleOpenModal('ApproveUserKYCRequest', { kycUpdateRequestId: row?._id, isApprove: true })}> Approve</span>
                                <span className='edit_btn edit-btn-action prTab ' onClick={() => handleOpenModal('RejectUserKYCRequest', { kycUpdateRequestId: row?._id, isApprove: false })}>Reject</span>

                            </>
                            : "-"
                    }
                </TableCell>
            }
        })
    ];

    useEffect(() => {
        getUserKYCListRequestDetails()
    }, [pagination.rowsPerPage, pagination.page, filterData?.statusField]);

    const getUserKYCListRequestDetails = () => {
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            status: filterData?.statusField !== 'All Status' ? filterData?.statusField : ''
        }
        Object?.keys(payload).forEach(ele => {
            if (payload[ele] === '' || payload[ele] === null) { delete payload[ele] }
        });
        setLoader(true);
        dispatch(getUserKYCListRequest(payload)).then(res => {
            setLoader(false)
            if (res.data?.success) {
                setRowData({ ...rowData, list: res?.data?.data?.docs, totalDocs: res?.data?.data?.totalDocs })
            } else {
                setRowData({ ...rowData, list: [], totalDocs: 0 })
            }
        });
    };

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter
                    filterData={filterData}
                    setFilterData={setFilterData}
                    searchApiHandler={''}
                    pagination={pagination}
                    setPagination={setPagination}
                    statusOption={['All Status', 'Approve', 'Pending', 'Reject']}
                    addPropsFilter={{ isKYCList: true, isGameList: true }}
                />
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getUserKYCListRequestDetails} />
            </CommonModal>
        </Box>
    )
}
export default UserKYCRequest