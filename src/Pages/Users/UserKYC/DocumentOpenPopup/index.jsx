import { Box } from "@mui/material";
import React, { useEffect, useState } from "react";
import Loader from "../../../../images/Loader";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 550,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const DocumentOpenPopup = ({ modalValue }) => {
    const [loader, setLoader] = useState(false);
    useEffect(() => {
        setLoader(true)
        setTimeout(() => {
            setLoader(false);
        }, 1000);
    }, [modalValue]);

    return (
        <Box sx={style}>
            {loader && <Loader />}
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{`Images`}</h2>
                </div>
                <div className={'document_images_section'}>
                    {
                        !loader &&
                        <img src={modalValue?.front} alt={'document img'} />
                    }
                </div>
                {
                    modalValue?.back &&
                    <div className={'document_images_section'}>
                        {
                            !loader &&
                            <img src={modalValue?.back} alt={'document img'} />
                        }
                    </div>
                }

            </div>
        </Box>
    )
}
export default DocumentOpenPopup