import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import PopComponent from "../../../../hoc/PopContent";
import TableCell from "@mui/material/TableCell";
import {ActionFunction} from "../../../../utils";
import moment from "moment";
import {getUserKYCList} from "../../../../Redux/user/action";
import Box from "@mui/material/Box";
import Loader from "../../../../images/Loader";
import Paper from "@mui/material/Paper";
import MainCommonFilter from "../../../../Components/MainCommonFilter";
import CustomTable from "../../../../hoc/CommonTable";
import CommonModal from "../../../../hoc/CommonModal";

const UserKYCListTab = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 })
    let Modal = PopComponent[modalDetails.modalName];
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
        exportFileName: 'Export File'
    });

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'ApprovedKYCPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'RejectedKYCPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'UpdateUserKYCPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'DocumentOpenPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const columns = [
        {
            id: 'numericId',
            numeric: false,
            disablePadding: false,
            label: 'Users ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className='edit_btn' onClick={() => navigate(`/users-tab/${row?._id}`)}>{`UID000${row?.numericId || 1}`}</span></TableCell>
            }
        },
        {
            id: 'userName',
            numeric: true,
            disablePadding: false,
            label: 'User Name',
        },
        {
            id: 'panCardNumber',
            label: 'Pan Card Number',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{(row?.panCardImage === null || row?.panCardImage === '') ? '-' : row?.panCardNumber}</TableCell>
            }

        },
        {
            id: '',
            label: 'Pan Card Photo',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell>
                    {
                        (row?.panCardImage === null || row?.panCardImage === '') ? '-' :
                            <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('DocumentOpenPopup', { front: row?.panCardImage })}>View</span>
                    }

                </TableCell>
            }
        },
        {
            id: 'aadharCardNumber',
            label: 'Aadhar Card Number',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{(row?.aadharCardNumber === null || row?.aadharCardNumber === '') ? '-' : row?.aadharCardNumber}</TableCell>
            }
        },
        {
            id: 'AadharCardPhoto',
            label: 'Aadhar Card Photo',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell>
                    {
                        (row?.aadharCardNumber === null || row?.aadharCardNumber === '') ? '-' :
                            <span className='edit_btn edit-btn-action u_border' onClick={() => handleOpenModal('DocumentOpenPopup', { front: row?.aadharCardFrontImage, back: row?.aadharCardBackImage })}>View</span>
                    }
                </TableCell>
            }
        },
        {
            id: 'aadharCardstatus',
            label: 'Aadhar Card Status',
        },
        {
            id: 'panCardStatus',
            label: 'Pan Card Status',
        },
        ActionFunction('user', {
            id: 'Action',
            label: 'Action',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    {/*<span className='edit_btn edit-btn-action  u_border' onClick={() => handleOpenModal('UpdateUserKYCPopup',row)}>Edit</span>*/}
                    {
                        row?.status === 'Reject' ?
                            <span className='edit_btn edit-btn-action prTab' onClick={() => handleOpenModal('ApprovedKYCPopup', { userKYCId: row?._id, isApprove: false, isApprovedUser: true })}>Approve</span>
                            :
                            (row?.status === 'Approve' || row?.aadharCardstatus === 'Verify' || row?.aadharCardstatus !== 'Approve') ?
                                "-"
                                :
                                (row?.aadharCardNumber && row?.aadharCardstatus !== 'Verify' && row?.aadharCardstatus !== 'Approve') ?
                                    <>
                                        <span className='edit_btn edit-btn-action u_border prTab' onClick={() => handleOpenModal('ApprovedKYCPopup', { userKYCAadharCardId: row?._id, isApprove: true })}>Aadhar Card Approve</span>
                                        <span className='edit_btn edit-btn-action prTab ' onClick={() => handleOpenModal('RejectedKYCPopup', { userKYCAadharCardId: row?._id, isApprove: false })}>Aadhar Card Reject</span>

                                    </>
                                    : "-"

                    }
                </TableCell>
            }
        })
    ];

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getUserKYCListDetails(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload]);

    const getUserKYCListDetails = (startDate, endDate, search) => {
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate:
                startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload
        }
        setLoader(true);
        dispatch(getUserKYCList(payload)).then(res => {
            setLoader(false)
            if (res?.data?.data?.filePath) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            } else {
                if (res.data?.success) {
                    setRowData({
                        ...rowData,
                        list: res?.data?.data?.docs,
                        totalDocs: res?.data?.data?.totalDocs
                    })
                } else {
                    setRowData({
                        ...rowData,
                        list: [],
                        totalDocs: 0
                    })
                }
            }

        });
    };

    useEffect(() => {
        if (filterData.startDate && filterData.endDate) {
            setPagination({
                ...pagination,
                page: 0
            })
            getUserKYCListDetails(filterData.startDate, filterData.endDate)
        }
    }, [filterData.startDate, filterData.endDate]);

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={getUserKYCListDetails} pagination={pagination} setPagination={setPagination} />
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getUserKYCListDetails} />
            </CommonModal>
        </Box>
    )
}
export default UserKYCListTab