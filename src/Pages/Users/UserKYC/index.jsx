import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import TabPanel from "../../../Components/TabPanel";
import UserKYCListTab from "./UserKYCListTab";
import UserKYCRequest from "./UserKYCRequest";

const UserKYC = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const a11yProps = (index) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    };
    return(
        <Box sx={{ width: '100%' }} className={'user_details_tab'}>
            <Box className="bg_white" sx={{ borderBottom: 1, borderColor: 'divider' }} pl={3} >
                <Tabs value={value} onChange={handleChange} className={'bg_white_tab'} scrollButtons="auto" variant="scrollable">
                    <Tab className={'tab_title'} label="User KYC List" {...a11yProps(0)} />
                    <Tab className={'tab_title'} label="User KYC  Request" {...a11yProps(1)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0} >
                <UserKYCListTab/>
            </TabPanel>
            <TabPanel value={value} index={1}>
               <UserKYCRequest/>
            </TabPanel>
        </Box>
    )
}
export default UserKYC;