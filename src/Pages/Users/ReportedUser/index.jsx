import React, { useEffect, useState } from "react";
import Loader from "../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import CommonModal from "../../../hoc/CommonModal";
import Box from "@mui/material/Box";
import { useDispatch } from "react-redux";
import PopComponent from "../../../hoc/PopContent";
import moment from "moment";
import { userReportedDetailsData } from "../../../Redux/user/action";
import TableCell from "@mui/material/TableCell";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import { currencyFormat } from "../../../utils";
import { useNavigate } from "react-router-dom";

const ReportedUser = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [loader, setLoader] = useState(false);
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });
    let Modal = PopComponent[modalDetails.modalName];
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
        exportFileName: 'Export File'
    });


    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getUserReportedListData(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.startDate, filterData.endDate]);

    //get Api details code
    const getUserReportedListData = (startDate, endDate, search) => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate:
                startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload
        }
        dispatch(userReportedDetailsData(payload)).then(res => {
            setLoader(false)

            if (res?.data?.data?.filePath && res.data.success) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            }
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res.data.data?.docs,
                    totalDocs: res.data.data.totalDocs
                });
            }
        })
    };

    useEffect(() => {
        if (filterData.startDate && filterData.endDate && filterData?.statusValue === 'Custom') {
            setPagination({
                ...pagination,
                page: 0
            })
            getUserReportedListData(filterData.startDate, filterData.endDate)
        }
    }, [filterData.startDate, filterData.endDate]);

    // table columns
    let columns = [
        {
            id: 'numericId',
            numeric: false,
            disablePadding: false,
            label: 'Users ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className='edit_btn' onClick={() => navigate(`/users-tab/${row.id}`)}>{`UID000${row?.numericId}`}</span></TableCell>
            }
        },
        {
            id: 'userName',
            label: 'User Name',

        },
        {
            id: 'mobile',
            label: 'Mobile',
        },
        {
            id: 'deviceType',
            label: 'Device Type',
        },
        {
            id: 'bonus',
            label: 'Bonus',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.bonus)}</TableCell>
            }
        },
        {
            id: 'winCash',
            label: 'Win Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.winCash)}</TableCell>
            }
        },
        {
            id: 'depositsCash',
            label: ' Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.cash)}</TableCell>
            }
        },
        {
            id: '',
            label: 'Reported List',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >
                    <span className='edit_btn edit-btn-action' onClick={() => handleOpenModal('ViewReportedUserList', row?._id)}>View</span>
                </TableCell>
            }
        },
    ];

    // custom PopUp function
    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'BlockUser': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewReportedUserList': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={getUserReportedListData} pagination={pagination} setPagination={setPagination} />
                {/*------------------ CustomTable ---------------------*/}
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            {/*--------------------------------------------------------Common Popup-------------------------------------------------*/}
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default ReportedUser