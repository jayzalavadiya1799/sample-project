import React, { useEffect, useState } from 'react';
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import moment from "moment";
import TableCell from "@mui/material/TableCell";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import { currencyFormat } from "../../../utils";
import { userPaymentHistoryList } from "../../../Redux/user/action";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import Loader from "../../../images/Loader";
import Box from "@mui/material/Box";


const PaymentHistory = () => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const [loader, setLoader] = useState(false);
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });

    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        filterClose: false,
        exportFileName: 'Export File'
    });
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getUserReportedListData(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.startDate, filterData.endDate]);

    //get Api details code
    const getUserReportedListData = (startDate, endDate, search) => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate:
                startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload,
            userId: id
        }
        dispatch(userPaymentHistoryList(payload)).then(res => {
            setLoader(false)
            if (res?.data?.data?.filePath && res.data.success) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            }
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res.data.data?.docs,
                    totalDocs: res.data.data.totalDocs
                });
            }
        })
    };

    const columns = [
        {
            id: 'date',
            numeric: true,
            disablePadding: true,
            label: 'Date',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.date).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        {
            id: 'orderId',
            numeric: true,
            disablePadding: false,
            label: 'Order Id',
        },
        {
            id: 'previousWinCash',
            disablePadding: false,
            label: 'Previous Win Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell > {currencyFormat(row?.previousWinCash)} </TableCell>
            }
        },
        {
            id: 'amount',
            disablePadding: false,
            label: 'Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.amount)}</TableCell>
            }
        },
        {
            id: 'currentWinCash',
            disablePadding: false,
            label: 'Current Win Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.currentWinCash)}</TableCell>
            }
        },
        {
            id: 'status',
            disablePadding: false,
            label: 'Status',
            type: 'custom',
            render: (row) => {
                return <TableCell ><span className={'status_field'}>{row?.status}</span></TableCell>
            }
        }
    ];

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper className="outerbox">
                <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={''} pagination={pagination} setPagination={setPagination} />
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
        </Box>

    )
}
export default PaymentHistory