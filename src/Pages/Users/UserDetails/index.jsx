import React, { useState } from 'react';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@mui/material/Box';
import { getAllCountriesRestrictGeo } from "../../../Redux/settings/action";
import UpdateUserInfo from "./UpdateUserInfo";
import UserInfo from "./UserInfo";

const UserDetails = ({ handleOpenModal }) => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const userProfile = useSelector(state => state.userReducer.userProfile);
    const [editUser, setEditUser] = useState(false);

    useEffect(() => {
        dispatch(getAllCountriesRestrictGeo({}))
    }, []);

    const editUserDetailsHandler = () => {
        setEditUser(true);
    };

    return (
        <Box className="outerbox bg_white box_shadow radius_8">
            {
                !editUser ?
                    <UserInfo userProfile={userProfile} editUserDetailsHandler={editUserDetailsHandler} handleOpenModal={handleOpenModal} />
                    :
                    <UpdateUserInfo id={id} setEditUser={setEditUser} handleOpenModal={handleOpenModal} userProfile={userProfile} />
            }
        </Box>
    )
}


export default UserDetails
