import { currencyFormat, generateAvatar, hideActionFunc } from "../../../../utils";
import React from "react";

const UserInfo = ({ userProfile, editUserDetailsHandler, handleOpenModal }) => {

    return (
        <div className={'user_details_inner_section'}>
            <div className={'user_details_inner_profile'}>
                <div className={'profile'}>
                    <img src={userProfile?.profileImage ? userProfile?.profileImage : generateAvatar(`${userProfile.fullName}`)} className={'fontFamily'} alt={'profile'} />
                    <h2 className={'fontFamily'}>{userProfile?.fullName}</h2>
                </div>
                {
                    hideActionFunc('user') &&
                    <div className={'last_section'}>
                        <button onClick={() => editUserDetailsHandler()}>Edit</button>
                    </div>
                }

            </div>
            <div className={'user_details_section'}>
                <div className={'user_inner_div'}>
                    <div className={'let_section'}>
                        <div className={'personal_information_content'}>
                            <div className={'form_data_row'}>
                                <div className={'form_data'}>
                                    <h6>User Name</h6> :
                                    <p> {userProfile?.fullName}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Email</h6> :
                                    <p>{userProfile?.email}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Contact Number</h6> :
                                    <p>{userProfile?.phoneNumber}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Country</h6> :
                                    <p>{userProfile?.country}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Device Type</h6> :
                                    <p>{userProfile?.deviceType}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={'let_section middle_section'}>
                        <div className={'personal_information_content'}>
                            <div className={'form_data_row'}>
                                <div className={'form_data'}>
                                    <h6>Pan Card Name</h6> :
                                    <p> {userProfile?.fullName}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Pan Card number</h6> :
                                    <p>{'PAN0000895'}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Aadhaar Card Name</h6> :
                                    <p>{userProfile?.fullName}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Aadhaar Card Number</h6> :
                                    <p>{'ADdfdkfd4554666'}</p>
                                </div>
                                {/*<div className={'form_data'}>*/}
                                {/*    <h6>Device Type</h6> :*/}
                                {/*    <p>{userProfile?.deviceType}</p>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </div>
                    <div className={'right_section'}>
                        <div className={'personal_information_content'}>
                            <div className={'form_data_row'}>
                                <div className={'form_data'}>
                                    <h6>Winning</h6> :
                                    <p>{currencyFormat(userProfile?.winCash)}</p>
                                </div>
                                <div className={'form_data user_field_content_row'}>
                                    <div className={'user_field_content_flex'}>
                                        <h6>Deposit Cash</h6> :
                                        <p>{currencyFormat(userProfile?.cash)}</p>
                                    </div>
                                    {
                                        hideActionFunc('user') &&
                                        <div className={'user_btn_details'}>
                                            <button className={'update_bonus_btn'} onClick={() => handleOpenModal('UpdateCashAndBonus', { isModalCash: true, id: userProfile?._id })}>+ Add Deposit Cash</button>
                                        </div>
                                    }

                                </div>
                                <div className={'form_data user_field_content_row'}>
                                    <div className={'user_field_content_flex'}>
                                        <h6>Bonus</h6> :
                                        <p>
                                            <span>{currencyFormat(userProfile?.bonus)}</span>
                                        </p>
                                    </div>
                                    {
                                        hideActionFunc('user') &&
                                        <div className={'user_btn_details'}>
                                            <button className={'update_bonus_btn'} onClick={() => handleOpenModal('UpdateCashAndBonus', { isModalCash: false, id: userProfile?._id })}>+ Add Bonus</button>
                                        </div>
                                    }

                                </div>
                                <div className={'form_data'}>
                                    <h6>Total Cash</h6> :
                                    <p>{currencyFormat(userProfile?.totalCash)}</p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Total Deposits</h6> :
                                    <p> {currencyFormat(userProfile?.totalDeposits)} </p>
                                </div>
                                <div className={'form_data'}>
                                    <h6>Total Withdrawals</h6> :
                                    <p>{currencyFormat(userProfile?.totalWithdrawals)}</p>
                                </div>
                                {/*<div className={'form_data user_field_content_row'}>*/}
                                {/*    <div className={'user_field_content_flex'}>*/}
                                {/*        <h6>Total Coins </h6> :*/}
                                {/*        <p>{currencyFormat(userProfile?.coins)}</p>*/}
                                {/*    </div>*/}
                                {/*    {*/}
                                {/*        hideActionFunc( 'user') &&*/}
                                {/*        <div className={'user_btn_details'}>*/}
                                {/*            <button className={'update_bonus_btn'} onClick={() => handleOpenModal('AddCoinPopup', { id: userProfile?._id })}>+ Add Coins</button>*/}
                                {/*        </div>*/}
                                {/*    }*/}

                                {/*</div>*/}
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}
export default UserInfo