import Avatar from "@mui/material/Avatar";
import EditIcon from "@mui/icons-material/Edit";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import React, { useCallback, useEffect, useRef, useState } from "react";
import SimpleReactValidator from "simple-react-validator";
import { useDispatch } from "react-redux";
import { updateUserProfile } from "../../../../Redux/user/action";
import FilledButton from "../../../../Components/FileButton";
import CountryDropdown from "../../../Settings/RestrictGeo/AddRestrictGeo/CountryDropdown";
import { getCountriesRestrictGeo } from "../../../../Redux/settings/action";

const UpdateUserInfo = ({ id, setEditUser, handleOpenModal, userProfile }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const [countryList, setCountryList] = useState({ country: [] });
    const simpleValidator = useRef(new SimpleReactValidator());
    const [formData, setFormData] = useState({ fullName: '', userId: id, country: '', isProfileImageUpdated: false, phoneNumber: '', profileImage: '', profileURL: '' });
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);

    useEffect(() => {
        dispatch(getCountriesRestrictGeo({})).then(res => {
            setCountryList({ ...countryList, country: res.data.data?.map((item) => { return { value: item?.name, label: item?.name, isoCode: item?.isoCode } }) || [] })
        })
    }, []);

    const closeModalError = () => {
        setEditUser(false)
    }
    const userImageChange = e => {
        const newImg = URL.createObjectURL(e.target.files[0]);
        const fileList = Math.round(e.target.files[0].size / 1024);
        if (fileList >= 2048) {
            handleOpenModal('CommonPop', { header: "Info", body: 'Please upload a file smaller than 2 MB' })
            return false;
        }
        else {
            setFormData({
                ...formData,
                profileImage: e.target.files[0],
                profileURL: newImg,
                isProfileImageUpdated: true
            })
        }
        return true;
    };

    const userDetailsSubmitHandler = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            updateUserInfoDetails()
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };
    const updateUserInfoDetails = () => {
        let payload = {
            ...formData,
            country: formData?.country
        }
        delete payload.profileURL;
        setLoader(true)
        if (!formData?.profileImage) { delete payload.profileImage }
        dispatch(updateUserProfile(payload)).then(res => {
            if (res.data.statusCode === 200 && res.data.success) {
                setLoader(false)
                handleOpenModal('CommonPop', { header: "Success", body: res.data.message, modalCloseHandle: setEditUser(false) })
            } else {
                setLoader(false)
                handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
            }
        })
    }
    const handleChange = (e) => {
        const { value, name } = e.target;
        setFormData({
            ...formData,
            [name]: value
        })
    }


    useEffect(() => {
        if (Object.keys(userProfile)?.length > 0) {
            setFormData({
                ...formData,
                fullName: userProfile.fullName,
                email: userProfile?.email,
                country: userProfile?.country,
                phoneNumber: userProfile?.phoneNumber
            })
        }
    }, [userProfile])

    return (
        <div className={'user-profile-tab user_info_edit'}>
            <div className='user-img'>
                <div className='user-profile'>
                    <Avatar className={'avatar_img_user'} src={formData?.profileURL ? formData?.profileURL : userProfile?.profileImage ? userProfile?.profileImage : "/broken-image.jpg"} />
                </div>
                <div className='EditIcon'>
                    <div className="input_file  d_flex_only">
                        <input type="file" id="file"
                            onChange={(e) => userImageChange(e)}
                            accept="png, jpeg" />
                        <span className='edit_icon' >
                            <EditIcon />
                        </span>
                    </div>
                </div>
            </div>
            <form onSubmit={(e) => userDetailsSubmitHandler(e)}>
                <div className='user-details pl_15 '>
                    <>
                        <div className={"text"}>
                            <div className=' sd_flex '>
                                <Box component="b" className={'fontFamily'}>  Full Name  </Box> :
                                <Box ml={2}>
                                    <TextField size="small" type="text" placeholder={'Enter Full Name'} className={'fontFamily'} value={formData.fullName} name="fullName" onChange={(e) => handleChange(e)} />
                                </Box>
                            </div>
                            {simpleValidator.current.message("fullName", formData?.fullName, "required")}
                        </div>
                        <div>
                            <div className='text sd_flex '>
                                <Box component="b" className={'fontFamily'}>Email</Box>  :
                                <Box ml={2}>
                                    <TextField size="small" type="text" className={'fontFamily'} placeholder={'Enter Email'} value={formData.email} name="email" onChange={(e) => handleChange(e)} />
                                    {simpleValidator.current.message("email", formData?.email, "required|email")}
                                </Box>
                            </div>
                        </div>
                        <div className='text sd_flex country-user-details'>
                            <Box component="b" className={'fontFamily'} >Country  </Box>  :
                            <Box ml={2} className={'select-dropdown-userInfo'}>
                                <div className="formData restrict_geo_section">
                                    <div className="emailWrap">
                                        <CountryDropdown options={countryList?.country} name={'country'} formData={formData} setFormData={setFormData} />
                                    </div>
                                    {simpleValidator.current.message("country", formData?.country, "required")}
                                </div>
                            </Box>
                        </div>
                        <div className='text sd_flex '>
                            <Box component="b" className={'fontFamily'} >Mobile No   </Box> :
                            <Box ml={2}>
                                <TextField size="small" className={'fontFamily'} placeholder={'Enter Mobile No'} type="text" value={formData?.phoneNumber} name="phoneNumber" onChange={(e) => handleChange(e)} />
                                {simpleValidator.current.message("phoneNumber", formData?.phoneNumber, "required")}
                            </Box>
                        </div>
                    </>
                    <div className='sd_flex edit_button'>
                        <FilledButton type={'Submit'} value={'Update'} loading={loader} className={'submit_btn loader_css'} />
                        <Button variant="contained" type={"reset"} onClick={() => closeModalError()}>Cancel</Button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default UpdateUserInfo;