import React, { useEffect, useState } from "react";
import TableCell from "@mui/material/TableCell";
import { ActionFunction } from "../../../../utils";
import Box from "@material-ui/core/Box";
import CustomTable from "../../../../hoc/CommonTable";
import { getUserBlockListDetails } from "../../../../Redux/user/action";
import { useDispatch } from "react-redux";
import CommonModal from "../../../../hoc/CommonModal";
import PopComponent from "../../../../hoc/PopContent";
import Loader from "../../../../images/Loader";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 950,
    bgcolor: 'white',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const UserBlockListView = ({ modalValue, redirectApiHandler }) => {
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });
    const handleOpenModalView = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'BlockUser': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'UnblockUserPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };
    const columns = [
        {
            id: 'id',
            label: 'Users ID',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className=''>{`UID000${row?.blockUserId?.numericId}`}</span></TableCell>
            }
        },
        {
            id: 'userName',
            label: 'User Name',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.blockUserId?.fullName}</TableCell>
            }
        },
        {
            id: 'mobileNumber',
            label: 'Mobile Number',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.blockUserId?.phoneNumber}</TableCell>
            }
        },
        ActionFunction('user', {
            id: 'Action',
            label: 'Action',
            type: 'custom',
            isDisbanding: true,
            render: (row) => {
                return <TableCell >
                    <span className='edit_btn edit-btn-action' onClick={() => handleOpenModalView('UnblockUserPopup', { blockUserId: row?.blockUserId?._id })}>Unblock</span>
                </TableCell>
            }
        })
    ];

    useEffect(() => {
        getUserBlockDetailsHandler()
    }, [pagination.rowsPerPage, pagination.page]);

    // get User Api and All Filter Api
    const getUserBlockDetailsHandler = () => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            userId: modalValue?.id
        }
        dispatch(getUserBlockListDetails(payload)).then(res => {
            setLoader(false)
            if (res.data?.success) {
                setRowData({
                    ...rowData,
                    list: res.data.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                })
            } else {
                setRowData({
                    ...rowData,
                    list: [],
                    totalDocs: 0
                })
            }
        })
    };

    return (
        <Box sx={style} >
            {loader ? <Loader /> : ""}
            <CustomTable
                headCells={columns}
                rowData={rowData?.list}
                totalDocs={rowData?.totalDocs}
                pagination={pagination}
                setPagination={setPagination}
            />
            {/*--------------------------------------------------------Common Popup-------------------------------------------------*/}
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModalView}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModalView} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getUserBlockDetailsHandler} />
            </CommonModal>
        </Box>
    )
}
export default UserBlockListView