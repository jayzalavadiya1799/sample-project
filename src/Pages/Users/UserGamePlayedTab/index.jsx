import React, { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import CustomTable from '../../../hoc/CommonTable';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getUserPlayedGamesList } from '../../../Redux/user/action';
import Loader from "../../../images/Loader";
import TableCell from "@mui/material/TableCell";


const UserGamePlayedTab = ({ handleOpenModal }) => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const [loader, setLoader] = useState(false)
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 })
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [filterData, setFilterData] = useState({
        statusField: 'All Status'
    })
    useEffect(() => {
        getAllGamePlayedList()
    }, [pagination.rowsPerPage, pagination.page, filterData?.statusField])

    const getAllGamePlayedList = () => {
        let payload = {
            userId: id,
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        }
        setLoader(true);
        dispatch(getUserPlayedGamesList(payload)).then(res => {
            setLoader(false);
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                })
            }
        })
    }

    const columns = [
        {
            id: 'numericId',
            numeric: true,
            disablePadding: false,
            label: 'Game ID',
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className=''>{`GID000${row?.numericId}`}</span></TableCell>
            }
        },
        {
            id: 'gameName',
            numeric: true,
            disablePadding: true,
            label: 'Game Name',
        },
        {
            id: 'tableId',
            numeric: true,
            disablePadding: false,
            label: 'Table ID',
        },
        {
            id: 'gameType',
            numeric: true,
            disablePadding: false,
            label: 'Game Type',
        },
        {
            id: 'userCount',
            disablePadding: false,
            label: 'User Count',
        },
        {
            id: 'Action',
            label: 'Action',
            disablePadding: false,
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell ><span className='edit_btn edit-btn-action' onClick={() => handleOpenModal('ViewGamePlayedHistory', { id: row?._id })}> View </span></TableCell>
            }
        }
    ];

    return (
        <div>
            {
                loader &&
                <Loader />
            }
            <Paper className="outerbox">
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
        </div>
    )
}
export default UserGamePlayedTab