import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import Paper from '@mui/material/Paper';
import { userDetailGameStatistics } from '../../../Redux/user/action';
import CustomTable from '../../../hoc/CommonTable';
import Loader from "../../../images/Loader";


const UserGameStatistics = () => {
    const dispatch = useDispatch();
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [loader, setLoader] = useState(false)
    const { id } = useParams();
    const columns = [
        {
            id: 'gameName',
            numeric: true,
            disablePadding: false,
            label: 'Game Name',
        },
        {
            id: 'headToHead',
            numeric: true,
            disablePadding: false,
            twoLineText: true,
            label: 'Head to Head </br> (One Vs One)',
        },
        {
            id: 'contest',
            numeric: true,
            disablePadding: false,
            twoLineText: true,
            label: 'Contest </br> (One Vs Many)',
        },
        {
            id: 'totalLoss',
            numeric: true,
            disablePadding: false,
            label: 'Total Loss',
        },
        {
            id: 'totalWin',
            disablePadding: false,
            label: 'Total Win',
        },
        {
            id: 'totalTie',
            disablePadding: false,
            label: 'Total Tie ',
        },
        {
            id: 'totalPlayed',
            numeric: true,
            disablePadding: false,
            label: 'Total Played ',
        },
    ];

    useEffect(() => {
        getGameStatisticList();
    }, [pagination.rowsPerPage, pagination.page]);

    const getGameStatisticList = () => {
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            userId: id
        };
        setLoader(true);
        dispatch(userDetailGameStatistics(payload)).then(res => {
            setLoader(false)
            setRowData({
                ...rowData,
                list: res?.data?.data?.docs,
                totalDocs: res?.data?.data?.totalDocs
            })
        })
    };

    return (
        <>
            {
                loader &&
                <Loader />
            }
            <Paper className="outerbox">
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
        </>
    )
}
export default UserGameStatistics