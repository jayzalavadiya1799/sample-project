import React, { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import CustomTable from '../../../hoc/CommonTable';
import { userDetailTransactions } from '../../../Redux/user/action';
import TableCell from "@mui/material/TableCell";
import moment from "moment";
import Loader from "../../../images/Loader";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import { decimalGenerate } from "../../../utils";


const UserTransition = ({ handleOpenModal }) => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const [loader, setLoader] = useState(false)
    const [filterData, setFilterData] = useState({
        startDate: moment().subtract(7, "days").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        statusValue: "Last 7 Days",
        exportFile: false,
        csvDownload: false,
        search: "",
        exportFileName: 'Export File'
    })
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    })
    const [userTransactions, setUserTransactions] = useState({
        limit: 5,
        totalDocs: 0,
        list: []
    })
    //AD_HB
    const columns = [
        {
            id: 'date',
            numeric: true,
            disablePadding: true,
            label: 'Date',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.date).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        {
            id: 'numericId',
            numeric: true,
            disablePadding: false,
            label: 'Order Id',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{`AD_HB000${row?.numericId}`}</TableCell>
            }
        },
        {
            id: 'gameName',
            numeric: true,
            disablePadding: false,
            label: 'Game Name',
            type: 'custom',
            render: (row, i) => {
                return <TableCell>{row?.gameName ? row?.gameName : '-'}</TableCell>
            }
        },
        {
            id: 'totalPreviousCash',
            disablePadding: false,
            label: 'Previous Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >${row?.totalPreviousCash}{decimalGenerate(row?.totalPreviousCash)}</TableCell>
            }
        },
        {
            id: 'amount',
            disablePadding: false,
            label: 'Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >${row?.amount}{decimalGenerate(row?.amount)}</TableCell>
            }
        },
        {
            id: 'totalCurrentWinCash',
            disablePadding: false,
            label: 'Current Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >${row?.totalCurrentWinCash}{decimalGenerate(row?.totalCurrentWinCash)}</TableCell>
            }
        },
        {
            id: 'status',
            disablePadding: false,
            label: 'Status',
            type: 'custom',
            render: (row) => {
                return <TableCell ><span className={'status_field'}>{row?.status}</span></TableCell>
            }
        },

        {
            id: 'Action',
            label: 'Action',
            disablePadding: false,
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                let temp = [];
                temp.push(row)
                return <TableCell ><span className='edit_btn edit-btn-action' onClick={() => handleOpenModal('ViewTransitionHistory', temp)}> View </span></TableCell>
            }
        }
    ];

    useEffect(() => {
        if (filterData.statusValue !== 'Custom') {
            getTransitionList(filterData.startDate, filterData.endDate)
        }

    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.statusValue])
    useEffect(() => {
        if (filterData.startDate && filterData.endDate) {
            setPagination({
                ...pagination,
                page: 0
            })
            getTransitionList(filterData.startDate, filterData.endDate)
        }
    }, [filterData.startDate, filterData.endDate]);

    const getTransitionList = (startDate, endDate, search) => {
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            startDate:
                startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload,
            userId: id
        }
        setLoader(true)
        dispatch(userDetailTransactions(payload)).then(res => {
            if (res.data.success) {
                setLoader(false)
                if (res?.data?.data?.filePath) {
                    setFilterData({ ...filterData, csvDownload: false, exportFile: false })
                    window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
                } else {
                    let payload = res.data.data
                    setUserTransactions({
                        ...userTransactions,
                        totalDocs: payload.totalDocs,
                        list: payload.docs
                    })
                }

            }
        })
    }


    return (
        <>
            {loader ? <Loader /> : ""}
            <Paper className="outerbox">
                <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={getTransitionList} pagination={pagination} setPagination={setPagination} />
                <CustomTable
                    headCells={columns}
                    rowData={userTransactions?.list}
                    totalDocs={userTransactions?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
        </>
    )
}


export default UserTransition