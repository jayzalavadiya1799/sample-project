import React, { useEffect, useState } from "react";
import Loader from "../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import { Box } from "@mui/material";
import PopComponent from "../../../hoc/PopContent";
import CommonModal from "../../../hoc/CommonModal";
import moment from "moment";
import TableCell from "@material-ui/core/TableCell";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import { ActionFunction, currencyFormat, dotGenerator } from "../../../utils";
import { getWithdrawRequests } from "../../../Redux/user/action";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const UserWithdrawalRequests = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [loader, setLoader] = React.useState(false);
    const [filterData, setFilterData] = useState({ search: "", filterClose: false, platformName: 'All Payout Method', statusField: 'All Status' });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    let Modal = PopComponent[modalDetails.modalName];

    const columns = [
        {
            id: 'userId',
            numeric: false,
            disablePadding: false,
            isDisbanding: true,
            label: 'Users ID',
            type: 'custom',
            render: (row) => {
                return <TableCell><span className='edit_btn' onClick={() => navigate(`/users-tab/${row?.userId?._id}`)}>{`UID000${row?.userId?.numericId}`}</span></TableCell>
            }
        },
        {
            id: 'orderId',
            numeric: true,
            disablePadding: false,
            label: 'Order Id',
        },
        {
            id: 'userName',
            numeric: true,
            disablePadding: false,
            label: 'Users Name',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell>{row?.userId?.fullName}</TableCell>
            }
        },

        {
            id: 'amount',
            disablePadding: false,
            label: 'Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.amount)}</TableCell>
            }
        },

        {
            id: 'payoutMethod',
            disablePadding: false,
            label: 'Payment Method',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell>{row?.razorpayAccountId?.payoutMethod === 'bank_account' ? 'Bank Account' : row?.razorpayAccountId?.payoutMethod === 'vpa' ? 'UPI' : ''}</TableCell>
            }
        },

        {
            id: 'createdAt',
            disablePadding: false,
            label: 'Create Date',
            type: "custom",
            render: (row) => {
                return <TableCell >{moment(row?.createdAt).format("MMM DD YYYY, HH:MM A")}</TableCell>
            }
        },
        {
            id: 'status',
            disablePadding: false,
            label: 'Status',
        },
        {
            id: 'description',
            numeric: true,
            disablePadding: false,
            label: 'Reject Reason',
            type: 'custom',
            isDisbanding: true,
            render: (row) => {
                return <TableCell >{row?.rejectReason ? dotGenerator(row?.rejectReason, handleOpenModal, 'Withdrawal Request Reject Reason') : ''}</TableCell>
            }
        },
        ActionFunction('user', {
            id: 'Action',
            numeric: true,
            disablePadding: false,
            label: 'Action',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell className={'role_field_id'}>
                    {
                        row?.approveStatus === "approve" ?
                            <span className={"CompletedMsg"}> Completed </span>
                            :
                            row?.approveStatus === "reject" ?
                                <span className={"invalid-feedback pl-4 d-block errorMsgReject"}>Reject</span>
                                :
                                <>
                                    <Box component="span" mr={1} className="link_color pointer u_border" onClick={() => handleOpenModal('ApprovedWithdrawalRequest', { withdrawalId: row?._id, isApprove: true })}>Approve</Box>
                                    <Box component="span" className="link_color pointer prTab" onClick={() => handleOpenModal('RejectedPopup', { withdrawalId: row?._id, isApprove: false, isUserWithdraw: true, isDelete: true, isUser: true })}>Reject</Box>
                                </>
                    }
                </TableCell>
            }
        })
    ];

    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'RejectedPopup': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ApprovedWithdrawalRequest': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewRejectedComment': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getWithdrawRequestsList(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.statusField, filterData.platformName]);

    // get User Api and All Filter Api
    const getWithdrawRequestsList = (startDate, endDate, search) => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            searchText: search,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload,
            payoutMethod: filterData.platformName === 'All Payout Method' ? null : filterData.platformName === 'Bank Transfer' ? 'bank_account' : 'vpa',
            status: filterData.statusField === 'All Status' ? null : filterData.statusField
        }

        dispatch(getWithdrawRequests(payload)).then(res => {
            setLoader(false)
            if (res?.data?.data?.filePath) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            } else {
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                })
            }
        })
    };

    return (
        <>
            {
                loader &&
                <Loader />
            }
            <Paper sx={{ mb: 2 }} className="outerbox">
                <MainCommonFilter
                    filterData={filterData}
                    setFilterData={setFilterData}
                    searchApiHandler={getWithdrawRequestsList}
                    pagination={pagination}
                    setPagination={setPagination}
                    statusOption={['All Status', 'processing', 'processed', 'cancelled', 'reversed']}
                    plateFormOption={['All Payout Method', 'Bank Transfer', 'UPI']}
                    addPropsFilter={{ isGameList: true, isWithdrawal: true }}
                />
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getWithdrawRequestsList} YesNoText={{ yes: "Yes", no: "Cancel" }} />
            </CommonModal>
        </>
    )
}
export default UserWithdrawalRequests