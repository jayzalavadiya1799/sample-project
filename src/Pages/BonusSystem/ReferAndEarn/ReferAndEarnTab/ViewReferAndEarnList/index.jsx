import Box from "@material-ui/core/Box";
import CustomTable from "../../../../../hoc/CommonTable";
import React, { useEffect, useState } from "react";
import TableCell from "@mui/material/TableCell";
import { getReferAndEarnListingView } from "../../../../../Redux/Master/action";
import { useDispatch } from "react-redux";
import Loader from "../../../../../images/Loader";
import { currencyFormat } from "../../../../../utils";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 950,
    bgcolor: 'white',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const ViewReferAndEarnList = ({ modalValue }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });

    useEffect(() => {
        getReferAndEarnView()
    }, [modalValue]);

    const getReferAndEarnView = () => {
        setLoader(true);
        let payload = {
            ...modalValue,
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        };
        dispatch(getReferAndEarnListingView(payload)).then(res => {
            setLoader(false)
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res.data.data?.docs,
                    totalDocs: res.data.data.totalDocs || 0
                })
            } else {
                setRowData([])
            }
        })
    }

    const columns = [
        {
            id: 'ReferralUserId',
            label: 'Referral UserId',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell><span className=''>{`UID000${row?.userId?.numericId}`}</span></TableCell>
            }
        },
        {
            id: '',
            numeric: true,
            disablePadding: false,
            label: 'Referral Username',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell> {row?.userId?.fullName}</TableCell>
            }
        },
        {
            id: 'refUserBonus',
            label: 'Referral Bonus',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.refUserBonus)}</TableCell>
            }
        },
    ];
    return (
        <Box sx={style} >
            {loader ? <Loader /> : ""}
            <CustomTable
                headCells={columns}
                rowData={rowData?.list}
                totalDocs={rowData?.totalDocs}
                pagination={pagination}
                setPagination={setPagination}
            />
        </Box>
    )
}
export default ViewReferAndEarnList