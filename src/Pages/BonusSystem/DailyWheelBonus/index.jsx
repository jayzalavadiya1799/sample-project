import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { useDispatch } from "react-redux";
import TableCell from "@mui/material/TableCell";
import PopComponent from "../../../hoc/PopContent";
import Loader from "../../../images/Loader";
import CustomTable from "../../../hoc/CommonTable";
import CommonModal from "../../../hoc/CommonModal";
import { getDailyWheelBonusList, getDayDailyWheelBonus } from "../../../Redux/Master/action";
import { ActionFunction, currencyFormat, dotGenerator, hideActionFunc } from "../../../utils";

const DailyWheelBonus = () => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 })
    let Modal = PopComponent[modalDetails.modalName];
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [bonusDays, setBonusDays] = useState({
        isDaysAvailable: '',
        message: ''
    })
    const handleOpenModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'AddDailyWheelBonusPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'ViewRejectedComment': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const columns = [
        {
            id: 'day',
            label: 'Day Name',
            type: 'custom',
            render: (row) => {
                return <TableCell>{row?.day} day</TableCell>
            }
        },
        {
            id: 'spinTitle',
            label: 'Title',
        },
        {
            id: 'spinDescription',
            label: 'Description',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{dotGenerator(row?.spinDescription, handleOpenModal, 'Daily Weel Bonus Description')}</TableCell>
            }
        },
        {
            id: 'bonusCashUpto',
            label: 'Bonus Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.bonusCashUpto)}</TableCell>
            }
        },
        {
            id: 'bonusCoinUpto',
            label: 'Bonus Coin',
        },
        {
            id: 'referralBooster',
            label: 'Referral Booster'
        },
        ActionFunction('bonus', {
            id: 'Action',
            label: 'Action',
            type: 'custom',
            isDisbanding: true,
            render: (row) => {
                return <TableCell >
                    <span className='edit_btn edit-btn-action' onClick={() => handleOpenModal('AddDailyWheelBonusPop', { isEdit: true, row })}>Edit</span>
                </TableCell>
            }
        })
    ];

    useEffect(() => {
        dispatch(getDayDailyWheelBonus()).then(res => {
            setBonusDays({
                ...bonusDays,
                isDaysAvailable: res.data.data?.isDaysAvalible,
                message: res?.data?.message
            })
        })
    }, []);

    useEffect(() => {
        getDailyWheelBonusListDetails()
    }, [pagination.rowsPerPage, pagination.page]);

    const getDailyWheelBonusListDetails = () => {
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        };
        setLoader(true);
        dispatch(getDailyWheelBonusList(payload)).then(res => {
            if (res.data.success) {
                setLoader(false);
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                });
            }
        })
    }
    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                {
                    hideActionFunc('bonus') &&
                    <div className={'admin_user_list'}>
                        <button className={'add_game_btn font-bold'} onClick={bonusDays?.isDaysAvalible ? (e) => handleOpenModal('AddDailyWheelBonusPop') : (e) => handleOpenModal('CommonPop', { header: "Info", body: bonusDays?.message })}> + Add Daily Wheel Bonus</button>
                    </div>
                }

                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModal} modalIsOpen={modalDetails.modalIsOpen} redirectApiHandler={getDailyWheelBonusListDetails} />
            </CommonModal>
        </Box>
    )
}
export default DailyWheelBonus