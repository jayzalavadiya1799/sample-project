import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import DaysFilterDropdown from "./DaysFilterDropdown";
import { useDispatch } from "react-redux";
import FilledButton from "../../../../Components/FileButton";
import { createDailyWheelBonusList, getDayDailyWheelBonus, updateDailyWheelBonusList } from "../../../../Redux/Master/action";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../hoc/PopContent";
import CommonModal from "../../../../hoc/CommonModal";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 580,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const AddDailyWheelBonusPop = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const [bonusDays, setBonusDays] = useState('')
    const [loader, setLoader] = useState(false);
    const [formData, setFormData] = useState({
        day: '',
        spinTitle: '',
        spinDescription: '',
        bonusCashUpto: '',
        bonusCoinUpto: '',
        referralBooster: ''
    });
    const simpleValidator = useRef(new SimpleReactValidator());
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);

    useEffect(() => {
        dispatch(getDayDailyWheelBonus()).then(res => {
            setBonusDays(res.data.data)
        })
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            setLoader(true)
            let payload = {
                ...formData
            }
            dispatch(createDailyWheelBonusList(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }
    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        if (modalValue?.isEdit) {

            const { row } = modalValue;

            setFormData({
                ...formData,
                day: row?.day,
                spinTitle: row?.spinTitle,
                spinDescription: row?.spinDescription,
                bonusCashUpto: row?.bonusCashUpto,
                bonusCoinUpto: row?.bonusCoinUpto,
                referralBooster: row?.referralBooster
            })
        }
    }, [modalValue])
    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            setLoader(true)
            let payload = {
                ...formData,
                dailyWheelBonusId: modalValue?.row?._id
            }
            delete payload?.day;
            dispatch(updateDailyWheelBonusList(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{modalValue?.isEdit ? " Edit Daily Wheel Bonus" : ' Add Daily Wheel Bonus'}</h2>
                </div>
                <div className={'add_daily_wheel_bonus_pop add_admin_user_popup_content'}>
                    <form className={'form'} onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        <div className={'daily_wheel_bonus_section'}>
                            <div className={'daily_wheel_bonus_section_filed'}>
                                <label>Day</label>
                                <DaysFilterDropdown option={bonusDays?.remaingDays} formData={formData} isEdit={modalValue?.isEdit} setFormData={setFormData} />
                                {simpleValidator.current.message("day", formData?.day, 'required')}
                            </div>
                            <div className={'daily_wheel_bonus_section_filed'}>
                                <label>Spin Title</label>
                                <div className={'daily_wheel_bonus_input_filed'}>
                                    <input type={'text'} name={'spinTitle'} value={formData?.spinTitle} onChange={(e) => handleChange(e)} />
                                </div>
                                {simpleValidator.current.message("spinTitle", formData?.spinTitle, 'required')}
                            </div>
                            <div className={'daily_wheel_bonus_section_filed'}>
                                <label>Spin Description</label>
                                <div className={'daily_wheel_bonus_input_filed'}>
                                    <textarea name={'spinDescription'} rows={3} value={formData?.spinDescription} onChange={(e) => handleChange(e)} />
                                </div>
                                {simpleValidator.current.message("spinDescription", formData?.spinDescription, 'required')}
                            </div>
                            <div className={'daily_wheel_bonus_section_filed bonus_type_section'}>
                                <label className={'main_label'}>Bonus Type</label>
                                <div className={'bonus_type_content_text'}>
                                    <div className={'daily_wheel_bonus_section_filed '}>
                                        <label>1) Bonus Cash : </label>
                                        <div className={'daily_wheel_bonus_input_filed'}>
                                            <input type={'number'} onWheel={event => event.currentTarget.blur()} name={'bonusCashUpto'} value={formData?.bonusCashUpto} placeholder={'Bonus Prize'} onChange={(e) => handleChange(e)} />
                                        </div>

                                    </div>
                                    {simpleValidator.current.message("bonusCash", formData?.bonusCashUpto, 'required')}
                                    <div className={'daily_wheel_bonus_section_filed '}>
                                        <label>2) Bonus Coin : </label>
                                        <div className={'daily_wheel_bonus_input_filed'}>
                                            <input type={'number'} onWheel={event => event.currentTarget.blur()} name={'bonusCoinUpto'} value={formData?.bonusCoinUpto} placeholder={'Bonus Coin'} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                    {simpleValidator.current.message("bonusCoin", formData?.bonusCoinUpto, 'required')}
                                    <div className={'daily_wheel_bonus_section_filed '}>
                                        <label>3) Referral Booster : </label>
                                        <div className={'daily_wheel_bonus_input_filed'}>
                                            <input type={'number'} onWheel={(event) => event.currentTarget.blur()} value={formData?.referralBooster} name={'referralBooster'} placeholder={'Referral Booster Prize (ex. 2X,3X,4X,5X,etc)'} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>
                                    {simpleValidator.current.message("referralBooster", formData?.referralBooster, 'required')}
                                    <div className={'daily_wheel_bonus_section_checkbox '}>
                                        <label>4) Better Luck Next Time</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default AddDailyWheelBonusPop