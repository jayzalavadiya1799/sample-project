import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import TextField from "@mui/material/TextField";
import FilledButton from "../../../../Components/FileButton";
import { useDispatch } from "react-redux";
import moment from "moment";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../hoc/PopContent";
import CommonModal from "../../../../hoc/CommonModal";
import { createOfferList, updateOfferList } from "../../../../Redux/Master/action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 550,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 0,
    borderRadius: "5px",
};

const AddOffer = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const [filterData, setFilterData] = useState({
        startDate: '',
        endDate: '',
        offerName: '',
        offerDescription: '',
        minDepositAmount: '',
        isCashbackBonusInPercentage: false,
        cashbackBonus: '',
    });
    const [openCale, setOpenCale] = useState({
        endDate: false,
        startDate: false
    })

    const handleChange = (e) => {
        const { value, name } = e.target;
        setFilterData({
            ...filterData,
            [name]: value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...filterData,
                startDate: moment(filterData?.startDate).format("YYYY-MM-DD"),
                endDate: moment(filterData?.endDate).format("YYYY-MM-DD"),
            }
            setLoader(true)
            dispatch(createOfferList(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    }

    useEffect(() => {
        if (modalValue?.isEdit) {
            setFilterData({
                ...modalValue?.row
            })
        }
    }, [modalValue]);

    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...filterData,
                startDate: moment(filterData?.startDate).format("YYYY-MM-DD"),
                endDate: moment(filterData?.endDate).format("YYYY-MM-DD"),
                offerId: filterData?.id
            }
            delete payload?.createdAt;
            delete payload?.updatedAt;
            delete payload?._id;
            delete payload?.id;
            setLoader(true)
            dispatch(updateOfferList(payload)).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    const handleDatePicker = (newValue, type) => {
        setFilterData({ ...filterData, [type]: newValue });
        setOpenCale({ ...openCale, [type]: false });
    }

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title offer-details-section-title'}>
                    <h2>{modalValue?.isEdit ? 'Update Offer' : 'Add Offer'}</h2>
                </div>
                <div className={'add_admin_user_popup_content coupon_section_form offer-details-section'}>
                    <form method={'POST'} onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        <div className="formData">
                            <label>Title</label>
                            <div className="emailWrap">
                                <input type="text" value={filterData?.offerName} name='offerName' onChange={(e) => handleChange(e)} />
                            </div>
                            {simpleValidator.current.message("offerName", filterData?.offerName, 'required')}
                        </div>
                        <div className="formData">
                            <label>Description</label>
                            <div className="emailWrap">
                                <textarea rows={4} name={'offerDescription'} value={filterData?.offerDescription} onChange={(e) => handleChange(e)} />
                            </div>
                            {simpleValidator.current.message("offerDescription", filterData?.offerDescription, 'required')}
                        </div>
                        <div className={'date-picker_coupon'}>
                            <div className={'start-date-picker'}>
                                <label>Start Date</label>
                                <div className={'date_picker_value'}>
                                    <LocalizationProvider dateAdapter={AdapterDateFns}  >
                                        <DatePicker
                                            name='startDate'
                                            value={filterData?.startDate}
                                            onChange={(newValue) => handleDatePicker(newValue, 'startDate')}
                                            open={openCale.startDate}
                                            onClose={() => setOpenCale({ ...openCale, startDate: false })}
                                            minDate={new Date()}
                                            renderInput={(params) => {
                                                return <TextField {...params} onClick={() => setOpenCale({ ...openCale, startDate: !openCale.startDate })} />
                                            }}

                                            inputProps={{ readOnly: true }}
                                            inputFormat="MMM dd, yyyy"
                                            className={'datePicker_details'}
                                        />
                                    </LocalizationProvider>
                                </div>
                                {simpleValidator.current.message("startDate", filterData?.startDate, 'required')}
                            </div>
                            <div className={'end-date-picker'}>
                                <label>End Date</label>
                                <div className={'date_picker_value'}>
                                    <LocalizationProvider dateAdapter={AdapterDateFns} >
                                        <DatePicker
                                            name='endDate'
                                            value={filterData?.endDate}
                                            onChange={(newValue) => handleDatePicker(newValue, 'endDate')}
                                            open={openCale.endDate}
                                            onClose={() => setOpenCale({ ...openCale, endDate: false })}
                                            inputFormat="MMM dd, yyyy"
                                            minDate={filterData?.startDate}
                                            renderInput={(params) => <TextField {...params} onClick={() => setOpenCale({ ...openCale, endDate: !openCale.endDate })} />}
                                            inputProps={{ readOnly: true }}
                                        />
                                    </LocalizationProvider>

                                </div>
                                {simpleValidator.current.message("endDate", filterData?.endDate, 'required')}
                            </div>
                        </div>
                        <div className="formData">
                            <label>Cashback  Bonus</label>
                            <div className="emailWrap">
                                <input onWheel={event => event.currentTarget.blur()} type="number" value={filterData?.cashbackBonus} name='cashbackBonus' onChange={(e) => handleChange(e)} />
                            </div>
                            {simpleValidator.current.message("cashBack", filterData?.cashbackBonus, 'required')}
                        </div>
                        <div className="filter_data_radio">
                            <label>Is Percentage Amount?</label>
                            <div className={'filter_data_radio_sub'}>
                                <div className="filter_data_radio_field tab_field_left">
                                    <input type="radio" name='isCashbackBonusInPercentage' checked={filterData?.isCashbackBonusInPercentage} onChange={() => setFilterData({ ...filterData, isCashbackBonusInPercentage: true })} />
                                    <label>Yes</label>
                                </div>
                                <div className="filter_data_radio_field tab_field_right">
                                    <input type="radio" name='isCashbackBonusInPercentage' checked={!filterData?.isCashbackBonusInPercentage} onChange={() => setFilterData({ ...filterData, isCashbackBonusInPercentage: false })} />
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div className="formData">
                            <label>Deposit Amount</label>
                            <div className="emailWrap">
                                <input onWheel={event => event.currentTarget.blur()} type="number" value={filterData?.minDepositAmount} name='minDepositAmount' onChange={(e) => handleChange(e)} />
                            </div>
                            {simpleValidator.current.message("DepositAmount", filterData?.minDepositAmount, 'required')}
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default AddOffer;