import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Loader from "../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../hoc/CommonTable";
import { useDispatch } from "react-redux";
import TableCell from "@mui/material/TableCell";
import { useNavigate } from "react-router-dom";
import { getTDSOverViewReportList } from "../../Redux/TDSReport/action";
import { currencyFormat } from "../../utils";


const TDSModule = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [loader, setLoader] = useState(false)
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });

    const columns = [
        {
            id: 'gameName',
            label: ' Game Name',
        },
        {
            id: 'totalEntryFee',
            label: 'Entrey Fees Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalEntryFee)}</TableCell>
            }
        },
        {
            id: 'totalWininigAmount',
            label: 'Winning Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalWininigAmount)}</TableCell>
            }
        },
        {
            id: 'totalCreditedAmount',
            label: 'Credited Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalCreditedAmount)}</TableCell>
            }
        },
        {
            id: 'totalTDSAmount',
            label: 'TDS Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalTDSAmount)}</TableCell>
            }
        },
        {
            id: '',
            label: 'Action',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell ><span className='edit_btn edit-btn-action' onClick={() => navigate(`/tds-report-details/${row?._id}`, { state: row })}>View</span> </TableCell>
            }
        },
    ];

    useEffect(() => {
        getTDSReportDetails();
    }, [pagination.rowsPerPage, pagination.page]);

    const getTDSReportDetails = () => {
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        };
        setLoader(true);
        dispatch(getTDSOverViewReportList(payload)).then(res => {
            setLoader(false)
            setRowData({
                ...rowData,
                list: res?.data?.data?.docs,
                totalDocs: res?.data?.data?.totalDocs
            })
        });
    };

    return (
        <Box>
            {loader ? <Loader /> : ""}
            <Paper sx={{ mb: 2 }} className="outerbox">
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                />
            </Paper>
        </Box>
    )
}
export default TDSModule