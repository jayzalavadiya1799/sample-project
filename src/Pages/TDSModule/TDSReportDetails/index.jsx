import React, { useEffect, useState } from "react";
import Loader from "../../../images/Loader";
import Paper from "@mui/material/Paper";
import CustomTable from "../../../hoc/CommonTable";
import { useDispatch } from "react-redux";
import MainCommonFilter from "../../../Components/MainCommonFilter";
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { getTDSDetailsReportList } from "../../../Redux/TDSReport/action";
import TableCell from "@mui/material/TableCell";
import { currencyFormat } from "../../../utils";
import moment from "moment";

const TDSReportDetails = () => {
    const { id } = useParams();
    const { state } = useLocation()
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [loader, setLoader] = useState(false);
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 });
    const [filterData, setFilterData] = useState({
        startDate: null,
        endDate: null,
        statusValue: "All Days",
        exportFile: false,
        csvDownload: false,
        exportFileName: 'Export File',
        gameQuarter: 'All Quarter'
    })

    const columnsTitle = [
        {
            id: 'totalEntryFee',
            label: 'Entrey Fees Amount',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalEntryFee)}</TableCell>
            }
        },
        {
            id: 'totalWininigAmount',
            label: 'Winning Amount',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalWininigAmount)}</TableCell>
            }
        },
        {
            id: 'totalCreditedAmount',
            label: 'Credited Amount',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalCreditedAmount)}</TableCell>
            }
        },
        {
            id: 'totalTDSAmount',
            label: 'TDS Amount',
            isDisbanding: true,
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalTDSAmount)}</TableCell>
            }
        },
    ];

    const columns = [
        {
            id: '_id',
            label: 'Table id',
        },
        {
            id: 'date',
            label: 'Date',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.date).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        {
            id: 'gameModeName',
            label: 'Game Mode Name',
        },
        {
            id: 'panCardNumber',
            label: 'Pancard Number'
        },
        {
            id: 'entryFee',
            label: 'Entrey Fees Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.entryFee)}</TableCell>
            }
        },
        {
            id: 'wininigAmount',
            label: 'Winning Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.wininigAmount)}</TableCell>
            }
        },
        {
            id: 'creditedAmount',
            label: 'Credited Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.creditedAmount)}</TableCell>
            }
        },
        {
            id: 'tdsAmount',
            label: 'TDS Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.tdsAmount)}</TableCell>
            }
        },
    ];

    useEffect(() => {
        if (filterData?.statusValue !== 'Custom') {
            getTDSReportDetailsList(filterData.startDate, filterData.endDate)
        }
    }, [pagination.rowsPerPage, pagination.page, filterData.exportFile, filterData.csvDownload, filterData.startDate, filterData.endDate]);

    const getTDSReportDetailsList = (startDate, endDate) => {
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            gameId: id,
            exportFile: filterData?.exportFile,
            csvDownload: filterData?.csvDownload,
            startDate: startDate ? moment(startDate).format("YYYY-MM-DD") : null,
            endDate: endDate ? moment(endDate).format("YYYY-MM-DD") : null,
        };
        Object?.keys(payload).forEach(ele => {
            if (payload[ele] === '' || payload[ele] === null) { delete payload[ele] }
        });
        setLoader(true);
        dispatch(getTDSDetailsReportList(payload)).then(res => {
            setLoader(false);
            if (res?.data?.data?.filePath) {
                setFilterData({
                    ...filterData,
                    csvDownload: false,
                    exportFile: false
                })
                window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
            } else {
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs || [],
                    totalDocs: res.data?.data?.totalDocs
                })
            }

        });
    };
    useEffect(() => {
        if (filterData.startDate && filterData.endDate && filterData?.statusValue === 'Custom') {
            setPagination({
                ...pagination,
                page: 0
            })
            getTDSReportDetailsList(filterData.startDate, filterData.endDate)
        }
    }, [filterData.startDate, filterData.endDate]);
    return (
        <>
            {loader ? <Loader /> : ""}
            <div className={'tds-main-section'}>
                <button className={'tds-back-btn'} onClick={() => navigate(-1)}> <KeyboardArrowLeftIcon /> Back</button>
                <h2>{state?.gameName} Game </h2>
            </div>

            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'admin_user_list winner_section'}>
                    <h2> TDS Report Overview</h2>
                </div>
                <CustomTable
                    headCells={columnsTitle}
                    rowData={[{ ...state }]}
                    totalDocs={0}
                    pagination={pagination}
                    setPagination={setPagination}
                    dragUpdater={''}
                    isWinnerTitle={true}
                />
            </Paper>
            <Paper sx={{ mb: 2 }} className="outerbox">
                <div className={'admin_user_list winner_section'}>
                    <h2>TDS Report List</h2>
                    <MainCommonFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={getTDSReportDetailsList} pagination={pagination} setPagination={setPagination} addPropsFilter={{ isTDSReport: true }} />
                </div>
                <CustomTable
                    headCells={columns}
                    rowData={rowData?.list}
                    totalDocs={rowData?.totalDocs}
                    pagination={pagination}
                    setPagination={setPagination}
                    dragUpdater={''}
                />
            </Paper>
        </>
    )
}
export default TDSReportDetails