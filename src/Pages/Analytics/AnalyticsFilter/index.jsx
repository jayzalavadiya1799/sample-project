import React, {useEffect, useState} from "react";
import ExportDropDown from "../../../Components/MainCommonFilter/ExportDropDown";
import DayWiseDropDown from "../../../Components/MainCommonFilter/DayWiseDropDown";
import CustomDateFilter from "../../../Components/MainCommonFilter/CustomDateFilter";
import GameFilterDropdown from "./GameFilterDropdown";
import {getLeaderboardGameList} from "../../../Redux/Master/action";
import {useDispatch} from "react-redux";

const AnalyticsFilter = ({filterData,setFilterData}) => {
    const dispatch = useDispatch()
    const [gameFilterData,setGameFilterData]= useState([])
    useEffect(()=>{
        dispatch(getLeaderboardGameList()).then(res=>{
            setGameFilterData(res.data.data?.docs)
        })
    },[]);
    return(
        <div className={'filter_details_tab_section '}>
            <div className={'filter_game_list'}>
                <GameFilterDropdown publisherList={gameFilterData} filterData={filterData} setFilterData={setFilterData} />
            </div>
            <div className={'filter_inner_tab_info'}>
                <div className={'filter_export_date_dropdown'}>
                    <ExportDropDown option={['Export File', 'CSV File', 'Excel File']} name={'exportFileName'} filterData={filterData} setFilterData={setFilterData} />
                    <DayWiseDropDown option={['Last 7 Days', 'Last 14 Days', 'Last 30 Days', 'Custom']} name={'statusValue'} filterData={filterData} setFilterData={setFilterData} />
                    {(filterData?.statusValue === 'Custom') && <CustomDateFilter filterData={filterData} setFilterData={setFilterData} />}
                </div>
            </div>
        </div>
    )
}
export default AnalyticsFilter