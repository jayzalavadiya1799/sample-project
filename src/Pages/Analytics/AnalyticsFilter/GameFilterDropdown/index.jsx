import React, {useEffect, useRef, useState} from "react";
import FormControl from "@mui/material/FormControl";

const GameFilterDropdown = ({ publisherList, setFilterData, filterData }) => {
    const ref = useRef();
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [publisherValue, setPublisherValue] = useState('')
    const [publisherOption, setPublisherOption] = React.useState([]);
    const [dropDownListVal, setDropDownListVal] = useState('');

    useEffect(()=>{
        setPublisherValue(filterData.gameName);
    },[publisherList])

    useEffect(() => {
        let tempVal = []
        if (publisherList?.length > 0) {
            publisherList?.map(user => {
                if (user.gameName && user.id) {
                    tempVal.push({
                        label: user.gameName,
                        value: user.id
                    })
                }

            })
            setPublisherOption([{label:"All Game",value:'0'},...tempVal])
        }
    }, [publisherList]);

    useEffect(() => {
        const checkIfClickedOutside = e => {
            if (isMenuOpen && ref.current && !ref.current.contains(e.target) && e.target.id !== 'profileIcon') { setIsMenuOpen(false) }
        }
        document.addEventListener("mousedown", checkIfClickedOutside)
        return () => { document.removeEventListener("mousedown", checkIfClickedOutside) }
    }, [isMenuOpen]);

    const handleAutoValue = (data) => {
        if (data.value !== 0) {
            setFilterData(data.value)
            setPublisherValue(data.label)
            setIsMenuOpen(false)
        } else {
            setFilterData( null)
            setIsMenuOpen(false)
        }


    }
    const handleClear = () => {
        let tempVal = []
        publisherList?.map(user => {
            tempVal.push({
                label: user.gameName,
                value: user.id
            })
        })
        setPublisherOption(tempVal)
        setDropDownListVal('')
    }
    const handleAutoComplete = (e, data) => {
        if (e.target.value) {
            let filteredData = data.filter(item => (item.label).toLowerCase().indexOf(e.target.value.toLowerCase()) > -1)
            setPublisherOption(filteredData)
            setDropDownListVal(filteredData?.label)
        } else {
            let tempVal = []
            publisherList?.map(user => {
                tempVal.push({
                    label: user.gameName,
                    value: user.id
                })
            })
            setPublisherOption(tempVal)
        }
    };

    return (
        <FormControl fullWidth>
            <div className={'default_dropdown'}>
                <div className={isMenuOpen ? 'dropdown_value_details dropdown-active-border' : 'dropdown_value_details'} id="profileIcon" onClick={() => setIsMenuOpen(!isMenuOpen)}>
                    <p>{publisherValue}</p>
                    <div className="mat-select-arrow-wrappe ">
                        <div className="mat-select-arrow "/>
                    </div>
                    <svg className="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium MuiSelect-icon css-i4bv87-MuiSvgIcon-root"
                         focusable="false" viewBox="0 0 24 24" aria-hidden="true" data-testid="ArrowDropDownIcon">
                        <path d="M7 10l5 5 5-5z"></path>
                    </svg>
                </div>
                {
                    isMenuOpen &&
                    <div className={'dropdown_content_value'} ref={ref}>
                        <div className={'search_filter_dropdown'}>
                            <input value={dropDownListVal} placeholder={'Search here'} onChange={(e) => handleAutoComplete(e, publisherOption)} />
                            <p onClick={() => handleClear()}>
                                <svg viewBox="0 0 24 24" x="1008" y="432" fit="" height="28" width="25" preserveAspectRatio="xMidYMid meet" focusable="false">
                                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" fill="#64748b" /></svg>
                            </p>
                        </div>
                        <div className={'dropdown_content_list'}>
                            <ul>
                                {
                                    publisherOption?.map(item => {
                                        return <li className={item.label === publisherValue ? 'active_label_list' : ''} onClick={() => handleAutoValue(item)}>{item?.label}</li>
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                }
            </div>
        </FormControl>
    )
}
export default GameFilterDropdown