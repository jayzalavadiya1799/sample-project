import React, {useState} from "react";
import Chart from 'react-apexcharts';
import moment from 'moment';

const AnalyticsUserReportContent = ({filterData}) => {
    const [filterChart, setFilterChart] = useState({
        dailyActiveUsers: {
            total: true,
            ios: true,
            android: true
        },
        dailyNewUsers: {
            total: true,
            ios: true,
            android: true
        },
        gamesUsers: {
            total: true,
            loss: true,
            win: true
        },
        revenueData: {
            userWithdrawal: true,
        },
        transactionData:{
            deposit:true,
            rakeAmount:true,
            winAmount:true,
            withdraw:true
        },
        retentionData:{
            D0:true,
            D1:true,
            D7:true,
            D30:true
        }
    });

    const DailyUsers = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount:5,
                min:0.0,
                max:1.0,
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "IOS",
                data: [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Android",
                data: [],
                color: "rgb(254, 176, 25)"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const DailyNewUsers = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount:5,
                min:0.0,
                max:1.0,
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "IOS",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Android",
                data: [],
                color: "rgb(254, 176, 25)"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const gamesUsers = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount:3,
                min:0.0,
                max:2.0,
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total",
                data: [],
                color: "#008ffb"
            },
            {
                name: "loss",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "win",
                data:  [],
                color: "rgb(254, 176, 25)"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const revenueUser = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0.00,
                max:5.00,
                labels: {
                    formatter: function (value) {
                        return  "$" + value.toFixed(2)  ;
                    },
                    fontSize:'10px'
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "userWithdrawal",
                data:  [],
                color: "#008ffb"
            },
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const transactionData = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            },
            yaxis: {
                tickAmount: 4,
                min:0.0,
                max:1.85,
                labels: {
                    formatter: function (value) {
                        return   "$" + value.toFixed(2);
                    }
                },
            }
        },
        series: [
            {
                name: "Entry Amount",
                data:  [],
                color: "rgb(0, 143, 251)"
            },
            {
                name: "Deposit Amount",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Win Amount",
                data:  [],
                color: "rgb(254, 176, 25)"
            },
            {
                name: "Withdraw Amount",
                data:  [],
                color: "#ff4560"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const retentionData = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0,
                max:5,
                labels: {
                    formatter: function (value) {
                        return   value   + "%";
                    }
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "D0",
                data:  [],
                color: "rgb(0, 143, 251)"
            },
            {
                name: "D1",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "D7",
                data:  [],
                color: "rgb(254, 176, 25)"
            },
            {
                name: "D30",
                data:  [],
                color: "#ff4560"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };

    const chartFiledDisabled = (type,field) =>{
        setFilterChart({
            ...filterChart,
            [type]: {
                ...filterChart[type],
                [field]: !filterChart[type][field]
            }
        });
    };

    return(
        <div className={'publisher-data-filter'}>
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Daily Active Users : 0</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Daily Active Users</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.dailyActiveUsers.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyActiveUsers','total')} >Total<span className="dot dot_blue" /></p>
                        <p className={filterChart.dailyActiveUsers.ios ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyActiveUsers','ios')} >Ios<span className="dot dot_green" /></p>
                        <p className={filterChart.dailyActiveUsers.android ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyActiveUsers','android')} >Android<span className="dot dot_yellow" /></p>
                    </div>
                </div>

                <div className="chart_details">
                    <Chart
                        options={DailyUsers?.options}
                        series={DailyUsers?.series}
                        type="line"
                        height='300'
                    />
                </div>
            </div>
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Daily New Users : 0</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Daily New Users</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.dailyNewUsers.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','total')} >Total<span className="dot dot_blue" /></p>
                        <p className={filterChart.dailyNewUsers.ios ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','ios')} >Ios<span className="dot dot_green" /></p>
                        <p className={filterChart.dailyNewUsers.android ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','android')}>Android<span className="dot dot_yellow" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={DailyNewUsers?.options}
                        series={DailyNewUsers?.series}
                        type="line"
                        height='300'
                    />

                </div>
            </div>
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Games Counter : 0</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Games</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.gamesUsers.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('gamesUsers','total')}>Total<span className="dot dot_blue" /></p>
                        <p className={filterChart.gamesUsers.loss ? "" : "disabled"} onClick={()=>chartFiledDisabled('gamesUsers','loss')} >Loss<span className="dot dot_green" /></p>
                        <p className={filterChart.gamesUsers.win ? "" : "disabled"} onClick={()=>chartFiledDisabled('gamesUsers','win')} >Win<span className="dot dot_yellow" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={gamesUsers?.options}
                        series={gamesUsers?.series}
                        type="line"
                        height='300'
                    />

                </div>
            </div>
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Transactions : {0}</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Transactions</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.transactionData.rakeAmount ? "" : "disabled"} onClick={()=>chartFiledDisabled('transactionData','rakeAmount')}>Entry Amount<span className="dot dot_blue" /></p>
                        <p className={filterChart.transactionData.deposit ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionData','deposit')} >Deposit Amount<span className="dot dot_green" /></p>
                        <p className={filterChart.transactionData.winAmount ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionData','winAmount')}>Win Amount<span className="dot dot_yellow" /></p>
                        <p className={filterChart.transactionData.withdraw ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionData','withdraw')}>Withdraw Amount<span className="dot dot_orange" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={transactionData?.options}
                        series={transactionData?.series}
                        type="line"
                        height='300'
                    />
                </div>
            </div>
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Retention : {0}</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Retention</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.retentionData.D0 ? "" : "disabled"} onClick={()=>chartFiledDisabled('retentionData','D0')}>D0<span className="dot dot_blue" /></p>
                        <p className={filterChart.retentionData.D1 ? "" : "disabled"} onClick={() => chartFiledDisabled('retentionData','D1')} >D1<span className="dot dot_green" /></p>
                        <p className={filterChart.retentionData.D7 ? "" : "disabled"} onClick={() => chartFiledDisabled('retentionData','D7')}>D7<span className="dot dot_yellow" /></p>
                        <p className={filterChart.retentionData.D30 ? "" : "disabled"} onClick={() => chartFiledDisabled('retentionData','D30')}>D30<span className="dot dot_orange" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={retentionData?.options}
                        series={retentionData?.series}
                        type="line"
                        height='300'
                    />
                </div>
            </div>
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Revenue : {0}</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Revenue</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.revenueData.userWithdrawal ? "" : "disabled"}  onClick={()=>chartFiledDisabled('revenueData','userWithdrawal')}>Revenue<span className="dot dot_blue" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={revenueUser?.options}
                        series={revenueUser?.series}
                        type="line"
                        height='300'
                    />
                </div>
            </div>
        </div>
    )
}
export default AnalyticsUserReportContent