import React, {useState} from "react";
import AnalyticsFilter from "../AnalyticsFilter";
import AnalyticsUserReportContent from "./AnalyticsUserReportContent";
import moment from "moment";

const UserReport = () =>{
    const [filterData, setFilterData] = useState({
        startDate: moment().subtract(7, "days").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        statusValue: "Last 7 Days",
        exportFile: false,
        csvDownload: false,
        exportFileName: 'Export File',
        gameId:'',
        gameName:'All Game',
    });

    return(
        <>
        <div className={'analytics_filter_details'}>
            <AnalyticsFilter filterData={filterData} setFilterData={setFilterData}/>
        </div>
        <div>
            <AnalyticsUserReportContent filterData={filterData}/>
        </div>
        </>
    )
}
export default UserReport