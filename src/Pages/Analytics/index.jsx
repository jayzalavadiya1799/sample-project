import React, {useState} from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import TabPanel from "../../Components/TabPanel";
import UserReport from "./UserReport";
import RevenueReport from "./RevenueReport";
import SystemTotals from "./SystemTotals";
import GameRackReport from "./GameRackReport";
import Chart from "react-apexcharts";
import moment from "moment";

const Analytics = () => {
    const [value, setValue] = useState(0);
    const [filterChart, setFilterChart] = useState({
        dailyActiveUsers: {
            total: true,
            ios: true,
            android: true
        },
        dailyNewUsers: {
            total: true,
            ios: true,
            android: true
        },
        gamesUsers: {
            total: true,
            loss: true,
            win: true
        },
        revenueData: {
            userWithdrawal: true,
        },
        transactionData:{
            deposit:true,
            rakeAmount:true,
            winAmount:true,
            withdraw:true
        },
        retentionData:{
            D0:true,
            D1:true,
            D7:true,
            D30:true
        }
    });
    const [filterData, setFilterData] = useState({
        startDate: moment().subtract(7, "days").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        statusValue: "Last 7 Days",
        exportFile: false,
        csvDownload: false,
        exportFileName: 'Export File',
        gameId:'',
        gameName:'All Game',
    });
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const a11yProps = (index) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    };



    const DailyUsers = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount:5,
                min:0.0,
                max:1.0,
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "IOS",
                data: [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Android",
                data: [],
                color: "rgb(254, 176, 25)"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const DailyNewUsers = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount:5,
                min:0.0,
                max:1.0,
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "IOS",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Android",
                data: [],
                color: "rgb(254, 176, 25)"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const gamesUsers = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount:3,
                min:0.0,
                max:2.0,
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total",
                data: [],
                color: "#008ffb"
            },
            {
                name: "loss",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "win",
                data:  [],
                color: "rgb(254, 176, 25)"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const revenueUser = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0.00,
                max:5.00,
                labels: {
                    formatter: function (value) {
                        return  "$" + value.toFixed(2)  ;
                    },
                    fontSize:'10px'
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "userWithdrawal",
                data:  [],
                color: "#008ffb"
            },
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const transactionData = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            },
            yaxis: {
                tickAmount: 4,
                min:0.0,
                max:1.85,
                labels: {
                    formatter: function (value) {
                        return   "$" + value.toFixed(2);
                    }
                },
            }
        },
        series: [
            {
                name: "Entry Amount",
                data:  [],
                color: "rgb(0, 143, 251)"
            },
            {
                name: "Deposit Amount",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Win Amount",
                data:  [],
                color: "rgb(254, 176, 25)"
            },
            {
                name: "Withdraw Amount",
                data:  [],
                color: "#ff4560"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const retentionData = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0,
                max:5,
                labels: {
                    formatter: function (value) {
                        return   value   + "%";
                    }
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "D0",
                data:  [],
                color: "rgb(0, 143, 251)"
            },
            {
                name: "D1",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "D7",
                data:  [],
                color: "rgb(254, 176, 25)"
            },
            {
                name: "D30",
                data:  [],
                color: "#ff4560"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };

    const chartFiledDisabled = (type,field) =>{
        setFilterChart({
            ...filterChart,
            [type]: {
                ...filterChart[type],
                [field]: !filterChart[type][field]
            }
        });
    };
    return(
        <>
            <div className={'publisher-data-filter'}>
                <div className="chart outBox">
                    <div className={"chart-title-data"}>
                        <p>Total Daily Active Users : 0</p>
                    </div>
                    <div className={'chart-details-content'}>
                        <h2>Daily Active Users Report</h2>
                        <div className="chart_info mb_20">
                            <p className={filterChart.dailyActiveUsers.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyActiveUsers','total')} >Total<span className="dot dot_blue" /></p>
                            <p className={filterChart.dailyActiveUsers.ios ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyActiveUsers','ios')} >Ios<span className="dot dot_green" /></p>
                            <p className={filterChart.dailyActiveUsers.android ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyActiveUsers','android')} >Android<span className="dot dot_yellow" /></p>
                        </div>
                    </div>

                    <div className="chart_details">
                        <Chart
                            options={DailyUsers?.options}
                            series={DailyUsers?.series}
                            type="line"
                            height='300'
                        />
                    </div>
                </div>
                <div className="chart outBox">
                    <div className={"chart-title-data"}>
                        <p>Total Monthly Active User : 0</p>
                    </div>
                    <div className={'chart-details-content'}>
                        <h2>Monthly Active User Report</h2>
                        <div className="chart_info mb_20">
                            <p className={filterChart.dailyNewUsers.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','total')} >Total<span className="dot dot_blue" /></p>
                            <p className={filterChart.dailyNewUsers.ios ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','ios')} >Ios<span className="dot dot_green" /></p>
                            <p className={filterChart.dailyNewUsers.android ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','android')}>Android<span className="dot dot_yellow" /></p>
                        </div>
                    </div>
                    <div className="chart_details">
                        <Chart
                            options={DailyNewUsers?.options}
                            series={DailyNewUsers?.series}
                            type="line"
                            height='300'
                        />

                    </div>
                </div>
                <div className="chart outBox">
                    <div className={"chart-title-data"}>
                        <p>Total Daily New Users : 0</p>
                    </div>
                    <div className={'chart-details-content'}>
                        <h2>Daily New Users Report</h2>
                        <div className="chart_info mb_20">
                            <p className={filterChart.dailyNewUsers.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','total')} >Total<span className="dot dot_blue" /></p>
                            <p className={filterChart.dailyNewUsers.ios ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','ios')} >Ios<span className="dot dot_green" /></p>
                            <p className={filterChart.dailyNewUsers.android ? "" : "disabled"} onClick={()=>chartFiledDisabled('dailyNewUsers','android')}>Android<span className="dot dot_yellow" /></p>
                        </div>
                    </div>
                    <div className="chart_details">
                        <Chart
                            options={DailyNewUsers?.options}
                            series={DailyNewUsers?.series}
                            type="line"
                            height='300'
                        />

                    </div>
                </div>
                <div className="chart outBox">
                    <div className={"chart-title-data"}>
                        <p>Total Users Report : 0</p>
                    </div>
                    <div className={'chart-details-content'}>
                        <h2>Total Users Report</h2>
                        <div className="chart_info mb_20">
                            <p className={filterChart.gamesUsers.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('gamesUsers','total')}>Total<span className="dot dot_blue" /></p>
                            <p className={filterChart.gamesUsers.loss ? "" : "disabled"} onClick={()=>chartFiledDisabled('gamesUsers','loss')} >Loss<span className="dot dot_green" /></p>
                            <p className={filterChart.gamesUsers.win ? "" : "disabled"} onClick={()=>chartFiledDisabled('gamesUsers','win')} >Win<span className="dot dot_yellow" /></p>
                        </div>
                    </div>
                    <div className="chart_details">
                        <Chart
                            options={gamesUsers?.options}
                            series={gamesUsers?.series}
                            type="line"
                            height='300'
                        />

                    </div>
                </div>
                <div className="chart outBox">
                    <div className={"chart-title-data"}>
                        <p>Total Paying Users : {0}</p>
                    </div>
                    <div className={'chart-details-content'}>
                        <h2>Paying Users Report</h2>
                        <div className="chart_info mb_20">
                            <p className={filterChart.transactionData.rakeAmount ? "" : "disabled"} onClick={()=>chartFiledDisabled('transactionData','rakeAmount')}>Entry Amount<span className="dot dot_blue" /></p>
                            <p className={filterChart.transactionData.deposit ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionData','deposit')} >Deposit Amount<span className="dot dot_green" /></p>
                            <p className={filterChart.transactionData.winAmount ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionData','winAmount')}>Win Amount<span className="dot dot_yellow" /></p>
                            <p className={filterChart.transactionData.withdraw ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionData','withdraw')}>Withdraw Amount<span className="dot dot_orange" /></p>
                        </div>
                    </div>
                    <div className="chart_details">
                        <Chart
                            options={transactionData?.options}
                            series={transactionData?.series}
                            type="line"
                            height='300'
                        />
                    </div>
                </div>
            </div>
        </>
        // <Box sx={{ width: '100%' }} className={'setting_tab_section user_details_tab'}>
        //     <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        //         <Tabs value={value} onChange={handleChange} className={'bg_white_tab'}>
        //             <Tab className={'tab_listing tab_title'} label="Game Rack Report" {...a11yProps(0)} />
        //             {/*<Tab className={'tab_listing tab_title'} label=" User Report" {...a11yProps(0)} />*/}
        //             {/*<Tab className={'tab_listing tab_title'} label="Revenue Report" {...a11yProps(1)} />*/}
        //             {/*<Tab className={'tab_listing tab_title'} label="System Totals" {...a11yProps(2)} />*/}
        //         </Tabs>
        //     </Box>
        //     <TabPanel value={value} index={0}>
        //       <GameRackReport/>
        //     </TabPanel>
        //     <TabPanel value={value} index={1}>
        //         <UserReport/>
        //     </TabPanel>
        //     {/*<TabPanel value={value} index={1}>*/}
        //     {/*    <RevenueReport/>*/}
        //     {/*</TabPanel>*/}
        //     {/*<TabPanel value={value} index={2}>*/}
        //     {/*    <SystemTotals/>*/}
        //     {/*</TabPanel>*/}
        // </Box>
    )
}
export default Analytics;