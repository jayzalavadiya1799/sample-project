import React, {useState} from "react";
import CustomTable from "../../../hoc/CommonTable";
import Paper from "@mui/material/Paper";
import TableCell from "@mui/material/TableCell";
import {currencyFormat} from "../../../utils";
import Loader from "../../../images/Loader";
import {useDispatch} from "react-redux";

const SystemTotals = () =>{
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false)
    const [pagination, setPagination] = useState({ rowsPerPage: 10, page: 0 })
    const [rowData,setRowData] = useState([]);

    const columns = [
        {
            id: 'totalCash',
            numeric: false,
            disablePadding: false,
            label: 'Total Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalCash)}</TableCell>
            }
        },
        {
            id: 'totalBonusCash',
            numeric: true,
            disablePadding: false,
            label: 'Total Bonus Cash',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.totalBonusCash)}</TableCell>
            }
        }
    ];
    return(
        <div>
            { loader ? <Loader /> : "" }
            <Paper sx={{ mb: 2 }} className="outerbox">
            <CustomTable
                headCells={columns}
                rowData={rowData}
                totalDocs={0}
                pagination={pagination}
                setPagination={setPagination}
                isAboutWebsite={true}
            />
            </Paper>
        </div>
    )
}
export default SystemTotals