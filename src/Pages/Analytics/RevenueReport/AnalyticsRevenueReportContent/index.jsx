import React, {useState} from "react";
import Chart from 'react-apexcharts';
import moment from 'moment';

const AnalyticsRevenueReportContent = ({filterData}) => {
    const [filterChart, setFilterChart] = useState({
        transactionsChart: {
            rakeAmount: true,
            winAmount: true,
            withdraw: true,
            deposit:true
        },
        winLossChartData: {
            loss: true,
            tie: true,
            win: true,
            total:true
        },
        commissionChartData: {
            total: true,
            developerCommission: true,
            platformFee: true
        },
        bonusClaimedChartData:{
            bonus:true
        },
        payments:{
            total:true,
            deposit:true,
            withdraw:true
        },
        withdrawals:{
            publisher:true,
            user:true,
            total:true
        }
    })
    const chartFiledDisabled = (type,field) =>{
        setFilterChart({
            ...filterChart,
            [type]: {
                ...filterChart[type],
                [field]: !filterChart[type][field]
            }
        });
    };

    const transactions = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 3,
                min:0.00,
                max:1.85,
                labels: {
                    formatter: function (value) {
                        return  "$" + value.toFixed(2)  ;
                    },
                    fontSize:'10px'
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Rake Amount",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "Deposit Amount",
                data: [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Win Amount",
                data:  [],
                color: "rgb(254, 176, 25)"
            },
            {
                name: "Withdraw Amount",
                data:  [],
                color: "#ff4560"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const winLoss = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total Counter",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "Win Counter",
                data: [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Loss Counter",
                data: [],
                color: "rgb(254, 176, 25)"
            },
            {
                name: "Tie Counter",
                data: [],
                color: "#ff4560"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const commissionChart = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0.00,
                max:5.00,
                labels: {
                    formatter: function (value) {
                        return  "$" + value.toFixed(2)  ;
                    },
                    fontSize:'10px'
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total Commission",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "Developer Commission",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Platform Commission",
                data:  [],
                color: "rgb(254, 176, 25)"
            },
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const paymentsChart = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0.00,
                max:5.00,
                labels: {
                    formatter: function (value) {
                        return  "$" + value.toFixed(2)  ;
                    },
                    fontSize:'10px'
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Total",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "Deposit",
                data:  [],
                color: "#00e396"
            },
            {
                name: "Withdraw",
                data:  [],
                color: "#feb019"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const withdrawalsChart = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0.00,
                max:5.00,
                labels: {
                    formatter: function (value) {
                        return  "$" + value.toFixed(2)  ;
                    },
                    fontSize:'10px'
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Publishers",
                data:  [],
                color: "#008ffb"
            },
            {
                name: "Users",
                data:  [],
                color: "rgb(0, 227, 150)"
            },
            {
                name: "Total",
                data:  [],
                color: "rgb(254, 176, 25)"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };
    const bonusClaimedChart = {
        options: {
            xaxis: {
                categories: '',
                labels: {
                    show: false
                },
                title: {
                    text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount: 5,
                min:0.00,
                max:5.00,
                labels: {
                    formatter: function (value) {
                        return  "$" + value.toFixed(2)  ;
                    },
                    fontSize:'10px'
                },
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Bonus",
                data:  [],
                color: "#008ffb"
            }
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };

    return (
        <div className={"publisher-data-filter"}>
            {/*transition*/}
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Transactions : {0}</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Transactions</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.transactionsChart.rakeAmount ? "" : "disabled"} onClick={()=>chartFiledDisabled('transactionsChart','rakeAmount')}> Rake Amount <span className="dot dot_blue" /></p>
                        <p className={filterChart.transactionsChart.deposit ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionsChart','deposit')} >Deposit Amount <span className="dot dot_green" /></p>
                        <p className={filterChart.transactionsChart.winAmount ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionsChart','winAmount')}>Win Amount <span className="dot dot_yellow" /></p>
                        <p className={filterChart.transactionsChart.withdraw ? "" : "disabled"} onClick={() => chartFiledDisabled('transactionsChart','withdraw')}>Withdraw Amount <span className="dot dot_orange" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={transactions?.options}
                        series={transactions?.series}
                        type="line"
                        height='300'
                    />

                </div>
            </div>
            {/*winLoss Chart */}
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total  : {0}</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Win Loss</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.winLossChartData.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('winLossChartData','total')}>Total Counter<span className="dot dot_blue" /></p>
                        <p className={filterChart.winLossChartData.win ? "" : "disabled"} onClick={() => chartFiledDisabled('winLossChartData','win')} >Win Counter<span className="dot dot_green" /></p>
                        <p className={filterChart.winLossChartData.loss ? "" : "disabled"} onClick={() => chartFiledDisabled('winLossChartData','loss')}>Loss Counter<span className="dot dot_yellow" /></p>
                        <p className={filterChart.winLossChartData.tie ? "" : "disabled"} onClick={() => chartFiledDisabled('winLossChartData','tie')}>Tie Counter<span className="dot dot_orange" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={winLoss?.options}
                        series={winLoss?.series}
                        type="line"
                        height='300'
                    />

                </div>
            </div>
            {/*bonus*/}
            <div className={"dauRevenueBox"}>
                <div className=" chart chartbox dauRevenue-inner">
                    <div className={"chart-title-data"}>
                        <p>Total Commission : {0}</p>
                    </div>
                    <div className={'chart-details-content'}>
                        <h2>Commission</h2>
                        <div className="chart_info mb_20">
                            <p className={filterChart.commissionChartData?.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('commissionChartData','total')}>Total<span className="dot dot_blue" /></p>
                            <p className={filterChart.commissionChartData?.developerCommission ? "" : "disabled"} onClick={()=>chartFiledDisabled('commissionChartData','developerCommission')}>Developer Commission<span className="dot dot_green" /></p>
                            <p className={filterChart.commissionChartData?.platformFee ? "" : "disabled"} onClick={()=>chartFiledDisabled('commissionChartData','platformFee')}>Platform Commission<span className="dot dot_yellow" /></p>
                        </div>
                    </div>
                    <div className="chart_details">
                        <Chart
                            options={commissionChart?.options}
                            series={commissionChart?.series}
                            type="line"
                            height='300'
                        />

                    </div>
                </div>
                <div className=" chart chartbox dauRevenue-inner">
                    <div className={"chart-title-data"}>
                        <p>Total Bonus Claimed : {0}</p>
                    </div>
                    <div className={'chart-details-content'}>
                        <h2>Bonus Claimed</h2>
                        <div className="chart_info mb_20">
                            <p className={filterChart.bonusClaimedChartData.bonus ? "" : "disabled"}   onClick={()=>chartFiledDisabled('bonusClaimedChartData','bonus')}>Bonus<span className="dot dot_blue" /></p>
                        </div>
                    </div>
                    <div className="chart_details">
                        <Chart
                            options={bonusClaimedChart?.options}
                            series={bonusClaimedChart?.series}
                            type="line"
                            height='300'
                        />

                    </div>
                </div>
            </div>
            {/*Payment Chart */}
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Payments  : {0}</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Payment</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.payments.total ? "" : "disabled"} onClick={()=>chartFiledDisabled('payments','total')}>Total<span className="dot dot_blue" /></p>
                        <p className={filterChart.payments.deposit ? "" : "disabled"} onClick={() => chartFiledDisabled('payments','deposit')} >Deposit<span className="dot dot_green" /></p>
                        <p className={filterChart.payments.withdraw ? "" : "disabled"} onClick={() => chartFiledDisabled('payments','withdraw')}>Withdraw<span className="dot dot_yellow" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={paymentsChart?.options}
                        series={paymentsChart?.series}
                        type="line"
                        height='300'
                    />

                </div>
            </div>
            {/*withdraw Chart */}
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    <p>Total Withdrawals  : {0}</p>
                </div>
                <div className={'chart-details-content'}>
                    <h2>Withdrawals</h2>
                    <div className="chart_info mb_20">
                        <p className={filterChart.withdrawals.publisher ? "" : "disabled"} onClick={()=>chartFiledDisabled('withdrawals','publisher')}>Publishers<span className="dot dot_blue" /></p>
                        <p className={filterChart.withdrawals.user ? "" : "disabled"} onClick={() => chartFiledDisabled('withdrawals','user')} >Users<span className="dot dot_green" /></p>
                        <p className={filterChart.withdrawals.total ? "" : "disabled"} onClick={() => chartFiledDisabled('withdrawals','total')}>Total<span className="dot dot_yellow" /></p>
                    </div>
                </div>
                <div className="chart_details">
                    <Chart
                        options={withdrawalsChart?.options}
                        series={withdrawalsChart?.series}
                        type="line"
                        height='300'
                    />

                </div>
            </div>
        </div>
    )
}
export default AnalyticsRevenueReportContent