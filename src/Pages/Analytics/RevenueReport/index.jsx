import React, {useState} from "react";
import moment from 'moment';
import AnalyticsFilter from "../AnalyticsFilter";
import AnalyticsRevenueReportContent from "./AnalyticsRevenueReportContent";

const RevenueReport = () =>{
    const [filterData, setFilterData] = useState({
        startDate: moment().subtract(7, "days").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        statusValue: "Last 7 Days",
        exportFile: false,
        csvDownload: false,
        exportFileName: 'Export File',
        gameId:'',
        gameName:'All Game',
    });
    return(
        <>
            <div className={'analytics_filter_details'}>
                <AnalyticsFilter filterData={filterData} setFilterData={setFilterData}/>
            </div>
            <div>
                <AnalyticsRevenueReportContent filterData={filterData}/>
            </div>
        </>
    )
}
export default RevenueReport