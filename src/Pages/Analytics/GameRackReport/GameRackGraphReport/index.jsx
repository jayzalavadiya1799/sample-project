import React, {useState} from "react";
import Chart from "react-apexcharts";
import {useSelector} from "react-redux";

const GameRackGraphReport = ({filterData}) => {
    const analyticsReducer = useSelector(state => state.analyticsReducer.gameRackReport);
    const [filterChart, setFilterChart] = useState({
        gameRackReport: {
            collectedAmount: true,
            givenWinAmount: true,
        },
    });

    const DailyUsers = {
        options: {
            xaxis: {
                categories: analyticsReducer?.gameRackReport?._id,
                labels: {
                    show: false
                },
                title: {
                    //text: `${moment(filterData.startDate).format('LL')} - ${filterData?.endDate ? moment(filterData?.endDate).format('LL') : moment(new Date()).format('LL')}`,
                    style: {
                        fontWeight: 900,
                        fontSize: '14px',
                        color: "rgb(55, 61, 63)",
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                tickAmount:5,
                min:0.0,
                max:1.0,
            },
            stroke: {
                width: 2
            },
            legend: {
                show: false
            }
        },
        series: [
            {
                name: "Collected Amount",
                data: filterChart.gameRackReport.collectedAmount ? analyticsReducer?.gameRackReport?.collectedAmount?.length > 0 ? analyticsReducer?.gameRackReport?.collectedAmount : [] : [],
                color: "#008ffb"
            },
            {
                name: "Given Win Amount",
                data: filterChart.gameRackReport.givenWinAmount ? analyticsReducer?.gameRackReport?.givenWinAmount?.length > 0 ? analyticsReducer?.gameRackReport?.givenWinAmount : [] : [],
                color: "rgb(0, 227, 150)"
            },
        ],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: ['30%'],
                endingShape: 'rounded'
            },
        },
    };

    const chartFiledDisabled = (type,field) =>{
        setFilterChart({
            ...filterChart,
            [type]: {
                ...filterChart[type],
                [field]: !filterChart[type][field]
            }
        });
    };
    return(
        <div className={'publisher-data-filter'}>
            <div className="chart outBox">
                <div className={"chart-title-data"}>
                    {/*//<p>Total Daily Active Users : 0</p>*/}
                </div>
                <div className={'chart-details-content'}>
                    {/*<h2>Daily Active Users</h2>*/}
                    <h2/>
                    <div className="chart_info mb_20">
                        <p className={filterChart?.gameRackReport?.collectedAmount ? "" : "disabled"} onClick={()=>chartFiledDisabled('gameRackReport','collectedAmount')} >Collected Amount<span className="dot dot_blue" /></p>
                        <p className={filterChart?.gameRackReport?.givenWinAmount ? "" : "disabled"} onClick={()=>chartFiledDisabled('gameRackReport','givenWinAmount')} >Given Win Amount<span className="dot dot_green" /></p>
                    </div>
                </div>

                <div className="chart_details">
                    <Chart
                        options={DailyUsers?.options}
                        series={DailyUsers?.series}
                        type="line"
                        height='300'
                    />
                </div>
            </div>
        </div>
    )
}
export default GameRackGraphReport