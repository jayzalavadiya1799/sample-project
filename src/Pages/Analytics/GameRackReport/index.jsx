import React, {useEffect, useState} from "react";
import GameRackGraphReport from "./GameRackGraphReport";
import moment from "moment";
import {useDispatch} from "react-redux";
import {getGameRackReport} from "../../../Redux/AnalyticsReport/action";

const GameRackReport = () => {
   const dispatch = useDispatch()
    const [loader, setLoader] = useState(false)
    const [filterData, setFilterData] = useState({
        startDate: moment().subtract(7, "days").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        statusValue: "Last 7 Days",
        exportFile: false,
        csvDownload: false,
        exportFileName: 'Export File',
        gameId:'',
        gameName:'All Game',
    });

    useEffect(() => {
        analyticsGameRackReportHandler()
    }, [filterData])

    const analyticsGameRackReportHandler = () => {
        let payload = {
            // startDate: filterData.startDate,
            // endDate: filterData.endDate,
            // gameId: filterData?.gameId === 0 ? null : filterData?.gameId,
            // publisherId: filterData?.publisherId === 0 ? null : filterData?.publisherId,
            // exportFile: filterData?.exportFile,
            // csvDownload: filterData?.csvDownload
        }

        if(filterData?.endDate !== null) {
            setLoader(true)
            dispatch(getGameRackReport(payload)).then(res => {
                if (res.data.statusCode === 200) {
                    setLoader(false)
                    if(res.data?.data?.filePath){
                        window.open(`${process.env.REACT_APP_END_POINT}${res?.data?.data?.filePath}`, '_blank');
                        filterData({
                            ...filterData,
                            csvDownload: false,
                            exportFile: false
                        })
                    }
                } else {
                    setLoader(false)
                }
            }).catch(e => {
                setLoader(false)
            })
        }

    }

    return(
        <div>
            <GameRackGraphReport filterData={filterData} setFilterData={setFilterData}/>
        </div>
    )
}
export default GameRackReport