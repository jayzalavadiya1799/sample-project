import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../hoc/PopContent";
import { addWithdrawalUPI, updateWithdrawalUPI } from "../../../Redux/Master/action";
import { jsonToFormData, profileImages } from "../../../utils";
import { Box } from "@mui/material";
import user from "../../../assets/images/avatar.png";
import icon_plus from "../../../assets/images/plus.svg";
import FilledButton from "../../../Components/FileButton";
import CommonModal from "../../../hoc/CommonModal";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const AddWithdrawalUPI = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const forceUpdate = useCallback(() => updateState({}), []);
    const [formData, setFormData] = useState({
        bankName: '',
        bankAndUPIIcon: '',
        isBankIconUpdated: false
    });

    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                UPIName: formData?.bankName,
                bankAndUPIIcon: formData?.bankAndUPIIcon,
                UPIId: modalValue?.row?._id,
                isUPIIconUpdated: formData?.isBankIconUpdated
            }
            if (!payload?.isUPIIconUpdated) {
                delete payload?.bankAndUPIIcon
            }
            setLoader(true)
            dispatch(updateWithdrawalUPI(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                UPIName: formData?.bankName,
                bankAndUPIIcon: formData?.bankAndUPIIcon
            }
            setLoader(true)
            dispatch(addWithdrawalUPI(jsonToFormData(payload))).then(res => {
                if (res.data.success) {
                    setLoader(false)
                    redirectApiHandler()
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenErrorModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const changeHandler = (e) => {
        const { value, name } = e.target;
        setFormData({
            ...formData,
            [name]: value
        })
    };

    const handleOpenErrorModal = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        if (modalValue?.isEdit) {
            setFormData({
                ...formData,
                bankAndUPIIcon: modalValue?.row?.bankIcon,
                bankName: modalValue?.row?.bankName,
            })
        }
    }, [modalValue?.isEdit]);

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{modalValue?.isEdit ? 'Update Withdrawal UPI' : 'Add New Withdrawal UPI'}</h2>
                </div>
                <div className={'add_admin_user_popup_content mt_15'}>
                    <form onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        <div className={'level_popup_form_field'}>
                            <div className='form_group profile new_game_section bank-icon-details' >
                                <div className='user_profile'>
                                    <div className='user_profile_pic'>
                                        {profileImages(formData?.bankAndUPIIcon, user)}
                                        <span className='addnew'>
                                            <img src={icon_plus} alt='' />
                                            <input type='file' name='bankAndUPIIcon' id='' onChange={(e) => setFormData({ ...formData, bankAndUPIIcon: e.target.files[0], isBankIconUpdated: true })} />
                                        </span>
                                    </div>
                                    <label htmlFor='' className='profile_label'> UPI Icon</label>
                                </div>
                                {simpleValidator.current.message("UpiIcon", formData?.bankAndUPIIcon, 'required')}
                            </div>
                            <div className={'level_popup_form_field_left'}>
                                <div className={'user_kyc_section'}>
                                    <div className={'user_kyc_section_filed'}>
                                        <label>UPI Name</label>
                                        <div className={'user_kyc_section_input_filed'}>
                                            <input type={'text'} value={formData?.bankName} name={'bankName'} onChange={(e) => changeHandler(e)} />
                                        </div>
                                        {simpleValidator.current.message("UpiName", formData?.bankName, 'required')}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenErrorModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenErrorModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default AddWithdrawalUPI