import Cookies from 'universal-cookie';
import {Navigate} from "react-router-dom";
import React from "react";
import NumberFormat from "react-number-format";
const cookies = new Cookies();


export const getAccessToken = () => cookies.get('token');
export const isAuthenticated = () => !!getAccessToken();


export const hideActionFunc=(role)=>{
    let agentCaption = (JSON.parse(localStorage.getItem('agentData')) || cookies.get('agentData'));
    return ((agentCaption !== 'null' && Object?.keys(agentCaption)?.length && agentCaption?.permission?.[role]?.viewer && agentCaption?.permission?.[role]?.editor) || ((JSON.parse(localStorage.getItem('userdata'))?.role || cookies.get('userdata')?.role) === 'Admin'))
}

export const ActionFunction = (role,props) =>{
    let agentCaption = (JSON.parse(localStorage.getItem('agentData')) || cookies.get('agentData'));
   return  ((agentCaption !== 'null' && Object?.keys(agentCaption)?.length && agentCaption?.permission?.[role]?.viewer && agentCaption?.permission?.[role]?.editor) || ((JSON.parse(localStorage.getItem('userdata'))?.role || cookies.get('userdata')?.role) === 'Admin'))  ?
            props
        :
       {
           id: 'action',
           type: 'hide'
       }
}

export default function CheckRoute({ authenticationPath, outlet, publicType}) {
    if(publicType ? !isAuthenticated() : isAuthenticated()) {
        return outlet
    } else {
        return <Navigate to={{ pathname: authenticationPath }} />
    }
};

function buildFormData(formData, data, parentKey) {
    if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
        Object.keys(data).forEach(key => {
            buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
        });
    } else {
        const value = data == null ? '' : data;
        formData.append(parentKey, value);
    }
};

export function jsonToFormData(data) {
    const formData = new FormData();
    buildFormData(formData, data);
    return formData;
}

export const generateAvatar = (text) => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    // Draw background
    context.fillStyle = stringToHslColor(text);
    context.fillRect(0, 0, canvas.width, canvas.height);

    // Draw text
    context.font = "bold 65px montserrat_regular";
    context.fillStyle = "#fff";
    context.textAlign = "center";
    context.textBaseline = "middle";

    let first = text?.split(' ')[0]?.charAt(0)?.toUpperCase();
    let last = text?.split(' ')[1]?.charAt(0)?.toUpperCase();

    if (!last) {
        last = text?.split(' ')[0]?.charAt(1)?.toUpperCase() || text?.split(' ')[0]?.charAt(0)?.toUpperCase();
    }
    if (!first) {
        first = "S";
        last = "U";
    }

    context.fillText(first + last, canvas.width / 2, canvas.height / 2);

    return canvas.toDataURL("image/png");
};

export const stringToHslColor = (str, s = 30, l = 80) => {
    let hash = 0;
    for (let i = 0; i < str?.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    let h = hash % 360;
    return 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
};

export function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
};


export const RouterSidebar = [
    {
        label:"Admin Users",
        adminUsers:true
    },
    {
      label: 'Add/Remove Admin Users',
        addRemoveAdminUsers:true
    },
    {
        label:"Users",
        usersList:true
    },
    {
      label: 'Edit User Info',
        editUserInfo:true
    },
    {
      label: "User Note" ,
        userNotes:true
    },
    {
      label: 'Block/Unblock Users',
        bloackUnblockUsers:true
    },
    {
        label:"Reported User",
        reportedUsersList:true
    },
    {
        label:"Games",
        games:true,
    },
    {
      label: 'Edit Game Info',
        editGameInfo:true
    },
    {
        label: 'Publisher',
        publishers:true
    },
    {
        label:"Analytics",
        viewAnalytics:true
    },
    {
        label:"SDK Management",
        sdkManagement:true
    },{
        label:"Approve/Reject Users Withdrawal Requests",
        approveRejectUserWithdrawls:true
    },
    {
        label:"Approve/Reject Publisher Commission Requests",
        approveRejectPublisherWithdrawls:true
    },
    {
        label: 'Change Password',
        changePassword:true
    },{
         label: 'Approve/Reject Games and Publishers',
        approveRejectGamesAndPublishers:true
    },{
        label: 'All Access',
        allAccess:true
    }

]

export const dotGenerator = (text,handleOpenModal,type) => {
    let comment = text;
    let result = "";
    comment = comment?.replaceAll('\n\n', ' ');
    comment = comment?.replaceAll('\n', ' ');
    let splitText = comment?.trim().split(' ');
    if (splitText?.length > 5 ) {
            result = <span>{`${splitText?.slice(0, 4).join(' ')}...`}
                         <button className={'rejected_reason_modal'} onClick={()=> handleOpenModal('ViewRejectedComment',{title:type,  data: text})}>more</button>
                     </span>
    }
    else {
        result = `${splitText?.slice(0, splitText?.length).join(' ')}`
    }
    return (result || "")
};

export const formatPhoneNumber = (str) => {
    let value = str;
    if (value) {
        value = value
            .replace(/\D+/g, "")
            .replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        return value;
    } else {
        return ''
    }

};

export const profileImages = (imgVal, commonImg) => {
    return  <img src={ imgVal ? (typeof imgVal === 'string' ? imgVal  :  URL.createObjectURL(imgVal)) : commonImg} alt='' />
}

export const decimalGenerate = (amount) =>{
    return amount % 1 === 0 ?'.00' : ''
}
export const expireTime = () => {
    return new Date(Date.now() + 24 + (60 * 60 * 1000));
};

export const AdminRole = [
    // {
    //     label: 'Admin Users',
    //     value: 'Admin Users'
    // },
    {
        label: 'User',
        value: 'user',
        user:{
            editor:false,
            viewer:false
        }
    },
    {
        label: 'Game',
        value: 'game',
        game:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Popular Game',
        value: 'popularGame',
        popularGame:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Bonus',
        value: 'bonus',
        bonus:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Revenue',
        value: 'revenue',
        revenue:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Master',
        value: 'master',
        master:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Website',
        value: 'webSite',
        webSite:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Setting',
        value: 'setting',
        setting:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'MGP Release',
        value: 'mgpRelease',
        mgpRelease:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Bank And UPI',
        value: 'bankAndUpi',
        bankAndUpi:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'TDS Report',
        value: 'tdsReport',
        tdsReport:{
            editor:false,
            viewer:false,
        }
    },
    {
        label: 'Help & Support',
        value: 'helpAndSupport',
        helpAndSupport:{
            editor:false,
            viewer:false,
        }
    },
]
export const currencyFormat = (cash) => {
    return <span> <NumberFormat value={cash || 0.00} displayType={'text'} thousandSeparator={true} prefix={'₹'} />{decimalGenerate(cash)}</span>
}