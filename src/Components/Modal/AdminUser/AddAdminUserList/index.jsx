import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import FilledButton from "../../../FileButton";
import { useDispatch, useSelector } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import { addAdminUserList, updateAdminUserList } from "../../../../Redux/AdminUser/action";
import DropDown from "./Dropdown";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 750,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const AddAdminUserList = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const adminUserRoleNames = useSelector(state => state?.adminUserReducer?.adminUserRoleNames);
    const [adminRole, setAdminRole] = useState({ email: '', fullName: '', adminUserRole: '', phoneNumber: '' });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setAdminRole({
            ...adminRole,
            [name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            addAdminHandler();
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const addAdminHandler = () => {
        setLoader(true);
        let payload = {
            ...adminRole,
            phoneNumber: adminRole?.phoneNumber?.replace(/[ ()-]/g, "")
        }
        dispatch(addAdminUserList(payload)).then(res => {
            if (res.data.success) {
                setLoader(false);
                redirectApiHandler();
                handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message });
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg });
            }
        });
    };

    useEffect(() => {
        if (modalValue.isEdit) {
            setAdminRole({
                ...adminRole,
                email: modalValue?.data?.email,
                fullName: modalValue?.data?.fullName,
                adminUserRole: modalValue?.data?.role,
                phoneNumber: modalValue?.data?.phoneNumber
            });
        }
    }, [modalValue]);
    const handleEditSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            editAdminHandler();
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const editAdminHandler = () => {
        let payload = {
            ...adminRole,
            adminUserId: modalValue?.data?._id
        }
        delete payload.email;
        setLoader(true);
        dispatch(updateAdminUserList(payload)).then(res => {
            if (res.data.success) {
                setLoader(false);
                redirectApiHandler();
                handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message });
            } else {
                handleOpenModal('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg });
            }
        })

    }

    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2>{`${modalValue?.isEdit ? 'Update Admin List' : 'Add Admin List'}`}</h2>
                </div>
                <div className={'add_admin_user_popup_content'}>
                    <form method={'POST'} onSubmit={modalValue?.isEdit ? (e) => handleEditSubmit(e) : (e) => handleSubmit(e)}>
                        <div className="formData">
                            <label>Enter Full Name </label>
                            <div className="emailWrap input_length_counter">
                                <input type="text" name='fullName' value={adminRole.fullName} placeholder={'Enter Full Name'} className={'wrap_input_modal'} maxLength={20} onChange={(e) => handleChange(e)} />
                                <span>{adminRole?.fullName?.length}/20</span>
                            </div>
                            {simpleValidator.current.message("fullName", adminRole?.fullName, 'required')}
                        </div>
                        <div className={'admin_list_flex'}>
                            <div className="formData left_side_filed">
                                <label>Enter Email</label>
                                <div className="emailWrap">
                                    <input type="email" name='email' placeholder={'Enter Email'} value={adminRole.email} onChange={(e) => handleChange(e)} readOnly={modalValue?.isEdit} />
                                </div>
                                {simpleValidator.current.message("email", adminRole?.email, "required|email")}
                            </div>
                            <div className="formData right_side_filed">
                                <label>Enter Mobile Number</label>
                                <div className="emailWrap">
                                    <input type="tel" name='phoneNumber' maxLength={10} placeholder={'Enter Mobile Number'} value={adminRole.phoneNumber} onChange={(e) => handleChange(e)} />
                                </div>
                                {simpleValidator.current.message("phoneNumber", adminRole?.phoneNumber, "required|min:10|numeric",)}
                            </div>
                        </div>
                        <div className={'add_admin_user_role'}>
                            <label>Role</label>
                            <DropDown adminRole={adminRole} setAdminRole={setAdminRole} name={'adminUserRole'} option={adminUserRoleNames} setValue={modalValue?.setValue} />
                            {/*<select className={'role_select_dropdown'} name={'adminUserRole'} value={adminRole.adminUserRole} onChange={(e) => handleChange(e)}>*/}
                            {/*    <option value={''}>Select Role</option>*/}
                            {/*    {*/}
                            {/*        adminUserRoleNames?.map(role => (*/}
                            {/*            <option value={role}>{role}</option>*/}
                            {/*        ))*/}
                            {/*    }*/}
                            {/*</select>*/}
                            {simpleValidator.current.message("adminUserRole", adminRole?.adminUserRole, "required")}
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={modalValue?.isEdit ? 'Update' : 'Save'} className={'submit_btn loader_css'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
        </Box>
    )
}
export default AddAdminUserList