import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";
import React from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const ViewRejectedComment = ({ modalValue }) => {
    return (
        <Box sx={style} className={'user_popup_section'}>
            <div className='sd_flex sd_justcenter '>
                <Typography id="modal-modal-title" variant="h6" component="h2" className={"block-user-title"}>
                    {
                        modalValue?.title
                    }
                </Typography>
            </div>
            <div className={'rejected_modal_content_text'}>
                <p> {modalValue?.data}</p>
            </div>
        </Box>
    )
}
export default ViewRejectedComment