import React, { useCallback, useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import AppleIcon from '@mui/icons-material/Apple';
import AndroidIcon from '@mui/icons-material/Android';
import FilledButton from "../../../FileButton";
import SelectDropdown from "../../../SelectDropdown";
import { useDispatch, useSelector } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import PopComponent from "../../../../hoc/PopContent";
import user from "../../../../assets/images/avatar.png";
import icon_plus from "../../../../assets/images/plus.svg";
import ScreenLockPortraitIcon from "@mui/icons-material/ScreenLockPortrait";
import ScreenLockLandscapeIcon from "@mui/icons-material/ScreenLockLandscape";
import {getSingleGameDetails, updateGame} from "../../../../Redux/games/action";
import CommonModal from "../../../../hoc/CommonModal";
import Tooltip from "@mui/material/Tooltip";
import PhonelinkIcon from "@mui/icons-material/Phonelink";
import { getGenreNames } from "../../../../Redux/games/GenreGame/action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 750,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const UpdateGame = ({ modalValue, handleOpenModal, redirectApiHandler }) => {
    const dispatch = useDispatch();
    const genreNamesList = useSelector(state => state?.gameReducer?.genreNamesList)
    const [loader, setLoader] = useState(false)
    const simpleValidator = useRef(new SimpleReactValidator());
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [filterStage, setFilterStage] = useState({ android: false, ios: false, crossPlatform: false });
    const [filterOrientation, setFilterOrientation] = useState({ portrait: false, landscape: false, orientationField: "" });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];
    const [formData, setFormData] = useState({
        gameName: '', genre: '', description: '', platform: '', isOrientationPortrait: '', format: '',
        engine: '', gameIcon: '', gameDesignDocLink: '', youtubeVideoLink: '', isGameModeOption: false, gameTag: 'New Game', isNoOfPlayer: false, isMultipleDeck: false
    });
    const gameDetails = useSelector(state => state?.gameReducer?.gameDetails)

    useEffect(() => {
        dispatch(getGenreNames({ genreStatus: 'Active' }));
    }, [])

    const stageCheckboxFilter = (e) => {
        const { checked, value, name } = e.target;
        setFilterStage({ ...filterStage, [e.target.name]: checked });
        setFormData({
            ...formData,
            platform: name
        })
        if (name === 'Android' && checked) {
            setFilterStage({ ...filterStage, android: true, ios: false, crossPlatform: false })
        }
        if (name === 'Ios' && checked) {
            setFilterStage({ ...filterStage, android: false, ios: true, crossPlatform: false })
        }
        if (name === 'Cross-Platform' && checked) {
            setFilterStage({ ...filterStage, android: false, ios: false, crossPlatform: true })
        }
    };

    const orientationCheckboxFilter = (e) => {
        const { checked, value, name } = e.target;
        setFilterOrientation({ ...filterOrientation, [e.target.name]: checked });
        if (name === 'portrait' && checked) {
            setFilterOrientation({ ...filterOrientation, portrait: true, landscape: false, orientationField: e.target.name })
        }
        if (name === 'landscape' && checked) {
            setFilterOrientation({ ...filterOrientation, portrait: false, landscape: true, orientationField: e.target.name })
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            let payload = {
                ...formData,
                format: formData?.format === 'Live Multiplayer (Sync)' ? 'Sync' : 'Async',
                isOrientationPortrait: filterOrientation?.orientationField === "portrait",
                isIconUpdated: typeof formData?.gameIcon !== "string",
                gameId: gameDetails?._id,
                publisherId: gameDetails?.publisherId?._id
            }
            if (typeof payload?.gameIcon === "string") {
                delete payload.gameIcon
            }
            setLoader(true)
            dispatch(updateGame(payload)).then(res => {
                if (res.data.success) {
                    if (!modalValue?.isGameHeader) {
                        redirectApiHandler();
                    }
                    if(modalValue?.isGameHeader){
                        dispatch(getSingleGameDetails({ gameId: gameDetails?._id }))
                    }
                    setLoader(false)
                    handleOpenModal('CommonPop', { header: "Success", body: res?.data?.message })
                } else {
                    setLoader(false)
                    handleOpenModalError('CommonPop', { header: "Info", body: res?.data?.message || res?.data?.msg })
                }
            })
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const handleChange = (e) => {
        const { value, name } = e.target;
        setFormData({
            ...formData,
            [name]: value
        })
    };

    const handleOpenModalError = (type, data) => {
        switch (type) {
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    useEffect(() => {
        if (Object?.keys(gameDetails)?.length > 0) {
            setFormData({
                ...formData,
                gameName: gameDetails?.gameName,
                youtubeVideoLink: gameDetails?.youtubeVideoLink,
                gameDesignDocLink: gameDetails?.gameDesignDocLink,
                description: gameDetails?.description,
                platform: gameDetails?.platform,
                format: gameDetails?.format === 'Sync' ? 'Live Multiplayer (Sync)' : 'Score Submission (Async)',
                engine: gameDetails?.engine,
                isMultipleDeck: gameDetails?.isMultipleDeck,
                genre: gameDetails?.genre,
                gameIcon: gameDetails?.gameIcon,
                gameTag: gameDetails?.gameTag,
                isNoOfPlayer: gameDetails?.isNoOfPlayer,
                isGameModeOption: gameDetails?.isGameModeOption
            });
            if (gameDetails?.platform === 'Android') {
                setFilterStage({ ...filterStage, android: true, ios: false, crossPlatform: false })
            } else if (gameDetails?.platform === 'Ios') {
                setFilterStage({ ...filterStage, android: false, ios: true, crossPlatform: false })
            } else {
                setFilterStage({ ...filterStage, android: false, ios: false, crossPlatform: true })
            }
            if (gameDetails?.isOrientationPortrait) {
                setFilterOrientation({ portrait: true, landscape: false, orientationField: "portrait" })
            } else {
                setFilterOrientation({ portrait: false, landscape: true, orientationField: "landscape" })
            }
        }
    }, [gameDetails]);

    return (
        <Box sx={style} className={'update_game_modal'}>
            <div className={'add_game_popup'}>
                <div className={'add_game_popup_title'}>
                    <h2>Update Game information</h2>
                </div>
                <div className={'add_game_popup_content'}>
                    <div className={'add_game_popup_content_form'}>
                        <form className={'popup_form'} method={'POST'} onSubmit={(e) => handleSubmit(e)}>
                            <div>
                                <div className={'add_game_section_content_form'}>
                                    <div className="formData">
                                        <label>Enter Game Name *</label>
                                        <div className="emailWrap input_length_counter">
                                            <input type="text" value={formData?.gameName} className={'wrap_input_modal'} maxLength={20} name='gameName' placeholder={'Game Name'} onChange={(e) => handleChange(e)} />
                                            <span>{formData?.gameName?.length}/20</span>
                                        </div>
                                        {simpleValidator.current.message("game name", formData?.gameName, 'required')}
                                    </div>

                                    <div className='form_group profile new_game_section'>
                                        <div className='user_profile'>
                                            <div className='user_profile_pic'>
                                                <img src={typeof formData?.gameIcon === 'string' ? formData?.gameIcon : typeof formData?.gameIcon !== 'string' ? URL.createObjectURL(formData?.gameIcon) : user} alt='' />
                                                <span className='addnew'>
                                                    <img src={icon_plus} alt='' />
                                                    <input type='file' name='member_photo' id='' onChange={(e) => setFormData({ ...formData, gameIcon: e.target.files[0] })} />
                                                </span>
                                            </div>
                                            <label htmlFor='' className='profile_label'>Game Logo</label>
                                        </div>
                                    </div>
                                </div>

                                <div className={'popup_form_checkbox'}>
                                    <div className="formData">
                                        <label>Platform *</label>
                                        <div className={'platform_field'}>
                                            {
                                                gameDetails?.platform === 'Ios' ?
                                                    <Tooltip title="Android Platform">
                                                        <div className={filterStage.android ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <AndroidIcon />
                                                            <input type="checkbox" name='Android' checked={filterStage.android} className={'disabled_checkBox'} />
                                                        </div>
                                                    </Tooltip>
                                                    :
                                                    <Tooltip title="Android Platform">
                                                        <div className={filterStage.android ? 'checkboxWrap activePlatformIcon' : "checkboxWrap "}>
                                                            <AndroidIcon />
                                                            <input type="checkbox" name='Android' checked={filterStage.android} onChange={(e) => stageCheckboxFilter(e)} />
                                                        </div>
                                                    </Tooltip>
                                            }
                                            {
                                                gameDetails?.platform === 'Android' ?
                                                    <Tooltip title="iOS Platform">
                                                        <div className={filterStage.ios ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <AppleIcon />
                                                            <input type="checkbox" name='Ios' checked={filterStage.ios} className={'disabled_checkBox'} />
                                                        </div>
                                                    </Tooltip>
                                                    :
                                                    <Tooltip title="iOS Platform">
                                                        <div className={filterStage.ios ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                            <AppleIcon />
                                                            <input type="checkbox" name='Ios' checked={filterStage.ios} onChange={(e) => stageCheckboxFilter(e)} />
                                                        </div>
                                                    </Tooltip>
                                            }

                                            <Tooltip title="Cross-Platform">
                                                <div className={filterStage.crossPlatform ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                    <PhonelinkIcon />
                                                    <input type="checkbox" name='Cross-Platform' checked={filterStage.crossPlatform} onChange={(e) => stageCheckboxFilter(e)} />
                                                </div>
                                            </Tooltip>
                                        </div>
                                        {simpleValidator.current.message("Platform", formData.platform, 'required')}
                                    </div>
                                    <div className="formData orientation_filed">
                                        <label>Orientation * </label>
                                        <div className={'platform_field'}>
                                            <Tooltip title="Portrait Orientation">
                                                <div className={filterOrientation.portrait ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                    <ScreenLockPortraitIcon />
                                                    <input type="checkbox" name='portrait' checked={filterOrientation.portrait} onChange={(e) => orientationCheckboxFilter(e)} />
                                                </div>
                                            </Tooltip>
                                            <Tooltip title="Landscape Orientation">
                                                <div className={filterOrientation.landscape ? 'checkboxWrap activePlatformIcon' : "checkboxWrap"}>
                                                    <ScreenLockLandscapeIcon />
                                                    <input type="checkbox" name='landscape' checked={filterOrientation.landscape} onChange={(e) => orientationCheckboxFilter(e)} />
                                                </div>
                                            </Tooltip>
                                        </div>
                                        {simpleValidator.current.message("Orientation", filterOrientation.orientationField, 'required')}
                                    </div>
                                </div>
                                <div className={'select_game_platform_value'}>
                                    <div className={'select_label tab01'}>
                                        <label>Genre *</label>
                                        <SelectDropdown name={'genre'} options={genreNamesList} formData={formData} setFormData={setFormData} handleOpenModal={handleOpenModal} />
                                        {simpleValidator.current.message("selectGenre", formData.genre, 'required')}
                                    </div>
                                    <div className={'select_label tab02'}>
                                        <label>Format *</label>
                                        <SelectDropdown name={'format'} isFormat={true} formData={formData} setFormData={setFormData} handleOpenModal={handleOpenModal} />
                                        {simpleValidator.current.message("selectFormat", formData.format, 'required')}
                                    </div>
                                </div>
                                <div className={'select_game_platform_value game_mode game_mode_main_section'}>
                                    <div className={'select_label tab01 game_mode_left_details'}>
                                        <label>Is Game Mode ? *</label>
                                        <div className={'game_mode_btn'}>
                                            <div className={'game_mode_btn_option'}>
                                                <input type={'radio'} name={'isGameMode'} checked={formData?.isGameModeOption} onChange={(e) => setFormData({ ...formData, isGameModeOption: true })} />
                                                <label>Yes</label>
                                            </div>
                                            <div className={'game_mode_btn_option tab_radio'}>
                                                <input type={'radio'} name={'isGameMode'} checked={!formData?.isGameModeOption} onChange={(e) => setFormData({ ...formData, isGameModeOption: false })} />
                                                <label>No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={'select_label tab02'}>
                                        <label>Engine *</label>
                                        <SelectDropdown name={'engine'} options={['Unity']} formData={formData} setFormData={setFormData} handleOpenModal={handleOpenModal} />
                                        {simpleValidator.current.message("selectEngine", formData.engine, 'required')}
                                    </div>
                                </div>

                                <div className="formData">
                                    <label>Enter Game Description (120 Chars) </label>
                                    <div className="text_Wrap">
                                        <textarea name='description' rows={3} value={formData?.description} maxLength={120} onChange={(e) => handleChange(e)} />
                                    </div>
                                </div>
                                {/*-------------------------------------------- Number of Player ----------------------------------------------------------------*/}
                                <div className={'select_game_platform_value game_mode game_mode_main_section'}>
                                    <div className={'select_label tab01 game_mode_left_details'}>
                                        <label>Is Number Of Player ? *</label>
                                        <div className={'game_mode_btn'}>
                                            <div className={'game_mode_btn_option'}>
                                                <input type={'radio'} name={'isNoOfPlayer'} checked={formData?.isNoOfPlayer} onChange={(e) => setFormData({ ...formData, isNoOfPlayer: true })} />
                                                <label>Yes</label>
                                            </div>
                                            <div className={'game_mode_btn_option tab_radio'}>
                                                <input type={'radio'} name={'isNoOfPlayer'} checked={!formData?.isNoOfPlayer} onChange={(e) => setFormData({ ...formData, isNoOfPlayer: false })} />
                                                <label>No</label>
                                            </div>
                                        </div>
                                    </div>
                                    {
                                        formData?.genre === 'Card' &&
                                        <div className={'select_game_platform_value game_mode game_mode_main_section edit-game-decks'}>
                                            <div className={'select_label tab01 game_mode_left_details'}>
                                                <label>Is Multiple Number Of Deck?<span className={'validation-star'}>*</span></label>
                                                <div className={'game_mode_btn'}>
                                                    <div className={'game_mode_btn_option'}>
                                                        <input type={'radio'} name={'isMultipleDeck'} checked={formData?.isMultipleDeck} onChange={(e) => setFormData({ ...formData, isMultipleDeck: true })} />
                                                        <label>Yes</label>
                                                    </div>
                                                    <div className={'game_mode_btn_option tab_radio'}>
                                                        <input type={'radio'} name={'isMultipleDeck'} checked={!formData?.isMultipleDeck} onChange={(e) => setFormData({ ...formData, isMultipleDeck: false })} />
                                                        <label>No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                                {/*--------------------------------------------End Number of Player ----------------------------------------------------------------*/}
                                <div className={'design_document'}>
                                    <div className={'design_document_title'}>
                                        <div className={'sub_title_content'}>
                                            <h4>How to Play Youtube Video Link</h4>
                                            <input type={'text'} name={'youtubeVideoLink'} value={formData?.youtubeVideoLink} placeholder={'Share with us the Youtube Video Link.'} onChange={(e) => handleChange(e)} />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className={'formData_btn'}>
                                <button className={'cancel_btn'} type={'reset'} onClick={() => handleOpenModal()}>Cancel</button>
                                <FilledButton type={'submit'} value={'Update'} className={'submit_btn loader_css'} loading={loader} />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleOpenModalError}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleOpenModalError} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default UpdateGame