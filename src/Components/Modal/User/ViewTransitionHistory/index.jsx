import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import TableCell from "@mui/material/TableCell";

import CustomTable from "../../../../hoc/CommonTable";
import { currencyFormat } from "../../../../utils";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 950,
    bgcolor: 'white',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const ViewTransitionHistory = ({ modalValue }) => {
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });

    const columns = [
        {
            id: 'currentBonus',
            numeric: true,
            disablePadding: false,
            label: 'Current Bonus',
            type: 'custom',
            render: (row) => {
                return <TableCell > {currencyFormat(row?.currentBonus)}</TableCell>
            }
        },
        {
            id: 'previousBonus',
            numeric: true,
            disablePadding: false,
            label: 'Previous Bonus',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.previousBonus)}</TableCell>
            }
        },
        {
            id: 'currentDepositsCash',
            numeric: true,
            disablePadding: false,
            label: 'current Deposits',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.currentDepositsCash)}</TableCell>
            }
        },
        {
            id: 'previousDepositsCash',
            numeric: true,
            disablePadding: true,
            label: 'Previous Deposits',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.previousDepositsCash)}</TableCell>
            }
        },
        {
            id: 'currentWinCash',
            numeric: true,
            disablePadding: false,
            label: 'Current Win',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.currentWinCash)}</TableCell>
            }
        },
        {
            id: 'previousWinCash',
            numeric: true,
            disablePadding: false,
            label: 'Previous Win',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat(row?.previousWinCash)}</TableCell>
            }
        },
    ];

    return (
        <Box sx={style} >
            <CustomTable
                headCells={columns}
                rowData={modalValue}
                totalDocs={modalValue?.length}
                pagination={pagination}
                setPagination={setPagination}
                isSystemTotal={true}
            />
        </Box>
    )
}
export default ViewTransitionHistory