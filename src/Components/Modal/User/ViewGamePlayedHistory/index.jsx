import Box from "@material-ui/core/Box";
import CustomTable from "../../../../hoc/CommonTable";
import React, { useEffect, useState } from "react";
import TableCell from "@mui/material/TableCell";
import moment from "moment";
import { useDispatch } from "react-redux";
import { getUserPlayedDetailsGames } from "../../../../Redux/user/action";
import { useParams } from "react-router-dom";
import { currencyFormat } from "../../../../utils";
import Loader from "../../../../images/Loader";
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'white',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const ViewGamePlayedHistory = ({ modalValue }) => {
    const dispatch = useDispatch();
    const { id } = useParams()
    const [loader, setLoader] = useState(false)
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });
    const columns = [
        {
            id: 'date',
            numeric: true,
            disablePadding: true,
            label: 'Date',
            type: 'custom',
            render: (row) => {
                return <TableCell >{moment(row?.createdAt).format('MMM DD YYYY, HH:MM a')}</TableCell>
            }
        },
        {
            id: 'Username',
            numeric: true,
            disablePadding: false,
            label: 'User Name',
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.userId?.fullName}</TableCell>
            }
        },
        {
            id: 'rank',
            numeric: true,
            disablePadding: false,
            label: 'Rank',
        },
        {
            id: 'score',
            numeric: true,
            disablePadding: false,
            label: 'Score',
        },
        {
            id: 'winStatus',
            numeric: true,
            disablePadding: false,
            label: 'Win Status',
        },
        {
            id: 'winAmount',
            numeric: true,
            disablePadding: false,
            label: 'Amount',
            type: 'custom',
            render: (row) => {
                return <TableCell >{currencyFormat((row?.winStatus === 'Win' || row?.winStatus === 'Tie') ? (row.winAmount || 0.00) : (row.entryFee || 0.00))} </TableCell>
            }
        },
    ];

    useEffect(() => {
        getUserPlayedDetailsHandler()
    }, [pagination.rowsPerPage, pagination.page]);

    // get User Api and All Filter Api
    const getUserPlayedDetailsHandler = () => {
        setLoader(true)
        let payload = {
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
            gameId: modalValue?.id,
            userId: id
        }
        dispatch(getUserPlayedDetailsGames(payload)).then(res => {
            setLoader(false)
            if (res.data?.success) {
                setRowData({
                    ...rowData,
                    list: res.data.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                })
            } else {
                setRowData({
                    ...rowData,
                    list: [],
                    totalDocs: 0
                })
            }
        })
    };
    return (
        <Box sx={style} >
            {loader ? <Loader /> : ""}
            <CustomTable
                headCells={columns}
                rowData={rowData?.list}
                totalDocs={rowData?.totalDocs}
                pagination={pagination}
                setPagination={setPagination}
            />
        </Box>
    )
}
export default ViewGamePlayedHistory