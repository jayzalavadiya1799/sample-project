import React, { useEffect, useState } from "react";
import TableCell from "@mui/material/TableCell";
import Box from "@material-ui/core/Box";
import CustomTable from "../../../../hoc/CommonTable";
import { useDispatch } from "react-redux";
import PopComponent from "../../../../hoc/PopContent";
import { userReportedListData } from "../../../../Redux/user/action";
import Loader from "../../../../images/Loader";
import { dotGenerator } from "../../../../utils";
import CommonModal from "../../../../hoc/CommonModal";
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'white',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const ViewReportedUserList = ({ modalValue }) => {
    const dispatch = useDispatch()
    const [loader, setLoader] = useState(false);
    const [rowData, setRowData] = useState({ list: [], totalDocs: 0 });
    const [modalDetails, setModalDetails] = useState({ modalValue: '', modalName: '', modalIsOpen: false });
    let Modal = PopComponent[modalDetails.modalName];

    const [pagination, setPagination] = useState({
        rowsPerPage: 10,
        page: 0
    });

    const columns = [
        {
            id: '',
            numeric: true,
            disablePadding: false,
            label: 'Reported User Id',
            isDisbanding: true,
            type: 'custom',
            render: (row, i) => {
                return <TableCell><span className=''>{`UID000${row?.reportedUserId?.numericId || (i + 1)}`}</span></TableCell>
            }
        },
        {
            id: 'Username',
            numeric: true,
            disablePadding: false,
            isDisbanding: true,
            label: 'Reported User Name',
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.reportedUserId?.fullName}</TableCell>
            }
        },
        {
            id: 'number',
            numeric: true,
            disablePadding: false,
            isDisbanding: true,
            label: 'Mobile Number',
            type: 'custom',
            render: (row) => {
                return <TableCell >{row?.reportedUserId?.phoneNumber}</TableCell>
            }
        },
        {
            id: 'description',
            numeric: true,
            disablePadding: false,
            isDisbanding: true,
            label: 'Description',
            type: 'custom',
            render: (row) => {
                return <TableCell >{dotGenerator(row?.descripton, handleErrorOpenModal, 'Reported Description')}</TableCell>
            }
        },
    ];

    useEffect(() => {
        let payload = {
            userId: modalValue,
            limit: pagination.rowsPerPage,
            start: ((pagination.page + 1) - 1) * pagination.rowsPerPage,
        }
        setLoader(true)
        dispatch(userReportedListData(payload)).then(res => {
            setLoader(false)
            if (res.data.success) {
                setRowData({
                    ...rowData,
                    list: res?.data?.data?.docs,
                    totalDocs: res?.data?.data?.totalDocs
                });
            }
        })
    }, [])

    const handleErrorOpenModal = (type, data) => {
        switch (type) {
            case 'ViewRejectedComment': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            case 'CommonPop': {
                setModalDetails({ ...modalDetails, modalValue: data, modalName: type, modalIsOpen: true });
                break;
            }
            default: {
                setModalDetails({ ...modalDetails, modalIsOpen: false })
            }
        }
    };

    return (
        <Box sx={style} >
            {loader ? <Loader /> : ""}
            <CustomTable
                headCells={columns}
                rowData={rowData?.list}
                totalDocs={rowData?.totalDocs}
                pagination={pagination}
                setPagination={setPagination}
            />
            {/*--------------------------------------------------------Common Popup-------------------------------------------------*/}
            <CommonModal className={'Approved-reject-section'} modalIsOpen={modalDetails.modalIsOpen} handleOpenModal={handleErrorOpenModal}>
                <Modal modalValue={modalDetails.modalValue} handleOpenModal={handleErrorOpenModal} modalIsOpen={modalDetails.modalIsOpen} />
            </CommonModal>
        </Box>
    )
}
export default ViewReportedUserList