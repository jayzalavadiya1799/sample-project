import React, { useCallback, useRef, useState } from "react";
import { Box } from "@mui/material";
import FilledButton from "../../../FileButton";
import SimpleReactValidator from "simple-react-validator";
import { updateBonus, updateDepositCash } from "../../../../Redux/user/action";
import { useDispatch } from "react-redux";
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 550,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};
const UpdateCashAndBonus = ({ modalValue, handleOpenModal }) => {
    const dispatch = useDispatch();
    const [loader] = useState(false);
    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => updateState({}), []);
    const [formData, setFormData] = useState({
        bonus: '',
        userId: modalValue.id,
        cash: ''
    })
    const simpleValidator = useRef(new SimpleReactValidator());

    const handleSubmit = (e) => {
        e.preventDefault();
        if (simpleValidator.current.allValid()) {
            updateBonusAndCashDetails();
        } else {
            simpleValidator.current.showMessages();
            forceUpdate();
        }
    };

    const updateBonusAndCashDetails = () => {
        let payload = {
            ...formData
        }
        if (modalValue?.isModalCash) {
            delete payload.bonus;
            dispatch(updateDepositCash(payload)).then(res => {
                if (res.data.statusCode === 200 && res.data.success) {
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message })
                } else {
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            })
        } else {
            delete payload.cash;
            dispatch(updateBonus(payload)).then(res => {
                if (res.data.statusCode === 200 && res.data.success) {
                    handleOpenModal('CommonPop', { header: "Success", body: res.data.message })
                } else {
                    handleOpenModal('CommonPop', { header: "Info", body: res.data.message || res?.data?.msg })
                }
            })
        }

    };

    const handleChange = (e) => {
        const { value, name } = e.target
        setFormData({
            ...formData,
            [name]: value
        })
    };


    return (
        <Box sx={style}>
            <div className={'add_admin_user_popup'}>
                <div className={'add_admin_user_popup_title'}>
                    <h2> {modalValue?.isModalCash ? 'Add Deposit Cash' : 'Add Bonus'}</h2>
                </div>
                <div className={'add_admin_user_popup_content_pop'}>
                    <form method={'POST'} onSubmit={(e) => handleSubmit(e)}>
                        <div className="formData">
                            <label>{modalValue?.isModalCash ? 'Deposit Cash' : 'Bonus'} * </label>
                            <div className="emailWrap">
                                {
                                    modalValue?.isModalCash ?
                                        <input type="number" onWheel={event => event.currentTarget.blur()} name='cash' value={formData?.cash} onChange={(e) => handleChange(e)} />
                                        :
                                        <input type="number" onWheel={event => event.currentTarget.blur()} name='bonus' value={formData?.bonus} onChange={(e) => handleChange(e)} />
                                }

                            </div>
                            {
                                modalValue?.isModalCash ?
                                    simpleValidator.current.message("cash", formData?.cash, "required")
                                    :
                                    simpleValidator.current.message("bonus", formData?.bonus, "required")
                            }
                        </div>
                        <div className={'formData_btn'}>
                            <button className={'cancel_btn'} onClick={() => handleOpenModal()}>Cancel</button>
                            <FilledButton type={'submit'} value={'Submit'} className={'submit_btn'} loading={loader} />
                        </div>
                    </form>
                </div>
            </div>
        </Box>
    )
}
export default UpdateCashAndBonus