import React from "react";
import SearchFilter from "./SearchFilter";
import ExportDropDown from "./ExportDropDown";
import DayWiseDropDown from "./DayWiseDropDown";
import CustomDateFilter from "./CustomDateFilter";
import GameStatusDropDown from "./GameStatusDropDown";
import GameQuarterDropDown from "./GameQuarterDropDown";

const MainCommonFilter = ({ filterData, setFilterData, searchApiHandler, pagination, setPagination, statusOption, plateFormOption, addPropsFilter, gameTagOption }) => {
    return (
        <div className={'filter_details_tab_section'}>
            <div className={'filter_inner_tab_info'}>
                <div className={'filter_export_date_dropdown'}>
                    {
                        !addPropsFilter?.isAvatar && (
                            <>
                                {(!addPropsFilter?.isWithdrawal && !addPropsFilter?.isGamePlayed && !addPropsFilter?.revenueGame && !addPropsFilter?.isKYCList) && <ExportDropDown option={['Export File', 'CSV File', 'Excel File']} name={'exportFileName'} filterData={filterData} setFilterData={setFilterData} />}
                                {addPropsFilter?.isGameList && (
                                    <>
                                        {(!addPropsFilter?.isPending || addPropsFilter?.isKYCList) && (<GameStatusDropDown option={statusOption} name={'statusField'} filterData={filterData} setFilterData={setFilterData} />)}
                                        {(!addPropsFilter?.isGamePlayed && !addPropsFilter?.isKYCList) && (<GameStatusDropDown option={plateFormOption} name={'platformName'} filterData={filterData} setFilterData={setFilterData} />)}
                                        {(addPropsFilter?.gameTag) && (<GameStatusDropDown option={gameTagOption} name={'gameTag'} filterData={filterData} setFilterData={setFilterData} />)}
                                    </>
                                )}
                                {(!addPropsFilter?.isWithdrawal && !addPropsFilter?.isGamePlayed && !addPropsFilter?.isKYCList) && (<DayWiseDropDown option={['All Days', 'Last 7 Days', 'Last 14 Days', 'Last 30 Days', 'Custom']} name={'statusValue'} filterData={filterData} setFilterData={setFilterData} />)}

                                {(filterData?.statusValue === 'Custom') && <CustomDateFilter filterData={filterData} setFilterData={setFilterData} />}
                            </>
                        )
                    }
                    {
                        addPropsFilter?.isTDSReport && (<GameQuarterDropDown option={['All Quarter', 'Quarter 1', 'Quarter 2', 'Quarter 3', 'Quarter 4', 'Financial Year']} name={'gameQuarter'} filterData={filterData} setFilterData={setFilterData} />)
                    }
                    {
                        (!addPropsFilter?.isGamePlayed && !addPropsFilter?.revenueGame && !addPropsFilter?.isTDSReport && !addPropsFilter?.isKYCList) &&
                        <SearchFilter filterData={filterData} setFilterData={setFilterData} searchApiHandler={searchApiHandler} pagination={pagination} setPagination={setPagination} />
                    }
                </div>
            </div>
        </div>
    )
}
export default MainCommonFilter;