import React from "react";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import { Button } from "@mui/material";

const SearchFilter = ({ filterData, setFilterData, searchApiHandler, pagination, setPagination }) => {
    const handleChange = (event) => {
        setFilterData({
            ...filterData,
            search: event.target.value,
            filterClose: true
        })
    };
    const handleSearch = () => {
        setPagination({
            ...pagination,
            page: 0
        })
        searchApiHandler(filterData.startDate, filterData.endDate, filterData.search.trim())
    };
    const filterSearchHandler = () => {
        setFilterData({
            ...filterData,
            filterClose: false,
            search: ""
        })
        searchApiHandler()
    };
    return (
        <div className={'search-filter-section'}>
            <FormControl className={'search-input'}>
                <OutlinedInput id='component-outlined' placeholder={'search'} className={'input_search_field  fontFamily'} name={'search'} value={filterData?.search} onChange={(e) => handleChange(e)} />
                {
                    filterData.filterClose &&
                    <p onClick={() => filterSearchHandler()}>
                        <svg viewBox="0 0 24 24" x="1008" y="432" fit="" height="28" width="25"
                            preserveAspectRatio="xMidYMid meet" focusable="false">
                            <path
                                d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z"
                                fill="#64748b" />
                        </svg>
                    </p>
                }
            </FormControl>
            <Button variant='contained' className={filterData?.search?.length > 0 ? "search_btn text-color active-filter-active" : "search_btn"} disabled={filterData?.search?.length < 1} onClick={() => handleSearch()} >
                <svg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' stroke='currentColor' height='100%' width='100%'
                    preserveAspectRatio='xMidYMid meet' focusable='false' style={{ width: '24px' }}>
                    <path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z' />
                </svg>
            </Button>
        </div>
    );
};

export default SearchFilter;