import React, { useState } from "react";
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

const CustomDateFilter = ({ filterData, setFilterData }) => {
    const [openCale, setOpenCale] = useState({
        endDate: false,
        startDate: false
    });

    const handleDatePicker = (newValue, type) => {
        if (type === 'startDate') {
            setFilterData({
                ...filterData,
                [type]: newValue,
                statusValue: "Custom",
                endDate: null
            });
        } else {
            setFilterData({ ...filterData, endDate: newValue });
        }
        setOpenCale({ ...openCale, [type]: false });
    };

    return (
        <div className={'custom_date_filter'}>
            <div className={'start-date-picker'}>
                <LocalizationProvider dateAdapter={AdapterDateFns} >
                    <DatePicker
                        name='start-date'
                        value={filterData?.startDate}
                        onChange={(newValue) => handleDatePicker(newValue, 'startDate')}
                        open={openCale.startDate}
                        onClose={() => setOpenCale({ ...openCale, startDate: false })}
                        renderInput={(params) => {
                            return <TextField {...params} onClick={() => setOpenCale({ ...openCale, startDate: !openCale?.startDate })} />
                        }}
                        inputFormat="MMM dd, yyyy"
                        inputProps={{ readOnly: true }}
                    />
                </LocalizationProvider>
            </div>
            <div className={'date-to'}>TO</div>
            <div className={'end-date-picker'}>
                <LocalizationProvider dateAdapter={AdapterDateFns} >
                    <DatePicker
                        name='end-date'
                        value={filterData.endDate}
                        onChange={(newValue) => handleDatePicker(newValue, 'endDate')}
                        open={openCale.endDate}
                        onClose={() => setOpenCale({ ...openCale, endDate: false })}
                        inputFormat="MMM dd, yyyy"
                        minDate={filterData?.startDate}
                        renderInput={(params) => <TextField {...params} onClick={() => setOpenCale({ ...openCale, endDate: !openCale?.endDate })} />}
                        inputProps={{ readOnly: true }}
                    />
                </LocalizationProvider>
            </div>
            {/*{*/}
            {/*    (filterData?.startDate &&  filterData.endDate) &&*/}
            {/*    <div>*/}
            {/*        <button>Clear Filter</button>*/}
            {/*    </div>*/}
            {/*}*/}

        </div>
    )
}
export default CustomDateFilter