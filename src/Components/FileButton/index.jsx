import React from 'react';
import { CircularProgress } from '@mui/material';

const FilledButton = ({ type = "button", className = "btn btn_gray", value = "Clear", loading, disabledLoading, loader_class, ...rest }) => {
    return (
        <button disabled={loading} {...rest} type={type} className={className}>
            {(loading && !disabledLoading) ?
                <div className={loader_class ? `btn-loader ${loader_class}` : "btn-loader"}>
                    <CircularProgress />
                </div> : value}
        </button>
    );
};

export default FilledButton;