
export const revenueGameWise = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("revenue/gameWise", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const revenueOverAll = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("revenue/overAll", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};