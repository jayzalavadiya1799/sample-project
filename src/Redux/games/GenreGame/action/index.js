export const getGenreNames = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getGenreNames", payload)
    .then((res) => {
      if (res.data.success) {
        dispatch({
          type: "GET_GENRE_NAMES",
          payload: res.data.data.genreNames,
        });
      }
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const getGenreList = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getGenres", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const genreDeleteCategory =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("genre", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const genreActiveCategory =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("genre/activeDeactiveGenre", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const addGenreList = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("genre", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const editGenreList = (payload) => async (dispatch, getState, api) => {
  return await api
    .put("genre", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
