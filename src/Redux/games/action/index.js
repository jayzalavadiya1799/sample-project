import { jsonToFormData } from "../../../utils";

export const getGameList = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("games", payload)
    .then((res) => {
      if (res.status === 200 && typeof res?.data?.data?.filePath !== "string") {
        dispatch({
          type: "GAME_LIST",
          payload: res?.data.data,
        });
      }
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const addGame = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("game", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const updateGame = (payload) => async (dispatch, getState, api) => {
  return await api
    .put("game", jsonToFormData(payload))
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const updateGameStatus =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/approveReject", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateGameStatusActiveDeactivated =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/activeDeactive", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const archivedGameStatus =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/archiveUnarchive", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//getGameDetails

export const getSingleGameDetails =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getGameDetails", payload)
      .then((res) => {
        if (res.data.success) {
          dispatch({
            type: "SINGLE_GAME_DETAILS",
            payload: res.data.data,
          });
        }
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const createHeadToHeadGame =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("headTohead", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateHeadToHeadGame =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("headTohead", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getHeadToHeadsGameList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getHeadToHeads", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const deleteHeadToHeadGame =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("headTohead", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const ActivateDeactivateHeadToHeadsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("headTohead/activeDeactive", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const gameOverViewDetails =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/overview", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//getGameNames

export const allGameListing = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getGameNames", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

///game/optimize/updateGameInfo

export const updateGameInfo = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("game/optimize/updateGameInfo", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

///game/optimize/updateGameWork

export const addOptimizeSDKStepOne =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/optimize/updateGameWork", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const gameSdkVerifyStepThree =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/optimize/gameSdkVerify", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const gameSdkVerifyStepFour =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/optimize/gameLoopVerify", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//game/optimize/getStatus
export const getOptimizeStatus =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/optimize/getStatus", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
///------------------------------------GameBuild ------------------------------------------
export const uploadGameBuild = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("gameBuild/upload", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const getGameBuildsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getGameBuilds", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const deleteGameBuildsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("gameBuild", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const getUniqueMgpReleases =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getUniqueMgpReleases", payload)
      .then((res) => {
        if (res.data?.success) {
          dispatch({
            type: "GET_GAME_BUILD_MPG_RELEASES_LIST",
            payload: res.data?.data,
          });
        }
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateGameBuild = (payload) => async (dispatch, getState, api) => {
  return await api
    .put("gameBuild", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
//---------------------------------------------------------------------------------------------------

export const updateYoutubeVideoLink =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("game/updateYoutubeVideoLink", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//getGameRelease
export const getGameReleaseList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getGameRelease", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const createGameReleaseList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("gameRelease", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateGameReleaseList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("gameRelease", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

//Game Mode-------------------------------
export const createGameModeList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("gameMode", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const getGameModeList = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getGameModes", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const updateGameModeList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("gameMode", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const deleteGameModeList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("gameMode", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
///lobbyLabel/getAllLabels
export const getAllLabels = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("lobbyLabel/getAllLabels", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
//howToPlay/uploadSliderImage
export const uploadSliderImagePlay =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("howToPlay/uploadSliderImage", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const addHowToPlay = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("howToPlay", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const getHowToPlay = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getHowToPlay", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const deleteHowToPlay = (payload) => async (dispatch, getState, api) => {
  return await api
    .delete("howToPlay", { data: payload })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
///--------------------------------Number of Decks -------------------------------------]
export const createGameNumberOfDeck =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("gameNumberOfDeck", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const getGameNumberOfDeck =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getGameNumberOfDeck", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

///--------------------------------Number of Players -------------------------------------]
export const createGameNumberOfPlayers =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("gameNumberOfPlayer", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const gameNumberOfPlayer =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getGameNumberOfPlayer", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

///-------------------------------- game Config -------------------------------------]
export const gameConfigList = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getGameConfig", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const createGameConfigList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("gameConfig", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
