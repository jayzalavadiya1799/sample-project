export const getHelpTicketType =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("helpTicketType/getTicket", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const createHelpTicketType =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("helpTicketType", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateHelpTicketType =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("helpTicketType", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const deleteHelpTicketType =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("helpTicketType", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

//
export const getHelpAndSupportList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("helpTicketType/getHelpAndSupportList", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
///helpTicketType/addHelpAndSupportMessage
export const addHelpAndSupportMessage =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("helpTicketType/addHelpAndSupportMessage", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getHelpAndSupportEmail =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("helpAndSupport/getEmail", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const addHelpAndSupportEmail =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("helpAndSupport/email", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const deleteHelpAndSupportEmail =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("helpAndSupport/email", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//----------------------------------------- CustomerCare ----------------------------------------------------
export const getCustomerCare = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getCustomerCare", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const addCustomerCare = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("customerCare", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const deleteCustomerCare =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("customerCare", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateCustomerCare =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("customerCare", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
