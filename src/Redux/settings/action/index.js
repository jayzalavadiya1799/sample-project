//UserBlockAction
export const getAgentsData = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getAgents", payload)
        .then((res) => {
            if(res.status === 200){
                dispatch({
                    type:"GET_AGENTS_DATA",
                    payload:res.data.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

//UserBlock
export const deactivatedUser = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("users/block", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateAgentRoleUser = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("agent", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const getAllAgentRoleNameAction = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getAllAgentRoleName", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const getAgentRolesAction = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getAgentRoles", payload)
        .then((res) => {
            if(res.data.statusCode === 200) {
                dispatch({
                    type:"GET_AGENT_ROLE",
                    payload:res.data.data
                })
            } else {
                dispatch({ type: 'UPDATE_ERROR_MODAL', payload: { open: true, data: { header: "Error", body: (res.data.message || res.data.msg) } } });
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateAgentRolesAction = (payload) => async (dispatch, getState, api) => {
    const {isEdit,isDelete} = payload;
    return await api[isEdit ? 'put' : isDelete ? 'delete' : 'post']("agentRole", isDelete ? {data: payload} : payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};


//config
export const configList = (payload) => async (dispatch, getState, api) => {
    return await api.post('getConfig',{})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const createConfigList = (payload) => async (dispatch, getState, api) => {
    return await api.put('settingConfig',payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

//GET_AGENTS_DATA

//FairPlayPolicy
//------------------------------Fair Play Policy[Start]--------------------------------------------------------------- //
export const createFairPlayPolicy = (payload) => async (dispatch,getState,api) => {
    return await api.post("fairPlayPolicy", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
export const getFairPlayPolicy = (payload) => async (dispatch, getState, api) => {
    return await api
        .get("fairPlayPolicy", payload)
        .then((res) => {
            if(res.data.success) {
                dispatch({
                    type:"GET_FAIR_PLAY_POLICY_DATA",
                    payload: res.data.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------------Fair Play Policy[End]--------------------------------------------------------------- //
//------------------------------Game Play Rules[Start]--------------------------------------------------------------- //
export const createGamePlayRules = (payload) => async (dispatch,getState,api) => {
    return await api.post("gameRules", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
export const getGameRules = (payload) => async (dispatch, getState, api) => {
    return await api
        .get("gameRules", payload)
        .then((res) => {
            if(res.data.success) {
                dispatch({
                    type:"GET_GAME_RULES_DATA",
                    payload: res.data.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------------Game Play Rules[End]--------------------------------------------------------------- //
//-----------------------------RestrictGeo[Start] -----------------------------------------------------------------------//
export const getAllCountriesRestrictGeo = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getAllCountryNames", payload)
        .then((res) => {
            dispatch({
                type:"SET_RESTRICTED_GEO_FIELD",
                payload: {name: "country", data: res.data.data}
            });
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getCountriesRestrictGeo = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getAllCountries", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getAllStateRestrictGeo = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getAllStates", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const createRestrictGeo = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("restrictGeo", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getRestrictGeoList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getRestrictGeo", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const deleteRestrictGeoList = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("restrictGeo", { data:payload })
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const updateRestrictGeo = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("restrictGeo",payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//-----------------------------RestrictGeo [End]-----------------------------------------------------------------------//
//-----------------------------Payment Method [Start]-----------------------------------------------------------------------//
export const createUserPaymentMethod = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("userPaymentMethod", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getUserPaymentMethodList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getUserPaymentMethod", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const deleteUserPaymentMethodList = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("userPaymentMethod", { data:payload })
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const updateUserPaymentMethod = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("userPaymentMethod",payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//-----------------------------Payment Method [End]-----------------------------------------------------------------------//