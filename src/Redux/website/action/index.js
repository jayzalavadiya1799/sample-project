export const createWebsiteHeader = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteHeader", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const getWebsiteHeaders = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getWebsiteHeaders", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateWebsiteHeader = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("webSiteHeader", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const deleteWebsiteHeaderList = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("webSiteHeader", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//getWebsiteGames
export const getWebsiteGamesList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getWebsiteGames", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const swapPositionHeaderSlider = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("swapPosition", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const addTopWebsiteGame = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteGames", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const deleteTopWebsiteGame = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("webSiteGames", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const updateTopWebsiteGame = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("webSiteGames", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const activeDeactiveTopWebsiteGame = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteGames/activeDeactiveWebSiteGameStatus", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const createWinnerList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteWinner", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const getWinnerList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getWinners", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const deleteWinnerList = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("webSiteWinner", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateWinnerList = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("webSiteWinner", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const createWebsiteAboutList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteAbout", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getWebsiteAboutList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getWebsiteAbout", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateWebsiteAboutList = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("webSiteAbout", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const deleteWebsiteAboutList = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("webSiteAbout", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const createWebsiteSocialMedia = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteSocialMedia", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getWebsiteSocialMedia = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getSocialMedia", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const deleteWebSiteSocialMedia = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("webSiteSocialMedia", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateWebSiteSocialMedia = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("webSiteSocialMedia", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//getDownloadList


export const getDownloadWebsiteNumberList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getDownloadList", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const webSiteWinnerTitle = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteWinnerTitle", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const getWebSiteWinnerTitle = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getWebsiteWinnerTitle", {})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const deleteWebSiteWinnerTitle = (payload) => async (dispatch, getState, api) => {

    return await api
        .delete("webSiteWinnerTitle", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const createWebSiteFooterTitle = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("webSiteFooter", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getWebSiteFooterTitle = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getFooter", {})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateWebSiteFooterTitle = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("webSiteFooter", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};


export const deleteWebSiteFooterTitle = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("webSiteFooter", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

//webSiteGames/updateScreenshots
export const updateScreenshots = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("/webSiteGames/updateScreenshots", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//webSiteGames/deleteScreenshots
export const deleteScreenshots = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("/webSiteGames/deleteScreenshots", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};