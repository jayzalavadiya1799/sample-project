export const getLegalPolicy = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getLegal", {})
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const createLegalPolicy =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("legal", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getPrivacyPolicyList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getPrivacyPolicy", {})
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const createPrivacyPolicy =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("privacyPolicy", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getTermsAndConditionList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getTermsAndCondition", {})
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const createTermsAndCondition =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("termsAndCondition", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
