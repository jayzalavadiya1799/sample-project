export const createPopularGames = (payload) => async (dispatch, getState, api) => {
    return await api
      .post("popularGame", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getPopularGamesDetails = (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getPopularGames", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const createUpcomingGames = (payload) => async (dispatch, getState, api) => {
    return await api
      .post("upcomingGame", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getUpcomingGamesDetails = (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getUpcomingGame", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateUpcomingGamesDetails =(payload) => async (dispatch, getState, api) => {
    return await api
      .put("getUpcomingGame", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const deleteUpcomingGamesDetails = (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("upcomingGame", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
