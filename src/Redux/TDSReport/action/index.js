export const getTDSOverViewReportList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getTdsReportList", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getTDSDetailsReportList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getTdsReports", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
