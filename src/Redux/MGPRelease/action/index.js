export const createMGPRelease =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("mgpRelease", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getMgpReleases = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getMgpReleases", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const haltRollOutDetails =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("mgpRelease/haltRollout", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateRollOutDetails =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("mgpRelease/updateRollout", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
