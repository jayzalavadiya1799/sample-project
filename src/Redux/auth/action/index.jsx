import Cookies from 'universal-cookie';
import { expireTime } from "../../../utils";
const cookies = new Cookies();

export const loginUser = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("auth/login", payload)
        .then((res) => {
            if (res.status === 200) {
                localStorage.setItem('userdata', JSON.stringify(res.data.data?.userData));
                localStorage.setItem('agentData', JSON.stringify(res.data.data?.agentData))
                cookies.set(`token`, res.data.data?.tokenData?.token, { path: '/', expires: expireTime() });
                cookies.set(`userdata`, JSON.stringify(res.data.data?.userData), { path: '/', expires: expireTime() });
                cookies.set(`agentData`, JSON.stringify(res.data.data?.agentData), { path: '/', expires: expireTime() });
            }

            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const changePasswordHandle = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("auth/changePassword", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const forgotPasswordHandler = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("auth/forgotPassword", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const resetPasswordHandle = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("auth/resetPassword", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};