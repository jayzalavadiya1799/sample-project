export const getPermissionsKey = () => async (dispatch, getState, api) => {
  return await api
    .post("adminUsers/getPermissions", {})
    .then((res) => {
      if (res?.data?.success) {
        dispatch({
          type: "GET_PERMISSION_KEY",
          payload: res.data.data.permissions,
        });
      }
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const addRoleCategory = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("adminUsers/addRoleCategory", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const getRoleCategoryList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("adminUsers/getAdminUserRoles", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateStatusRoleCategoryList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("adminUsers/activeDeactiveRoleCategory", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const deleteRoleCategoryList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("adminUsers/deleteRoleCategory", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateRoleCategoryList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("adminUsers/updateRoleCategory", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const addAdminUserList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("adminUsers", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getAllAdminUserRoleName =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("adminUsers/getAllAdminUserRoleName", {})
      .then((res) => {
        if (res.data.success) {
          dispatch({
            type: "GET_ADMIN_USER_ROLE",
            payload: res.data.data.adminUserRoleNames,
          });
        }
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const getAdminUserListing =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getAdminUsers", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateAdminUserList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("adminUsers", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const deleteAdminUserList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("adminUsers/delete", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const updateStatusAdminUserList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("adminUsers/blockUnblock", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
