const init = {
  gameRackReport: {},
};

const AnalyticsReportReducer = (state = init, { type, payload }) => {
  switch (type) {
    case "GET_GAME_RACK_REPORT": {
      let analyticsData = {};
      let mergeData = payload.reduce((acc, cur) => {
        let dateKey = Object.keys(cur);
        let temp = {};
        temp = dateKey.reduce((pre, next) => {
          return Object.keys(acc)?.length > 0
            ? { ...pre, [next]: [cur[next], ...acc[next]] }
            : { ...pre, [next]: [cur[next]] };
        }, {});
        return { ...acc, ...temp };
      }, {});
      analyticsData = { ...analyticsData, gameRackReport: { ...mergeData } };
      return { ...state, gameRackReport: { ...analyticsData } };
    }
    default: {
      return state;
    }
  }
};
export default AnalyticsReportReducer;
