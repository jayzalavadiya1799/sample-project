export const getGameRackReport =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("reports", payload)
      .then((res) => {
        if (res.status === 200) {
          dispatch({
            type: "GET_GAME_RACK_REPORT",
            payload: res.data.data,
          });
        }
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
