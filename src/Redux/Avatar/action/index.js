//getAvatars
export const getAllAvatars = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getAvatars", payload)
    .then((res) => {
      if (res.status === 200) {
        dispatch({
          type: "GET_ALL_AVATAR",
          payload: res.data.data,
        });
      }
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const createAvatarData =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("avatar", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const editAvatarData = (payload) => async (dispatch, getState, api) => {
  return await api
    .put("avatar", payload)
    .then((res) => {
      if (res.status === 200) {
        dispatch({
          type: "UPDATE_AVATAR_DATA",
          payload: res.data.data,
        });
      }
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const deleteAvatarData =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("avatar", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
