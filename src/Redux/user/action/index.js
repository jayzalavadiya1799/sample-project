import {jsonToFormData} from "../../../utils";

export const userDetailsData = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("users", payload)
        .then((res) => {
            if (res.status === 200 && !res?.data?.data?.filePath) {
                dispatch({
                    type:"USER_DATA_DETAILS",
                    payload:res?.data?.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const userBlockAction = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("users/block", payload)
        .then((res) => {
            if (res.status === 200) {
                dispatch({
                    type:"USER_BLOCK_DETAILS",
                    payload:res?.data?.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const getUserProfile = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("users/getUserProfile", payload)
        .then((res) => {
            if (res.status === 200) {
                dispatch({
                    type:"USER_PROFILE_DETAILS",
                    payload:res?.data?.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateUserProfile = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("userDetails/updateUserPersonalInfo", jsonToFormData(payload))
        .then((res) => {
            if (res.status === 200) {
                dispatch({
                    type:"USER_UPDATE_PROFILE",
                    payload:res?.data?.data?.userData
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const userDetailGameStatistics = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("users/getGameStatistics", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const userDetailOverview = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("users/userDetailOverview", payload)
        .then((res) => {
            if (res.status === 200 && typeof res?.data?.data?.filePath !== 'string') {
                dispatch({
                    type:"USER_DETAILS_OVERVIEW",
                    payload:res?.data?.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

//userDetailTransactions
export const userDetailTransactions = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getTransactionHistory", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};


//getUsersNote
export const getUsersNote = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getUsersNote", payload)
        .then((res) => {
            dispatch({
                type:"GET_NOTE_DATA_LIST",
                payload:res.data.data
            })
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const createUsersNote = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("userNote", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const updateUsersNote = (payload) => async (dispatch, getState, api) => {
    return await api
        .put("userNote", payload)
        .then((res) => {
            dispatch({
                type:"UPDATE_NOTE_DATA",
                payload:res.data.data
            })
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const deleteUsersNote = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("userNote", {data:payload})
        .then((res) => {
            dispatch({
                type:"UPDATE_NOTE_DATA",
                payload:res.data.data
            })
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateBonus = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("userDetails/updateUserBonus", payload)
        .then((res) => {
            if (res.status === 200) {
                dispatch({
                    type:"UPDATE_DEPOSITS_CASH",
                    payload:res?.data?.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const updateDepositCash = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("userDetails/updateUserCash", payload)
        .then((res) => {
            if (res.data.success) {
                debugger
                dispatch({
                    type:"UPDATE_DEPOSITS_CASH",
                    payload:res?.data?.data
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};

export const updateCoins = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("userDetails/updateUserCoins", payload)
        .then((res) => {
            if (res.data.success) {
                debugger
                dispatch({
                    type:"UPDATE_COINS",
                    payload:res?.data?.data?.userData
                })
            }
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------------ User KYC Api [Start]-----------------------------------------------------//
export const getUserKYCList = (payload) => async (dispatch,getState,api) => {
    return await api.post("users/getKYC", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
export const getUserKYCListRequest = (payload) => async (dispatch,getState,api) => {
    return await api.post("userKYCUpdateRequest/getUpdateRequests", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
export const userKYCViewRequestList = (payload) => async (dispatch,getState,api) => {
    return await api.post("userKYCUpdateRequest/viewUpdateRequest", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
export const ApproveRejectUserKYCListRequest = (payload) => async (dispatch,getState,api) => {
    return await api.post("userKYCUpdateRequest/approveReject", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
export const updateUserKYCList = (payload) => async (dispatch,getState,api) => {
    return await api.put("userKYC", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
export const activateDeactivateUserKYCList = (payload) => async (dispatch,getState,api) => {
    return await api.post("userKYC/aadharCard/approveReject", payload)
        .then((res) => {
            return res;
        })   .catch((err) => {
            return err.response;
        });
}
//------------------------Payment History [Start] -------------------------------------------------------------- //
export const userPaymentHistoryList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("paymentHistory/getUserPaymentHistory", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------Payment History [End] -------------------------------------------------------------- //
//userKYC/approveReject
//------------------------------ User KYC Api [End]-----------------------------------------------------//
//------------------------user reported [Start]-------------------------------------------------------------- //
export const userReportedDetailsData = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getReportedUser", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const userReportedListData = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getReportedList", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------user reported [End] -------------------------------------------------------------- //
//------------------------user Block [start] -------------------------------------------------------------- //
export const getUserBlockListData = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("blockUser/getList", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getUserBlockListDetails = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("blockUser/getList/view", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const userBlockUnblockListData = (payload) => async (dispatch, getState, api) => {
    return await api
        .delete("blockUser", {data:payload})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------user Block [End] -------------------------------------------------------------- //

//------------------------user Played Tab [start] -------------------------------------------------------------- //
export const getUserPlayedGamesList = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getUserPlayedGames", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const getUserPlayedDetailsGames = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getUserPlayedGames/view", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------user Played Tab [End] -------------------------------------------------------------- //
//------------------------user KYC Tab [start] -------------------------------------------------------------- //
export const getSingleUserKYC = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("getSingleUserKYC", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
//------------------------user KYC Tab [End] -------------------------------------------------------------- //
export const getWithdrawRequests = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("users/getWithdrawRequests", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};
export const approveRejectUserWithdrawalRequest = (payload) => async (dispatch, getState, api) => {
    return await api
        .post("approveRejectUserWithdrawalRequest", payload)
        .then((res) => {
            return res;
        })
        .catch((err) => {
            return err.response;
        });
};