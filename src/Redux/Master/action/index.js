//--------------------------------------------------------- Coupon Module Api [Start]--------------------------------------------//

export const getCouponCodeList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getCouponCode", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const createCouponCode =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("couponCode", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateCouponCode =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("couponCode", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const deleteCouponCode =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("couponCode", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

//--------------------------------------------------------- Coupon Module Api [End]--------------------------------------------//

//--------------------------------------------------------- Offer Module Api [Start]--------------------------------------------//

export const getOfferList = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getOffer", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const createOfferList = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("offer", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const updateOfferList = (payload) => async (dispatch, getState, api) => {
  return await api
    .put("offer", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const deleteOfferList = (payload) => async (dispatch, getState, api) => {
  return await api
    .delete("offer", { data: payload })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

//--------------------------------------------------------- Offer Module Api [End]--------------------------------------------//

//--------------------------------------------------------- Internal Ads Module Api [Start]--------------------------------------------//
export const getInternalAdsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getInaternalAds", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const createInternalAdsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("internalAds", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateInternalAdsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("internalAds", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const deleteInternalAdsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("internalAds", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const activeDeactivateInternalAdsList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("internalAds/activeDactive", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//--------------------------------------------------------- Offer Module Api [End]-----------------------------------------------------//
//--------------------------------------------------------- Daily Wheel Bonus [Start]-----------------------------------------------------//
export const getDailyWheelBonusList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getDailyWheelBonus", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const createDailyWheelBonusList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("dailyWheelBonus", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateDailyWheelBonusList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("dailyWheelBonus", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const getDayDailyWheelBonus =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("dailyWheelBonus/getDay", {})
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//--------------------------------------------------------- Daily Wheel Bonus [End]-----------------------------------------------------//
//--------------------------------------------------------- Refer And Earn [Start]-----------------------------------------------------//
export const getReferAndEarn = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getReferAndEarn", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const createReferAndEarn =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("referAndEarn", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateReferAndEarn =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("referAndEarn", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const deleteReferAndEarnList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("referAndEarn", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const activateReferAndEarnList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("referAndEarn/activeDeactive", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const getReferAndEarnListingDetails =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("referDetails/getReferralDetail", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//referDetails/getUsersReferrals
export const getReferAndEarnListingView =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("referDetails/getUsersReferrals", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//--------------------------------------------------------- Refer And Earn [End]-----------------------------------------------------//
//--------------------------------------------------------- Leaderboard [Start]-----------------------------------------------------//

export const getLeaderboardList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("/leaderboard/getList", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const getLeaderboardGameList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("/games", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//--------------------------------------------------------- Leaderboard [End]-----------------------------------------------------//

export const getLobbyLabelList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getLobbyType", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const addLobbyLabelList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("lobbyType", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateLobbyLabelList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("lobbyType", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

export const deleteLobbyLabelList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("lobbyType", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//--------------------------------[Start] Bank and UPI -------------------------------------------
export const getBankAndUrl = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("getBankList", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const getWithdrawalUPI =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("getUPIList", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const addWithdrawalUPI =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .post("bankAndUPI/addUPI", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const updateWithdrawalUPI =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("bankAndUPI/updateUPI", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const addBankAndUPI = (payload) => async (dispatch, getState, api) => {
  return await api
    .post("bankAndUPI/addBank", payload)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
export const updateBankAndUPI =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .put("bankAndUPI/updateBank", payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
export const deleteBankAndUPIList =
  (payload) => async (dispatch, getState, api) => {
    return await api
      .delete("bank", { data: payload })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };
//--------------------------------[End] Bank and UPI -------------------------------------------
// Leaderboard position
export const addLeaderboardBonusList =
    (payload) => async (dispatch, getState, api) => {
        return await api
            .post("leaderboardBonus", payload)
            .then((res) => {
                return res;
            })
            .catch((err) => {
                return err.response;
            });
    };

export const geLeaderboardBonus =
    (payload) => async (dispatch, getState, api) => {
        return await api
            .post("getLeaderboardBonus", payload)
            .then((res) => {
                return res;
            })
            .catch((err) => {
                return err.response;
            });
    };
