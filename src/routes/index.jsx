import React from "react";
//auth
const Login = React.lazy(() => import("Pages/Login"));
const ChangePassword = React.lazy(() => import("Pages/ChangePassword"));
//Dashboard && AdminUser
const Dashboard = React.lazy(() => import("Pages/Dashboard"));
const AdminUser = React.lazy(() => import("Pages/AdminUser"));
//Users
const UserData = React.lazy(() => import("../Pages/Users"));
const UsersTab = React.lazy(() => import("../Pages/Users/UsersTab"))
const UserWithdrawalRequests = React.lazy(() => import("../Pages/Users/UserWithdrawalRequests"));
const UserKYC = React.lazy(() => import("../Pages/Users/UserKYC"));
const BlockUserList = React.lazy(() => import("../Pages/Users/BlockUserList"));
const ReportedUser = React.lazy(() => import("Pages/Users/ReportedUser"));

//Games
const AddNewGame = React.lazy(() => import("../Pages/Games/AddNewGame"))
const AllGameList = React.lazy(() => import("../Pages/Games/AllGameList"))
const ApprovedGameList = React.lazy(() => import("../Pages/Games/ApprovedGameList"))
const PendingGameList = React.lazy(() => import("../Pages/Games/PendingGameList"))
const RejectedGameList = React.lazy(() => import("../Pages/Games/RejectedGameList"))
const ArchiveGames = React.lazy(() => import("../Pages/Games/ArchiveGames"));
const GameDetails = React.lazy(() => import("../Pages/Games/GameDetails"))
//Settings

const RestrictGeo = React.lazy(() => import("../Pages/Settings/RestrictGeo"));
const Config = React.lazy(() => import("../Pages/Settings/Config"));
const UsersPaymentMethod = React.lazy(() => import("../Pages/Settings/UsersPaymentMethod"));
const InternalAdsList = React.lazy(() => import("../Pages/Master/InternalAdsList"));

//Master category

const Leaderboard = React.lazy(() => import("../Pages/Master/Leaderboard"));
const GenreCategory = React.lazy(() => import("../Pages/Master/GenreCategory"));
const Avatar = React.lazy(() => import("../Pages/Master/Avatar"));
const Document = React.lazy(() => import("../Pages/Master/Document"));
const LobbyLabel = React.lazy(() => import("../Pages/Master/LobbyLabel"));
const MGPOnlinePlayer = React.lazy(() => import("../Pages/Master/MGPOnlinePlayer"));
//BonusSystem
const DailyWheelBonus = React.lazy(() => import("../Pages/BonusSystem/DailyWheelBonus"));
const ReferAndEarn = React.lazy(() => import("../Pages/BonusSystem/ReferAndEarn"));
const CouponCode = React.lazy(() => import("../Pages/BonusSystem/CouponCode"));
const Offer = React.lazy(() => import("../Pages/BonusSystem/Offer"));
//Revenue Category
const GameWiseRevenue = React.lazy(() => import("../Pages/Revenue/GameWiseRevenue"));
const OverallRevenue = React.lazy(() => import("../Pages/Revenue/OverallRevenue"));
const TimeOutReportRevenue = React.lazy(() => import("../Pages/Revenue/TimeOutReportRevenue"));
//StorePack && SDKManagement && Analytics && PopularGamesDetails && Website

const PopularGames = React.lazy(() => import("../Pages/PopularGames"));
const Website = React.lazy(() => import("../Pages/Website"));
const MGPRelease = React.lazy(() => import("../Pages/MGPRelease"));
const BankAndUPI = React.lazy(() => import("../Pages/BankAndUPI"));

const HelpAndSupport = React.lazy(() => import("../Pages/HelpAndSupport"));

const Analytics = React.lazy(() => import("../Pages/Analytics"));

const TDSModule = React.lazy(() => import("../Pages/TDSModule"));
const TDSReportDetails = React.lazy(() => import("../Pages/TDSModule/TDSReportDetails"));
const ResetPassword = React.lazy(() => import("../Pages/ResetPassword"));
const GameModeDesignConfig = React.lazy(() => import("../Pages/Master/GameModeDesignConfig"));

export const PublicroutesArray = [
    { path: "/", exact: true, component: Login },
    { path: "/reset/:id/:id", component: ResetPassword, title: "Reset Password" },
];

export const PrivateroutesArray = [
    { path: "/dashboard", component: Dashboard, title: "Dashboard" },
    { path: "Users", component: UserData, title: "Users" },
    { path: "users-tab/:id", component: UsersTab, title: "User Details" },
    { path: "/user-reported", component: ReportedUser, title: "Reported User" },
    { path: "/change-password", component: ChangePassword, title: "Change Password" },
    { path: "/admin-users", component: AdminUser, title: "Admin Role" },
    { path: "/analytics", component: Analytics, title: "Analytics" },

    { path: "/games/all-games", component: AllGameList, title: "Games" },
    { path: "/games/approved-games", component: ApprovedGameList, title: "Approved Games" },
    { path: "/games/pending-games", component: PendingGameList, title: "Pending Games" },
    { path: "/games/rejected-games", component: RejectedGameList, title: "Rejected Games" },
    { path: "/games/archive-games", component: ArchiveGames, title: "Archive Games" },
    { path: "/games/add-game", component: AddNewGame, title: "Add New Game" },
    { path: "/game-tab/:id", component: GameDetails, title: "Game Details" },
    { path: "/games/category", component: GenreCategory, title: "Game Category" },

    { path: "/avatars", component: Avatar, title: "Avatar" },
    { path: "/user-withdrawal-requests", component: UserWithdrawalRequests, title: "Users Withdrawal Requests" },

    { path: "/setting/config", component: Config, title: "Config" },
    { path: "/setting/restrict-geo", component: RestrictGeo, title: "Restrict Geo" },
    { path: "/setting/users-payment-method", component: UsersPaymentMethod, title: "Payment Method" },
    { path: "/website", component: Website, title: "Website" },

    { path: "/user-kyc", component: UserKYC, title: "User KYC" },
    { path: "/block-user", component: BlockUserList, title: "Block User" },

    { path: "/leaderboard", component: Leaderboard, title: "Leaderboard" },
    { path: "/daily-wheel-bonus", component: DailyWheelBonus, title: "Daily Wheel Bonus" },
    { path: "/refer-and-earn", component: ReferAndEarn, title: "Refer & Earn" },
    { path: "/coupon-code", component: CouponCode, title: "Coupon Code" },
    { path: "/offer", component: Offer, title: "Offer" },
    { path: "/ads-list", component: InternalAdsList, title: "Internal Ads List" },
    { path: "/popular-games", component: PopularGames, title: "Popular Games" },
    { path: "/online-players", component: MGPOnlinePlayer, title: "MGP Online Players" },
    //MGPOnlinePlayer

    { path: "/revenue/game-wise-revenue", component: GameWiseRevenue, title: "Game Wise Revenue" },
    { path: "/revenue/overall-revenue", component: OverallRevenue, title: "Overall Revenue" },
    { path: "/revenue/time-out-report", component: TimeOutReportRevenue, title: "Time Out Report" },
    //Document
    { path: "/documentation", component: Document, title: "Documentation" },
    //HelpAndSupport
    { path: "/help-and-support", component: HelpAndSupport, title: "Help & Support" },
    //MGPRelease
    { path: "/release", component: MGPRelease, title: "MGP Release" },
    { path: "/lobby-label", component: LobbyLabel, title: "Lobby Type" },
    //BankAndUPI
    { path: "/bank-and-upi", component: BankAndUPI, title: "Withdrawal Bank And UPI" },
    //TDSModule
    { path: "/TDS-report", component: TDSModule, title: "TDS Report" },
    { path: "/tds-report-details/:id", component: TDSReportDetails, title: "TDS Report" },
    { path: "/gameModeDesignConfig", component: GameModeDesignConfig, title: "Game Mode Design Config" },
];

//GameCategory