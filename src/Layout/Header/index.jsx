import React from 'react'
import MenuIcon from '@mui/icons-material/Menu';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { Link } from 'react-router-dom';

function Header(props) {
    const { title } = props;

    return (
        <Box className='inner-header' >
            <div className='main_header'>
                <h4 className='main_header_title'>{title}</h4>
                <Box className='user_header_side' sx={{ display: 'flex' }}>
                    <div className={'change-password'}>
                        <Link to={'/change-password'}>Change Password</Link>
                    </div>
                </Box>
                <Button className='humburger' onClick={props.MenuToggle} ><MenuIcon /></Button></div>
        </Box>
    )
}

export default Header