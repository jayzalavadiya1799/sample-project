import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import List from '@mui/material/List';
import { Avatars } from 'images/Avatars';
import Cookies from 'universal-cookie';
import GameIcon from "../../images/Game";
import SDKIcon from "../../images/SDKIcon";
import UserIcon from "../../images/UserIcon";
import Collapse from '@mui/material/Collapse';
import Coupon from "../../images/Coupon";
import SettingIcon from "../../images/settingIcon";
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import AnalyticsIcon from "../../images/AnalyticsIcon";
import Logo from "../../assets/images/logo75.png";
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import ListItemButton from '@mui/material/ListItemButton';
import WebAssetIcon from '@mui/icons-material/WebAsset';
import LeaderboardIcon from '@mui/icons-material/Leaderboard';
import HelpCenterIcon from '@mui/icons-material/HelpCenter';
import RevenueIcon from "../../images/RevenueIcon";
import { hideActionFunc } from "../../utils";
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';

function LeftContent() {
    const cookies = new Cookies();
    const navigate = useNavigate();
    const [, updateState] = React.useState({});
    const forceUpdate = React.useCallback(() => updateState({}), []);
    const [menusList, setMenuList] = useState([
        {
            icon: <UserIcon />,
            label: "Admin Users",
            src: "admin-users",
            value: 'admin',
            hasMoreMenu: false,
        },
        {
            icon: <UserIcon />,
            label: "Users",
            hasMoreMenu: true,
            isExpanded: false,
            id: 'userId',
            value: 'user',
            expandArray: [
                {
                    icon: '',
                    label: "All Users",
                    src: "Users",
                },
                {
                    icon: '',
                    label: "User Blocked",
                    src: "block-user",
                },
                {
                    icon: '',
                    label: "User Reported",
                    src: "user-reported",
                },
                {
                    icon: '',
                    label: "User KYC",
                    src: "user-kyc",
                },
                {
                    icon: '',
                    label: "User Withdrawal Requests",
                    src: "user-withdrawal-requests",
                },
            ]
        },
        {
            icon: <GameIcon />,
            label: "Games",
            hasMoreMenu: true,
            isExpanded: false,
            id: 'gameId',
            value: 'game',
            expandArray: [
                {
                    icon: '',
                    label: "All Games",
                    src: "games/all-games",
                },
                {
                    icon: '',
                    label: "Approved Games",
                    src: "games/approved-games",
                },
                {
                    icon: '',
                    label: "Archive Games",
                    src: "games/archive-games",
                },
                {
                    icon: '',
                    label: "Pending Games",
                    src: "games/pending-games",
                },
                {
                    icon: '',
                    label: "Rejected Games",
                    src: "games/rejected-games",
                },
                hideActionFunc('game') &&
                {
                    icon: '',
                    label: "Add New Game",
                    src: "games/add-game",
                },
            ]
        },
        {
            icon: <AnalyticsIcon />,
            label: "Analytics",
            src: "analytics",
            hasMoreMenu: false,
            viewAnalytics: true,
            value: 'analytics',
            routeKey: 'viewAnalytics',
        },
        {
            icon: <GameIcon />,
            label: "Popular Games",
            src: "popular-games",
            value: 'popularGame',
            hasMoreMenu: false,
        },
        {
            icon: <Coupon />,
            label: "Bonus",
            hasMoreMenu: true,
            isExpanded: false,
            id: 'bonusId',
            value: 'bonus',
            expandArray: [
                {
                    icon: '',
                    label: "Daily Bonus",
                    src: "daily-wheel-bonus",
                },
                {
                    icon: '',
                    label: "Refer & Earn",
                    src: "refer-and-earn",
                },
            ]
        },
        {
            icon: <RevenueIcon />,
            label: "Revenue",
            hasMoreMenu: true,
            isExpanded: false,
            id: 'revenue',
            value: 'revenue',
            expandArray: [
                {
                    icon: '',
                    label: "Game Wise Revenue",
                    src: "revenue/game-wise-revenue",
                },
                {
                    icon: '',
                    label: "Overall Revenue",
                    src: "revenue/overall-revenue",
                },
                {
                    icon: '',
                    label: "Time Out Report",
                    src: 'revenue/time-out-report'
                }
            ]
        },
        {
            icon: <LeaderboardIcon />,
            label: "Master",
            hasMoreMenu: true,
            isExpanded: false,
            id: 'master',
            value: 'master',
            expandArray: [
                {
                    icon: '',
                    label: "Avatars",
                    src: "avatars",
                },
                {
                    icon: '',
                    label: "Game Category",
                    src: 'games/category'
                },
                {
                    icon: '',
                    label: "Leaderboard",
                    src: "leaderboard",
                },
                {
                    icon: '',
                    label: "Internal Ads List",
                    src: "ads-list",
                },
                {
                    icon: '',
                    label: 'Documentation',
                    src: 'documentation'
                },
                {
                    icon: '',
                    label: 'Lobby Type',
                    src: 'lobby-label'
                },
                {
                    icon: '',
                    label: 'MGP Online Players',
                    src: 'online-players'
                },
                {
                    icon: '',
                    label: 'Game Mode Design Config',
                    src: 'gameModeDesignConfig'
                }
            ]
        },
        {
            icon: <WebAssetIcon />,
            label: "Website",
            src: "website",
            value: 'webSite',
            hasMoreMenu: false,
        },
        {
            icon: <SettingIcon />,
            label: "Settings",
            hasMoreMenu: true,
            isExpanded: false,
            id: 'settings',
            value: 'setting',
            expandArray: [
                {
                    icon: '',
                    label: "Restrict Geo",
                    src: "setting/restrict-geo",
                },
                {
                    icon: '',
                    label: "Payment Method",
                    src: "setting/users-payment-method",
                },
                {
                    icon: '',
                    label: "Config",
                    src: "setting/config",
                },
            ]
        },
        {
            icon: <SDKIcon />,
            label: "MGP Release",
            src: "release",
            value: 'mgpRelease',
            hasMoreMenu: false,
        },
        {
            icon: <AccountBalanceIcon />,
            label: "Withdrawal Bank and UPI",
            src: "bank-and-upi",
            hasMoreMenu: false,
            value: 'bankAndUpi',
            routeKey: 'help-and-support',
        },
        {
            icon: <AnalyticsIcon />,
            label: "TDS Report",
            src: "TDS-report",
            value: 'tdsReport',
            hasMoreMenu: false,
        },
        {
            icon: <HelpCenterIcon />,
            label: "Help & Support",
            src: "help-and-support",
            hasMoreMenu: false,
            value: 'helpAndSupport',
            routeKey: 'help-and-support',
        },

    ]);

    const handleClicked = (e) => {
        navigate(`/${e}`)
    };
    
    const ToggleMenu = (Id) => {
        menusList.filter(item => item.id === Id)[0].isExpanded = !menusList.filter(item => item.id === Id)[0].isExpanded;
        forceUpdate();
    };

    return (
        <div className='gp_left-bar'>
            <Link className='side_logo' to="/dashboard">
                {/*<SideLogo />*/}
                <img src={Logo} alt={"Logo"} className={"Logo"} />
            </Link>
            <List sx={{ width: '100%', maxWidth: 360, padding: '0' }} component="nav" aria-labelledby="nested-list-subheader" className={"tab_sidebar_details"}>
                {
                    menusList?.map((menu, i) => {
                        let agentDataDetails = JSON.parse(localStorage.getItem('agentData')) || cookies.get('agentData');
                        let AgentCondition = agentDataDetails !== 'null' && (agentDataDetails && menu.value === (Object?.keys(agentDataDetails?.permission)[Object?.keys(agentDataDetails?.permission)?.indexOf(menu.value)]))
                        return (
                            <React.Fragment key={i}>
                                {
                                    (((JSON.parse(localStorage.getItem('userdata'))?.role || cookies.get('userdata')?.role) === 'Admin') || AgentCondition) && (
                                        <>
                                            <ListItemButton sx={{ padding: "5px 15px" }} selected={menu?.src === window.location.pathname.replace('/', '')} className='list_item fontFamily' onClick={() => menu.hasMoreMenu ? ToggleMenu(menu?.id) : handleClicked(menu?.src)}>
                                                <ListItemIcon style={{ minWidth: "20px" }} className={'icon-left-side fontFamily'}>
                                                    {menu?.icon}
                                                </ListItemIcon>
                                                <ListItemText primary={menu?.label} className="menu_label fontFamily" />
                                                {menu?.hasMoreMenu ? menu?.isExpanded ? <ExpandLess /> : <ExpandMore /> : ''}
                                            </ListItemButton>

                                            {menu?.expandArray &&
                                                menu?.expandArray?.map((list, i) => {

                                                    return <Collapse in={menu?.isExpanded} timeout="auto" unmountOnExit key={i} className={((window.location.pathname.includes('/users-tab') && list?.label === 'All Users') || (window.location.pathname.includes('/game-tab') && list?.label === 'All Games')) ? 'inner_list_details activeClass' : 'inner_list_details'}>
                                                        <List component="div" disablePadding onClick={() => handleClicked(list?.src)} >
                                                            <ListItemButton className='list_item' selected={list?.src === window.location.pathname.replace('/', '')} >
                                                                {
                                                                    list?.icon && <ListItemIcon>
                                                                        {list?.icon}
                                                                    </ListItemIcon>
                                                                }
                                                                <ListItemText primary={list?.label} className="menu_label fontFamily" />
                                                            </ListItemButton>
                                                        </List>
                                                    </Collapse>
                                                })
                                            }
                                        </>
                                    )
                                }
                            </React.Fragment>
                        )
                    })
                }
            </List>
        </div>
    )
}

export default LeftContent