import React from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import PrivateRouters from "Routers/PrivateRouter";
import PublicRouters from "Routers/PublicRouter";
import { PublicroutesArray, PrivateroutesArray } from "./routes";
import store from "./Redux/store";
import Layout from "./Layout";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Loader from "./images/Loader";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from "./ErrorFallback";


const loading = () => (
  <div className="site_loader">
    <Loader />{" "}
  </div>
);
function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
          <ErrorBoundary FallbackComponent={ErrorFallback}>
              <React.Suspense fallback={loading()}>
                  <Routes>
                      {PublicroutesArray.map(({ component: Component, path }, key) => {
                          return (
                              <Route
                                  path={path}
                                  element={
                                      <PublicRouters>
                                          <Component />
                                      </PublicRouters>
                                  }
                                  key={key}
                              />
                          );
                      })}
                      {PrivateroutesArray?.map(
                          ({ component: Component, path, title }, key) => (
                              <Route
                                  path={path}
                                  element={
                                      <PrivateRouters>
                                          <Layout title={title}>
                                              <Component />
                                          </Layout>
                                      </PrivateRouters>
                                  }
                                  key={key}
                              />
                          )
                      )}
                  </Routes>
              </React.Suspense>
          </ErrorBoundary>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
